export function addProcess(state) {
    state.items.push({
        id:null,
        type:null,
        option:null,
        frequency:null,
        period:null,
        hour:null,
        Hour_input:null,
        minutes:null,
        active:1,
        options_select:[],
        email:[],
        manage_hour:true,
        manage_days:false,
        days:null
    });
}
export function setItems(state, process){
    state.items = process
}