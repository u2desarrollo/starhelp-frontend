import $http from "../../axios";
import {
    Notification,
    MessageBox
} from 'element-ui';

export async function fetchOptions(
    { commit, state }, type
) {
    let params = {
        filter: type == 1 ? 'P%' : 'A%'
    }, options = []
    await $http.get('/api/opciones-aurora/108', { params })
        .then((response) => {
            options = response.data;
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 2000,
                customClass: 'notification-box',
            });
        })
    return options
}

export async function createProcess({ commit, state }, row) {
    let process = null
    await $http.post('/api/aurora/', row)
        .then((response) => {
            process = response.data.data
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 2000,
                customClass: 'notification-box',
            });
        })
    return process
}

export async function getProcess({ commit, state }) {
    await $http.get('/api/aurora/')
        .then((response) => {
            commit('setItems', response.data.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 2000,
                customClass: 'notification-box',
            });
        })
}

export async function deleteProcess({ commit, state }, id) {
    await $http
        .delete(`/api/aurora/${id}`)
        .then(response => {
            // commit('setShowPdf', response.data.data)
        })
        .catch(error => {
            let data = error.response.data
            Notification.error({
                title: "Error!",
                message: data.message,
                type: "error",
                duration: 1500,
                customClass: "notification-box"
            });
        });
}
