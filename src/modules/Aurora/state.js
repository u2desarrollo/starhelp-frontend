export default {
    items:[],
    types:[
        {
            id:1,
            value:'Proceso'
        },
        {
            id:2,
            value:'Alerta'
        }
    ],
    frequency:[
        {
            id:1,
            value:'Cada minuto',
        },
        {
            id:2,
            value:'Cada Hora',
        },
        {
            id:3,
            value:'Diariamente',
        },
        {
            id:4,
            value:'Semanal Lunes',
        },
        {
            id:5,
            value:'Semanal Martes',
        },
        {
            id:6,
            value:'Semanal Miercoles',
        },
        {
            id:7,
            value:'Semanal Jueves',
        },
        {
            id:8,
            value:'Semanal Viernes',
        },
        {
            id:9,
            value:'Semanal Sabado',
        },
        {
            id:10,
            value:'Semanal Domingo',
        },
        {
            id:11,
            value:'Mensual',
        },
    ],
}
  