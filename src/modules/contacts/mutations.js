import {
  defaultContacts,
  defaultSelectedClerk,
  defaultSelectedContact,
  defaultSelectedDocument,
  defaultSelectedWarehouse
} from "./state";

export function setContacts(state, contacts) {
  state.contacts = contacts;
}

export function setContact(state, contact) {
  state.selectedContact = JSON.parse(JSON.stringify(contact));
}

export function setAction(state, action) {
  state.action = action;
}

export function contactError(state, payload) {
  state.error = true;
  state.errorMessage = payload;
  state.todo = [];
}

export function setGetContactEdit(state, getContactEdit) {
  state.getContactEdit = getContactEdit;
}

export function setPositionGeneral(state, dataMarker) {
  state.selectedContact.length = dataMarker.lng;
  state.selectedContact.latitude = dataMarker.lat;
}

//Establece el valor por defecto de selectedContact
export function resetSelectedContact(state) {
  Object.assign(state.selectedContact, defaultSelectedContact());
}

//Establece el valor por defecto de contacts
export function resetContacts(state) {
  Object.assign(state.contacts, defaultContacts());
}

export function setIsCustomer(state, is_customer) {
  state.is_customer = is_customer;
  state.params.is_customer = is_customer;
}

export function setCustomerCategories(state, customerCategories) {
  state.customerCategories = customerCategories;
  state.params.filter_customer = customerCategories;
}

export function setIsProvider(state, is_provider) {
  state.is_provider = is_provider;
  state.params.is_provider = is_provider;
}

export function setProviderCategories(state, providerCategories) {
  state.providerCategories = providerCategories;
  state.params.filter_provider = providerCategories;
}

export function setIsEmployee(state, is_employee) {
  state.is_employee = is_employee;
  state.params.is_employee = is_employee;
}

export function setEmployeeCategories(state, employeeCategories) {
  state.employeeCategories = employeeCategories;
  state.params.filter_employee = employeeCategories;
}

//------------------------ PAGINACIÓN ------------------------//

export function setFilter(state, filter) {
  state.params.page = 0;
  state.params.query = filter;
}

export function setPerPage(state, perPage) {
  state.params.page = 1;
  state.params.perPage = perPage;
}

export function setPage(state, page) {
  state.params.page = page;
}

export function setSortBy(state, sortBy) {
  state.params.sortBy = sortBy;
}

export function setSort(state, sort) {
  state.params.sort = sort;
}

export function setTooltipParamTable(state, paramTable) {
  state.parameters.map(p => {
    if (p.paramtable_id == paramTable.id) {
      p.tooltip = paramTable.code_table + " - " + paramTable.name_table;
    }
  });
}

//------------------------ PESTAÑA GENERAL------------------------//

// Establece un valor para identificationTypes
export function setIdentificationTypes(state, identificationTypes) {
  state.identificationTypes = identificationTypes;
}

// Establece un valor para lines
export function setLines(state, lines) {
  state.lines = lines.map(p => {
    return { value: p.id, label: p.line_description };
  });
}

// Establece un valor para departments
export function setDepartments(state, departments) {
  state.departments = departments.map(p => {
    return { value: p.id, label: p.department };
  });
}

// Establece un valor para cities
export function setCities(state, cities) {
  state.cities = cities.map(p => {
    return { value: p.id, label: p.city };
  });
}

// Establece un valor para pointsOfSale
export function setPointsOfSale(state, pointsOfSale) {
  state.pointsOfSale = pointsOfSale;
}

// Establece un valor para personClasses
export function setPersonClasses(state, personClasses) {
  state.personClasses = personClasses;
}

// Establece un valor para taxpayerTypes
export function setTaxpayerTypes(state, taxpayerTypes) {
  state.taxpayerTypes = taxpayerTypes;
}

// Establece un valor para taxRegimes
export function setTaxRegimes(state, taxRegimes) {
  state.taxRegimes = taxRegimes;
}

//------------------------ PESTAÑA CLIENTES------------------------//

// Establece un valor para categoriesCustomers
export function setCategoriesCustomers(state, categoriesCustomers) {
  state.categoriesCustomers = categoriesCustomers;
}

// Establece un valor para sellersCustomers
export function setSellersCustomers(state, sellersCustomers) {
  state.sellersCustomers = sellersCustomers;
}

// Establece un valor para priceListsCustomers
export function setPriceListsCustomers(state, priceListsCustomers) {
  state.priceListsCustomers = priceListsCustomers;
}
// Establece un valor para fiscalResponsibilitys
export function setFiscalResponsibilitys(state, fiscalResponsibilitys) {
  state.fiscalResponsibilitys = fiscalResponsibilitys;
}

// Establece un valor para zones
export function setZones(state, zones) {
  state.zones = zones;
}

export function setLogoCustomer(state, logo) {
  state.selectedContact.customer.logo = logo;
}

//------------------------ PESTAÑA PROVEEDORES------------------------//

// Establece un valor para categoriesProviders
export function setCategoriesProviders(state, categoriesProviders) {
  state.categoriesProviders = categoriesProviders;
}

// Establece un valor para sellersProviders
export function setSellersProviders(state, sellersProviders) {
  state.sellersProviders = sellersProviders;
}

// Establece un valor para priceListsProviders
export function setPriceListsProviders(state, priceListsProviders) {
  state.priceListsProviders = priceListsProviders;
}

// Establece un valor para paymentBanks
export function setPaymentBanks(state, paymentBanks) {
  state.paymentBanks = paymentBanks;
}

export function setLogoProvider(state, logo) {
  state.selectedContact.provider.logo = logo;
}

//------------------------ PESTAÑA EMPLEADOS------------------------//

// Establece un valor para categoriesEmployees
export function setCategoriesEmployees(state, categoriesEmployees) {
  state.categoriesEmployees = categoriesEmployees;
}

// Establece un valor para costCenters
export function setCostCenters(state, costCenters) {
  state.costCenters = costCenters;
}

// Establece un valor para sellerZones
export function setSellerZones(state, sellerZones) {
  state.sellerZones = sellerZones;
}

// Establece un valor para positions
export function setPositions(state, positions) {
  state.positions = positions;
}

export function setPhotoEmployee(state, photo) {
  state.selectedContact.employee.photo = photo;
}

//------------

// Establece un valor para icaCodes
export function setIcaCodes(state, icaCodes) {
  state.icaCodes = icaCodes;
}

export function setContactsModal(state, contact) {
  state.contactsModal = contact;
}
export function setContactFromVariousVouchers_setcontact(state, setcontact) {
  state.contactFromVariousVouchers.setcontact = setcontact;
}
export function setContactFromVariousVouchers_contact(state, contact) {
  // console.log(contact);
  if (state.contactFromVariousVouchers.setcontact) {
    state.contactFromVariousVouchers.contact = contact;
  }
}

// Establece un valor para categoriesEmployees
export function setCategoriesOthers(state, categoriesOthers) {
  state.categoriesOthers = categoriesOthers;
}

export function setDownloadExcelDataCreditLoading(state, value) {
  state.downloadExcelDataCreditLoading = value;
}
