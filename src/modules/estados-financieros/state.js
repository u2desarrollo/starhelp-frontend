export default {
    loading: {
        module: true
    },
    params: {
        year: '2021',
        account_from: null,
        account_up: null,
        description_account_from: null,
        description_account_up: null,
        contact_id: null,
    },
    params_cotact:{
        is_customer: false,
        is_provider: false,
        is_employee: false,
        filter_customer: [],
        filter_provider: [],
        filter_employee: [],
        paginate: true,
        page: 1,
        perPage: 50,
        query: '',
        sortBy: 'id',
        sort: 'ASC',
    },
    accounts_from: [],
    contacts:[],
    state_finaze:[],
    ag_grid:{
        selectedTheme:'ag-theme-balham'
    }
}