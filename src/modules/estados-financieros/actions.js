import $http from "../../axios";
import {
    Notification,
    MessageBox
} from 'element-ui';



//Obtener los terceros
export async function fetchContacts({
    commit,
    state
}, requestParameters = {
    params: state.params_cotact
}) {
    await $http.get('/api/terceros', requestParameters)
        .then((response) => {
            commit('setContacts', response.data.data.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('contactError', error.message)
        })
}

// obtener Data libro Contable
export async function fetchstatefinaze({commit, state}) {

    await $http.get('/api/estados-financieros', { params : state.params})
        .then((response) => {
            commit('setstate_finanze', response.data.data);
            state.loading.module = false;
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: 'Error',
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}