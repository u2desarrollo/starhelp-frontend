import $http from '../../axios'
import { Notification } from 'element-ui'

export async function getDaysDebt({commit,state},payload){
	const days = payload.days
	await $http.get(`/api/client-debt/${days}`).then((res) => {
		commit('getDaysDebt',res.data)
	}).catch((error) => {
		Notification.error({
			title: '¡Error!',
			message: error.message,
			type: 'error',
			duration: 1500,
			customClass: 'notification-box'
		})
		commit('ecommerceError', error.message)
	})
}
