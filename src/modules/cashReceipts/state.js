export const defaultFilterDate = () => {
  const end = new Date();
  const start = new Date();
  start.setTime(start.getTime() - 3600 * 1000 * 24);
  return [start, end]
}


export default {
  cashReceipts: {
    data: []
  },
  params: {
    filter_date: defaultFilterDate(),
    filter_text: '',
    paginate: true,
    page: 1,
    filter_document:null,
    customer:null,
    seller:null
  },
  preloader: false
}
