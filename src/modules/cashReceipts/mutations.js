export function setCashReceipts (state, cashReceipts) {
	state.cashReceipts = cashReceipts
}

export function setPreloader (state, preloader) {
    state.preloader = preloader
}
