import $http from '../../axios'
import { Notification } from "element-ui";

export async function fetchCashReceipts({ commit, state }) {
  commit('setPreloader', true)

  let params = {
    params: state.params
  }

  await $http.get('/api/recibos-de-caja', params).then((response) => {
    commit('setCashReceipts', response.data.data)
  }).catch((error) => {
    Notification.error({
      title: '¡Error!',
      message: error.message,
      type: 'error',
      duration: 1500,
      customClass: 'notification-box'
    })
  })
  commit('setPreloader', false)
}

export function setFilterPagination({ commit, dispatch }, filter) {
  commit('setFilter', filter.target.value)
  dispatch('fetchOrders')
}

export function setPagePagination({ commit, dispatch }, page) {
  commit('setPage', parseInt(page))
  dispatch('fetchOrders')
}

export async function getPdf({ state, commit }, cashReceipt) {
  var response = await $http.get(`/api/reciboCaja/descargar-pdf/${cashReceipt.id}`,
    {

      responseType: "blob"
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", `Rec-caja RC-${cashReceipt.consecutive}.pdf`); //or any other extension
      document.body.appendChild(link);
      link.click();
    }).catch(
      (error) => {
        console.log(error.message);
        Notification.error({
          title: '¡Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box'
        }
        );


      }
    )
}
