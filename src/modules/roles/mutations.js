import { roleDefault } from './state'

export function setRoles(state, roles) {
    state.roles = roles
}

export function setPermissions(state, permissions) {
    state.permissions = permissions;
}

export function setCategories(state, categories) {
  state.categories = categories;
}

export function setCategorySelected(state, category_selected) {
  state.category.selected = category_selected;
}

export function setModule(state, module) {
    state.module = module
}

export function setRoleDefault(state) {
    state.role = roleDefault();
    state.copyPermissions = [];
}
export function setRole(state, role) {
    state.role.id = role.id;
    state.role.name = role.role;
    state.role.permissions = role.getPermissions;
    state.copyPermissions = JSON.parse(JSON.stringify(role.getPermissions));
}
export function setActionForm(state, actionForm) {
    state.actionForm = actionForm;
}
export function setPromotionLoader(state, status) {
    state.promotionLoader = status
}

export function setPermissionsRole(state, permissions) {
    state.role.permissions = permissions
}

export function setUsers(state,users){
    state.users=users;

}

export function setPerPage(state, perPage) {
    state.params.page = 1;
    state.params.perPage = perPage;
  }

  export function setPage(state, page) {
    state.params.page = page;
  }
