export const roleDefault = () => {
    return {
        id: null,
        name: null,
        permissions: []
    }
}

export default {
    role: roleDefault(),
    permissions: [],
    categories: [],
    category: {
      selected:null
    },
    copyPermissions: [],
    roles: [],
    actionForm: 'create',
    users:[],
    params: {
        page: 1,
        perPage: 15,
        name:''

      },


}
