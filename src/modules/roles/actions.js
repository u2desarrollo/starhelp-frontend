import $http from '../../axios';
import {
    Notification,
    MessageBox
} from 'element-ui';
import { parse } from 'path';

export async function getRoles({ commit }) {
    $http.get('/api/rol').then((element) => {
        commit('setRoles', element.data);
    }).catch(error => {
    })
}

export async function deleteRole({ commit }, id) {
    await $http.delete(`/api/rol/${id}`)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'El rol se ha eliminado correctamente',
                type: 'success',
                duration: 2000,
                customClass: 'notification-box',
            });
        })
        .catch((error) => {
            console.log(error)
        })
}



export async function getPermissions({ commit }) {
    await $http.get('/api/permisos')
        .then((response) => {
            commit('setPermissions', (response.data.permissions))
            commit('setCategories', (response.data.categories))
            commit('setCategorySelected', (response.data.categories[0].id))
        })
        .catch(error => {
            console.log(error)
        })
}

export async function createRol({ commit }, role) {
    $http.post('/api/rol', role)
        .then(response => {
            commit('setRoleDefault')
            Notification.success({
                title: 'Exito!',
                message: 'El rol se ha creado correctamente',
                type: 'success',
                duration: 2000,
                customClass: 'notification-box',
            });
        }).catch(({ response }) => {
            if (response.data.message.includes('SQLSTATE[23505]')) {
                Notification.error({
                    title: 'Error!',
                    message: 'El nombre del rol ya existe',
                    type: 'error',
                    duration: 2000,
                    customClass: 'notification-box',
                });
            }
        })

}

export async function getRoleById({ commit }, id) {
    $http.get(`/api/rol/${id}`).
        then((response) => {
            commit('setRole', response.data);
        }).
        catch();
}

export async function updateRol({ state }, id) {

    await $http.put(`/api/rol/${id}`, state.role).then((response) => {
        Notification.success({
            title: 'Exito!',
            message: 'El rol se ha editado correctamente',
            type: 'success',
            duration: 2000,
            customClass: 'notification-box',
        });
    }).catch(error => {
        console.log(error)
    })
}


export async function getUsers({ state, commit }) {
    $http.get('/api/users', {
        params: state.params
    }).then(response => {
        commit('setUsers', response.data);
    })
        .catch(error => {
            Notification.error({
                title: "Error!",
                message: "Error Al Consultar los Usuarios",
                type: "error",
                duration: 1500,
                customClass: "notification-box"
            });
        })

}


export function setPerPagePagination({ commit, dispatch }, perPage) {
    commit("setPerPage", parseInt(perPage.target.value));
    dispatch("getUsers");
}

export function setPagePagination({ commit, dispatch }, page) {
    commit("setPage", parseInt(page));
    dispatch("getUsers");
}


export async function updateRoll({ state, commit }, user) {
    await $http.post('/api/usuario-editar-rol', user).then(response => {

        Notification.success({
            title: 'Exito!',
            message: 'Al Usuario se le a Editado el Rol Correctamente',
            type: 'success',
            duration: 2000,
            customClass: 'notification-box',
        });

    }).catch(
        error => {
            Notification.error({
                title: "Error!",
                message: "Error Al Editar el Rol del Usuario",
                type: "error",
                duration: 1500,
                customClass: "notification-box"
            });
        }
    );
}
