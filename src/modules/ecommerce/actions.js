import $http from '../../axios'
import { Notification } from 'element-ui'

export async function fetchCustomers ({ commit, state }, /* query, */ requestParameters = { params: state.params }) {
	//let user = JSON.parse(localStorage.getItem('user'))
	let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice)
	let data = state.params

	data.branchoffice_id = selected_branchoffice

	let params = {
		params: data
	}

	await $http.get('/api/establecimientos-vendedor', params).then((response) => {
		commit('setCustomers', response.data.data)
	}).catch((error) => {
		Notification.error({
			title: '¡Error!',
			message: error.message,
			type: 'error',
			duration: 1500,
			customClass: 'notification-box'
		})
		commit('ecommerceError', error.message)
	})
}

export async function loginB2b ({ commit, state }) {

	let customer_id = state.selectedCustomer.id
	localStorage.setItem('user_b2b', JSON.stringify(state.selectedCustomer))
	commit('login/setUserLoguedB2b', state.selectedCustomer, { root: true })
}

export async function runGetTypeIdentification ({ commit, state }) {
	await $http.get('/api/extra-data/type-identification')
		.then(response => {
			return response.data
		})
		.then(response => {
			commit('getTypeIdentification', response[0].extra_parameters)
		})
		.catch(error => {
			Notification.error({
				title: '¡Error!',
				message: error.message,
				type: 'error',
				duration: 1500,
				customClass: 'notification-box'
			})
			commit('ecommerceError', error.message)
		})
}

export function setFilterPagination ({ state, commit, dispatch }) {
		commit('setFilter', state.params.filter)
		dispatch('fetchCustomers')
}

export function setPerPagePagination ({ commit, dispatch }, perPage) {
	commit('setPerPage', parseInt(perPage.target.value))
	dispatch('fetchCustomers')
}

export function setPagePagination ({ commit, dispatch }, page) {
	commit('setPage', parseInt(page))
	dispatch('fetchCustomers')
}

export function setSortByPagination ({ commit, dispatch }, sortBy) {
	commit('setSortBy', sortBy)
	// dispatch('fetchLicenses')
}

export function setSortPagination ({ commit, dispatch }, sort) {
	commit('setSort', sort)
	// dispatch('fetchLicenses')
}
