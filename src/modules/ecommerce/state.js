export const defaultSelectedCustomer = () => {
	return {
    id: null
	}
}

export default {
	customers: [],
	selectedCustomer: defaultSelectedCustomer(),
	error: false,
	errorMessage: '',
    typeIdentifications:[],

    params: {
        id:'',
        employee_type:'',
        paginate: true,
        page: 1,
        filter: '',
        is_customer: true
    }
}
