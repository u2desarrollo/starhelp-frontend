export const defaultTableDateFirms = () => {
  return {
    items: [],
    fields: [
      {
        name: "Fecha Firme General",
        field: "firm_date_general",
        format: "text",
        style: { width: 100 }
      },
      {
        field: "account_id",
        name: "Cuenta",
        format: "text",
        style: { width: 350, short: true }
      },
      {
        field: "firm_date_account",
        name: "Fecha Firme Cuenta"
      },
      {
        field: "observations",
        name: "Observaciones",
        format: "text",
        style: { width: 50 }
      },
      {
        field: "actions",
        name: "Opciones",
        format: "text",
        style: { width: 300, short: true }
      }
    ],
    pagination: "",
    params: {
      perPage: 100,
      filterText: "",
      currentPage: 1,
      page: 1,
      pagination: true
    }
  };
};

export default {
  tableDateFirms: defaultTableDateFirms(),
  dateFirm: {},
  action: "create",
  errorForm: false
};
