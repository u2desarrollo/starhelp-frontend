export function setDateFirms(state, data) {
  // Agregamos los datos de la tabla
  state.tableDateFirms.items = data.data;

  // Quitamos los valores de la tabla del arreglo
  delete data.data;

  // Agregamos los datos de la paginacion
  state.tableDateFirms.pagination = data;
}

export function setErrorForm(state, value) {
  state.errorForm = value;
}

export function setAction(state, value) {
  state.action = value;
}

export function setDateFirm(state, data) {
  state.dateFirm = data;
}

export function resetDateFirm(state, data) {
  state.dateFirm = {};
}

export function setAttributeDateFirm(state, data) {
  state.dateFirm = {
    ...state.dateFirm,
    ...data
  };
}
