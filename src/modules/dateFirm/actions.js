import $http from "../../axios";
import { Notification } from "element-ui";

const apiName = "/api/fecha-firme";

export async function fetchDateFirms({ commit, state }) {
  await $http
    .get(apiName, {
      params: state.tableDateFirms.params
    })
    .then(response => {
      commit("setDateFirms", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchDateFirmById({ commit }, id) {
  await $http
    .get(`${apiName}/${id}`)
    .then(response => {
      commit("setDateFirm", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function createDateFirm({ commit, state, dispatch }) {
  commit("setErrorForm", false);

  await $http
    .post(apiName, state.dateFirm)
    .then(response => {
      dispatch("fetchDateFirms");
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setErrorForm", true);
    });
}

export async function updateDateFirm({ commit, state, dispatch }) {
  commit("setErrorForm", false);

  await $http
    .put(`${apiName}/${state.dateFirm.id}`, state.dateFirm)
    .then(response => {
      dispatch("fetchDateFirms");
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setErrorForm", true);
    });
}

export async function deleteDateFirm({ commit, dispatch, state }, id) {
  await $http
    .delete(`${apiName}/${id}`)
    .then(response => {
      dispatch("fetchDateFirms");
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setErrorForm", true);
    });
}
