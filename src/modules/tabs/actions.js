//Crear tab
export function addVisitedView({commit}, route) {
    commit('addVisitedViews', route)
}

//Eliminar un tab
export function removeVisitedView({commit}, route) {
    commit('removeVisitedViews', route)
}
