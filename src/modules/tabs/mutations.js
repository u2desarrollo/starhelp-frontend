//Crear tab
export function addVisitedViews(state, route) {
  let exists = true;
  for (let i = 0, len = state.tabs.length; i < len; i++) {
    if (state.tabs[i].meta.title == route.meta.title) {
      state.tabs.splice(i, 1, route);
      exists = false;
      break;
    }
  }

  if (exists) {
    state.tabs = state.tabs.slice(-9);
    state.tabs.push(route);
  }
}

//Eliminar un tab
export function removeVisitedViews(state, route) {
  for (var i = 0, len = state.tabs.length; i < len; i++) {
    if (state.tabs[i].meta.title == route.meta.title) {
      state.tabs.splice(i, 1);
      break;
    }
  }
}

//Eliminar un tab
export function removeLastVisitedViews(state, route) {
  const index = state.tabs.length -1
  state.tabs.splice(index, 1)
}

//Eliminar un tab en blanco
export function removeVisitedViewUndefined(state, route) {
  for (var i = 0, len = state.tabs.length; i < len; i++) {
    if (
      state.tabs[i].meta.title === undefined ||
      state.tabs[i].meta.title === null ||
      state.tabs[i].meta.title === "undefined"
    ) {
      state.tabs.splice(i, 1);
      break;
    }
  }
}

export function removeInventoryMovimentRepeat(state, route) {
  let pathAdd = route.path.split("/");
  var path = "";
  for (var i = 0, len = state.tabs.length; i < len; i++) {
    if (state.tabs[i].path !== undefined) {
      path = state.tabs[i].path.split("/");

      if (path[1] === pathAdd[1]) {
        if (state.tabs[i].path !== route.path) {
          state.tabs.splice(i, 1);
        }
      }
    }
  }
}
