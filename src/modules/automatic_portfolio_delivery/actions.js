import $http from '../../axios'
import { Notification } from 'element-ui'



export async function create({ commit,  }, params) {

  await $http.post('/api/automatic-portfolio-delivery', params).then((response) => {
    Notification.success({
      title: 'Exito!',
      message: 'La configuracion ha sido creada correctamente ',
      type: 'success',
      duration: 2000,
      customClass: 'notification-box',
    });
  }).catch(
    (response) => {
      commit('setResponse', false);
    }
  )

}


export async function getPortfolioShipments({ commit, state }) {
  await $http.get('/api/automatic-portfolio-delivery',).then((response) => {
    commit('setCopyData',JSON.parse(JSON.stringify(response.data)));
    commit('setData', response.data);
  }).catch((error) => {
  })
}


export async function deletePortfolioShipments({ commit, state }, id) {

  await $http.delete(`/api/automatic-portfolio-delivery/${id}`).then((response) => {
    Notification.success({
      title: 'Exito!',
      message: 'La configuracion ha sido eliminada ',
      type: 'success',
      duration: 2000,
      customClass: 'notification-box',
    });

  }).catch();
}


export async function updatePortfolioShipments({ commit, dispatch,  state }, params) {

  await $http.put(`/api/automatic-portfolio-delivery/${params.id}`, params)
    .then((response) => {
      Notification.success({
        title: 'Exito!',
        message: 'La configuración ha sido actualizada',
        type: 'success',
        duration: 2000,
        customClass: 'notification-box',
      });
      dispatch('getPortfolioShipments')
    }).catch((error) => {
    }

    );
}
