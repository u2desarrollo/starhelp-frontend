export const defaultSelectedSubcategory = () => {
    return {
        subcategorie_code: '',
        description: '',
    }
}

export default {

    subcategories: [],
    subCategoriesList: [],
    selectedSubcategory: defaultSelectedSubcategory(),
    error: false,
    errorMessage: '',
    actionSubcategory: 'create',
    getContactEdit: true,

}