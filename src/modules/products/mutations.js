import {
  defaultSelectedProduct
} from './state'
import {getApiDomain} from "../../util/domains";

// trae todos los productos
export function setStateProducts (state, products) {
  state.products = products;
}

// Guardar los data sheets
export function setProductsDataSheets (state, data_sheets) {
  state.data_sheets = data_sheets;
}

// trae al estate un solo producto
export function setProduct (state, product) {
  state.selectedProduct = product;
}

export function setProductsAttachments (state, products_attachments) {

  products_attachments.map(pa => {
    pa.src = getApiDomain() + pa.src
    pa.thumb = getApiDomain() + pa.thumb
  })

  state.products_attachments = products_attachments;
}

export function setAction (state, action) {
  state.action = action;
}

export function productError (state, error) {
  state.error = true
  state.errorMessage = error
  state.products = []
}

export function setGetProductEdit (state, getProductEdit) {
  state.getProductEdit = getProductEdit;
}

//Establece el valor por defecto de selectedLine
export function resetSelectedProduct (state) {
  let id = state.selectedProduct.id
  Object.assign(state.selectedProduct, defaultSelectedProduct())
  state.products_attachments = []
  state.selectedProduct.id = id
}

export function setLines (state, lines) {
  state.lines = lines
}

export function setImagenCarga (state, image) {
  state.selectedProduct.products_attachments.url = image
}

export function setDocumentPath (state, data_sheet) {
  state.selectedProduct.products_technnical_data.link_data_sheet = data_sheet;
}

export function setSublines (state, sublines) {

  if (sublines.length <= 0) { return }

  state.sublines = sublines.map(p => {
    return {
      value: p.id,
      label: `${p.subline_code} - ${p.subline_description}`
    }
  })
}

export function setBrands (state, brands) {

  if (brands.length <= 0) { return }

  state.brands = brands.map(p => {
    return {
      value: p.id,
      label: p.description
    }
  })
}

export function setSubbrands (state, subbrands) {
  state.subbrands = subbrands.map(p => {
    return {
      value: p.id,
      label: p.description
    }
  })
}

export function setCategories (state, categories) {
  state.categories = categories.map(p => {
    return {
      value: p.id,
      label: p.description
    }
  })
}

export function setSubcategories (state, subcategories) {
  state.subcategories = subcategories.map(p => {
    return {
      value: p.id,
      label: p.description
    }
  })
}

// Establece un valor para tipo de producto
export function setProductType (state, productType) {
  state.productType = productType
}

// Establece un valor para Talla
export function setSize (state, size) {
  state.size = size
}

export function setColor (state, color) {
  state.color = color
}

// Establece un valor para taxpayerTypes
export function setRotationType (state, rotationType) {
  state.rotationType = rotationType;
}

// Establece un valor para taxRegimes
export function setApplicationUnit (state, applicationUnit) {
  state.applicationUnit = applicationUnit
}

export function setFilterBranch (state, branch) {
  state.search_params.branch = branch;
}

export function setParams (state, new_params) {

  let params = JSON.parse(JSON.stringify(state.params))

  for (let np in new_params) {
    let add = false
    for (let p in params) {
      if (p == np) {
        params[p] = new_params[np]
        add = true
      }
    }

    if (!add) {
      params[np] = new_params[np]
    }
  }

  state.params = params
}

export function setErrorFileImport (state, data) {
  state.errorFileImport = data;
}

export function setChangeStateLoadFile (state, data) {
  state.loadFileImport = data;
}

export function clearDataSheets (state) {
  state.data_sheets = [];
}



/*
 * Estos mutadores se encargar de normalizar la informacion
 * para mostrar en la tabla de productos.
 *
 * @author Kevin Galindo
 */

export function setProductsTable (state, data) {
  //console.log(data.data);
  // Agregamos los datos de la tabla
  state.tableProduct.data = data.data;

  // Quitamos los valores de la tabla del arreglo
  delete data.data

  // Agregamos los datos de la paginacion
  state.tableProduct.pagination = data
}

export function setParametersTableInRow (state, data) {
  state.data_sheets.map((ds, index) => {
    if (ds.data_sheet_id == data.row.data_sheet_id) {
      state.data_sheets[index].data_sheet_detail.parameters = data.data
      state.data_sheets[index].data_sheet_detail.paramtable_id = data.paramtable_id
    }
  })
}

export function setbranchoffice_warehouse_id (state, warehouse_id) {
  state.params.branchoffice_warehouse_id = warehouse_id
}
export function setBranchoffice (state, branchoffice) {
  state.params.branchoffice = branchoffice
}
export function setContactIdParams (state, contact_id) {
  state.params.contact_id = contact_id
}
export function setProviders(state, providers){
  state.providers = providers
}
