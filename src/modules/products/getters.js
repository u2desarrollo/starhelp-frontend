export function getParams(state) {
    return state.params;
}

export function getProductDataSheet(state) {
    return state.data_sheets;
}

export function ListProductsTable(state) {
    return state.tableProduct;
}