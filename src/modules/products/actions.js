import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";

// Por favor no mover este metodo
export async function searchProducts({ state, commit }, model_id = null) {
  let requestParameters = {
    params: state.params
  };

  if (model_id !== null) {
    requestParameters.params.model_id = model_id;
  }

  await $http.get("/api/lista-productos", requestParameters).then(response => {
    commit("setStateProducts", response.data.data);
  });
}

export async function getProductForCode({ commit, state, rootState }, code) {
  await $http
    .get(`/api/productos/id/${code}`)
    .then(response => {
      if (response.data.success) {
        state.productEdit = response.data.data.id;
      }
    })
    .catch(error => {
      state.productEdit = null;
      commit("productError", error.message);
    });
}
export async function addProduct({ commit, state }) {
  //data_sheets
  await $http
    .post("/api/productos", state.selectedProduct)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "El producto ha sido creado",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setProduct", response.data.data);
      state.error = false;
    })
    .catch(error => {
      MessageBox.alert(error.message, "Error", {
        dangerouslyUseHTMLString: true,
        confirmButtonText: "Aceptar",
        type: "error",
        customClass: "notification-error"
      });
      commit("productError", error.message);
    });
}
export async function getProduct({ commit, state }, id) {
  if (state.getProductEdit) {
    let params = {
      params: {
        branchoffice_id: localStorage.selected_branchoffice
      }
    };

    await $http
      .get(`/api/productos/${id}/editar`, params)
      .then(response => {
        commit("setProduct", response.data.data);
        commit("setProductsAttachments", response.data.data.products_attachments);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        commit("productError", error.message);
      });

    commit("setGetProductEdit", false);
  }
}
export async function updateProduct({ commit, state }) {
  state.selectedProduct.data_sheets = [];

  state.data_sheets.map(item => {
    state.selectedProduct.data_sheets.push({
      value: item.value,
      data_sheet_id: item.data_sheet_id
    });
  });

  await $http
    .put(`/api/productos/${state.selectedProduct.id}`, state.selectedProduct)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "El producto ha sido actualizado",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setProduct", response.data.data);
    })
    .catch(error => {
      MessageBox.alert(error.message, "Error", {
        confirmButtonText: "Aceptar",
        type: "error"
      });
      commit("productError", error.message);
    });
}

export function cargaImagenProducto({ commit }, image) {
  commit("setImagenCarga", image);
}

export function documentChangePath({ commit }, path) {
  commit("setDocumentPath", path);
}

//Descargar documento
export async function downloadDocument({ state }) {
  let extension = state.selectedProduct.products_technnical_data.link_data_sheet
    .split(".")
    .pop();
  await $http({
    url: state.selectedProduct.products_technnical_data.link_data_sheet,
    method: "GET",
    responseType: "blob" // important
  })
    .then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute(
        "download",
        state.selectedProduct.description + "." + extension
      );
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 3000,
        customClass: "notification-box"
      });
    });
}

export async function fetchSublines(
  { dispatch, state, rootState, commit },
  requestParameters = { params: state.category_params }
) {
  let params = {
    params: {
      lines: state.tableProduct.options.filter_lines,
      filter_brands: state.tableProduct.options.filter_brands
    }
  };

  await dispatch("sublines/listSublines", params, { root: true });
  commit("setSublines", rootState.sublines.sublines);
}

export async function fetchBrands({ dispatch, state, rootState, commit }) {
  await dispatch(
    "brands/fetchBrands",
    {
      params: {
        paginate: false,
        lines: state.tableProduct.options.filter_lines
      }
    },
    { root: true }
  );
  commit("setBrands", rootState.brands.brands);
}
export async function fetchSubbrands(
  { dispatch, state, rootState, commit },
  requestParameters = {
    params: state.tableProduct.options
  }
) {
  let params = {
    params: {
      brands: state.tableProduct.options.filter_brands
    }
  };
  await dispatch("subbrands/listSubbrands", params, {
    root: true
  });
  commit("setSubbrands", rootState.subbrands.subbrands);
}
export async function fetchCategories({ dispatch, state, rootState, commit }) {
  if (!state.categories.length) {
    await dispatch(
      "categories/fetchCategories",
      {
        params: {
          paginate: false,
          lines: state.params.lines
        }
      },
      {
        root: true
      }
    );
    commit("setCategories", rootState.categories.categories);
  }
}
export async function fetchSubcategories(
  { dispatch, state, rootState, commit },
  requestParameters = {
    params: state.params
  }
) {
  await dispatch("subcategories/listSubcategories", requestParameters, {
    root: true
  });
  commit("setSubcategories", rootState.subcategories.subcategories);
}

function mapParameters(parameters) {
  return parameters.map(p => {
    return {
      value: p.id,
      label: p.name_parameter
    };
  });
}

//-------Suscriptores-----------//
// Obtiene los tipos de producto
export async function fetchProductType({ dispatch, state, rootState, commit }) {
  if (!state.productType.length) {
    await dispatch(
      "parameters/listParameter",
      {
        params: {
          idParamTable: 13,
          paginate: false
        }
      },
      {
        root: true
      }
    );
    commit("setProductType", mapParameters(rootState.parameters.parameters));
  }
}

// Obtiene las tallas
export async function fetchSize({ dispatch, state, rootState, commit }) {
  if (!state.size.length) {
    await dispatch(
      "parameters/listParameter",
      {
        params: {
          idParamTable: 17,
          paginate: false
        }
      },
      {
        root: true
      }
    );
    commit("setSize", mapParameters(rootState.parameters.parameters));
  }
}

// Obtiene los colores
export async function fetchColor({ dispatch, state, rootState, commit }) {
  if (!state.color.length) {
    await dispatch(
      "parameters/listParameter",
      {
        params: {
          idParamTable: 16,
          paginate: false
        }
      },
      {
        root: true
      }
    );
    commit("setColor", mapParameters(rootState.parameters.parameters));
  }
}

// Obtiene los tipos de rotatcion
export async function fetchRotationType({
  dispatch,
  state,
  rootState,
  commit
}) {
  if (!state.rotationType.length) {
    await dispatch(
      "parameters/listParameter",
      {
        params: {
          idParamTable: 4,
          paginate: false
        }
      },
      {
        root: true
      }
    ); //TODO: Determinar el idParamTable
    commit("setRotationType", mapParameters(rootState.parameters.parameters));
  }
}

// Obtiene las unidades de aplicación
export async function fetchApplicationUnit({
  dispatch,
  state,
  rootState,
  commit
}) {
  if (!state.applicationUnit.length) {
    await dispatch(
      "parameters/listParameter",
      {
        params: {
          idParamTable: 3,
          paginate: false
        }
      },
      {
        root: true
      }
    ); //TODO: Determinar el idParamTable
    commit(
      "setApplicationUnit",
      mapParameters(rootState.parameters.parameters)
    );
  }
}

//Exportar archivo para crear productos.
export async function generateFileExport({ commit, state }, line_id) {
  await $http({
    url: `/api/productos/ficha-datos/exportar/${line_id}`,
    method: "GET",
    responseType: "blob" // important
  }).then(response => {
    var type_file = response.data.type.split("/")[1];

    //Descargar el archivo
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", `plantilla-crear-productos.${type_file}`);
    document.body.appendChild(link);
    link.click();
  });
}

//Exportar archivo para crear productos.
export async function uploadFileProducts({ commit, state }, data) {
  state.errorFileImport = [];
  commit("setChangeStateLoadFile", true);

  await $http
    .post(`/api/productos/ficha-datos/importar/${data.line_id}`, data.file)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: response.data.message.text,
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      state.errorFileImport = [];
    })
    .catch(error => {
      //setErrorFileImport
      Notification.error({
        title: "Error!",
        message: "Error al importar",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setErrorFileImport", error.response.data);
    });

  commit("setChangeStateLoadFile", false);
}

//Traemos los data sheets
export async function productsDataSheets({ commit, dispatch, state }, id) {
  // commit('clearDataSheets')
  await $http
    .get(`/api/productos-ficha-datos/${id}`)
    .then(response => {
      commit("setProductsDataSheets", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.response.data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

//Crear una nueva ficha de dato
export async function createProductsDataSheets(
  { commit, state, dispatch },
  data
) {
  await $http
    .post(`/api/ficha-datos-productos/`, data)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: response.data.message,
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      dispatch("productsDataSheets", data.product_id);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.response.data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

//Actualizar ficha de dato del producto
export async function updateProductsDataSheets(
  { commit, state, dispatch },
  data
) {
  await $http
    .put(`/api/ficha-datos-productos/${data.product_id}`, data.value)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: response.data.message,
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      dispatch("productsDataSheets", data.product_id);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.response.data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

//Eliminar ficha de dato del producto
export async function deleteProductsDataSheets(
  { commit, state, dispatch },
  data
) {
  await $http
    .delete(
      `/api/ficha-datos-productos/${data.product_id}?data_sheet_id=${data.data_sheet_id}`
    )
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: response.data.message,
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      dispatch("productsDataSheets", data.product_id);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.response.data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/*
 * Estos metodos se encargar de traer la infromacion para
 * mostrar en la tabla de productos.
 *
 * @author Kevin Galindo
 */
export async function getProductsTable({ commit, state }, data) {
  state.tableProduct.loading = true;

  let params = {
    params: state.tableProduct.options
  };

  await $http
    .get(`/api/tabla-productos/`, params)
    .then(response => {
      commit("setProductsTable", response.data);
    })
    .catch(error => {
      console.error(error);
    });

  state.tableProduct.loading = false;
}

//Este metodo se encarga de guardar un producto.
export async function createProduct({ commit, state }) {
  state.selectedProduct.data_sheets = [];

  state.data_sheets.map(item => {
    state.selectedProduct.data_sheets.push({
      value: item.value,
      data_sheet_id: item.data_sheet_id
    });
  });

  return await new Promise(async (resolve, reject) => {
    await $http
      .post("/api/productos", state.selectedProduct)
      .then(response => {
        Notification.success({
          title: "Exito!",
          message: "El producto ha sido creado",
          type: "success",
          duration: 1500,
          customClass: "notification-box"
        });
        commit("setProduct", response.data.data)
        resolve(response.data.data.id)
      })
      .catch(error => {
        MessageBox.alert('Error en la creación del producto', "Error", {
          dangerouslyUseHTMLString: true,
          confirmButtonText: "Aceptar",
          type: "error",
          customClass: "notification-error"
        });
        reject()
      });
  })


}

//Eliminar productos
export async function deleteProduct({ commit, state }, id) {
  await $http
    .delete(`/api/productos/${id}`)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "El producto ha sido eliminado",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      let data = error.response.data.message
      Notification.error({
        title: "Error!",
        message: "No se pudo eliminar el producto: " + data,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function getParameterByTable({ commit, state }, data) {
  state.parametersByTable = [];
  let response = await $http.get(
    `/api/parametros-por-tabla/${data.paramtable_id}`
  );
  if (response.status == 200) {
    commit("setParametersTableInRow", {
      data: response.data,
      row: data.row,
      paramtable_id: data.paramtable_id
    });
  }
}

export async function fetchLines({ dispatch, state, rootState, commit }) {
  if (!state.lines.length) {
    await dispatch(
      "lines/fetchLines",
      { params: { paginate: false } },
      { root: true }
    );
    commit("setLines", rootState.lines.lines);
  }
}

export async function fetchProviders({ commit }, filter) {
  await $http
    .get("/api/lista-proveedores", filter)
    .then(response => {
      commit("setProviders", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function reportGenerate({}) {
  await $http({
    url: "/api/reporte-productos",
    method: "GET",
    responseType: "blob" // important
  })
    .then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "reporte-productos.csv");
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 3000,
        customClass: "notification-box"
      });
    });
}

export async function getImages({ commit, state }) {
  await $http
    .get(`/api/imagenes-producto/${state.selectedProduct.id}`)
    .then(response => {
      commit('setProductsAttachments', response.data.data)
    })
    .catch(error => {
      let data = error.response.data.message
      Notification.error({
        title: "Error!",
        message: "No se pudo eliminar el producto: " + data,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}


export async function deleteImage({ dispatch, commit, state }, product_attachment_id) {
  await $http
    .delete(`/api/imagenes-producto/${product_attachment_id}`)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "La imagen ha sido eliminada",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      dispatch('getImages')
    })
    .catch(error => {
      let data = error.response.data.message
      Notification.error({
        title: "Error!",
        message: 'Error al eliminar la imagen',
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function setMainToImage({ dispatch, commit, state }, product_attachment_id) {
  await $http
    .put(`/api/imagenes-producto/${product_attachment_id}`)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "La imagen principal ha cambiado",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      dispatch('getImages')
    })
    .catch(error => {
      let data = error.response.data.message
      Notification.error({
        title: "Error!",
        message: 'Error al cambiar la imagen principal',
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
