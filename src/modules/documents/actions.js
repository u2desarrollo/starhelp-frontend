import $http from "../../axios"
import { Notification,  MessageBox } from 'element-ui'

export async function getDocuments({commit,state},requestParameters = {params: state.params}){
    await $http.get('/api/documents',requestParameters).then((res)=>{
        commit('setDocuments',res.data);
    }).catch((error)=>{
        Notification.error({
			title: '¡Error!',
			message: error.message,
			type: 'error',
			duration: 1500,
			customClass: 'notification-box'
		})
        commit('documentError', error.message)
    })
}
export async function getVouchersType({commit,state},parameter){
    await $http.get('/api/voucherstype',parameter).then((res)=>{
        commit('setVouchersType',res.data);
    }).catch((error)=>{
        Notification.error({
			title: '¡Error!',
			message: error.message,
			type: 'error',
			duration: 1500,
			customClass: 'notification-box'
		})
        commit('voucherTypeError', error.message)
    })
}
export async function getCategory({commit,state},requestParameters ={params: state.category}){
    await $http.get('/api/category',requestParameters).then((res)=>{
        commit('setCategory',res.data);
    }).catch((error)=>{
        Notification.error({
			title: '¡Error!',
			message: error.message,
			type: 'error',
			duration: 1500,
			customClass: 'notification-box'
		})
        commit('CategoryError', error.message)
    })
}

