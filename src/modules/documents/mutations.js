export function setDocuments(state, document) {
  state.documents = document;
}

export function setVouchersType(state, voucher) {
  state.vouchersType = voucher;
}

export function setCategory(state, category) {
  state.categories = category;
}

export function setFilterCategory(state, category) {
  state.category.id = category;
}

export function setFilterVouchersType(state, voucherType) {
  state.params.voucherType = voucherType;
}

export function setFilterBranch(state, branch) {
  state.params.branch = branch;
}

export function setFilterDate(state, date) {
  state.params.date = date;
}
