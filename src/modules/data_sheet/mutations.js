export function setDataSheets (state, data) {
    state.dataSheets = data;
}

export function setTypeValDataSheet (state, data) {
    state.typeValueDataSheet = data;
}

export function setDataSheet (state, data) {
    state.dataSheetLayout = data;
}

export function setAction (state, action) {
    state.action = action;
}

export function emptyDataSheet (state) {
    state.dataSheetLayout = {
        internal_name: '',
        visible_name: '',
        type_value: '',
        tooltip: '',
        id: ''
    };
}

export function setError (state, payload) {
    state.error = true;
    state.errorMessage = JSON.parse(JSON.stringify(payload));
}

/*
 * Estos mutadores se encargar de normalizar la informacion
 * para mostrar en la tabla de productos.
 *
 * @Author Kevin Galindo
 */

export function setDataSheetsTable (state, data) {

    // Agregamos los datos de la tabla
    state.tableDataSheets.data = data;

    // Quitamos los valores de la tabla del arreglo
    //delete data.data;

    // Agregamos los datos de la paginacion
    //state.tableDataSheets.pagination = data;
}

export function setRepeatedDataSheet (state, data) {
    state.repeatedDataSheet = data;
}