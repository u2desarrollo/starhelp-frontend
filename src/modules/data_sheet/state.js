const tableDataSheets = {
    loading: false,
    data: [],
    fields: [{
            field: "visible_name",
            name: "Nombre Visible"
        },
        {
            field: "internal_name",
            name: "Nombre Interno"
        },
        {
            field: "tooltip",
            name: "Texto de ayuda"
        },
        {
            field: "type_value_name.name_parameter",
            name: "Tipo valor"
        },
        {
            field: "actions",
            name: "Opciones"
        }
    ],
    errors: {
        has: false,
        message: '',
        code: 0,
        data: []
    },
    pagination: [],
    options: {
        perPage: 10,
        search: '',
        currentPage: 1
    },
}

export default {
    dataSheets: [],
    dataSheetLayout: {
        internal_name: '',
        visible_name: '',
        type_value: '',
        tooltip: '',
        id: '',
        paramtable_id: ''
    },
    typeValueDataSheet: [],
    action: 'create',
    repeatedDataSheet: '',
    //ERROR STATE
    error: false,
    errorMessage: '',
    // Tabla
    tableDataSheets
}