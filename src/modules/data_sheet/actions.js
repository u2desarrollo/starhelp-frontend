import $http from "../../axios";
import {
    Notification,
    MessageBox
} from 'element-ui';

//Obtener todos los registros de la ficha de datos.
export async function fetchDataSheets({
    commit,
    state
}, requestParameters = {
    params: state.params
}) {

    await $http.get('/api/ficha-datos-listado')
        .then((response) => {
            commit('setDataSheets', response.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
        });
}

//Obtener los valores de la tabla
export async function getDataSheetsTable({
    commit,
    state
}, data) {

    state.tableDataSheets.loading = true;

    await $http.get(`/api/ficha-datos/?page=${state.tableDataSheets.options.currentPage}&perPage=${state.tableDataSheets.options.perPage}&search=${state.tableDataSheets.options.search}`)
        .then((response) => {
            commit('setDataSheetsTable', response.data)
        }).catch((error) => {
        })

    state.tableDataSheets.loading = false;

}

//Obtener solo un registro segun el id
export async function findById({
    commit,
    state
}, id) {
    await $http.get(`/api/ficha-datos/${id}`)
        .then((response) => {
            commit('setDataSheet', response.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
        });
}

//Crear un registro de una ficha de datos.
export async function create({
    commit,
    state
}) {

    state.error = false;
    await $http.post('/api/ficha-datos', state.dataSheetLayout)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'La ficha de dato ha sido creada',
                type: 'success',
                duration: 1500,
                customClass: 'notification-box',
            });
            state.error = false;
            commit('setDataSheet', response.data.data)

        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.response.data.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('setError', error.response);
        });
}

//Actualizar un registro segun el id de la fecha de datos.
export async function update({
    commit,
    state
}) {

    state.error = false;
    await $http.put(`/api/ficha-datos/${state.dataSheetLayout.id}`, state.dataSheetLayout)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'La ficha de dato ha sido actualizada.',
                type: 'success',
                duration: 1500,
                customClass: 'notification-box',
            });
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.response.data.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('setError', error.response);
        });
}

//Eliminamos una ficha de dato segun su id
export async function destroyDataSheet({
    commit,
    state
}, id) {

    state.error = false;
    await $http.delete(`/api/ficha-datos/${id}`)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'La ficha de dato ha sido eliminada.',
                type: 'success',
                duration: 1500,
                customClass: 'notification-box',
            });
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.response.data.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('setError', error.response);
        });
}

//
//Obtener los parametros
export async function getTypeVal({
    commit,
    state
}) {
    await $http.get(`/api/ficha-datos-tipo-valor`)
        .then((response) => {
            commit('setTypeValDataSheet', response.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
        });
}

//Verifica si una ficha de dato existe
export async function checkExistDataSheet({
    commit,
    state
}) {

    state.repeatedDataSheet = '';
    var search = state.dataSheetLayout.visible_name;

    await $http.get(`/api/ficha-datos-verificar-existe?search=${search}`)
        .then((response) => {
            commit('setRepeatedDataSheet', response.data)
        })
        .catch((error) => {

        });
}