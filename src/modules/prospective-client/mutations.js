import { defaultProspective } from "./state";

export function setProspective(state, prospective) {
  state.prospective = prospective;
}

export function cleanInputCity(state) {
  state.prospective.city_id = "";
}

export function setTargetImg(state, payload) {
  state.prospective.photo = payload;
}

export function setPhotoFile(state, photo) {
  state.prospective.photo = photo;
}

export function setDepartments(state, departments) {
  state.departments = departments.map(p => {
    return { value: p.id, label: p.department };
  });
}

export function setCities(state, cities) {
  state.cities = cities.map(p => {
    return { value: p.id, label: p.city };
  });
}

export function prospectiveError(state, payload) {
  state.error = payload;
}

export function setIdentificationTypes(state, identificationTypes) {
  state.identificationTypes = identificationTypes;
}

export function setLines(state, lines) {
  state.lines = lines.map(p => {
    return { value: p.id, label: p.line_description };
  });
}

export function resetProspectiveForm(state) {
  Object.assign(state.prospective, defaultProspective());
}

export function getProspectiveWarehouseId(state, warehouse_id) {
  state.prospective.warehouse_id = warehouse_id;
}

export async function assignNewClientIdent(state, payload) {
  state.prospective.identification_type = payload;
}

export function seStatImage(state) {
  state.image.url = null;
  state.showRotate = false;
}
