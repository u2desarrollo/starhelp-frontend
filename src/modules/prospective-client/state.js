export const defaultProspective = () => {
	return {
	  identification_type: null,
		identification: '',
		check_digit: 0,
		name: '',
		address: '',
		main_telephone: null,
		cell_phone: '',
		observations: '',
		photo: null,
		contact_name: '',
		surname: '',
		gender: null,
		email: '',
		latitud: '',
		longitud: '',
		city_id: '',
		warehouse_id: '',
		tradename:''
	}
}

export default {
	prospective: defaultProspective(),
	departments: [],
	cities: [],
	identificationTypes: [],
	lines: [],
	error: false,
	errorMessage: '',
	params: {
		// something
	},
	loader: false,
	person:{
		type:0
	},
	image:{
		url:null,
		showRotate:false
	}
	
}
