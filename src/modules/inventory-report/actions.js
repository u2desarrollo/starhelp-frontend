import $http from "../../axios";
import {Notification, MessageBox} from "element-ui";

/**
 * Obtiene todas las sedes del suscriptor actual
 * @param dispatch
 * @param state
 * @param rootState
 * @param commit
 * @returns {Promise<void>}
 */
export async function fetchBranchOffices({dispatch, state, rootState, commit}) {
  let params = {
    params: {
      subscriber_id: 1,
      paginate: false
    }
  };

  await $http
    .get("/api/sedes", params)
    .then(response => {
      commit("setBranchoffices", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

/**
 * Obtiene todas las bodegas de la sede seleccionada
 * @param dispatch
 * @param state
 * @param rootState
 * @param commit
 * @returns {Promise<void>}
 */
export async function fetchBranchOfficesWarehouses({dispatch, state, rootState, commit}) {

  commit('clearParamsBranchofficesWarehouseId')
  if (!state.params.branchoffices_warehouses) {
    let params = {
      params: {
        branchoffice_id: state.params.branchoffice_id,
        paginate: false
      }
    };

    await $http
      .get("/api/bodegas-sedes", params)
      .then(response => {
        commit("setBranchofficesWarehouses", response.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 2000,
          customClass: "notification-box"
        });
      });
  } else {
    commit("setBranchofficesWarehouses", []);
  }
}

/**
 *
 * @param dispatch
 * @param state
 * @param rootState
 * @param commit
 * @returns {Promise<void>}
 */
export async function fetchLines({dispatch, state, rootState, commit}) {
  await dispatch(
    "lines/fetchLines",
    {params: {paginate: false}},
    {root: true}
  );
  commit("setLines", rootState.lines.lines);
}

/**
 *
 * @param dispatch
 * @param state
 * @param rootState
 * @param commit
 * @returns {Promise<void>}
 */
export async function fetchBrands({dispatch, state, rootState, commit}) {
  await dispatch(
    "brands/fetchBrands",
    {
      params: {
        paginate: false,
        lines: []
      }
    },
    {root: true}
  );
  commit("setBrands", rootState.brands.brands);
}


/**
 *
 * @returns {Promise<void>}
 */
export async function reportGenerate({state, commit}) {
  commit('setPreloader', true)
  await $http({
    url: "/api/inventario-valorizado",
    method: "GET",
    responseType: "blob",
    params: state.params
  })
    .then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "Inventario-valorizado.csv");
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 3000,
        customClass: "notification-box"
      });
    });
  commit('setPreloader', false)
}



