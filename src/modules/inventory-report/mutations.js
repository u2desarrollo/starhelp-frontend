export function setBranchoffices(state, branchoffices){
  branchoffices.unshift({id: -1, name: 'TODOS SEPARADO POR SEDE - BODEGA'})
  branchoffices.unshift({id: -2, name: 'TODOS CONSOLIDADO'})
  state.branchoffices = branchoffices
}

export function setBranchofficesWarehouses(state, branchoffices_warehouses){
  state.branchoffices_warehouses = branchoffices_warehouses
}

export function setLines(state, lines){
  state.lines = lines
}

export function setBrands(state, brands){
  state.brands = brands
}

export function clearParamsBranchofficesWarehouseId(state){
  state.params.branchoffice_warehouse_id = null
}

export function setPreloader(state, preloader){
  state.preloader = preloader
}
