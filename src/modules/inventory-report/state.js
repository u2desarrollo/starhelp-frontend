
export default {
  branchoffices: [],
  branchoffices_warehouses: [],
  lines: [],
  brands: [],
  preloader: false,
  params: {
    date: new Date(),
    branchoffice_id: -1,
    branchoffice_warehouse_id: null,
    line_id: null,
    brand_id: null
  }
};
