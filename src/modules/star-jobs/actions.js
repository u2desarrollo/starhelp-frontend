import $http from "../../axios";
import { Notification } from "element-ui";

export async function fetchUsers(
  { commit, state },
  requestParameters = { params: state.users_params }
) {
  await $http
    .get("/api/usuarios", requestParameters)
    .then(response => {
      commit("setUsers", response.data.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchUsersName(
  { commit, state }
) {
  await $http
    .get("/api/usuarios-username")
    .then(response => {
      commit("setUsersNames", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchJob(
  { commit, state }, id
) {
  await $http
    .get("/api/tareas/"+id, )
    .then(response => {
      commit("setJob", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetDocument(
  { commit, state }, id
) {
  await $http
    .get("/api/tareas/"+id+"/archivos/ver", )
    .then(response => {
      commit("setModalUrl", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}


export async function fetDocumentinteraction(
  { commit, state }, id
) {
  await $http
    .get("/api/interaccion/"+id+"/archivos/ver", )
    .then(response => {
      commit("setModalUrl", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function closeJob(
  { commit, state }, id
) {
  await $http
    .delete("/api/tareas/"+id, )
    .then(response => {
      commit("setDefaultJob", response);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: data,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function createJob({ commit, state }) {
  let formData = new FormData();
  let job = null

  formData.append("name", state.job.name );
  formData.append("description", state.job.description );

  if (state.job.closing_date != null) {
    formData.append("closing_date", state.job.closing_date );
  }
  if (state.job.limit_date != null) {
    formData.append("limit_date", state.job.limit_date+' 00:00:00');
  }

  state.job.files.map(fn => {
    formData.append("files[]", fn);
  });

  await $http
    .post(`/api/tareas/`, formData)
    .then(response => {
      job = response.data.data
      Notification.success({
        title: "Hecho!",
        message: "Se Ha Creado El Job Exitosamente",
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      console.log(error);
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
    return job
}

export async function updateJob({ commit, state }, id) {
  let data = {
    name:state.job.name,
    description: state.job.description,
    closing_date: state.job.closing_date,
    limit_date: state.job.limit_date,
  };
  let job = null

  // formData.append("name", state.job.name );
  // formData.append("description", state.job.description );

  // if (state.job.closing_date != null) {
  //   formData.append("closing_date", state.job.closing_date );
  // }
  // if (state.job.limit_date != null) {
  //   if (state.job.limit_date.length == 10) {
  //     formData.append("limit_date", state.job.limit_date+' 00:00:00');
  //   }else{
  //     formData.append("limit_date", state.job.limit_date);
  //   }
  // }

  // state.job.files.map(fn => {
  //   formData.append("files[]", fn);
  // });


  await $http
    .put(`/api/tareas/${id}`, data)
    .then(response => {
      job = response.data.data
      Notification.success({
        title: "Hecho!",
        message: "Se Ha Creado El Job Exitosamente",
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      console.log(error);
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
    return job
}

export async function createInteraction({ commit, state }, id) {
  let formData = new FormData();
  formData.append("description", state.interaction.description );
  state.interaction.files.map(fn => {
    formData.append("files[]", fn);
  });

  await $http
    .post(`/api/tareas/${id}/interaccion`, formData)
    .then(response => {
      Notification.success({
        title: "Hecho!",
        message: "La interaccion ha sido creada",
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      console.log(error);
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function addFileJob({ commit, state }, id) {
  let formData = new FormData();

  state.job.files.map(fn => {
    formData.append("files[]", fn);
  });

  await $http
    .post(`/api/tareas/${id}/agregar-archivos`, formData)
    .then(response => {
      // Notification.success({
      //   title: "Hecho!",
      //   message: "La interaccion ha sido creada",
      //   type: "success",
      //   duration: 2000,
      //   customClass: "notification-box"
      // });
    })
    .catch(error => {
      console.log(error);
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function addMembers({ commit, state }, id) {
  let params =  {members : state.newmembers} 
  await $http
    .post(`/api/tareas/${id}/agregar-integrandes`, params)
    .then(response => {
      // Notification.success({
      //   title: "Hecho!",
      //   message: "La interaccion ha sido creada",
      //   type: "success",
      //   duration: 2000,
      //   customClass: "notification-box"
      // });
    })
    .catch(error => {
      console.log(error);
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}




export async function downloadDoc({ commit, state },{ id, name}) {
  try {
    var response = await $http.get(`/api/tareas/${id}/archivos/descargar`, {
      responseType: "blob"
    });

    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", name); //or any other extension
    document.body.appendChild(link);
    link.click();
  } catch (error) {
    Notification.error({
      title: "Error!",
      message: error.message,
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  }
}

export async function downloadInteractionDoc({ commit, state }, {id, name}) {
  try {
    var response = await $http.get(`/api/interaccion/${id}/archivos/descargar`, {
      responseType: "blob"
    });

    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", name); //or any other extension
    document.body.appendChild(link);
    link.click();
  } catch (error) {
    Notification.error({
      title: "Error!",
      message: error.message,
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  }
}