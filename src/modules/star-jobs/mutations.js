import {
    defaultJob
} from "./state";

export function setUsers(state, users) {
    state.users = users
    state.users.map(el => {
        el.src = state.arrayAvatars[
            Math.floor(Math.random() * state.arrayAvatars.length)
        ]
    })
}
export function setUsersNames(state, userName) {
    userName.map(el => {
        state.userName.push(el.username)
    })
    state.userNamesId = userName;
}

export function setModalAttachment(state, show) {
  state.modalAttachment.show = show
}

export function setJob(state, job){
    state.job.user_creation_id = job.user_creation_id
    state.job.name = job.name
    state.job.description = job.description
    state.job.closing_date = job.closing_date
    state.job.limit_date = job.limit_date

    state.job.interactions = job.interactions
    state.job.members = job.members

    state.job.members.map(el => {
        el.src = state.users.find(elu => elu.id == el.user_id).src
    })

    state.job.interactions.map(el => {
        el.src = state.users.find(elu => elu.id == el.user_id).src
    })

    state.newmembers = []
    job.members.map(el => {
        state.newmembers.push({
            user_id : el.user_id,
            user_inviting_id : el.user_inviting_id,
        })
    })

    state.files = job.files
}
export function setDefaultJob(state, job){
    state.job = defaultJob()
}
export function setModalUrl(state, url){
    state.modal.url = url
}