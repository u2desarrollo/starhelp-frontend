export const defaultJob = () => {
    return {
        user_creation_id:null,
        name:null,
        description:null,
        closing_date:null,
        limit_date:null,

        interactions:[],
        interactions_docs:[],
        members:[],
        files:[],
    }
}

export const defaultInteraction = () =>{
    return {
        description:null,
        files:[],
    }
}

export default {
    job:defaultJob(),
    interaction:defaultInteraction(),
    users:[],
    users_params:{
        paginate: true,
        page: 1,
        perPage: 200,
        filter: '',
        sortBy: 'name',
        sort: 'ASC'
    },
    modalAttachment:{
      show: false
    },
    userName:[],
    userNamesId:[],
    newmembers:[],
    files:[],
    loading:{
        form:false,
        addInteraction:false,
        starJob:true
    },
    arrayAvatars: [
        "https://www.assyst.de/cms/upload/sub/digitalisierung/15-M.jpg",
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTcJ09qqoI5APeSsXNPJhTivcDWO2nILEBiGSXmST-P9V1MHdrJjB-8uXHPHEypUtfn7Ok&usqp=CAU",
        "https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png",
        "https://storage.needpix.com/rsynced_images/profile-1719207_1280.jpg",
        "https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-3.png"
    ],
    modal:{
        type:'',
        name:'',
        url:'',
        jobs_interactions_docs_id:null,
        jobs_docs_id:null,
        jobs_interactions_docs_name:'',
        jobs_docs_name:'',
    }
}
