import { defaultSelectedProjects } from "./state";

export function setProjects(state, projects) {
  state.projects = projects;
}
export function setProjectsTable(state, projects) {
  state.projectsTable.items = projects.data;

  delete projects.data;

  state.projectsTable.pagination = projects;
}

export function setAction(state, action) {
  state.action = action;
}

export function setProjectsTypes(state, data) {
  state.projectsTypes = data;
}

export function setErrors(state, data) {
  state.errorForm = data;
}

export function resetSelectedProjects(state) {
  state.selectedProjects = defaultSelectedProjects();
}

export function setSelectedProjects(state, data) {
  state.selectedProjects = data;
}
