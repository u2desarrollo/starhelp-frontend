import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";

//Obtener los marcas
export async function fetchProjects(
  { commit, state },
  requestParameters = {
    params: state.paramsTable
  }
) {
  await $http
    .get("/api/proyectos", requestParameters)
    .then(response => {
      if (requestParameters.params.paginate) {
        commit("setProjectsTable", response.data.data);
      } else {
        commit("setProjects", response.data.data);
      }
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function findProjectsById({ commit }, id) {
  await $http
    .get(`/api/proyectos/${id}`)
    .then(response => {
      commit("setSelectedProjects", response.data.data);
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function deleteProjects({ commit, dispatch }, id) {
  await $http
    .delete(`/api/proyectos/${id}`)
    .then(response => {
      dispatch("fetchProjects");
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

// crear
export async function createProjects({ commit, state }) {
  await $http
    .post("/api/proyectos", state.selectedProjects)
    .then(response => {
      commit("setSelectedProjects", response.data.data);
      commit("setErrors", "");
      Notification.success({
        title: "Exito!",
        message: "Se ha Creado el Negocio",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      commit("setErrors", "Error al crear el negocio.");
    });
}

// actualizar
export async function updateProjects({ commit, state }) {
  await $http
    .put(`/api/proyectos/${state.selectedProjects.id}`, state.selectedProjects)
    .then(response => {
      commit("setSelectedProjects", response.data.data);
      commit("setErrors", "");
      Notification.success({
        title: "Exito!",
        message: "Se ha Actualizado el Negocio",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      commit("setErrors", "Error al actualizar el negocio.");
    });
}

export async function fetchProjectsTypes({
  dispatch,
  state,
  rootState,
  commit
}) {
  await dispatch(
    "parameters/listParameter",
    { params: { idParamTable: 9, paginate: false } },
    { root: true }
  ); //TODO: Determinar el idParamTable
  commit("setProjectsTypes", mapParameters(rootState.parameters.parameters));
}

function mapParameters(parameters) {
  return parameters.map(p => {
    return { value: p.id, label: p.name_parameter, code: p.code_parameter };
  });
}
