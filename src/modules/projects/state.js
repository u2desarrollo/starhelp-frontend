export function defaultSelectedProjects() {
  return {
    name: "",
    description: "",
    observation: "",
    code: "",
    contact_id: "",
    type_id: "",
    status: ""
  };
}

export default {
  projects: [],
  projectsTypes: [
    {
      value:1,
      code:'01',
      label:'Importación'
    },
    {
      value:2,
      code:'05',
      label:'Otros'
    }
  ],
  errorForm: "",
  projectsStatus: [
    {
      value: 1,
      label: "En Proceso"
    },
    {
      value: 8,
      label: "Suspendida"
    },
    {
      value: 9,
      label: "Cerrada"
    }
  ],
  action: "create",
  paramsTable: {
    paginate: true,
    page: 1,
    perPage: 15,
    filter: ""
  },
  statusProjects: {
    "1": { name: "Activo" },
    "8": { name: "Suspendido" },
    "9": { name: "Cerrado Obligatorio" }
  },
  selectedProjects: defaultSelectedProjects(),
  projectsTable: {
    items: [],
    columns: [
      {
        name: "Código",
        field: "code",
        format: "text"
      },
      {
        name: "Descripción",
        field: "description",
        format: "text"
      },
      {
        name: "Tipo Negocio",
        field: "type",
        format: "text"
      },
      {
        name: "Estado",
        field: "status",
        format: "text"
      },
      {
        name: "Opciones",
        field: "options",
        format: "text"
      }
    ],
    pagination: {},
    params: {
      paginate: true,
      page: 1,
      perPage: 15,
      filter: ""
    }
  }
};
