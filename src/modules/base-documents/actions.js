import $http from "../../axios";
import { Notification } from "element-ui";

/** Obtiene el modelo */
export async function fetchModel({ commit, state }, model) {

  if (+state.model.id !== +model) {
    await $http
      .get(`/api/obtener-modelo/${model}`)
      .then(response => {
        commit("setModel", response.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "¡Error!",
          message: error.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        commit("errorMessage", error.message);
      });
  }
}

/** Obtiene los documentos base */
export async function fetchBaseDocuments(
  { commit, state, dispatch },
  voucher_type
) {
  commit("setLoadingBaseDocuments", true);
  let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice);

  if (state.params.state_auth.find(sa => sa == 0) !== undefined) {
    state.params.state_auth = [0];
    state.params.state_backorder = [];
  }

  await $http
    .get("/api/documentos-base", {
      params: {
        branchoffice_id: selected_branchoffice,
        voucher_type: voucher_type,
        filter: state.params.filter,
        fromDate:
          state.params.filterDate != null ? state.params.filterDate[0] : "",
        toDate:
          state.params.filterDate != null ? state.params.filterDate[1] : "",
        model_id: state.model.id,
        closed: state.params.closed,
        state_wallet: state.params.state_auth,
        state_backorder: state.params.state_backorder,
        seller_id: state.params.seller_id
      }
    })
    .then(response => {
      commit("setLoadingBaseDocuments", false);
      commit("setBaseDocuments", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setLoadingBaseDocuments", false);
      commit("errorMessage", error.message);
    });
}

/** Obtiene las sedes */
export async function fetchBranchoffices({
  commit,
  state,
  rootState,
  dispatch
}) {
  if (!state.branchoffices.length) {
    await dispatch(
      "branchOffices/fetchBranchOffices",
      {
        params: { paginate: false }
      },
      { root: true }
    );
    commit("setBranchoffices", rootState.branchOffices.branchOffices);
  }
}

/** Obtiene las bodegas */
export async function fetchBranchofficeWarehouses({
  commit,
  state,
  rootState,
  dispatch
}) {
  if (!state.warehouses.length && state.branchoffices.id) {
    await dispatch(
      "branchOfficeWarehouses/fetchBranchOfficeWarehouses",
      {
        params: {
          paginate: false,
          branchoffice_id: state.branchoffices.id
        }
      },
      { root: true }
    );
    commit(
      "setWarehouses",
      rootState.branchOfficeWarehouses.branchoffice_warehouses
    );
  }
}

/** Obtiene los estados de los documentos */
export async function fetchStatus({ commit, state, rootState, dispatch }) {
  if (!state.status.length) {
    $http.get("/api/estados-de-documentos").then(response => {
      commit("setStatus", response.data.data);
    });
  }
}

/** Filtrar por búsqueda */
export function filterSearch({ commit, dispatch }, filter) {
  commit("setFilterSearch", filter.target.value);
  dispatch("fetchBaseDocuments");
}

/**
 * Esta funcion se encarga de
 * cambiar el estado de "cerrado" al documento
 *
 * @author Kevin Galindo
 */
export async function closeDocument({ dispatch, state }, data) {
  const { id } = data;
  const voucher_type_id = state.model.voucher_type_id
    ? state.model.voucher_type_id
    : state.model.voucherType_id;

  if (id) {
    await $http
      .post(`/api/documentos/cerrar/${id}`, data)
      .then(response => {
        Notification.success({
          title: "Exito!",
          message: "Registro actualizado.",
          type: "success",
          duration: 1500,
          customClass: "notification-box"
        });
        dispatch("fetchBaseDocuments", voucher_type_id);
      })
      .catch(error => {
        console.error(error);

        Notification.error({
          title: "¡Error!",
          message: "Error al actualizar",
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
      });
  }
}

// Traer los pedidos retenidos
export async function fetchListOrderReject({ commit, state }, status) {
  let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice);

  let params = {
    params: {
      paginate: false,
      search: state.params.filter,
      fromDate: state.params.filterDate[0],
      toDate: state.params.filterDate[1],
      motiveOrder: "TODO",
      branchoffice_id: selected_branchoffice,
      model_id: 1
    }
  };

  commit("setLoadingBaseDocuments", true);

  await $http
    .get(`/api/lista-pedidos/${status}`, params)
    .then(response => {
      commit("setBaseDocuments", response.data);
      // commit("setListOrdersReject", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.response.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });

  commit("setLoadingBaseDocuments", false);
}

export async function getDocumentByAuthorize({ commit, state }) {
  const user = JSON.parse(localStorage.user);

  let params = {
    params: {
      user_id: user.id
    }
  };

  commit("setLoadingBaseDocuments", true);

  await $http
    .get(`/api/documentos-autorizar`, params)
    .then(response => {
      commit("setDocumentsByAuthorize", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.response.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });

  commit("setLoadingBaseDocuments", false);
}

export async function downloadDocumentsMissingBackOrder({ commit, state }) {
  let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice);
  let params = {
    params: {
      branchoffice_id: selected_branchoffice,
      model_id: state.model.id
    }
  };
  await $http
    .get(`/api/documentos-backorder-faltante`, params)
    .then(response => {
      let objArray = response.data.data;
      let objArrayDetail = response.data.dataDetail;
      let nameDoc = response.data.name_excel;

      const array =
        typeof objArray !== "object" ? JSON.parse(objArray) : objArray;
      const arrayDetail =
        typeof objArrayDetail !== "object"
          ? JSON.parse(objArrayDetail)
          : objArrayDetail;

      //Header
      let str =
        `${Object.values(response.data.header)
          .map(value => `"${value}"`)
          .join(";")}` + "\r\n";

      let dataCsv = array.reduce((str, next) => {
        next.DESCRIP_PROD = next.DESCRIP_PROD.replace('#', '')
        str +=
          `${Object.values(next)
            .map(value => `"${value}"`)
            .join(";")}` + "\r\n";
        return str;
      }, str);

      // Detail
      //--Header
      let str2 =
        `${Object.values(response.data.headerDetail)
          .map(value => `"${value}"`)
          .join(";")}` + "\r\n";

      //--detailData
      let dataCsvDetail = arrayDetail.reduce((str2, next) => {
        next.DESCRIP_PROD = next.DESCRIP_PROD.replace('#', '')
        str2 +=
          `${Object.values(next)
            .map(value => `"${value}"`)
            .join(";")}` + "\r\n";
        return str2;
      }, str2);

      dataCsv += "\n\n";
      dataCsv += dataCsvDetail;

      var hiddenElement = document.createElement("a");
      hiddenElement.href = "data:text/csv;charset=utf-8," + encodeURI(dataCsv);
      hiddenElement.target = "_blank";
      hiddenElement.download = "BackOrderFaltante.csv";
      hiddenElement.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al Descargar la informacion.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 *
 * @returns {Promise<void>}
 */
export async function generateReportAuthorizedDocuments(
  { state, commit },
  dates
) {
  let url = "/api/reportes/autorizacion-documentos?";
  url += "dates[]=" + dates[0];
  url += "&dates[]=" + dates[1];

  commit("setLoadingAuthorizedDocuments", true);
  await $http({
    url: url,
    method: "GET",
    responseType: "blob"
  })
    .then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute(
        "download",
        "Reporte de Autorización de documentos.xlsx"
      );
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message:
          "No fue posible generar el reporte con las fechas seleccionadas",
        type: "error",
        duration: 5000,
        customClass: "notification-box"
      });
    });
  commit("setLoadingAuthorizedDocuments", false);
}
