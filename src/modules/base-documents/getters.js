export function getParams (state) {
	return state.params
}

export function getSelectedRows (state) {
  return state.selected_rows
}
