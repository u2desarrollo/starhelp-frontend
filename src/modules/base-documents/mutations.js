/** Asignar valor a model */
export function setModel(state, model) {
  state.model = model;
}

/** Asignar valor a baseDocuments */
export function setBaseDocuments(state, baseDocuments) {
  if (state.model.select_validate_wms) {
    baseDocuments.map(d => {
      d.disable_select = !d.documents_products.some(dp => +dp.quantity_wms > 0)
    })
  }

  state.baseDocuments = baseDocuments;
}

export function resetBaseDocuments(state) {
  state.baseDocuments = [];
}

/** Agrega un mensaje en caso de error*/
export function errorMessage(state, payload) {
  state.error = true;
  state.errorMessage = payload;
}

/** Asignar valor a branchoffices */
export function setBranchoffices(state, branchoffices) {
  state.branchoffices = branchoffices.map(p => {
    return { value: p.id, label: p.name };
  });
}

/** Asignar valor a warehouses */
export function setWarehouses(state, warehouses) {
  state.warehouses = warehouses.map(p => {
    return { value: p.id, label: p.warehouse_description };
  });
}

/** Asignar valor a status */
export function setStatus(state, status) {
  state.status = status.map(p => {
    return { value: p.id, label: p.name_parameter };
  });
}

/** Filtrar por búsqueda */
export function setFilterSearch(state, filter) {
  state.params.page = 1;
  state.params.filter = filter;
}
export function setLoadingBaseDocuments(state, data) {
  state.loadingBaseDocuments = data;
}

export function setDocumentsByAuthorize(state, data) {
  state.documentsByAuthorize = data;
}

export function setDateRangeParam(state) {
  // Seteamos la fecha
  var dt = new Date();

  // Fecha Actual
  state.params.filterDate[1] = dt.toISOString().substring(0, 10);

  //Hace 30 Dias
  dt.setDate(dt.getDate() - 30);
  state.params.filterDate[0] = dt.toISOString().substring(0, 10);
}

export function setLoadingAuthorizedDocuments(state, loadingAuthorizedDocuments){
  state.loadingAuthorizedDocuments = loadingAuthorizedDocuments;
}

export function setSelectedRows(state, selected_rows){
  state.selected_rows = selected_rows;
}
