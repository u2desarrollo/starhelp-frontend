export default {
  baseDocuments: [],
  branchoffices: [],
  warehouses: [],
  status: [],
  model: {
    operation_type: {
      code: null
    },
    voucher_type: {
      code_voucher_type: null
    }
  },
  params: {
    paginate: false,
    page: 1,
    perPage: 15,
    filter: "",
    filterDate: [],
    sortBy: "voucher_date",
    sort: "DESC",
    closed: false,
    retained: false,
    rejected: false,
    seller_id: null,
    state_auth: ["03", "05"],
    state_backorder: ["1", "2"]
  },
  loadingBaseDocuments: false,
  error: false,
  errorMessage: "",
  documentsByAuthorize: [],
  loadingAuthorizedDocuments: false,
  selected_rows: []
};
