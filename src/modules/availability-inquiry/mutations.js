export function setStateProducts(state, products) {
  state.products = products;
}
export function setbranchoffice_warehouse(state, branchoffice_warehouse) {
  state.params.branchoffice_warehouse_id = branchoffice_warehouse;
}
export function setAvailability(state, availability) {
  state.availability = availability;
}
export function setavAilability_totals(state, availability_totals) {
  state.availability_totals = availability_totals;
}
export function setStateProductspagination(state, productspagination) {
  state.productspagination = productspagination;
}
export function setJustify_availability(state, justify_availability) {
  state.justify_availability = justify_availability;
}
export function setbranchoffice_warehouse_id(state, warehouse_id) {
  state.params.branchoffice_warehouse_id = warehouse_id;
}

export function setWarehouseId(state, warehouse_id) {
  state.params.warehouse_id = warehouse_id;
}
export function setTitleBranchoffice(state, data) {
  state.titleBranchoffice = data[0].branchoffice;
}
