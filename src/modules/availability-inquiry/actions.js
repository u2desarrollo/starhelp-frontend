import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";

export async function searchProducts({ state, commit }, model_id = null) {
  let requestParameters = {
    params: state.params
  };

  requestParameters.params.branchoffice_id = localStorage.selected_branchoffice;

  if (model_id !== null) {
    requestParameters.params.model_id = model_id;
  }

  await $http.get("/api/lista-productos", requestParameters).then(response => {
    commit("setStateProductspagination", response.data.data);
    commit("setStateProducts", response.data.data.data);
    commit("setTitleBranchoffice", response.data.data.data);
  });
}

export async function justifyAvailability({ state, commit }, parameters) {

  let params = {
    branchoffice_id: parameters.branchoffice_id,
    voucher_type: 4,
    filter: "",
    model_id: 1,
    product_id: parameters.product_id,
    closed: false
  };

  await $http
    .get("/api/documentos-diponibilidad", { params })
    .then(response => {
      commit("setJustify_availability", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function availability({ state, commit }, product_id) {
  await $http
    .get(`/api/diponibilidad-sedes/${product_id}`)
    .then(response => {
      commit("setavAilability_totals", response.data[0]);
      commit("setAvailability", response.data[1]);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function updatelocation({ state }) {
  // let selected_branchoffice_warehouse = JSON.parse(localStorage.selected_branchoffice_warehouse)

  var params = state.params_location;
  try {
    var response = await $http.put(`/api/actualizar-localizacion`, params);
    if (response.data.status) {
      Notification.success({
        title: "Exito!",
        message: response.data.message,
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    } else {
      Notification.error({
        title: "Error!",
        message: response.data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    }
  } catch (error) {
    Notification.error({
      title: "Error!",
      message: "Error al actualizar",
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  }
}
