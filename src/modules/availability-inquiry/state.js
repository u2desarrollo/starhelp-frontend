export default {
  params: {
    paginate: false,
    product: "",
    branchoffice_warehouse_id: "",
    branchoffice: null,
    filter: "",
    page: 1,
    perPage: 15
  },
  titleBranchoffice: "",
  products: [],
  productspagination: {
    from: null,
    to: null,
    total: null
  },
  availability: [],
  availability_totals: [],
  justify_availability: [],
  params_location: {
    product: null,
    branchoffice_warehouse: null,
    location: null,
    warehouse_id: null
  }
};
