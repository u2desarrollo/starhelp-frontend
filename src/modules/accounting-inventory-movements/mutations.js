export function setLoadingAccounting(state, value) {
  state.loadingAccounting = value;
}
export function setDetailAccounting(state, data) {
  state.detailAccounting = data;
}
export function setAccountingCountDocuemnts(state, value) {
  state.accountingCountDocuemnts = value.documents;
}
