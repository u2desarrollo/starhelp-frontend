import $http from "../../axios";
import { Notification } from "element-ui";

export async function accountingInventoryOperations({ commit, state }) {
  return new Promise(async (resolve, reject) => {
    commit("setLoadingAccounting", true);
    $http
      .post("/api/contabilizacion-documento", state.paramsAccounting)
      .then(response => {
        commit("setDetailAccounting", response.data);
        commit("setLoadingAccounting", false);

        resolve(response.data);
      })
      .catch(error => {
        commit("setLoadingAccounting", false);

        reject(error);
      });
  });
}
export async function accountingInventoryOperationsCountDocuments({
  commit,
  state
}) {
  if (state.paramsAccounting.filterDate.length !== 0) {
    // commit("setLoadingAccounting", true);

    $http
      .post("/api/contabilizacion-documento-cantidad", state.paramsAccounting)
      .then(response => {
        commit("setAccountingCountDocuemnts", response.data);
        // commit("setLoadingAccounting", false);
      })
      .catch(error => {
        // commit("setLoadingAccounting", false);

        Notification.error({
          title: "¡Error!",
          message: "Error al buscar documentos.",
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
      });
  }
}
