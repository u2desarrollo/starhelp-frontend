import $http from '../../axios';
import { Notification } from "element-ui";

export async  function getSellers({state,commit}) {
    await $http.get('/api/lista-vendedores').then(response=>{
        commit('setSellers',response.data.data);
    }).catch(error=>{
        Notification.error({
            title: "Error!",
            message: error.message,
            type: "error",
            duration: 3000,
            customClass: "notification-box"
          });
    }
    );
}


export async function massReassignSeller({state,commit}) {
    await $http.get('/api/reasignacion-vendedor', {params:state.sellerParam}).then(response=>{
        Notification.success({
            title: "success!",
            message: 'La Reasignación de vendedor se ha realizado exitosamente',
            type: "success",
            duration: 3000,
            customClass: "notification-box"
          });
 
    }).catch(error=>{
        Notification.error({
            title: "Error!",
            message: error.message,
            type: "error",
            duration: 3000,
            customClass: "notification-box"
          });

    });
    
}