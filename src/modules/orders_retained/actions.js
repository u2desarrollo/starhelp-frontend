import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";

//Obtener todos los registros de la ficha de datos.
export async function fetchListOrderReject({ commit, state, dispatch }) {
  let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice);

  let model_id = await dispatch("cart/getModelBycode", '205', {root: true});

  let params = {
    params: {
      filter: state.tableOrderReject.options.search,
      fromDate: state.tableOrderReject.options.fromDate[0],
      toDate: state.tableOrderReject.options.fromDate[1],
      motive_rejected: state.tableOrderReject.options.typeMotive,
      branchoffice_id: state.tableOrderReject.options.sede,
      state_wallet: [state.tableOrderReject.options.state_auth],
      state_backorder: ["1", "2"],
      detained: true,
      model_id: model_id,
      myActivity: state.tableOrderReject.options.myActivity,
      dateFromMyActivity: state.tableOrderReject.options.dateFromMyActivity
    }
  };

  // En caso de que la sede este vacia se agrega una por defecto.
  // if (!params.params.branchoffice_id) {
  //   params.params.branchoffice_id = selected_branchoffice;
  //   state.tableOrderReject.options.sede = selected_branchoffice;
  // }

  state.tableOrderReject.loading = true;

  await $http
    .get(`/api/documentos-retenidos`, params)
    .then(response => {
      commit("setListOrdersReject", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.response.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });

  state.tableOrderReject.loading = false;
}

export async function changeStatusOrder({ dispatch, commit, state }) {
  var user = JSON.parse(localStorage.user);
  state.dataApproved.user_id_authorizes = user.id;
  // state.tableOrderReject.loading = true;

  await $http
    .put(`/api/pedido/actualizar-estado`, state.dataApproved)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: response.data.message,
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });

      commit(
        "removeDocumentListOrdersRejectById",
        state.dataApproved.document_id
      );
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "Error!",
        message: error.response.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
  // await dispatch("fetchListOrderReject");

  // state.tableOrderReject.loading = false;
}

export async function retainOrderById({ dispatch, commit, state }, id) {
  var user = JSON.parse(localStorage.user);
  state.dataApproved.user_id_authorizes = user.id;
  state.dataApproved.status_id_authorized = 914;

  await $http
    .put(`/api/pedido/${id}/retener`, state.dataApproved)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: response.data.message,
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.response.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
  await dispatch("fetchListOrderReject");
}
