export function setListOrdersReject(state, data) {
  // Agregamos los datos de la tabla
  state.tableOrderReject.data = data;

  // Asignamos la opcion isActive
  if (state.tableOrderReject.data.length > 0) {
    state.tableOrderReject.data.map(tor => {
      tor.isSelected = false;
    });
  }

  // Quitamos los valores de la tabla del arreglo
  //delete data.data;

  // Agregamos los datos de la paginacion
  //state.tableOrderReject.pagination = data;
}

export function removeDocumentListOrdersRejectById(state, document_id) {
  if (state.tableOrderReject.data.length > 0) {
    let keyDoc = state.tableOrderReject.data.findIndex(
      tor => tor.id == document_id
    );

    if (keyDoc >= 0) {
      state.tableOrderReject.data.splice(keyDoc, 1);
    }
  }
}

export function resetDataApproved(state) {
  state.dataApproved = {
    document_id: "",
    observation_authorized: "",
    max_value_authorized: "",
    status_id_authorized: "",
    user_id_authorizes: ""
  };
}
