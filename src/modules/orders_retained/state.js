const tableOrderReject = {
  loading: false,
  data: [],
  fieldsMyActivity: [
    {
      key: "selected",
      label: ""
    },
    {
      key: "detail",
      label: ""
    },
    {
      key: "document_date",
      label: "Fecha",
      thStyle: {
        "min-width": "100px !important"
      }
    },
    {
      key: "consecutive",
      label: "Orden de Venta",
      thStyle: {
        "min-width": "30px !important"
      }
    },
    {
      key: "total_value",
      label: "Valor Total",
      thStyle: {
        "min-width": "30px !important"
      }
    },
    {
      key: "available_quota",
      label: "Cupo Disponible",
      thStyle: {
        "min-width": "30px !important"
      }
    },
    {
      key: "warehouse.description",
      label: "Cliente",
      thStyle: {
        "min-width": "150px !important"
      }
    },
    {
      key: "warehouse.branchoffice.name",
      label: "Sede Cliente",
      thStyle: {
        "min-width": "150px !important"
      }
    },
    {
      key: "user_authorizes_wallet.name",
      label: "Usuario",
      thStyle: {
        "min-width": "100px !important"
      }
    },
    {
      key: "status_authorized",
      label: "Estado",
      thStyle: {
        "min-width": "200px !important"
      }
    },
    {
      key: "date_authorized",
      label: "Fecha Estado",
      thStyle: {
        "min-width": "200px !important"
      }
    },
    {
      key: "observation_authorized",
      label: "Observacion Estado",
      thStyle: {
        "min-width": "200px !important"
      }
    },
    {
      key: "actions",
      label: "Opciones",
      thStyle: {
        "min-width": "100px !important"
      }
    }
  ],
  fields: [
    {
      key: "selected",
      label: ""
    },
    {
      key: "detail",
      label: ""
    },
    {
      key: "document_date",
      label: "Fecha",
      thStyle: {
        "min-width": "100px !important"
      }
    },
    {
      key: "consecutive",
      label: "Ord Venta",
      thStyle: {
        "min-width": "30px !important"
      }
    },
    {
      key: "total_value",
      label: "Vr-Total",
      thStyle: {
        "min-width": "55px !important"
      }
    },
    {
      key: "backorder_value",
      label: "Vr-Pendiente",
      sortable: true,
      thStyle: {
        "min-width": "55px !important"
      }
    },
    {
      key: "available_quota",
      label: "Cupo Disponible",
      thStyle: {
        "min-width": "30px !important"
      }
    },
    {
      key: "motive_retained",
      label: "Motivo",
      thStyle: {
        "min-width": "200px !important"
      }
    },
    {
      key: "warehouse.description",
      label: "Cliente",
      thStyle: {
        "min-width": "150px !important"
      }
    },
    {
      key: "warehouse.branchoffice.name",
      label: "Sede Cliente",
      thStyle: {
        "min-width": "150px !important"
      }
    },
    {
      key: "seller.name",
      label: "Vendedor",
      thStyle: {
        "min-width": "90px !important"
      }
    },
    {
      key: "actions",
      label: "Opciones",
      thStyle: {
        "min-width": "100px !important"
      }
    }
  ],
  errors: {
    has: false,
    message: "",
    code: 0,
    data: []
  },
  pagination: [],
  options: {
    perPage: 30,
    search: "",
    currentPage: 1,
    fromDate: ["", ""],
    typeMotive: "",
    statusOrder: "",
    sede: null,
    myActivity: false,
    dateFromMyActivity: "",
    state_auth: "07"
  }
};

const dataApproved = {
  document_id: "",
  observation_authorized: "",
  max_value_authorized: "",
  status_id_authorized: "",
  user_id_authorizes: ""
};
export default {
  orderReject: [],
  //ERROR STATE
  error: false,
  errorMessage: "",
  // Tabla
  tableOrderReject,
  dataApproved
};
