import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";

export async function getDocument({commit},id) {
  await  $http.get(`/api/detalle-documento/${id}`).then(response=>{
    commit('setDocument', response.data);

  }).catch(error=>{
    Notification.error({
        title: 'Error!',
        message: error.message,
        type: 'error',
        duration: 2000,
        customClass: 'notification-box',
    });
})
}

export async function  getPdf({state,commit}) {
  state.disabled=true;
  var response = await $http.get(`/api/documentos/descargar-pdf/${state.document[0].id}`,
    {
     responseType: "blob"
    }
    )

    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", `Factura ${state.document[0].prefix}-${state.document[0].consecutive}.pdf`); //or any other extension
    document.body.appendChild(link);
    link.click();
    state.disabled=false;
}
