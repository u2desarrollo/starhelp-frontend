export function setOrders (state, orders) {
	state.orders = orders
}

export function setFilterDate(state, date) {
	state.params.filter_date = date
}

export function setFilterStatus(state, status) {
	state.params.filter_status = status
}

export function setFilter (state, filter) {
	state.params.page = 1
	state.params.filter = filter
}

export function setStates (state, states) {
  state.states = states
}

export function setPerPage(state, perPage) {
	state.params.page = 1
	state.params.perPage = perPage
}

export function setPage (state, page) {
	state.params.page = page
}

export function setSortBy (state, sortBy) {
	state.params.sortBy = sortBy
}

export function setSort (state, sort) {
	state.params.sort = sort
}

export function setSellers (state, sellers) {
	state.sellers = sellers
}

export function setPreloader (state, preloader) {
    state.preloader = preloader
}

export function setSellerId (state, seller_id) {
  state.params.seller_id = seller_id
}
