export default {
  orders: {
    data: []
  },
  error: false,
  errorMessage: '',
  params: {
    filter_date: [],
    filter_status: '',
    paginate: true,
    page: 1,
    perPage: 15,
    sortBy: 'total_order_value',
    sort: 'DESC',
    seller_id: null
  },
  sellers: [],
  preloader: false,
  states: []
}
