import $http from '../../axios'
import {Notification} from "element-ui";

export async function fetchOrders ({ commit, state }, requestParameters = { params: state.params }) {
    commit('setPreloader', true)
	await $http.get('/api/pedidos-vendedor', requestParameters).then((response) => {
		if (Object.keys(response.data) != '') {
			commit('setOrders', response.data.data)
		} else {
			commit('setOrders', '')
		}
	}).catch((error) => {
		Notification.error({
			title: '¡Error!',
			message: error.message,
			type: 'error',
			duration: 1500,
			customClass: 'notification-box'
		})
		commit('error', error.message)
	})
    commit('setPreloader', false)
}

export function setFilterPagination ({ commit, dispatch }, filter) {
	commit('setFilter', filter.target.value)
	dispatch('fetchOrders')
}

export function setPerPagePagination({commit, dispatch}, perPage) {
	commit('setPerPage', parseInt(perPage.target.value))
	dispatch('fetchOrders')
}

export function setPagePagination({ commit, dispatch }, page) {
	commit('setPage', parseInt(page))
	dispatch('fetchOrders')
}

export function setSortByPagination({ commit }, sortBy) {
	commit('setSortBy', sortBy)
}

export function setSortPagination({ commit }, sort) {
	commit('setSort', sort)
}

export async function getSellers({commit, state}, text) {
  let params = { filter:text}
  await $http.get('/api/lista-vendedores', {params} )
    .then((response) => {
      commit('setSellers', response.data.data)
    })
    .catch((error) => {
      Notification.error({
        title: 'Error!',
        message: error.message,
        type: 'error',
        duration: 1500,
        customClass: 'notification-box',
      });
    })
}

//Obtener los parametros de una tabla
export async function getStates({commit, state}) {
  await $http.get(`/api/parametros?paramtable_id=64&paginate=false`)
    .then((response) => {
      commit('setStates', response.data.data)
    })
    .catch((error) => {
      Notification.error({
        title: 'Error!',
        message: error.message,
        type: 'error',
        duration: 2000,
        customClass: 'notification-box',
      });
    })
}

export async function downloadDocument({}, documentToPdf) {

  let name = documentToPdf.vouchertype.name_voucher_type + ' No. ' + documentToPdf.consecutive + '.pdf'

  let url = ''

  if (documentToPdf.operationType_id == 4){
    url ='/api/traslado/descargar-pdf/' + documentToPdf.id
  }else if (documentToPdf.operationType_id == 3){
    url = '/api/traslado/descargar-pdf/' + documentToPdf.thread
  }else{
    url = '/api/documentos/descargar-pdf/' + documentToPdf.id
  }

  await $http({
    url: url,
    method: 'GET',
    responseType: 'blob',
  }).then((response) => {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', name);
    document.body.appendChild(link);
    link.click();
  }).catch((error) => {
    Notification.error({
      title: 'Error!',
      message: error.message,
      type: 'error',
      duration: 3000,
      customClass: 'notification-box',
    });

  })
}
