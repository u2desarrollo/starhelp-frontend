import $http from "../../axios";
import {
    Notification,
    MessageBox
} from 'element-ui';



//Obtener los terceros
export async function fetchContacts({
    commit,
    state
}, requestParameters = {
    params: state.params_cotact
}) {
    await $http.get('/api/terceros', requestParameters)
        .then((response) => {
            commit('setContacts', response.data.data.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('contactError', error.message)
        })
}

// obtener Data libro Contable
export async function fetchMovementsAccountContact({commit, state}) {
    await $http.get('/api/movimientos-cuenta-tercero', { params : state.params})
        .then((response) => {
            console.log(response.data.data);
            commit('setauxiliary_book', response.data.data);
            state.loading.module = false;
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: 'Error',
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}


export async function getBalanceDocumentContactAccount({commit, state},account_client) {
    await $http.get('/api/documentos-balance-cuenta-usuario/'+account_client.contact_id+"/"+account_client.account_id, { params : state.params})
        .then((response) => {
            commit('setdocument_balance_account_contact', response.data);
            state.loading.module = false;
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: 'Error',
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}