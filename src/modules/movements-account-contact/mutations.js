

export function setAccountsFrom(state, accounts_from) {
    state.accounts_from = accounts_from
}

export function setContacts(state, contacts) {
    state.contacts = contacts;
}
export function setauxiliary_book(state, auxiliary_book) {
    state.auxiliary_book = auxiliary_book
}
export function setParams(state, params){
    state.params = params
}

export function setdocument_balance_account_contact(state, setdocument_balance_account_contact) {
    state.document_balance_account_contact = setdocument_balance_account_contact
}