import { defaultSelectedServiceRequest } from './state'

export function setSellers(state, sellers) {
  state.sellers = sellers;
}
export function resetSellers(state, sellers) {
  state.sellers = [];
}
export function setContacts(state, contacts) {
  state.contacts = contacts;
}

export function setContactsWarehouse(state, contactsWarehouse) {
  state.contactsWarehouses = contactsWarehouse;
}
export function setProducts(state, products) {
  state.products = products;
}

export function setTypes(state, types) {
  state.types = types;
}

/**
 * Este metodo se encarga de resetear el formulario de registro.
 *
 * @author Kevin Galindo
 */
export function resetFormRequestService(state, types) {
  state.params = {
    observation: null,
    type: null,
    contact: null,
    seller: null,
    product: null,
    user: null
  };
}

/**
 *
 * Este mutador se encarga de setear la informacion de la
 * tabla con su paginacion.
 *
 * @author Kevin Galindo
 */
export function setServiceRequests(state, data) {
  // Agregamos los datos de la tabla
  state.tableServiceRequests.data = data.data;

  // Quitamos los valores de la tabla del arreglo
  delete data.data;

  // Agregamos los datos de la paginacion
  state.tableServiceRequests.pagination = data;
}

/**
 *
 * Este mutador de setear el
 * registro seleccionado para poder editar
 * o ver informacion sobre el
 *
 * @author Kevin Galindo
 */
export function setSelectedServiceRequest(state, item) {
  // Agregamos el registro
  state.selectedServiceRequest.data = item;
}

/**
 *
 * Este mutador se encarga de organizar el
 * historico de los estados por registro
 *
 * @author Kevin Galindo
 */
export function setHistoryStateServiceRequest(state) {
  let actual_state = state.selectedServiceRequest.data.actual_state.state;
  let serviceRequest = state.selectedServiceRequest.data;
  let allStatesByType = serviceRequest.type.states;
  let completedStates = serviceRequest.status_history;
  let historyStates = [];
  let historyStatesReturn = [];

  // Validamos los estados que ya hayan sido finalizados
  allStatesByType.map((ast, astIndex) => {
    let stateExists = completedStates.find(cp => cp.state.id == ast.id);

    // Si existe el estado se agrega la informacion
    if (stateExists) {
      historyStates.push({
        id: stateExists.state.id,
        name: stateExists.state.name,
        date: stateExists.date,
        user: stateExists.user,
        observation: stateExists.observation,
        completed: true,
        service_requests_state_id: stateExists.state.service_requests_state_id,
        document: stateExists.document
      });

      return;
    }

    // Si no existe se agrega sin info
    historyStates.push({
      id: ast.id,
      name: ast.name,
      date: "",
      user: "",
      observation: "",
      completed: false,
      service_requests_state_id: ast.service_requests_state_id
    });
  });

  // Quitamos los estados
  historyStates.map((hs, indexhs) => {
    if (hs.completed) {
      historyStatesReturn.push(hs);
      return;
    }
    let existSubState = historyStates.find(
      hhs => actual_state.id == hhs.service_requests_state_id
    );

    if (existSubState) {
      if (actual_state.id == hs.service_requests_state_id) {
        historyStatesReturn.push(hs);
      }
    } else {
      if (
        actual_state.service_requests_state_id == hs.service_requests_state_id
      ) {
        historyStatesReturn.push(hs);
      }
    }
  });

  //Organizamos la informacion
  historyStatesReturn.sort(function(a, b) {
    let keyA = a.id;
    let keyB = b.id;

    // Compare the 2 dates
    if (keyA < keyB) return -1;
    if (keyA > keyB) return 1;
    return 0;
  });

  // Agregamos la informacion
  state.selectedServiceRequest.historyStates = historyStatesReturn;
}

/**
 *
 * este metodo se encarga de finalizar
 * la actualizacion de un documento
 *
 * @author Kevin Galindo
 */
export function finalizeSelectedServiceRequest(state) {
  state.selectedServiceRequest.observation = "";
}

/**
 *
 * este metodo se encarga cambiar el estado del
 * loader del modal
 *
 * @author Kevin Galindo
 */
export function setStateLoaderSelectedServiceRequest(state, value) {
  state.selectedServiceRequest.loader = value;
}

/**
 *
 * este metodo se encarga cambiar el estado del
 * loader de la tabla
 *
 * @author Kevin Galindo
 */
export function setStateLoadertableServiceRequests(state, value) {
  state.tableServiceRequests.loader = value;
}

export function setDocument(state, id) {
  state.document = document;
}

export function setPDf(state, pdf) {
  state.fileName = pdf;
  state.showModalPdf = true;
}

export function setShowModalPdf(state, showModalPdf) {
  state.showModalPdf = showModalPdf;
}
export function setServiceRequestsStates(state, serviceRequestsStates) {
  state.serviceRequestsStates = serviceRequestsStates;
}

export function setDocumentActualState(state, data) {
  let serviceRequest = state.selectedServiceRequest.data;

  const { actual_state, document } = data;

  let stateUpdate = serviceRequest.status_history.find(
    hs => hs.state.id === actual_state.state.id
  );

  if (stateUpdate) {
    stateUpdate.document = document;
  }
}

export function setDefaulrSelectedServiceRequest(state, data){
  Object.assign(state.selectedServiceRequest, defaultSelectedServiceRequest())

}

export function setdoccument_id(state, doccument_id) {
  state.doccument_id =doccument_id
}
