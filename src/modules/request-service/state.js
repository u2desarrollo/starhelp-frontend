const tableServiceRequests = {
  data: [],
  pagination: {},
  params: {
    observation: null,
    paginate: false,
    perPage: 100,
    page: 1,
    close: false,
    rejected: false,
    dateRange: [],
    service_requests_types_id: null,
    service_requests_state_id: null
  },
  loader: false
};

export const defaultSelectedServiceRequest = () => {
	return {
    document_id:'',
		data: null,
    historyStates: [],
    observation: "",
    stateSelectedUpdate: null,
    loader: false,
    product_id: null,
    products: []
	}
}

export default {
  transfer_ch_cr : null,
  transfer_ch_pro : null,
  serviceRequestsStates: [],
  tableServiceRequests,
  selectedServiceRequest:defaultSelectedServiceRequest(),
  sellers: [],
  paramsquery: {
    query: null
  },
  contacts: [],
  contactsWarehouses: [],
  products: [],
  params: {
    observation: null,
    type: null,
    contact: null,
    seller: null,
    product: null,
    user: null,
    lot: null,
    due_date: null,
    observation: null,
    unit_value: null,
    number_batteries: 0,
    contactsWarehouse: null
  },
  types: [],
  document: null,
  fileName: "",
  showModalPdf: false,
  paramsProduct: {
    code_line_filter: null
  }
};
