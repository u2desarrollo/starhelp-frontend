import $http from "../../axios";
import { Notification } from "element-ui";

export async function getServiceRequests({ state, commit }) {
  //Loader
  commit("setDefaulrSelectedServiceRequest")
  commit("setStateLoadertableServiceRequests", true);

  // Sede
  const branchoffice_id = localStorage.selected_branchoffice;

  // Parametros
  let params = {
    params: state.tableServiceRequests.params
  };

  params.params.branchoffice_id = branchoffice_id;

  // Peticion
  await $http
    .get("/api/solicitudes-de-servicio", params)
    .then(response => {
      commit("setServiceRequests", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al traer las solicitudes de servicio.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      console.error(error);
    });

  commit("setStateLoadertableServiceRequests", false);
}

export async function updateStateServiceRequest({ state, commit }) {
  state.transfer_ch_cr = state.selectedServiceRequest.data.unit_value;
  state.transfer_ch_pro  = state.selectedServiceRequest.product_id;
  if (
    state.selectedServiceRequest.stateSelectedUpdate.id == 5 ||
    state.selectedServiceRequest.stateSelectedUpdate.id == 10 ||
    state.selectedServiceRequest.stateSelectedUpdate.id == 11
  ) {
    //validar existencia
    let validation = true;
    await $http
      .get(
        `/api/disponibilidad-producto?product_id=${state.selectedServiceRequest.product_id}&branchoffice_id=${localStorage.selected_branchoffice}&servicerequeststate=${state.selectedServiceRequest.stateSelectedUpdate.id}`
      )
      .then(response => {
        if (response.data.stock == 0 ||  !response.data) {
          validation = false;
        }
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 2000,
          customClass: "notification-box"
        });
      });

    if (!validation) {
      Notification.error({
        title: "Error!",
        message: "No hay disponibilidad.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      return;
    }
  }

  // Validamos si se selecciono un estado
  if (state.selectedServiceRequest.stateSelectedUpdate == null) {
    Notification.error({
      title: "Error!",
      message: "Debe seleccionar un estado para continuar.",
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
    return;
  }

  // Loader
  commit("setStateLoaderSelectedServiceRequest", true);

  // Traemos el usuario logeado
  let user = JSON.parse(localStorage.user);

  // Armamos el objeto
  let setData = {
    service_requests_id: state.selectedServiceRequest.data.id,
    service_requests_state_id:
      state.selectedServiceRequest.stateSelectedUpdate.id,
    user_id: user.id,
    observation: state.selectedServiceRequest.observation,
    product_id: state.selectedServiceRequest.product_id,
    unit_value: null
  };

  if (state.selectedServiceRequest.products.length > 0) {
    setData.products = state.selectedServiceRequest.products;
  }

  if (state.selectedServiceRequest.stateSelectedUpdate.id == 11) {
    setData.unit_value = state.selectedServiceRequest.data.unit_value;
  }

  var haserror = false;

  //creacion documentos
  if (state.selectedServiceRequest.stateSelectedUpdate.model_id != null) {
    if (Number(state.selectedServiceRequest.stateSelectedUpdate.template.voucherType_id) == 13) {
      if (state.selectedServiceRequest.stateSelectedUpdate.id == 10) {
        var s_entrada = 282;
      }
      if (state.selectedServiceRequest.stateSelectedUpdate.id == 11) {
        //serviio tecnico
        var s_entrada = 282;
      }

      let contact_id = JSON.parse(localStorage.getItem('user'))

      let dataTransfer = {
        params: {
          servicerequest_id: Number(state.selectedServiceRequest.data.id),
          servicerequeststate:
            state.selectedServiceRequest.stateSelectedUpdate.id,
          pdf: true,
          voucherType_id: Number(state.selectedServiceRequest.stateSelectedUpdate.template.voucherType_id),
          sede_salida: localStorage.selected_branchoffice,
          //bodega_salida: 280,
          documentEntry: null,
          sede_entrada: localStorage.selected_branchoffice,
          //bodega_entrada: s_entrada,
          contact_id: {
            id: contact_id.id
          },
          observation: state.selectedServiceRequest.data.contact.identification + ' ' +state.selectedServiceRequest.data.contact.name + ' ' +state.selectedServiceRequest.data.type.name
        },
        product: {
          products_transfer: [
            {
              minimum_amount: "1",
              product: {
                brand: state.selectedServiceRequest.data.product.brand_id,
                code: state.selectedServiceRequest.data.product.code,
                description:
                  state.selectedServiceRequest.data.product.description,
                found: "",
                id: state.selectedServiceRequest.product_id,
                line: state.selectedServiceRequest.data.product.line_id,
                price:
                  state.selectedServiceRequest.data.product.price.price,
                quantity: 1,
                subline: null
              }
            }
          ]
        }
      };


      if (state.selectedServiceRequest.stateSelectedUpdate.id == 10) {
        dataTransfer.unit_value = state.transfer_ch_cr
        dataTransfer.product_id = state.transfer_ch_pro
      }
      await $http
      .post("/api/tets", dataTransfer)
      .then(response => {
        setData.document_id = response.data.data.id
        // commmit('setdoccument_id', response.data.data.id);
        commit("setPDf", response.data.data.file_name);
        Notification.success({
          title: "Hecho!",
          message: "Se Ha creado el traslado",
          type: "success",
          duration: 2000,
          customClass: "notification-box"
        });
      })
      .catch(error => {
        this.haserror = true;
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 2000,
          customClass: "notification-box"
        });
      });
    } else {
      let data = {
        request_service_id: Number(state.selectedServiceRequest.data.id),
        branchoffice_warehouse_id: Number(state.selectedServiceRequest.data.actual_state.state.branchoffice_warehouse_id),
        vouchertype_id: Number(state.selectedServiceRequest.stateSelectedUpdate.template.voucherType_id),
        model_id: Number(state.selectedServiceRequest.stateSelectedUpdate.model_id),
        contact_id: state.selectedServiceRequest.data.contact_id,
        user_id: JSON.parse(localStorage.user).id
      };

      if (state.selectedServiceRequest.stateSelectedUpdate.id == "42") {
        data.manage_inventory = false;
        data.show_values_pdf = false;
      }

      if (state.selectedServiceRequest.stateSelectedUpdate.id == "19") {
        data.show_values_pdf = false;
      }

      await $http
      .post("/api/solicitudes-de-servicio/crear-documento", data)
      .then(async response => {
        // console.log(response);
        setData.document_id = response.data.id
        // commmit('setdoccument_id', response.data.id);
        commit("setPDf", response.data.filename);
        await commit("setDocumentActualState", {
          document: response.data.data,
          actual_state: state.selectedServiceRequest.data.actual_state
        });
      })
      .catch(error => {
        this.haserror = true;
        Notification.error({
          title: "Error!",
          message: error.response.data.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        console.error(error);
      });


    }

  }

  // console.log(this.haserror);
  if (!this.haserror) {
    // Realizamos la peticion
    await $http
      .post("/api/solicitudes-de-servicio/actualizar-estado", setData)
      .then(async response => {
        let stateServiceRequest = response.data.data;
        await commit("setSelectedServiceRequest", stateServiceRequest);


        await commit("setHistoryStateServiceRequest");
        await commit("finalizeSelectedServiceRequest");
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: "Error al actualizar el estado de la solicitudes de servicio.",
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        console.error(error);
      });

    // Loader
  }
  commit("setStateLoaderSelectedServiceRequest", false);
  state.selectedServiceRequest.stateSelectedUpdate = null;
}

export async function pdf(documentid) {
}

export async function fetchContacts(
  { commit, state },
  requestParameters = { params: state.paramsquery }
) {
  await $http
    .get("/api/terceros", requestParameters)
    .then(response => {
      commit("setContacts", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchContactsWarehouse({ commit, state }, data) {
  let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice);

  data.branchoffice_id = selected_branchoffice;

  let params = {
    params: data
  };

  await $http
    .get("/api/establecimientos-vendedor/no-paginar", params)
    .then(response => {
      commit("setContactsWarehouse", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchTypes({ commit, state }) {
  await $http
    .get("/api/lista-tipos-solicitud")
    .then(response => {
      commit("setTypes", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchsellers({ commit, state }, text) {
  let params = { filter: text };
  await $http
    .get("/api/lista-vendedores", { params })
    .then(response => {
      commit("setSellers", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function searchProducts({ state, commit }, filter) {
  let params = state.paramsProduct;


  params.filter = filter;
  params.paginate = false;
  params.branchoffice_id = localStorage.selected_branchoffice;
  params.branchoffice_warehouse_id =
    localStorage.selected_branchoffice_warehouse;

    if (state.selectedServiceRequest.stateSelectedUpdate && state.selectedServiceRequest.stateSelectedUpdate.id == 10) {
      params.code_line_filter = '050'
      params.branchoffice_warehouse_code = '02'
    }else{
      params.code_line_filter = ''
      params.branchoffice_warehouse_code = ''
    }

  await $http.get("/api/lista-productos", { params }).then(response => {
    commit("setProducts", response.data.data.data);
  });
}

export async function createRequest({ state, commit }) {
  // Traemos la sede que esta actualmente
  const branchoffice_id = localStorage.selected_branchoffice;

  let data = state.params;
  data.branchoffice_id = branchoffice_id;

  await $http
    .post("/api/solicitudes-de-servicio/crear", data)
    .then(response => {
      commit("resetFormRequestService");
      Notification.success({
        title: "Exito!",
        message: "Registro Creado",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al crear el registro.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Este metodo trae los estados por tipo de solicitud
 *
 * @author Kevin Galindo
 */
export async function getStateByServiceRequestType(
  { state, commit },
  service_requests_type_id
) {
  let params = {
    params: {
      service_requests_type_id: service_requests_type_id
    }
  };

  await $http
    .get("/api/estados-por-solicitud-de-servicio", params)
    .then(response => {
      commit("setServiceRequestsStates", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al traer tipos de solicitudes de servicio.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
