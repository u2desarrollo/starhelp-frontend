import $http from "../../axios"
import { Notification, MessageBox } from 'element-ui'

export async function fetchRequestHeaders ({ commit, state }, requestParameters = { params: state.params }) {
  await $http.get('/api/request_headers', requestParameters).then((res) => {

    commit('setRequestHeaders', res.data.data);
    commit('setCopyRequestHeaders', res.data.data);
  }).catch((error) => {
    Notification.error({
      title: '¡Error!',
      message: error.message,
      type: 'error',
      duration: 1500,
      customClass: 'notification-box'
    })
    commit('requestHeadersError', error.message)
  });
}
export async function updateRequestHeaders ({ commit, state, dispatch }, params) {

  $http.put(`/api/request_headers/${params.id}`, params)
    .then((response) => {
      Notification.success({
        title: 'Exito!',
        message: 'La solcitud ha sido actualizada',
        type: 'success',
        duration: 2000,
        customClass: 'notification-box',
      });
      dispatch('fetchRequestHeaders')
    })
    .catch((error) => {
      let message;

      if (error.response.status == 422) {
        message = error.response.data.errors.license_code[0]
      } else {
        message = error.message
      }

      MessageBox.alert(message, 'Error', {
        dangerouslyUseHTMLString: true,
        confirmButtonText: 'Aceptar',
        type: 'error',
        customClass: 'notification-error'
      })
      // commit('requestHeadersError', error.message)
    })
}


export async function contactWarehousesBiId ({ state, commit }, params) {
  await $http.get(`/api/establecimientos/${params}`).then((response) => {
    commit('setContactWarehouses', response.data.data);
  })
}

export async function contactWarehousesBiContact ({ state, commit }) {
  await $http.get(`/api/establecimientos/ids`, { params: state.params }).then((response) => {
    commit('setParamWarehouse', response.data.data);
  })
}
