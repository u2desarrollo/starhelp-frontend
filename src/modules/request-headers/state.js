
export default {
  requestHeaders: [],
  copyRequestHeaders: [],
  error: false,
  errorMessage: '',
  role: null,
  typeUser:'',
  renderB2B:false  ,
  contactsWarehouse: null,
  action: 'create',
  params: {
    page: 1,
    perPage: 50,
    contactB2B: null,
    filter: null,
    date: '',
    contact_id: null,
    role: null,
  typeUser:'',
  warehouse:null,
    requestType: '',
    status_request: false,
  }
}
