import { stat } from "fs";

export function setRequestHeaders(state, requestHeaders) {
  state.requestHeaders = requestHeaders;
}
export function setCopyRequestHeaders(state, requestHeaders) {
  state.copyRequestHeaders = JSON.parse(JSON.stringify(requestHeaders));
}
export function requestHeadersError(state, payload) {
  state.error = true
  state.errorMessage = payload
  state.requestHeaders = []
}
export function setFilterDate(state, date) {
  state.params.date = date;
}


export function setPerPage(state, perPage) {
  state.params.page = 1
  state.params.perPage = perPage
}

export function setPage(state, page) {
  state.params.page = page
}
export function setFilter(state, filter) {
  state.params.page = 1
  state.params.filter = filter
}


export function setContactB2B(state,user) {

  state.params.contactB2B = user;
}


export function setProducts(state, products) {
  state.selectedRequestProduct.request_products = products;
}
export function setContactWarehouses(state, contactWarehouse) {
  state.contactsWarehouse = contactWarehouse;
}

export function setRole(state, role) {
  state.role = role;
}



export function setTypeUser(state, typeUser) {
  state.typeUser=typeUser;
}

export function setRenderB2B(state, renderB2B) {
  state.renderB2B=renderB2B;
}

export function setContactId(state,contactId) {

  state.params.contact_id = contactId;
}

export function setParamRole(state, role) {
  state.params.role = role;
}



export function setParamTypeUser(state, typeUser) {
  state.params.typeUser=typeUser;
}



export function setParamWarehouse(state, warehouse) {
  state.params.warehouse=warehouse;
}




