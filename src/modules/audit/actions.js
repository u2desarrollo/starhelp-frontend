
import $http from "../../axios";
import {Notification, MessageBox} from 'element-ui';


export async  function getDocument({commit},id) {
    await $http.get(`/api/detalle-documento/${id}`).then(response=>{
        commit('setDocument', response.data);
    }).catch(error=>{
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
          });
    })
}


export async  function getAudits({commit},id) {

   await $http.get(`/api/auditoria-documento/${id}`).then(response=>{
        commit('setAudits',response.data.auditsDocument);
    }).catch(error=>{
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
          });

    });
    
}