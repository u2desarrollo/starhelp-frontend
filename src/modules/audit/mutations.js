export function setDefaultView(state) {
    for (const prop in state.view) {
        state.view[prop]=false;
      }
}


export function setDocument(state, document){
  state.document=document;
}

export function setAudits(state,audits) {
  state.audits=audits;
}