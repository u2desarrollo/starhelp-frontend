import $http from "../../axios";
import { Notification } from "element-ui";

export async function fetchAccounts(
  { commit, state },
  requestParameters = { params: state.params }
) {
  await $http
    .get("/api/cuentas", requestParameters)
    .then(response => {
      commit("setAccounts", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchGroups(
  { commit, state },
) {
  await $http
    .get("/api/parametros-por-tabla/97")
    .then(response => {
      commit("setGroups", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchLevels(
  { commit, state },
) {
  await $http
    .get("/api/parametros-por-tabla/98")
    .then(response => {
      commit("setLevels", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

//cuentas para select
export async function fetchAccountss(
  { commit, state },
  requestParameters = { params: state.params }
) {
  await $http
    .get("/api/cuentas", requestParameters)
    .then(response => {
      commit("setAccountss", response.data.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchCategories(
  { commit, state },
) {
  await $http
    .get("/api/parametros-por-tabla/99")
    .then(response => {
      commit("setCategories", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}


export async function fetchBranchOfficeWarehouse(
  { commit, state },
) {
  await $http
    .get("/api/sedes")
    .then(response => {
      commit("fetchBranchOfficeWarehouse", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchLines(
  { commit, state },
) {
  await $http
    .get("/api/lista-lineas")
    .then(response => {
      commit("fetchLines", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchContacts({commit,state}, requestParameters = {params: state.paramsquery}) {
  await $http.get('/api/terceros', requestParameters)
      .then((response) => {
          commit('setContacts', response.data.data.data)

      })
      .catch((error) => {
          Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
      });
  })
}

export async function fetchWalletType(
  { commit, state },
) {
  await $http
    .get("/api/parametros-por-tabla/100")
    .then(response => {
      commit("setWalletType", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchcost_center_1(
  { commit, state },
) {
  await $http
    .get("/api/parametros-por-tabla/102")
    .then(response => {
      commit("setcost_center_1", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchcost_center_2(
  { commit, state },
) {
  await $http
    .get("/api/parametros-por-tabla/103")
    .then(response => {
      commit("setcost_center_2", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchcost_center_3(
  { commit, state },
) {
  await $http
    .get("/api/parametros-por-tabla/104")
    .then(response => {
      commit("setcost_center_3", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchTaxes(
  { commit, state },
) {
  await $http
    .get("/api/impuestos")
    .then(response => {
      commit("setTaxes", response.data.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function searchLevel(
  { commit, state }, level_id
) {
  await $http
    .get(`/api/parametros/${level_id}`)
    .then(response => {
      commit("setlevelToValidate", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function createAccount(
  { commit, state }
) {
  await $http
    .post(`/api/cuentas/`, state.params_create)
    .then(response => {
      Notification.success({
        title: "Hecho!",
        message: "Se Ha Creado La Cuenta Exitosamente",
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      console.log(error);
      let data = error.response.data
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function updateAccount(
  { commit, state }
) {
  await $http
    .put(`/api/cuentas/${state.id_to_edit}`, state.params_create)
    .then(response => {
      Notification.success({
        title: "Hecho!",
        message: "Se Ha Modificado La Cuenta Exitosamente",
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      console.log(error);
      let data = error.response.data
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchAccountForClone({commit , state}, id_account_clone){
  await $http
    .get(`/api/cuentas/${id_account_clone}`)
    .then(response => {
      commit("setInfoDataClone", response.data.data);
    })
    .catch(error => {
      let data = error.response.data
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchAccountForEdit({commit , state}, id_account_edit){
  await $http
    .get(`/api/cuentas/${id_account_edit}`)
    .then(response => {
      commit("setInfoDataEdit", response.data.data);
    })
    .catch(error => {
      let data = error.response.data
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function deleteAccount({commit, state}, id){
  await $http
    .delete(`/api/cuentas/${id}`)
    .then(response => {
      Notification.error({
        title: "Exito!",
        message: data.message,
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      let data = error.response.data
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchcommissionable_concepts({commit, state}){
  await $http
  .get("/api/parametros-por-tabla/97")
  .then(response => {
    commit("setcommissionable_concepts", response.data);
  })
  .catch(error => {
    Notification.error({
      title: "Error!",
      message: error.message,
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
    commit("modelError", error.message);
  });
} 