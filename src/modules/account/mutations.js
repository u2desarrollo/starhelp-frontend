export function setAccounts (state, accounts){
  state.accounts = accounts
}
export function setGroups (state, groups){
  state.groups = groups
}
export function setLevels (state, levels){
  state.levels = levels
}
export function setAccountss (state, accountss){
  state.accountss = accountss
}
export function setCategories (state, categories){
  state.categories = categories
}
export function fetchBranchOfficeWarehouse (state, branchOffices){
  state.branchOffices = branchOffices
}
export function fetchLines (state, fetchLines){
  state.lines = fetchLines
}
export function setContacts (state, contacts){
  state.contacts = contacts
}
export function setWalletType (state, wallet_types){
  state.wallet_types = wallet_types
}
export function setcost_center_1 (state, cost_center_1){
  state.cost_center_1 = cost_center_1
}
export function setcost_center_2 (state, cost_center_2){
  state.cost_center_2 = cost_center_2
}
export function setcost_center_3 (state, cost_center_3){
  state.cost_center_3 = cost_center_3
}
export function setTaxes (state, taxes){
  state.taxes = taxes
}
export function setlevelToValidate (state, level_to_validate){
  state.level_to_validate = level_to_validate
}
export function setLoading (state, loading_form){
  state.loading_form = loading_form
}
export function serLoading_(state, loading){
  state.loading = false
}
export function setParamsAccounts(state, params){
  state.params = params
}
export function setInfoDataClone (state, account) {
  state.code_to_clone = account.code
  
  state.params_create.level = account.level
  state.params_create.name = account.name
  state.params_create.short_name = account.short_name
  state.params_create.debit_credit = account.debit_credit
  state.params_create.group = account.group
  state.params_create.description = account.description
  state.params_create.account_id_totalize = account.account_id_totalize
  state.params_create.category = account.category
  state.params_create.branchoffice_id = account.branchoffice_id
  state.params_create.contact_id = account.contact_id
  state.params_create.manage_contact_balances = account.manage_contact_balances ? 1 : 0
  state.params_create.is_customer = account.is_customer? 1 : 0
  state.params_create.is_provider = account.is_provider? 1 : 0
  state.params_create.is_employee = account.is_employee? 1 : 0
  state.params_create.is_others = account.is_others? 1 : 0
  state.params_create.resume_beginning_year = account.resume_beginning_year? 1 : 0
  state.params_create.wallet_type = account.wallet_type
  state.params_create.document_description = account.document_description
  state.params_create.manage_quota_balances = account.manage_quota_balances? 1 : 0
  state.params_create.manage_document_balances = account.manage_document_balances? 1 : 0
  state.params_create.cost_center_2 = account.cost_center_2
  state.params_create.cost_center_3 = account.cost_center_3
  state.params_create.cost_center_1 = account.cost_center_1
  state.params_create.manage_projects = account.manage_projects? 1 : 0
  state.params_create.manage_projects_stage = account.manage_projects_stage? 1 : 0
  state.params_create.manage_bank_document = account.manage_bank_document? 1 : 0
  state.params_create.manage_way_to_pay = account.manage_way_to_pay? 1 : 0
  state.params_create.tax_account = account.tax_account
  state.params_create.Active_account = account.Active_account? 1 : 0
  state.params_create.pevious_account = account.pevious_account
  state.params_create.manage_base_value = account.manage_base_value? 1 : 0
  state.params_create.line_id = account.line_id? 1 : 0
}

export function setInfoDataEdit (state, account) {
  state.code_to_edit = account.code
  state.id_to_edit = account.id
  
  state.params_create.code = account.code
  state.params_create.level = account.level
  state.params_create.name = account.name
  state.params_create.short_name = account.short_name
  state.params_create.debit_credit = account.debit_credit
  state.params_create.group = account.group
  state.params_create.description = account.description
  state.params_create.account_id_totalize = account.account_id_totalize
  state.params_create.category = account.category
  state.params_create.branchoffice_id = account.branchoffice_id
  state.params_create.contact_id = account.contact_id
  state.params_create.manage_contact_balances = account.manage_contact_balances ? 1 : 0
  state.params_create.is_customer = account.is_customer? 1 : 0
  state.params_create.is_provider = account.is_provider? 1 : 0
  state.params_create.is_employee = account.is_employee? 1 : 0
  state.params_create.is_others = account.is_others? 1 : 0
  state.params_create.resume_beginning_year = account.resume_beginning_year? 1 : 0
  state.params_create.wallet_type = account.wallet_type
  state.params_create.document_description = account.document_description
  state.params_create.manage_quota_balances = account.manage_quota_balances? 1 : 0
  state.params_create.manage_document_balances = account.manage_document_balances? 1 : 0
  state.params_create.cost_center_2 = account.cost_center_2 ? 1 : 0
  state.params_create.cost_center_3 = account.cost_center_3 ? 1 : 0
  state.params_create.cost_center_1 = account.cost_center_1 ? 1 : 0
  state.params_create.manage_projects = account.manage_projects? 1 : 0
  state.params_create.manage_projects_stage = account.manage_projects_stage? 1 : 0
  state.params_create.manage_bank_document = account.manage_bank_document? 1 : 0
  state.params_create.manage_way_to_pay = account.manage_way_to_pay? 1 : 0
  state.params_create.tax_account = account.tax_account
  state.params_create.Active_account = account.Active_account? 1 : 0
  state.params_create.commissionable_concept = account.commissionable_concept ? 1 : 0
  state.params_create.results_account = account.results_account
  state.params_create.pevious_account = account.pevious_account
  state.user_last_modification = account.user_modification != null  ? account.user_modification.name +' '+account.user_modification.surname + '  -  ' + account.updated_at.substr(0, 10) + ' Hora ' + account.updated_at.substr(11, 8) : null
  state.params_create.manage_base_value = account.manage_base_value? 1 : 0
  state.params_create.line_id = account.line_id
}
export function setcommissionable_concepts(state, commissionable_concepts){
  state.commissionable_concepts = commissionable_concepts
}
export function setloading(state, loading){
  state.loading = loading
}