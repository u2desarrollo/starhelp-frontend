export const defaultproducts = () => {
    return {
      products_kardex: []
    }
  }

export const defaultContacts = () => {
    return {
        data: [],
        total: 1
    }
}

export default {
  loading:{
    download_pdf:false,
    kardex:false
  },
  products: [],
  kardex: [],
  contacts: [],

    branchOffices: [],
    branchoffice_warehouses: [],

    branchoffice_warehouses_entry: [], 
    product:defaultproducts(),

    params: {
        sede_salida:null,
        bodega_salida: null,
        date: null,
        contact_id:'',
        button:true,
        product_code:null,
        product:null
    },

    params_reconstructor: {
      date: null,
      product:null,
      document_product:null
  },

    paramsquery:{
        is_customer: false,
        is_provider: false,
        is_employee: false,
        filter_customer: [],
        filter_provider: [],
        filter_employee: [],
        paginate: true,
        page: 1,
        perPage: 50,
        query: '',
        sortBy: 'id',
        sort: 'ASC',
    },
    paramsProducts:{
        validate_exhausted : false,
        paginate: false,
        product: '',
        branchoffice_warehouse_id: '',
        filter: '',
        page: 1,
        perPage: 15
    },
    document:'',
}