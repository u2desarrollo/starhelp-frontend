import {defaultContacts} from "./state";

export function setBranchOffices(state, branchOffices){
    state.branchOffices = branchOffices;
}

//Establece el valor por defecto de contacts
export function resetContacts(state) {
    Object.assign(state.contacts, defaultContacts())

}

export function setBranchofficeWarehouses(state, branchofficeWarehouses){
    state.branchoffice_warehouses = branchofficeWarehouses;
}
export function setSedeSalida(state, sede_salida) {
    state.params.sede_salida = sede_salida
}
export function setBodegaSalida(state, bodega_salida) {
    state.params.bodega_salida = bodega_salida    
}

export function setContacts(state, contacts) {
    state.contacts = contacts;
}
// trae todos los productos
export function setStateProducts(state, products) {
    state.products = products;
  }

export function setKardex(state, kardex) {
    state.kardex = kardex;
}