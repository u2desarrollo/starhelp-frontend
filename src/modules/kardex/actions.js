import $http from "../../axios";
import {
  Notification,
  MessageBox
} from 'element-ui';

//Obtener las bodegas
export async function fetchBranchOfficeWarehouses({commit, state}, brancheoffice) {
    await $http.get(`/api/bodegas-sedes?branchoffice_id=${brancheoffice}`)
        .then((response) => {
        commit('setBranchofficeWarehouses', response.data.data)
        })
        .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}

//Obtener las sedes
export async function fetchBranchOffices({commit, state}) {
    await $http.get('/api/sedes')
      .then((response) => {
        commit('setBranchOffices', response.data)
      })
      .catch((error) => {
        Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 2000,
          customClass: 'notification-box',
        });
        commit('BranchOfficeError', error.message)
      })
  }

  //Obtener los terceros
export async function fetchContacts({
    commit,
    state
}, requestParameters = {
    params: state.paramsquery
}) {

    await $http.get('/api/terceros', requestParameters)
        .then((response) => {
            commit('setContacts', response.data.data.data)

        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('contactError', error.message)
        })
}

//consulta kardex
export async function getKardex({commit,state}) {
    state.loading.kardex = true
    var data = {
        params: state.params,
        product: state.product
    }
    await $http.get('/api/kardex', data)
        .then((response) => {
            commit('setKardex', response.data.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
        })
  state.loading.kardex = false
}

export async function searchProducts ({
    state,
    commit
}) {

    let requestParameters = {
        params: state.paramsProducts
    }
    await $http.get('/api/lista-productos', requestParameters).then((response) => {
        commit('setStateProducts', response.data.data.data)
    }

    )
}


export async function fixvalues ({ commit }, params) {
    let requestParameters = { params:params }
    await $http.get(`/api/reconstructor`, requestParameters)
      .then((response) => {
      })
      .catch((error) => {
        Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 2000,
          customClass: 'notification-box',
        });
    })
  }

export async function downloadDocument({state}, documentToPdf) {
  state.loading.download_pdf = true

  let name = documentToPdf.vouchertype.name_voucher_type + ' No. ' + documentToPdf.consecutive + '.pdf'

  let url = ''

  if (documentToPdf.operationType_id == 4){
    url ='/api/traslado/descargar-pdf/' + documentToPdf.id
  }else if (documentToPdf.operationType_id == 3){
    url = '/api/traslado/descargar-pdf/' + documentToPdf.thread
  }else{
    url = '/api/documentos/descargar-pdf/' + documentToPdf.id
  }

  await $http({
    url: url,
    method: 'GET',
    responseType: 'blob',
  }).then((response) => {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', name);
    document.body.appendChild(link);
    link.click();
  }).catch((error) => {
    Notification.error({
      title: 'Error!',
      message: error.message,
      type: 'error',
      duration: 3000,
      customClass: 'notification-box',
    });

  })
  state.loading.download_pdf = false
}
