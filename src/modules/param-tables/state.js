const tableParamTable = {
    loading: false,
    data: [],
    fields: [
        {
            field: 'code_table',
            name: 'Código',
            width: 100
        },
        {
            field: 'name_table',
            name: 'Nombre',
            width: 250
        },
        {
            field: 'actions',
            label: 'Opciones',
            width: 280
        },
    ],
    errors: {
        has: false,
        message: '',
        code: 0,
        data: []
    },
    pagination: [],
    options: {
        perPage: 10,
        search: '',
        currentPage: 1
    },
}

export const defaultSelectedParamTable = () => {
    return {
        subscriber_id: '',
        code_table: '',
        name_table: '',
        desc_code_parameter: 'Código',
        desc_name_parameter: 'Descripcion',
        desc_alphanum_data_first: '',
        desc_alphanum_data_second: '',
        desc_alphanum_data_third: '',
        desc_numeric_data_first: '',
        desc_numeric_data_second: '',
        desc_numeric_data_third: '',
        desc_date_data_first: '',
        desc_date_data_second: '',
        desc_date_data_third: '',
        desc_image: '',
        observation: '',
        protected: 0,
        line: 0,
        subline: 0,
        city: 0,
        branchoffice_warehouses: 0,
    }
}

export const defaultSelectedParameter = () => {
    return {
        code_parameter: '',
        name_parameter: '',
        alphanum_data_first: '',
        alphanum_data_second: '',
        alphanum_data_third: '',
        numeric_data_first: '',
        numeric_data_second: '',
        numeric_data_third: '',
        date_data_first: '',
        date_data_second: '',
        date_data_third: '',
        image: ''
    }
}
export default {
    tableParamTable,
    paramTables: [],
    paramTablesList: [],
    selectedParamTable: defaultSelectedParamTable(),
    error: false,
    errorMessage: '',
    action: 'create',
    paramTableEdit: null,

    getParamTableEdit: true,
    params: {
        paginate: true,
        page: 1,
        perPage: 15,
        filter: '',
        sortBy: 'code_table',
        sort: 'ASC'
    },

    parameters: [],
    selectedParameter: defaultSelectedParameter(),
    actionParameter: 'create',

    getParameterEdit: true,
    lines :[],
    sublines :[],
    cities:[],
    branchOffices:[],
}

/* export default {
    parameters: [],
    selectedParameter: defaultSelectedParameter(),
    error: false,
    errorMessage: '',
    actionParameter: 'create',

    getParameterEdit: true,

} */
