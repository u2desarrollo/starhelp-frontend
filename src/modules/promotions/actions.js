import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";
import { parse } from "path";

export async function getPromotions({ commit, state }) {
  await $http
    .get(`/api/promociones`, {
      params: state.params
    })
    .then(response => {
      commit("setPromotions", response.data.data);
    })
    .catch(response => {});
}

export async function getPromotionById({ commit }, id) {
  await $http
    .get(`/api/promocion/${id}`, {
      params: {
        branchoffice_storage: localStorage.getItem("selected_branchoffice")
      }
    })
    .then(response => {
      response = response.data.data;
      response.date = [response.start_date, response.end_date];

      commit("setPromotionSelectes", response);
    });
}
export function setPerPagePagination({ commit, dispatch }, perPage) {
  commit("setPerPage", parseInt(perPage.target.value));
  dispatch("getPromotions");
}

export function setPagePagination({ commit, dispatch }, page) {
  commit("setPage", parseInt(page));
  dispatch("getPromotions");
}

export async function getLines({ commit }) {
  await $http.get("/api/lineas").then(response => {
    commit("setLines", response.data.data);
  });
}
export async function getSubLines({ commit, state }) {
  await $http
    .get("/api/sublineas", {
      params: { lines: state.selected_promotion.line_id }
    })
    .then(response => {
      commit("setSubLines", response.data.data);
    });
}

export async function getBrands({ commit }) {
  await $http.get("/api/marcas").then(response => {
    commit("setBrands", response.data.data);
  });
}
export async function getSubBrands({ commit, state }) {
  await $http
    .get("/api/submarcas", {
      params: {
        brand_id: state.selected_promotion.brand_id
      }
    })
    .then(response => {
      commit("setSubbrands", response.data.data);
    });
}

export async function getProductByParam({ commit }, param) {
  await $http.get(`/api/promociones/search/${param}`).then(response => {
    commit("setProducts", response.data.data);
  });
}

export async function getProductById({ commit }, id) {
  if (id) {
    await $http.get(`/api/productos/${id}`).then(response => {
      commit("setProducts", response.data.data);
    });
  }
}

export async function deletePromotion({}, id) {
  await $http.delete(`/api/promociones/${id}`).then(re => {
    Notification.success({
      title: "Exito!",
      message: "La promocion ha sido eliminada correctamente",
      type: "success",
      duration: 2000,
      customClass: "notification-box"
    });
  });
}

export async function createPromotion({ state, commit }) {
  commit("setPromotionLoader", true);
  let formData = new FormData();
  formData.append("image", state.selected_promotion.image2);
  state.selected_promotion.image = null;

  for (const val in state.selected_promotion) {
    if (val == "products_promotions") {
      formData.append(val, JSON.stringify(state.selected_promotion[val]));
    } else {
      if (
        state.selected_promotion[val] &&
        state.selected_promotion[val] != null
      ) {
        formData.append(val, state.selected_promotion[val]);
      } else {
        formData.append(val, "");
      }
    }
  }

  await $http.post(`/api/promociones`, formData).then(response => {
    Notification.success({
      title: "Exito!",
      message: "La promoción se ha creado correctamente",
      type: "success",
      duration: 2000,
      customClass: "notification-box"
    });
  });

  commit("setPromotionLoader", false);
}

export async function updatePromotion({ state }, id) {
  // if (state.selected_promotion.gift_type == 2) {
  //   state.selected_promotion.specific_product_id = null;
  // }
  let formData = new FormData();
  formData.append("image", state.selected_promotion.image2);

  for (const val in state.selected_promotion) {
    if (val == "products_promotions") {
      formData.append(val, JSON.stringify(state.selected_promotion[val]));
    } else {
      if (
        state.selected_promotion[val] &&
        state.selected_promotion[val] != null
      ) {
        formData.append(val, state.selected_promotion[val]);
      } else {
        formData.append(val, "");
      }
    }
  }
  ///formData no funciona con metodo post realice el siguiente metodo y remplaze la solicitud por un metodo post
  formData.append("_method", "put");
  await $http.post(`/api/promociones/${id}`, formData).then(response => {
    Notification.success({
      title: "Exito!",
      message: "La promocion ha sido editada correctamente",
      type: "success",
      duration: 2000,
      customClass: "notification-box"
    });
  });
}

export async function getBranchoffices({ commit }) {
  await $http
    .get("/api/branchs")
    .then(response => {
      commit("setBranchoffices", response.data.data);
    })
    .catch(response => {});
}

/**
 *
 * @author Kevin Galindo
 */
export async function getPromotionsActives({ commit, state }, params) {
  await $http
    .get(`/api/promociones-activas`)
    .then(response => {
      commit("setPromotionsActives", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al traer las promociones",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 *
 * @author Kevin Galindo
 */
export async function fetchProductsByClasification({ commit, state }) {
  //contact_id
  let user_b2b = JSON.parse(localStorage.user_b2b);

  let params = {
    params: {
      search: state.params.clasification,
      line_id: state.selectedPromotion.line_id,
      subline_id: state.selectedPromotion.subline_id,
      brand_id: state.selectedPromotion.brand_id,
      subbrand_id: state.selectedPromotion.subbrand_id,
      promotion_id: state.selectedPromotion.id
    }
  };

  if (user_b2b) {
    params.params.branchoffice_id = user_b2b.branchoffice.id;
  }

  await $http
    .get(`/api/productos-clasificacion`, params)
    .then(response => {
      commit("setProductsFiltered", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al traer los productos por clasificacion",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Este metodo se encarga de setear el beneficio
 *
 * @author Kevin Galindo
 */
export async function validateBenefitPromotion({ state }) {
  if (!state.selectedPromotion.comply) {
    return;
  }

  let total_quantity = 0;
  let total_val_brut = 0;
  let comply_quantity_promotion = 0;

  // Calculamos la cantidad y el valor bruto
  state.selectedPromotion.products_promotions.map((pp, index_pp) => {
    total_quantity += pp.quantity ? parseInt(pp.quantity) : 0;
    if (pp.discount_value) {
      total_val_brut += pp.quantity
        ? pp.product.price.price * pp.quantity - pp.discount_value
        : 0;
    } else {
      total_val_brut += pp.quantity ? pp.product.price.price * pp.quantity : 0;
    }
  });

  comply_quantity_promotion = Math.floor(
    total_quantity / state.selectedPromotion.minimum_amount
  );

  // Producto de menor precio
  if (state.selectedPromotion.gift_type == 2) {
    var product_benefit = null;
    var min_val = null;

    // Seleccionamos el producto de menor precio
    state.selectedPromotion.products_promotions.map((pp, index_pp) => {
      if (!min_val) {
        min_val = parseInt(pp.product.price.price);
        product_benefit = JSON.parse(JSON.stringify(pp.product));
      } else {
        if (parseInt(pp.product.price.price) < min_val) {
          min_val = parseInt(pp.product.price.price);
          product_benefit = JSON.parse(JSON.stringify(pp.product));
        }
      }
    });

    var iva = product_benefit.line.taxes.find(t => t.code == 10);
    product_benefit.quantity = comply_quantity_promotion; //#

    product_benefit.val_brut =
      Math.round(product_benefit.price.price) * product_benefit.quantity;
    product_benefit.val_iva =
      (Math.round(product_benefit.val_brut) * iva.percentage_sale) / 100;
    product_benefit.is_benefit = true;
    product_benefit.selected = false;

    // Validamos si hay un producto con igual precio
    let specificProductRepeat = [];

    state.selectedPromotion.products_promotions.map((pp, index_pp) => {
      if (pp.quantity == 0) {
        return;
      }
      if (pp.product.id == product_benefit.id) {
        return;
      }

      // Si el producto es del igual valor se guarda en el arreglo para su proxima seleccion
      if (
        Math.round(pp.product.price.price) ===
        Math.round(product_benefit.price.price)
      ) {
        let productRepeat = pp.product;

        var iva = productRepeat.line.taxes.find(t => t.code == 10);

        productRepeat.quantity = comply_quantity_promotion; //#

        productRepeat.val_brut =
          Math.round(productRepeat.price.price) * productRepeat.quantity;
        productRepeat.val_iva =
          (Math.round(productRepeat.val_brut) * iva.percentage_sale) / 100;
        productRepeat.is_benefit = true;
        productRepeat.selected = false;

        specificProductRepeat.push(productRepeat);
      }
    });

    // Agregamos el/los beneficios
    specificProductRepeat.push(product_benefit);

    state.benefitSelected = specificProductRepeat[0].id;
    state.selectedPromotion.specific_product = specificProductRepeat;
  }

  // Producto especifico
  if (state.selectedPromotion.gift_type != 2) {
    if (state.selectedPromotion.specific_product_id != null) {
      await $http
        .get(`/api/productos/${state.selectedPromotion.specific_product_id}`)
        .then(response => {
          product_benefit = response.data.data;

          var iva = product_benefit.line.taxes.find(t => t.code == 10);

          product_benefit.quantity = comply_quantity_promotion; //#
          product_benefit.val_brut =
            product_benefit.price.price * product_benefit.quantity;
          product_benefit.val_iva =
            (product_benefit.val_brut * iva.percentage) / 100;
          product_benefit.is_benefit = true;
          state.selectedPromotion.specific_product = product_benefit;
        })
        .catch(error => {
          Notification.error({
            title: "¡Error!",
            message: "Error al traer el beneficio",
            type: "error",
            duration: 1500,
            customClass: "notification-box"
          });
        });
    }
  }
}

/**
 *
 * @author Jhon García
 */
export async function updatePromotions({ commit, state }) {
  await $http
    .get(`/api/actualizar-promociones`)
    .then(response => {
      Notification.success({
        title: "Muy bien",
        message: "Las promociones se están actualizando.",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al actualizar las promociones",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
