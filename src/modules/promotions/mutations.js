import { defaultSelectedPromotion } from "./state";

export function setPromotions(state, promotions) {
  state.promotions = promotions;
}

export function setDefaultSelectedPromotion(state) {
  state.selected_promotion = defaultSelectedPromotion();
}

export function setPromotion(state, selected_promotion) {
  state.selected_promotion = selected_promotion;
}

export function setPromotionSelectes(state, selected_promotion) {
  state.original_copy_selected_promotion = JSON.parse(
    JSON.stringify(selected_promotion)
  );
  state.selected_promotion = selected_promotion;
}

export function setPerPage(state, perPage) {
  state.params.page = 1;
  state.params.perPage = perPage;
}

export function setPage(state, page) {
  state.params.page = page;
}
export function setFilter(state, filter) {
  state.params.page = 1;
  state.params.filter = filter;
}

export function setProducts(state, products) {
  state.products = products;
}

export function setLines(state, lines) {
  state.lines = lines;
}

export function setSubLines(state, sublines) {
  state.sublines = sublines;
}

export function setBrands(state, brands) {
  state.brands = brands;
}

export function setSubbrands(state, subbrands) {
  state.subbrands = subbrands;
}

export function modifyIsDisabled(state, bol) {
  state.isDisabled = bol;
}

export function setImage(state, image) {
  state.selected_promotion.image = image;
}

export function setImage2(state, image) {
  state.selected_promotion.image2 = image;
}

export function setBranchoffices(state, branchoffices) {
  state.branchoffices = branchoffices;
}
export function setPromotionLoader(state, status) {
  state.promotionLoader = status;
}

/**
 *
 * Este metodo se encarga de setear
 * la informacion de las promociones activas
 *
 * @author Kevin Galindo
 */
export function setPromotionsActives(state, promotionsActives) {
  state.promotionsActives = promotionsActives;
}

export function setBenefitSelected(state, benefitSelected) {
  state.benefitSelected = benefitSelected;
}

/**
 *
 * Este metodo setea la promocion seleccionada
 *
 * @author Kevin Galindo
 */
export async function setSelectedPromotion(state, promotion) {
  if (promotion.promotion_type_id == 3) {
    state.columnsGrid = state.mixColumnsGrid;
  } else {
    if (!promotion.specific_product_id && promotion.discount) {
      state.columnsGrid = state.mixColumnsGrid;
      promotion.promotionHandlesDiscount = true;
    } else {
      state.columnsGrid = state.defaultColumnsGrid;
    }
  }

  // promotion.specific_product = [];

  await setBenefitSelected(state, promotion.specific_product_id);
  await emptyProductsFiltered(state);

  state.selectedPromotion = promotion;

  state.selectedPromotion.products_promotions.map(pp => {
    calculateValuesProductDiscount(state, pp.product_id);
  });
}

export function calculateValuesProductDiscount(state, product_id) {
  let productKey = state.selectedPromotion.products_promotions.findIndex(
    pp => pp.product_id === product_id
  );

  let product = state.selectedPromotion.products_promotions.find(
    pp => pp.product_id === product_id
  );

  if (product) {
    if (state.selectedPromotion.discount) {
      let price = product.product.price.price;
      let discount_value =
        (parseInt(price) * parseInt(state.selectedPromotion.discount)) / 100;

      product.discount = state.selectedPromotion.discount;
      product.discount_value = parseInt(price - discount_value);

      state.selectedPromotion.products_promotions[productKey] = product;
    }
  }
}

/**
 *
 * Este metodo setear los
 * productos buscados
 *
 * @author Kevin Galindo
 */
export function setProductsFiltered(state, products) {
  state.productsFiltered = products;
}

/**
 *
 * @author Kevin Galindo
 */
export function setStatusPromotion(state, data) {
  state.selectedPromotion.comply = data;
}

/**
 *
 * @author Kevin Galindo
 */
export function setPromotionByProduct(state, product) {
  // Agregamos el producto seleccionado alas promociones
  if (product.id) {
    product.product_promotions.map((p, indexpp) => {
      let product_promotion = {
        product_id: product.id,
        product: JSON.parse(JSON.stringify(product)),
        promotion_id: p.id,
        quantity: 0
      };

      let added = p.products_promotions.find(pp => pp.product_id == product.id);

      if (!added) {
        p.products_promotions.push(product_promotion);
      }
    });
  }

  state.promotionsActives = product.product_promotions;
}

export function emptyProductsFiltered(state) {
  state.productsFiltered = [];
}

export function setSpecificProducts(state, data) {
  state.benefitSelected = data[0].id;
  state.selectedPromotion.specific_product = data;
}
