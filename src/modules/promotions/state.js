export const defaultSelectedPromotion = () => {
  return {
    image: null,
    image2: null,
    line_id: null,
    specific_product_id: "",
    products_promotions: [],
    discount: null,
    pay_soon: null,
    cash_payment: null,
    minimum_gross_value: null,
    gift_type: null,
    description2: null,
    title: null,
    branchoffice_id: null
  };
};

// Columnas por defecto
export const defaultColumnsGrid = () => {
  return [
    { name: "Código", field: "product.code" },
    { name: "Descripción", field: "product.description" },
    { name: "Precio Lista", field: "product.price.price", format: "currency" },
    { name: "Cantidad", field: "quantity", format: "number", editable: true },
    { name: "Valor Total", field: "total_promotion" },
    { name: "Acciones", field: "actions" }
  ];
};

// Columnas promociones MIX
export const mixColumnsGrid = () => {
  return [
    { name: "Código", field: "product.code" },
    { name: "Descripción", field: "product.description" },
    { name: "Precio Lista", field: "product.price.price", format: "currency" },
    { name: "Precio Promocio", field: "discount_value", format: "currency" },
    { name: "Porcentaje Desc %", field: "discount", format: "number" },
    { name: "Cantidad", field: "quantity", format: "number", editable: true },
    { name: "Valor Total", field: "total_promotion" },
    { name: "Acciones", field: "actions" }
  ];
};

export default {
  promotions: null,
  lines: [],
  sublines: [],
  brands: [],
  subbrands: [],
  products: [],
  branchoffices: [],
  selected_promotion: defaultSelectedPromotion(),
  original_copy_selected_promotion: null,
  params: {
    page: 1,
    perPage: 50,
    sede_salida: null,
    filter: null,
    clasification: "",
    branchoffice: "",
    line:'',
    brand:''
  },
  promotionLoader: true,
  //
  promotionsActives: [],
  selectedPromotion: {
    id: null,
    specific_product: []
  },
  productsFiltered: [],
  columnsGrid: defaultColumnsGrid(),
  //
  mixColumnsGrid: mixColumnsGrid(),
  defaultColumnsGrid: defaultColumnsGrid(),

  benefitSelected: null
};
