import $http from "../../axios";
import {
    Notification,
    MessageBox
} from 'element-ui';

//Obtener las sedes
export async function fetchBranchOffices({ commit, state }) {
    await $http.get('/api/sedes')
        .then((response) => {
            commit('setBranchOffices', response.data.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 2000,
                customClass: 'notification-box',
            });
            commit('BranchOfficeError', error.message)
        })
}

//cuentas para select Desde
export async function fetchAccountsFrom(
    { commit, state },
    requestParameters = { params: state.params_accounts_from }
) {
    await $http
        .get("/api/cuentas", requestParameters)
        .then(response => {
            commit("setAccountsFrom", response.data.data.data);
        })
        .catch(error => {
            Notification.error({
                title: "Error!",
                message: error.message,
                type: "error",
                duration: 1500,
                customClass: "notification-box"
            });
            commit("modelError", error.message);
        });
}

//cuentas para select Hasta
export async function fetchAccountsUp(
    { commit, state },
    requestParameters = { params: state.params_accounts_up }
) {
    await $http
        .get("/api/cuentas", requestParameters)
        .then(response => {
            commit("setAccountsUp", response.data.data.data);
        })
        .catch(error => {
            Notification.error({
                title: "Error!",
                message: error.message,
                type: "error",
                duration: 1500,
                customClass: "notification-box"
            });
            commit("modelError", error.message);
        });
}

export async function fetchContactWarehouses(
    { commit, state },
    requestParameters = { params: state.params_contact_warehouse }
) {
    await $http
        .get("/api/sucursales", requestParameters)
        .then(response => {
            console.log(response );
            commit("setContactWarehouses", response.data);
        })
        .catch(error => {
            Notification.error({
                title: "Error!",
                message: error.message,
                type: "error",
                duration: 1500,
                customClass: "notification-box"
            });
            commit("modelError", error.message);
        });
}

//Obtener los terceros
export async function fetchContacts({
    commit,
    state
}, requestParameters = {
    params: state.params_cotact
}) {
    await $http.get('/api/terceros', requestParameters)
        .then((response) => {
            commit('setContacts', response.data.data.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('contactError', error.message)
        })
}

// obtener los tipos de comprobante
export async function fetchVouchertype({commit, state}) {
    await $http.get('/api/tipos-de-comprobantes', { params : state.paramsVoucherType})
        .then((response) => {
            commit('setVouchersTypes', response.data.data)
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}

// obtener Data libro Contable
export async function fetchAuxiliaryBook({commit, state}) {
    await $http.get('/api/libro-auxiliar', { params : state.params})
        .then((response) => {
            console.log(response);
            commit('setauxiliary_book', response.data.data);
            state.loading.module = false;
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: 'Error',
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}