export default {
    loading: {
        module: true
    },
    params: {
        year_month_from: null,
        year_month_up: null,
        contact_warehouse_id:null,
        branchoffice_id: null,
        account_from: null,
        account_up: null,
        description_account_from: null,
        description_account_up: null,
        contact_id: null,
        vouchertype_id: null
    },
    params_accounts_from: {
        notAux: false,
        onlyAux: false,
        paginate: true,
        page: 1,
        perPage: 30,
        filter: "",
        sortBy: "code",
        sort: "ASC"
    },
    params_accounts_up: {
        notAux: false,
        onlyAux: false,
        paginate: true,
        page: 1,
        perPage: 30,
        filter: "",
        sortBy: "code",
        sort: "ASC"
    },
    params_cotact:{
        is_customer: false,
        is_provider: false,
        is_employee: false,
        filter_customer: [],
        filter_provider: [],
        filter_employee: [],
        paginate: true,
        page: 1,
        perPage: 50,
        query: '',
        sortBy: 'id',
        sort: 'ASC',
    },
    params_contact_warehouse:{
        filter:'',
        contact_id:null
    },
    paramsVoucherType:{
        paginate : false,
        filter : null,
        branchoffice : null,
    },
    branchOffices: [],
    accounts_from: [],
    accounts_up: [],
    contacts:[],
    sales:[],
    contact_warehouses:[],
    auxiliary_book:[],
    voucherTypes:[],
    ag_grid:{
        selectedTheme:'ag-theme-balham'
    }
}