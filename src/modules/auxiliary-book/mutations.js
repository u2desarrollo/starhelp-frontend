
export function setBranchOffices(state, branchOffices) {
    state.branchOffices = branchOffices;
}

export function setAccountsFrom(state, accounts_from) {
    state.accounts_from = accounts_from
}

export function setAccountsUp(state, accounts_up) {
    state.accounts_up = accounts_up
}

export function setContacts(state, contacts) {
    state.contacts = contacts;
}

export function setVouchersTypes(state, vouchertype) {
    state.voucherTypes = vouchertype    
}

export function setContactWarehouses(state, contact_warehouses){
    state.contact_warehouses = contact_warehouses
}
export function setauxiliary_book(state, auxiliary_book) {
    state.auxiliary_book = auxiliary_book
}
export function setParams(state, params){
    state.params = params
}