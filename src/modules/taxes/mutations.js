export function setTaxes (state, taxes) {
  state.taxes = taxes;
}


export function setSelectedTax (state, selectedTax) {
  state.selectedTax = selectedTax;
}



export function setAction (state, action) {
  state.action = action;
}

export function getAction (state) {
  return state.action;
}

export function clearTax (state) {
  state.selectedTax = {};
}

export function modifyIsDisabled (state, bol) {
  state.isDisabled = bol;
}

export function setPerPage (state, perPage) {
  state.params.page = 1
  state.params.perPage = perPage
}

export function setPage (state, page) {
  state.params.page = page
}
