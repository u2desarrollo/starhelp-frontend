import $http from '../../axios'
import { Notification, MessageBox } from 'element-ui'




export async function getTaxes ({ commit, state }) {

  //para enviar parametros porurl en un metodo get
  //recuerde pasarle un objeto como parametro example {params:  state.params}
  await $http.get('/api/impuestos', { params: state.params }).then((response) => {
    commit('setTaxes', response.data.data)
  })



}


export async function getTax ({ commit }, id) {
  await $http.get(`/api/impuestos/${id}`).then((response) => {
    commit('setSelectedTax', response.data.data)
  }).catch((error) => {
    console.error(error.message)
  })
}



export async function newTax ({ state }) {
  await $http.post('/api/impuestos', state.selectedTax).then((response) => {
    Notification.success({
      title: 'Exito!',
      message: 'El impuesto ha sido creado correctamente',
      type: 'success',
      duration: 2000,
      customClass: 'notification-box',
    });
  })
}


export async function updateTax ({ state }) {
  await $http.put(`/api/impuestos/${state.selectedTax.id}`, state.selectedTax).then(() => {
    Notification.success({
      title: 'Exito!',
      message: 'El impuesto ha sido editado correctamente',
      type: 'success',
      duration: 2000,
      customClass: 'notification-box',
    });
  })
}



export async function deleteTax (context, id) {

  await $http.delete(`/api/impuestos/${id}`).then(() => {
    Notification.success({
      title: 'Exito!',
      message: 'El impuesto ha sido eliminado correctamente',
      type: 'success',
      duration: 2000,
      customClass: 'notification-box',
    });

  }).catch(error => {
    console.error(error.message)
  })
}

export function setPerPagePagination ({ commit, dispatch }, perPage) {
  commit('setPerPage', parseInt(perPage.target.value))
  dispatch('getTaxes')
}

export function setPagePagination ({ commit, dispatch }, page) {
  commit('setPage', parseInt(page))
  dispatch('getTaxes')
}


/* export async function imagen({ commit, dispatch }, image) {

  // este metodo es el funcional para la subida de archivos desde vue a back
  let formData = new FormData();
  formData.append('image', image);

  let selected_promotion = {
    id: 1,
    name: 'sadgfklj'
  }

  await $http.post('/api/imagen',formData).then(()=>{
  })
} */



