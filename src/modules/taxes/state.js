export default {
  taxes: [],
  selectedTax: {},
  action: '',
  isDisabled: false,
  params: {
    page: 1,
    perPage: 50,
    filter: ''
  }
}
