import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";

export async  function getReport({ state }) {
    await $http({
        url: '/api/informe-terceros',
        params: state.params,
        method: 'GET',
        responseType: "blob",
    }).
        then(response => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement("a");
            link.href = url;
            let date=Date();
            link.setAttribute("download", `INFORME-TERCEROS-${date.toString()}.xlsx`);
            document.body.appendChild(link);
            link.click();
        }).catch(error => {
            Notification.error({
                title: "Error!",
                message: error.message,
                type: "error",
                duration: 3000,
                customClass: "notification-box"
            });
        });
}