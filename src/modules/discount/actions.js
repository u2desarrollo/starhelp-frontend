import $http from "../../axios"
import { Notification, MessageBox } from 'element-ui'


export async function fetchContacts ({ commit, state, rootState }) {

    let params = {
      params: state.params_contacts
    }
  
    await $http.get('/api/terceros', params)
    .then((response) => {
        commit('setContacts', response.data.data)
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function fetchLines ({ commit, state }) {
  
    await $http.get('/api/lista-lineas')
    .then((response) => {
        commit('setLines', response.data)
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function fetchBrands ({ commit, state }) {
  
    await $http.get('/api/lista-marcas')
    .then((response) => {
        commit('setBrands', response.data)
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function createDiscount ({ commit, state }, params) {
  
    await $http.post('/api/crear-descuento', params)
    .then((response) => {        
        commit('setDiscount', response.data)
        Notification.success({
            title: 'Hecho!',
            message: 'Se ha registrado correctamente',
            type: 'success',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function fetchDiscounts ({ commit, state }) {
  
    await $http.get('/api/lista-descuentos')
    .then((response) => {
        commit('setDiscounts', response.data.data)
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function fetchDiscount ({ commit, state }, id) {
  
    await $http.get(`/api/descuento/${id}`)
    .then((response) => {
        commit('setDiscount', response.data)
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function updateDiscount ({ commit, state }, params) {
  
    await $http.put('/api/actualizar-descuento', params)
    .then((response) => {
        Notification.success({
            title: 'Hecho!',
            message: 'Se ha actualizado correctamente',
            type: 'success',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function deleteDiscount ({ commit, state }, id) {    
    await $http.delete('/api/eliminar-descuento/'+id, )
    .then((response) => {        
        Notification.success({
            title: 'Hecho!',
            message: 'Se ha Eliminado correctamente',
            type: 'success',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function fetchParameters ({ commit }, id) {
    await $http.get(`/api/parametros?paramtable_id=5`)
        .then((response) => {
            commit('setContacts', response.data.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 2000,
                customClass: 'notification-box',
            });
        })
}
