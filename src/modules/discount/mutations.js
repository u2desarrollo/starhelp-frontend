export function setContacts (state, contacts) {
    state.contacts = contacts
}
export function setParamsContacts (state, filter) {
    state.params_contacts.filter = filter
}
export function setLines (state, lines) {
    state.lines = lines 
}
export function setBrands (state, brands) {
    state.brands = brands 
}
export function setDiscount (state, discount) {
    state.discount = discount 
}
export function setDiscounts (state, discounts) {
    state.discounts = discounts 
}