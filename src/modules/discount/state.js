export default {
    discount: null,
    discounts: [],
    contacts: [],
    lines : [],
    brands : [],
    params_contacts: {
        filter: '',
        type: 'is_customer',
        paginate: false,
    },
    params : {
        client: null,
        line : null,
        brand: null,
        percentage: null,
    }
}