export function setNotifications(state, notifications) {
  state.notifications = notifications
}

export function addNotification(state, notification) {

  let exist = state.notifications.find(n => n.type == notification.type && n.id == notification.id)

  if (!exist) {
    state.notifications.push(notification)
  }

}

export function addNotifications(state, notifications) {
  if (notifications[0]) {
    let newNotifications = state.notifications.filter(n => n.type !== notifications[0].type)
    state.notifications = newNotifications.concat(notifications)
  }
}

export function deleteNotificationForIndex(state, index) {
  if (state.notifications[index]) {
    state.notifications.splice(index, 1)
  }
}
