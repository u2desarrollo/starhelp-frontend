import $http from '../../axios'
import {getApiDomain} from "../../util/domains";

/**
 * Obtiene los documentos pendientes por autorizar
 * @param commit
 * @returns {Promise<void>}
 */
export async function getDocumentsToAuthorize({commit, state}, notificationDesktopShow = true) {
  await $http.get('/api/documentos-por-autorizar')
    .then((response) => {

      let currentNotifications = state.notifications.filter(n => n.type === 'authorize_document')

      let notifications = []
      response.data.data.map((document) => {
        let notification = {
          id: document.id,
          title: 'AUTORIZACIÓN DE DOCUMENTOS',
          text: [
            {key: 'Fecha', value: document.document_date},
            {key: 'Documento', value: document.template.description + ' - ' + document.branchoffice.name},
            {key: 'Tercero', value: document.contact.name + ' ' + document.contact.surname},
            {key: 'Solicitado por', value: document.user.name + ' ' + document.user.surname}
          ],
          type: 'authorize_document',
          url: '/documentos-por-autorizar'
        }
        notifications.push(notification)
      })

      commit('addNotifications', notifications)

      if (response.data.data.length > currentNotifications.length) {
        if (notificationDesktopShow) {
          if (Notification.permission !== 'granted') {
            Notification.requestPermission()
          } else {
            let notification = new Notification('Documentos por autorizar',
              {
                icon: 'static/favicon.png',
                body: 'Tienes ' + notifications.length + ' documentos por autorizar'
              }
            )
            notification.onclick = () => {
              window.open(getApiDomain() + '/#/documentos-por-autorizar')
            }
          }
        }

      }

    })
}

/**
 * Obtiene nuevos jobs
 * @param commit
 * @returns {Promise<void>}
 */
export async function getJobs({commit, state}, notificationDesktopShow = true) {

  await $http.get('/api/tareas-usuario')
    .then((response) => {

      let notifications = []
      response.data.data.map((job) => {
        let notification = {
          id: job.id,
          title: job.name,
          text: [
            {key: 'Creado por:', value: job.user_creation.name + ' - ' + job.user_creation.surname},
            {key: 'Fecha de creación', value: job.created_at},
            {key: 'Fecha limite', value: job.limit_date}
          ],
          type: 'job',
          url: '/job/' + job.id
        }

        notifications.push(notification)
      })

      commit('addNotifications', notifications)
    })
}
