export function setwallet(state, wallet){
    state.wallet = wallet
}
export function settransactions_wallet(state, transactions_wallet){
    state.transactions_wallet = transactions_wallet
}
export function setWalletType(state, wallet_types){
    state.wallet_types = wallet_types
}
export function setParamsWalletType(state, wallet_type){
    state.params.wallet_type = wallet_type
}