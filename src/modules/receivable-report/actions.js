import $http from "../../axios";
import { Notification } from "element-ui";


export async function getWallet({ state, commit }, params) {
  state.params.seller_id = params.seller_id
  state.params.contact_id = params.contact_id
  state.params.wallet_type = params.wallet_type
  await $http.get("/api/consulta-cartera", {params: state.params})
    .then(response => {
      commit('setwallet', response.data)
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
  state.loading.wallet = false;
}

export async function fetchWalletType(
  { commit, state },
) {
  await $http
    .get("/api/parametros-por-tabla/100")
    .then(response => {
      commit("setWalletType", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function getTransactions({ state, commit }, id) {
  await $http.get("/api/transacciones-cartera/"+id, {params: state.params})
    .then(response => {
      console.log(response);
      commit('settransactions_wallet', response.data.data)
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}