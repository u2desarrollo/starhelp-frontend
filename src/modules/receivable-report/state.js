export default {
    params: {
      wallet_type : null,
      year_month: null,
      contact_id : null
    },
    ag_grid:{
      selectedTheme:'ag-theme-balham'
    },
    wallet:[],
    transactions_wallet:[],
    wallet_types:[],
    loading:{
      preloader:false,
      wallet:true,
    }
  };
  