import defaultSelectedVisit from './state'

export function setSellers(state, sellers) {
	state.sellers = sellers
}

export function sellerError(state, payload) {
	state.error = true
	state.errorMessage = payload
	state.sellers = []
}

export function visitError(state, payload) {
	state.error = true
	state.errorMessage = payload
	state.visits = []
}

export function setFilterDate(state, date) {
	state.params.filter_date = date
}
