import $http from "../../axios";
import { Notification } from 'element-ui'

export async function fetchSellers({ commit, state }, requestParameters = { params: state.params }) {
	await $http.get('/api/estadisticas-vendedores', requestParameters).then((response) => {
		commit('setSellers', response.data)
	}).catch((error) => {
		Notification.error({
			title: '¡Error!',
			message: error.message,
			type: 'error',
			duration: 1500,
			customClass: 'notification-box'
		})
		commit('sellerError', error.message)
	})
}

export async function fetchVisits({ commit, state }) {
	await $http.get('/api/seguimiento-vendedores').then((response) => {
		commit('setSellers', response.data.data)
	}).catch((error) => {
		Notification.error({
			title: '¡Error!',
			message: error.message,
			type: 'error',
			duration: 1500,
			customClass: 'notification-box'
		})
		commit('visitError', error.message)
	})
}

/**
 *
 * @returns {Promise<void>}
 */
export async function reportGenerate({state, commit}) {
  // commit('setPreloader', true)
  await $http({
    url: "/api/reportes/estadisticas-vendedores",
    method: "GET",
    responseType: "blob",
    params: state.params
  })
    .then(response => {
       const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      console.log(url);
      link.setAttribute("download", "Visitas vendedores.xlsx");
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 3000,
        customClass: "notification-box"
      });
    });
  // commit('setPreloader', false)
}
