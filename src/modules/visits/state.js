export const defaultSelectedVisit = () => {
	return {
		seller_id: '',
		contact_id: '',
		contact_warehouse_id: '',
		visit_type: '',
		longitude_checkin: '',
		latitude_checkin: '',
		latitude_checkout: '',
		longitude_checkout: '',
		motive: [],
		test: '',
		distance: '',
		checkin_date: '',
		checkout_date: '',
		observation: '',
		photo: '',
		checkout_ok: '',
		range_date: []
	}
}

let date = new Date();
let month=date.getMonth()+1;
let dateOne=date.getFullYear()+'-'+month+'-01';
let dateTwo=date.getFullYear()+'-'+month+'-'+date.getDate();

export default {
	visits: [],
	sellers: [],
	selectedVisit: defaultSelectedVisit(),
	error: false,
	errorMessage: '',
	params: {
		paginate: true,
		page: 1,
		perPage: 15,
		filter: '',
		sortBy: 'seller_id',
		sort: 'ASC',
		filter_date: [dateOne,dateTwo]
	}
}
