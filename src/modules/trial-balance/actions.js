import $http from "../../axios";
import {
    Notification,
    MessageBox
} from 'element-ui';

//Obtener las Levels
export async function fetchLevels(
  { commit, state },
) {
  await $http
    .get("/api/parametros-por-tabla/98")
    .then(response => {
      commit("setLevels", sortJSON(response.data, 'code_parameter', 'asc'));
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

//cuentas para select Desde
export async function fetchAccountsFrom(
    { commit, state },
    requestParameters = { params: state.params_accounts_from }
) {
    await $http
        .get("/api/cuentas", requestParameters)
        .then(response => {
            commit("setAccountsFrom", response.data.data.data);
        })
        .catch(error => {
            Notification.error({
                title: "Error!",
                message: error.message,
                type: "error",
                duration: 1500,
                customClass: "notification-box"
            });
            commit("modelError", error.message);
        });
}

//cuentas para select Hasta
export async function fetchAccountsUp(
    { commit, state },
    requestParameters = { params: state.params_accounts_up }
) {
    await $http
        .get("/api/cuentas", requestParameters)
        .then(response => {
            commit("setAccountsUp", response.data.data.data);
        })
        .catch(error => {
            Notification.error({
                title: "Error!",
                message: error.message,
                type: "error",
                duration: 1500,
                customClass: "notification-box"
            });
            commit("modelError", error.message);
        });
}

export async function fetchContactWarehouses(
    { commit, state },
    requestParameters = { params: state.params_contact_warehouse }
) {
    await $http
        .get("/api/sucursales", requestParameters)
        .then(response => {
            console.log(response );
            commit("setContactWarehouses", response.data);
        })
        .catch(error => {
            Notification.error({
                title: "Error!",
                message: error.message,
                type: "error",
                duration: 1500,
                customClass: "notification-box"
            });
            commit("modelError", error.message);
        });
}


// obtener Data libro Contable
export async function fetchTrialBalance({commit, state}) {
    await $http.get('/api/trial-balance', { params : state.params})
        .then((response) => {
            console.log(response);
            commit('settrial_balance', response.data.data);
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: 'Error',
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}

function sortJSON(data, key, orden) {
    return data.sort(function (a, b) {
        var x = a[key],
        y = b[key];

        if (orden === 'asc') {
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        }

        if (orden === 'desc') {
            return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        }
    });
}