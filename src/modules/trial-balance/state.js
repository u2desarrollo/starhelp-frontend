export default {
    loading: {
        module: true
    },
    params: {
        year_month: null,
        account_from: null,
        account_up: null,
        description_account_from: null,
        description_account_up: null,
        view_delete_from: null,
        view_delete_up: null,
        filter_text: ""
    },
    params_accounts_from: {
        notAux: false,
        onlyAux: false,
        paginate: true,
        page: 1,
        perPage: 30,
        filter: "",
        sortBy: "code",
        sort: "ASC"
    },
    params_accounts_up: {
        notAux: false,
        onlyAux: false,
        paginate: true,
        page: 1,
        perPage: 30,
        filter: "",
        sortBy: "code",
        sort: "ASC"
    },
    accounts_from: [],
    accounts_up: [],
    auxiliary_book:[],
    trial_balance:[],
    ag_grid:{
        selectedTheme:'ag-theme-balham'
    },
    iconAcordion: "ti-angle-down",
}