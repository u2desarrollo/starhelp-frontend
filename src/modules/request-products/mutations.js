import { defaultSelectedRequesProduct } from "./state";
export function setRequestsProduct (state, requestProducts) {
  state.requestProducts = requestProducts;
}
export function requestProductsError (state, payload) {
  state.error = true
  state.errorMessage = payload
  state.requestProducts = []
}
export function setRequestProduct (state, requestProduct) {
  state.selectedRequestProduct = requestProduct;
}

export function setProducts (state, products) {
  state.selectedRequestProduct.request_products = products;
}

export function setContactWarehouses (state, contactWarehouse) {
  state.contactsWarehouse = contactWarehouse;
}
export function setRequestProductwithName (state, requestProduct) {
  let $array = requestProduct.request_products;
  requestProduct.request_products = [];
  $array.forEach(element => {
    element.description = element.product.description;


    requestProduct.request_products.push(element);
  });
  state.selectedRequestProduct = requestProduct;
}


export function resetdefaultSelectedRequesProduct (state) {
  let contact_id = state.selectedRequestProduct.contacts_warehouse_id;
  state.selectedRequestProduct = defaultSelectedRequesProduct()
  if (state.resetForm) {
    state.selectedRequestProduct.contacts_warehouse_id = contact_id;
  }
}


export function setFormEditable (state, params) {
  state.formEditable = params;
}


export function setContactsWarehouseId (state, contactWarehouseId) {
  state.selectedRequestProduct.contacts_warehouse_id = contactWarehouseId;
}

export function setResetForm (state, resetForm) {
  state.resetForm = resetForm;
}

export function setParamRole (state, role) {
  state.paramsRP.role = role;
}


export function setParamTypeUser (state, typeUser) {
  state.paramsRP.typeUser = typeUser;
}


export function setTypeUser (state, typeUser) {
  state.typeUser = typeUser;
}

export function setRenderB2B (state, renderB2B) {
  state.renderB2B = renderB2B;
}

export function setContactId (state, contactId) {
  state.selectedRequestProduct.contacts_id = contactId;
}

export function setParamContactId (state, contactId) {
  state.paramsRP.contact_id = contactId;
}

export function setParamWarehouse (state, warehouse) {
  state.paramsRP.warehouse = warehouse;
}
