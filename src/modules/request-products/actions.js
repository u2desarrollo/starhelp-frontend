import $http from "../../axios"
import { Notification, MessageBox } from 'element-ui'

export async function fetchRequestHeaders ({ commit, state }) {
  await $http.get('api/request_headers').then((res) => {
    commit('setRequestsProduct', res.data);

  }).catch((error) => {
    Notification.error({
      title: '¡Error!',
      message: error.message,
      type: 'error',
      duration: 1500,
      customClass: 'notification-box'
    })
    commit('requestProductsError', error.message)
  });
}
export async function addRequestProduct ({ commit, state }) {
  await $http.post('/api/insert-request', state.selectedRequestProduct)
    .then((response) => {
      Notification.success({
        title: 'Exito!',
        message: 'Solicitud registrada correctamente',
        type: 'success',
        duration: 1500,
        customClass: 'notification-box',
      });
      commit('setRequestsProduct', response.data.data)
      state.error = false
    })

    .catch((error) => {
      MessageBox.alert(error.message, 'Error nada', {
        dangerouslyUseHTMLString: true,
        confirmButtonText: 'Aceptar',
        type: 'error',
        customClass: 'notification-error'
      })
      commit('requestProductsError', error.message)
    })
}


export async function getContactsWarehouses ({ commit, state }, params) {
  let p = { params: params }
  await $http.get('/api/cwarehouses', p).then(
    (response) => {
      commit('setContactWarehouses', response.data.data);
    }
  )
}


export async function getRequestHeraderBiId ({ state, commit }, id) {
  await $http.get(`/api/request_headers/${id}`).then((response) => {
    commit('setRequestProductwithName', response.data.data);
  })
}

export async function contactWarehousesBiId ({ state, commit }, params) {
  await $http.get(`/api/establecimientos/${params}`).then((response) => {
    commit('setContactWarehouses', response.data.data);
  })
}

export async function contactWarehousesBiContact ({ state, commit }) {
  await $http.get(`/api/establecimientos/ids`, { params: state.paramsRP }).then((response) => {
    commit('setParamWarehouse', response.data.data);
  })
}


export async function getByIdArray ({ commit, state }) {

  await $http.get('/api/establecimientos/array', { params: state.paramsRP }).then(
    (response) => {
      commit('setContactWarehouses', response.data);
    }
  )
}
