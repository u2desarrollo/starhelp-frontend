export const defaultSelectedRequesProduct = () => {
  return {
    id: '',
    contact_id: '',
    contacts_warehouse_id: '',
    request_date: '',
    request_type: '',
    maintenance_type: '',
    units: '',
    direc_observation: '',
    customer_observation: '',
    provider_observation: '',
    request_products: []
  }
}
export default {
  requestProducts: [],
  contactsWarehouse: [],
  formEditable: false,
  error: false,
  renderB2B: false,

  errorMessage: '',
  resetForm: false,
  selectedRequestProduct: defaultSelectedRequesProduct(),
  action: 'create',
  paramsRP: {
    typeUser: '',
    role: 33,
    warehouse: null,
    contact_id: null,
  }

}
