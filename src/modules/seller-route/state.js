export default {
    sellers: [],
    dates:[],
    coordinates:[],
    visits:[],
    show_current_location:false,
    params:{
        seller:null,
        date:null,
        duration:2000,
        center:false,
    }
}
