import defaultSelectedVisit from './state'
export function setSellers(state, sellers) {
    state.sellers = sellers
}

export function setdates(state, dates) {
    state.dates = dates
}
export function setshow_current_location(state, show_current_location){
    state.show_current_location = show_current_location
}

export function setCoordinates(state, coordinates){
    state.coordinates = coordinates
}

export function setVisits(state, visits){
    state.visits = visits
}