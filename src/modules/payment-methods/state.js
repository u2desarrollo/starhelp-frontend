
export const defaultPaymentMethods = () => {
    return {
        code: null,
        description: null,
        accounting_account: null,
        commission_percentage: null,
        user_id: null
    };
};


export default {

    selectedPaymentMethods: defaultPaymentMethods(),
    params: {
        perPage: 50,
        page: 1
    },
    disabledInputs: false,
    actionForm:'create',
    paymentMethods:[]
}
