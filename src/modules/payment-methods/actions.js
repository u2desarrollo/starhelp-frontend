import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";


export async  function setPerPagePagination({ commit, dispatch }, perPage) {
    commit("setPerPage", parseInt(perPage.target.value));
    dispatch("getPaymentMethods");

  }

  export async function setPagePagination({ commit, dispatch }, page) {
    commit("setPage", parseInt(page));
    dispatch("getPaymentMethods");
  }

  export async function getPaymentMethods({state,commit}){
      await $http.get('/api/payment-methods',{params:state.params}).then(response=>{

           commit('setPaymentMethods',response.data.data);
      }).catch(error=>{})
  }

  export async function createPaymentMethod({state,commit}){
    $http.post('/api/payment-methods',state.selectedPaymentMethods).then(response=>{
      Notification.success({
        title: "Exito!",
        message: "La Forma de Pago Ha Sido Creada  Correctamente",
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });

    }).catch(error=>{
      Notification.error({
        title: "error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    })
  }


  export async  function getPaymentMethodById( {commit}, id) {
    await $http.get(`/api/payment-methods/${id}`).then(response=>{
      commit('setSelectedPaymentMethods',response.data.data);
    }).catch(error=>{
      Notification.error({
        title: "error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });

  }

  export async function updatePaymentmethod({state}){
    $http.put(`/api/payment-methods/${state.selectedPaymentMethods.id}`, state.selectedPaymentMethods).then(resposnse=>{
      Notification.success({
        title: "Exito!",
        message: 'Se ha Actualizado Correctamente la Forma de Pago',
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });
    }).catch(error=>{
      Notification.error({
        title: "error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });

  }

  export async function deletePaymentMethodById({},id) {
   await  $http.delete(`/api/payment-methods/${id}`).then(response=>{
    Notification.success({
      title: "Exito!",
      message: 'Se ha Eliminado Correctamente la Forma de Pago',
      type: "success",
      duration: 2000,
      customClass: "notification-box"
    });
    }).catch(error=>{
      Notification.error({
        title: "error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
  }

