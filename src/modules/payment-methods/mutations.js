import { defaultPaymentMethods } from "./state";

export function setPerPage(state, perPage) {
    state.params.page = 1;
    state.params.perPage = perPage;
}

export function setPage(state, page) {
    state.params.page = page;
}


export function clearSelectedModel(state) {
    state.selectedPaymentMethods = defaultPaymentMethods();
}

export function setActionForm(state, action) {
    state.actionForm = action;
}

export function setdisabledInputs(state, disabled) {
    state.disabledInputs = disabled;
}


export function setPaymentMethods(state, paymentMethods){
    state.paymentMethods=paymentMethods;
}


export function setSelectedPaymentMethods(state, selectedPaymentMethods){
   state.selectedPaymentMethods=selectedPaymentMethods;
}