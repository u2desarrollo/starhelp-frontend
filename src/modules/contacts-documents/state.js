export const defaultSelectedDocument = () => {
  return {
    contact_id: null,
    document_type_id: '',
    action: 'n',
    name: '',
    observation: '',
    file: null
  }
}

export default {

  documents: [],
  selectedDocument: defaultSelectedDocument(),
  actionDocument: 'create',
  documentsTypes: [],

}
