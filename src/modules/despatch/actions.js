import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";
import { map } from "d3";

/*export async function fetchDataSheets({
    commit,
    state
}) {


}*/
export async function fetchDespatchInvoice({ commit, state }) {
  let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice);
  let selected_branchoffice_warehouse = JSON.parse(
    localStorage.selected_branchoffice_warehouse
  );

  // Loading
  state.tableDespatch.loading = true;

  // Definimos las opciones
  let params = {
    params: {
      page: state.tableDespatch.options.currentPage,
      perPage: state.tableDespatch.options.perPage,
      search: state.tableDespatch.options.search,
      //date': null,
      zone_id: state.tableDespatch.options.zone_id,
      contact_id: state.tableDespatch.options.contact_id,
      state: state.tableDespatch.options.state,
      branchoffice_id: selected_branchoffice,
      branchoffice_warehouse_id: selected_branchoffice_warehouse,
      model_id: 1,
      guide: state.tableDespatch.options.guide,
      protractor : state.tableDespatch.options.protractor,
      consecutive : state.tableDespatch.options.consecutive,
    }
  };
  if (state.tableDespatch.options.date.length > 0) {
    params.params.fromDate = state.tableDespatch.options.date[0];
    params.params.toDate = state.tableDespatch.options.date[1];
  }

  await $http
    .get(`/api/lista-despachos/`, params)
    .then(response => {
      commit("setColumnTableDespatch", params.params.state == 1000);
      commit("setDespatchInvoiceTable", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });

  state.tableDespatch.loading = false;
}

//Obtener los parametros de una tabla
export async function fetchParametersZone({ commit, rootState }) {
  await $http
    .get(`/api/parametros?paramtable_id=6`)
    .then(response => {
      commit("setParametersZone", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

//Obtener los transportadores
export async function fetchConveyor({ commit, rootState }) {
  await $http
    .get(`/api/lista-transportadores`)
    .then(response => {
      commit("setconyevors", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function updateDespatchInvoice({ commit, state, dispatch }, data) {
  await $http
    .put(`/api/despachos/${data.id}`, data)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "Registro actualizado.",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      // dispatch('fetchDespatchInvoice')
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al actualizar",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

// Actualizamos el registro.
export async function updateDespatchStatus({ commit, state, dispatch }) {
  var despatch = state.tableDespatch.data.filter(value => {
    return value.pack_off;
  });

  var productLot = state.tableDocumentsProductsLot.data;
  var send = {
    despatch,
    productLot
  };

  send.give_back = state.tableDespatch.options.state === null ? false : true;

  try {
    var response = await $http.put(`/api/despachos-masivo`, send);
    Notification.success({
      title: "Exito!",
      message: response.data,
      type: "success",
      duration: 1500,
      customClass: "notification-box"
    });
    dispatch("fetchDespatchInvoice");
  } catch (error) {
    Notification.error({
      title: "Error!",
      message: "Error al actualizar",
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  }
}

// Actualizamos el registro.
export async function updateDespatchStatusOA({ commit, state, dispatch }) {
  var despatch = state.tableDespatch.data.filter(value => {
    return value.pack_off;
  });

  var productLot = state.tableDocumentsProductsLot.data;
  var send = {
    despatch,
    productLot
  };

  send.give_back = state.tableDespatch.options.state === null ? false : true;
  send.admin_operations = true;

  try {
    var response = await $http.put(`/api/despachos-masivo`, send);
    Notification.success({
      title: "Exito!",
      message: response.data,
      type: "success",
      duration: 1500,
      customClass: "notification-box"
    });
    dispatch("fetchDespatchInvoice");
  } catch (error) {
    Notification.error({
      title: "Error!",
      message: "Error al actualizar",
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  }
}

export async function fetchContacts({
  commit,
  state
}, requestParameters = {
  params: state.paramsquery
}) {

  await $http.get('/api/terceros', requestParameters)
      .then((response) => {
          commit('setContacts', response.data.data.data)

      })
      .catch((error) => {
        console.log(error);
          Notification.error({
              title: 'Error!',
              message: 'Error',
              type: 'error',
              duration: 1500,
              customClass: 'notification-box',
          });
          // commit('contactError', error.message)
      })
}

export async function generatePdf({ commit, state }) {
  let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice);
  let selected_branchoffice_warehouse = JSON.parse(
    localStorage.selected_branchoffice_warehouse
  );
  var despatch = state.tableDespatch.data.filter(value => {
    return value.pack_off;
  });

  var validate_pro = 0;
  var validate = false;
  var row;

  despatch.forEach(element => {
    if (element.documents_information != null) {
      if (element.documents_information.protractor == null) {
        validate_pro++;
      }
    }
  });

  despatch = despatch.map(function(item) {
    return item.id;
  });
  var send = {
    // date: state.tableDespatch.options.date,
    state: state.tableDespatch.options.state,
    fromDate: state.tableDespatch.options.date[0],
    toDate: state.tableDespatch.options.date[1],
    branchoffice_id: selected_branchoffice,
    model_id: 1,
    selected_branchoffice_warehouse: selected_branchoffice_warehouse,
    despatch
  };

  if (validate_pro > 0) {
    await MessageBox.alert(
      "Hay facturas sin Transportador. ¿Desea Continuar?",
      "Error",
      {
        // dangerouslyUseHTMLString: true,
        confirmButtonText: "Continual",
        cancelButtonText: "Cancelar",
        type: "warning"
        // customClass: 'notification-error'
      }
    )
      .then(() => {
        validate = true;
      })
      .catch(() => {
        validate = false;
      });
  } else {
    validate = true;
  }

  if (validate) {
    try {
      var response = await $http.get(`/api/pdfdespatch`, {
        params: send,
        responseType: "blob"
      });

      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "Transportador.pdf"); //or any other extension
      document.body.appendChild(link);
      link.click();
    } catch (error) {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    }
  }
}

export async function generatePickin({ commit, state }) {
  let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice);
  let selected_branchoffice_warehouse = JSON.parse(
    localStorage.selected_branchoffice_warehouse
  );
  var despatch = state.tableDespatch.data.filter(value => {
    return value.pack_off;
  });
  despatch = despatch.map(function(item) {
    return item.id;
  });
  var send = {
    // date: state.tableDespatch.options.date,
    state: state.tableDespatch.options.state,
    fromDate: state.tableDespatch.options.date[0],
    toDate: state.tableDespatch.options.date[1],
    branchoffice_id: selected_branchoffice,
    model_id: 1,
    selected_branchoffice_warehouse: selected_branchoffice_warehouse,
    despatch
  };

  try {
    var response = await $http.get(`/api/pdfpickin`, {
      params: send,
      responseType: "blob"
    });

    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement("a");
    link.href = url;
    link.setAttribute("download", "pickin.pdf"); //or any other extension
    document.body.appendChild(link);
    link.click();
  } catch (error) {
    Notification.error({
      title: "Error!",
      message: error.message,
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  }
}

//Actualizar un registro uno a uno.
export async function updateDespatchStatusOne(
  { commit, state, dispatch },
  data
) {
  try {
    var response = await $http.put(`/api/despachos-cambiar-estado/${data.id}`, {
      state: data.state
    });
    Notification.success({
      title: "Exito!",
      message: response.data,
      type: "success",
      duration: 1500,
      customClass: "notification-box"
    });
    dispatch("fetchDespatchInvoice");
  } catch (error) {
    Notification.error({
      title: "Error!",
      message: "Error al actualizar",
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  }
}

export async function updatelocation({ state }) {
  let selected_branchoffice_warehouse = JSON.parse(
    localStorage.selected_branchoffice_warehouse
  );

  var params = {
    branchoffice_warehouse: selected_branchoffice_warehouse,
    product: state.params.product,
    location: state.params.location
  };

  try {
    var response = await $http.put(`/api/actualizar-localizacion`, params);
    if (response.data.status) {
      Notification.success({
        title: "Exito!",
        message: response.data.message,
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    } else {
      Notification.error({
        title: "Error!",
        message: response.data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    }
  } catch (error) {
    Notification.error({
      title: "Error!",
      message: "Error al actualizar",
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  }
}

export async function generateexcel({ commit, state }) {
  let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice);
  let selected_branchoffice_warehouse = JSON.parse(
    localStorage.selected_branchoffice_warehouse
  );

  // Definimos las opciones
  let params = {
    params: {
      page: state.tableDespatch.options.currentPage,
      // perPage: state.tableDespatch.options.perPage,
      search: state.tableDespatch.options.search,
      //date': null,
      zone_id: state.tableDespatch.options.zone_id,
      contact_id: state.tableDespatch.options.contact_id,
      state: state.tableDespatch.options.state,
      branchoffice_id: selected_branchoffice,
      branchoffice_warehouse_id: selected_branchoffice_warehouse,
      model_id: 1,
      guide: state.tableDespatch.options.guide,
      protractor : state.tableDespatch.options.protractor,
      perPage: state.tableDespatch.pagination.total,
    }
  };
  if (state.tableDespatch.options.date.length > 0) {
    params.params.fromDate = state.tableDespatch.options.date[0];
    params.params.toDate = state.tableDespatch.options.date[1];
  }

  await $http({
    url: `/api/exceldespachos`,
    method: "GET",
    params: params.params,
    responseType: "blob" // important
  })
    .then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "despachos.csv"); //or any other extension
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function generateexcelPickin({ commit, state }) {
  let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice);
  let selected_branchoffice_warehouse = JSON.parse(
    localStorage.selected_branchoffice_warehouse
  );
  var despatch = state.tableDespatch.data.filter(value => {
    return value.pack_off;
  });
  despatch = despatch.map(function(item) {
    return item.id;
  });
  var send = {
    // date: state.tableDespatch.options.date,
    state: state.tableDespatch.options.state,
    fromDate: state.tableDespatch.options.date[0],
    toDate: state.tableDespatch.options.date[1],
    branchoffice_id: selected_branchoffice,
    model_id: 1,
    selected_branchoffice_warehouse: selected_branchoffice_warehouse,
    despatch
  };

  await $http({
    url: `/api/excelpickin`,
    method: "GET",
    params: send,
    responseType: "blob" // important
  })
    .then(response => {

      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "pickin.csv"); //or any other extension
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}


export async function printLabels_({ commit, state }, link) {
  let selected_branchoffice = JSON.parse(localStorage.selected_branchoffice);
  let selected_branchoffice_warehouse = JSON.parse(
    localStorage.selected_branchoffice_warehouse
  );
  var despatch = state.tableDespatch.data.filter(value => {
    return value.pack_off;
  });
  despatch = despatch.map(function(item) {
    return item.id;
  });
  let params = {
    params: {
      page: state.tableDespatch.options.currentPage,
      perPage: state.tableDespatch.options.perPage,
      search: state.tableDespatch.options.search,
      //date': null,
      zone_id: state.tableDespatch.options.zone_id,
      contact_id: state.tableDespatch.options.contact_id,
      state: state.tableDespatch.options.state,
      branchoffice_id: selected_branchoffice,
      branchoffice_warehouse_id: selected_branchoffice_warehouse,
      model_id: 1,
      guide: state.tableDespatch.options.guide,
      protractor : state.tableDespatch.options.protractor,
      consecutive : state.tableDespatch.options.consecutive,
      link : link,
      despatch
    }
  };
  if (state.tableDespatch.options.date.length > 0) {
    params.params.fromDate = state.tableDespatch.options.date[0];
    params.params.toDate = state.tableDespatch.options.date[1];
  }

  await $http({
    url: `/api/excel-etiquetas`,
    method: "GET",
    params: params.params,
    responseType: "blob" // important
  })
    .then(response => {

      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "Impresion-Stickers.csv"); //or any other extension
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
