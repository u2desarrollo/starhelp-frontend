export function setDespatchInvoiceList(state, data) {
  state.despatchInvoiceList = data;
}

export function setDespatchInvoiceTable(state, data) {
  data.data.map((val, index_val) => {
    val.pack_off = false;

    // Recorremos los productos de la factura
    val.documents_products.map((dp, index_dp) => {
      dp.products_lots = {
        id: null,
        documents_products_id: dp.id,
        num_lot: null,
        expiration_date: null
      };
    });
  });

  // Agregamos los datos de la tabla
  state.tableDespatch.data = data.data;

  // Quitamos los valores de la tabla del arreglo
  delete data.data;

  // Agregamos los datos de la paginacion
  state.tableDespatch.pagination = data;
}

export function setParametersZone(state, data) {
  state.contactZone = data;
}

export function setconyevors(state, conyevors) {
  state.conyevors = conyevors;
}

export function setDocumentsProductsLot(state, data) {
  // Vaciamos el contenido
  state.tableDocumentsProductsLot.data = [];

  // Recorremos los datos de despachos
  state.tableDespatch.data.map((d, index) => {
    // Recorremos los productos de la factura
    d.documents_products.map((dp, index) => {
      // Validamos si existe
      var find = state.tableDocumentsProductsLot.data.find(
        dd => dd.id == dp.id
      );
      if (find !== undefined) return false;
      if (dp.product.line.line_code != "1155") return false;
      if (!d.pack_off) return false;

      dp.products_lots.num_lot = null;
      state.tableDocumentsProductsLot.data.push(dp);
    });
  });

  // Recorremos el objeto para agregar las marcas
  state.tableDocumentsProductsLot.data.map((dpl, index_dpl) => {
    var find = state.tableDocumentsProductsLot.brands.find(
      db => db.id == dpl.product.brand.id
    );
    if (find !== undefined) return;

    state.tableDocumentsProductsLot.brands.push({
      id: dpl.product.brand.id,
      description: dpl.product.brand.description
    });
  });
}

export function setConsecutiveProductLot(state, data) {
  // Variables
  var consecutive = Number(
    state.tableDocumentsProductsLot.dataFilter[0].products_lots.num_lot
  );

  // Validamos que el indice sea 0
  if (consecutive == 0) return;

  // Realizamos el sonsecutivo
  for (let i = 0; i < state.tableDocumentsProductsLot.dataFilter.length; i++) {
    state.tableDocumentsProductsLot.dataFilter[
      i
    ].products_lots.num_lot = consecutive;
    consecutive++;
  }
}

export function setExpirationDateProductLot(state) {
  // Variables
  var expiration_date =
    state.tableDocumentsProductsLot.dataFilter[0].products_lots.expiration_date;

  // Validar
  if (expiration_date == null || expiration_date == "") return;

  // Asignamos el valor

  state.tableDocumentsProductsLot.dataFilter.map((df, index) => {
    df.products_lots.expiration_date = expiration_date;
  });
}

export function searchProductLotByBrand(state, e) {
  /*state.tableDocumentsProductsLot.data.map((dpl, index_dpl) => {
        dpl.products_lots.num_lot = null;
    })*/

  var data = state.tableDocumentsProductsLot.data.filter((dpl, index_dpl) => {
    return dpl.product.brand.id == e;
  });

  state.tableDocumentsProductsLot.dataFilter = data;
}

export function setColumnTableDespatch(state , visible) {
  let fields = [
    {
      key: "detail",
      label: ""
    },
    {
      key: "document_date",
      label: "Fecha",
      thStyle: {
        "min-width": "100px !important"
      }
    },
    {
      key: "consecutive",
      label: "Numero Documento"
    },
    {
      key: "document_date",
      label: "Factura"
    },
    {
      key: "contact.name",
      label: "Cliente"
    },
    {
      key: "contact.address",
      label: "Direccion"
    },
    {
      key: "total_value",
      label: "Vr-total"
    },
    // {
    //   key: "warehouse.zone_despatch_id",
    //   label: "Zona",
    //   variant: "success"
    // },
    {
      key: "contact_city",
      label: "Ciudad"
    },
    {
      key: "documents_information.protractor",
      label: "Transportador",
      variant: "success"
    },
    {
      key: "documents_information.guide_number",
      label: "Numero de guia",
      variant: "success"
    },
    {
      key: "number_stickers",
      label: "Numero de cajas",
      variant: "success"
    },
    {
      key: "documents_information.delivery_observation",
      label: "Obs Despacho",
      variant: "success"
    },
    {
      key: "documents_information.invoice_filed",
      label: "Factura Radicada",
      thClass:
        visible ? "" : "d-none",
      tdClass: visible ? "" : "d-none"
    },
    {
      key: "actions",
      label: "Opciones",
      thClass:
        state.tableDespatch.options.state == "DESPACHADO" ? "" : "d-none",
      tdClass: state.tableDespatch.options.state == "DESPACHADO" ? "" : "d-none"
    }
  ];

  state.tableDespatch.fields = fields;
}

export function setDateRangeParam(state) {
  // Seteamos la fecha
  var dt = new Date();

  // Fecha Actual

  state.tableDespatch.options.date[1] = dt.toISOString().substring(0, 10);

  //Hace 30 Dias
  dt.setDate(dt.getDate() - 1);
  state.tableDespatch.options.date[0] = dt.toISOString().substring(0, 10);
}
export function setloading_print_sticker(state, loading_print_sticker) {
  state.loading_report_butons.print_sticker = loading_print_sticker
}
export function setContacts(state, contacts) {
  state.contacts = contacts;
}