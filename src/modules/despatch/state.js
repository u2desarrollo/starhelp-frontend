const tableDespatch = {
  loading: false,
  data: [],
  fields: [],
  fieldSubTable: [
    {
      key: "product.code",
      label: "Codigo"
    },
    {
      key: "product.description",
      label: "Descripción"
    },
    {
      key: "ubi",
      label: "Ubicación",
      variant: "success"
    },
    {
      key: "quantity",
      label: "Cantidad"
    },
    {
      key: "Num_-_Lote",
      label: "Num - Lote"
    },
    {
      key: "product.weight",
      label: "Peso"
    },
    {
      key: "unit_value_before_taxes",
      label: "Vr-Unitario"
    },
    {
      key: "total_value_brut",
      label: "Vr-Bruto"
    },
    {
      key: "total_value_iva",
      label: "Vr-IVA"
    },
    {
      key: "total_value",
      label: "Vr-Total"
    }
  ],
  errors: {
    has: false,
    message: "",
    code: 0,
    data: []
  },
  pagination: [],
  options: {
    zone_id: "",
    perPage: 100,
    search: "",
    currentPage: 1,
    date: [],
    state: null,
    guide: null,
    protractor:null,
    contact_id:null,
    consecutive:null


  }
};

var tableDocumentsProductsLot = {
  data: [],
  dataFilter: [],
  brands: [],
  brandsSelected: null,
  fields: [
    {
      key: "product.code",
      label: "Codigo"
    },
    {
      key: "product.description",
      label: "Descripción"
    },
    {
      key: "products_lots.num_lot",
      label: "Num Lote"
    },
    {
      key: "products_lots.expiration_date",
      label: "Fecha Vencimiento"
    }
  ]
};

export default {
  contacts:[],
  paramsquery:{
    is_customer: false,
    is_provider: false,
    is_employee: false,
    filter_customer: [],
    filter_provider: [],
    filter_employee: [],
    paginate: true,
    page: 1,
    perPage: 50,
    query: '',
    sortBy: 'id',
    sort: 'ASC',
  },
  despatchInvoiceList: [],
  action: "create",
  //ERROR STATE
  error: false,
  errorMessage: "",
  // Tabla
  conyevors: [],
  tableDespatch,
  tableDocumentsProductsLot,
  contactZone: [],
  params: {
    location: null,
    product: null
  },
  loading_report_butons:{
    print_sticker:false,
    excel_despatch:false,
    excel_pickin:false,
    pdf_pickin:false,
    pdf_proactor:false,
  }
};
