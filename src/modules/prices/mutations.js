/* export function setPrices(state,prices){
    prices.data.map(p => {
        if(!p.price.price){
            p.price.id = 0
            p.price.price = 0
            p.price.product_id = p.id
            p.price.price_list_id = 1
            p.branchoffice_id = 1
        }
    })

    state.prices=prices;
} */
export function setBranchs(state, branch) {
  state.branchs = branch;
}
export function setListPrices(state, listPrice) {
  state.listPrices = listPrice;
}
export function setPrice(state, price) {
  state.idChangedPrice = price;
}
export function setFilterPrice(state, id) {
  state.params.id = id;
}
export function setFilterPriceList(state, list) {
  state.params.list = list;
}
export function setBranchOffices(state, branchOffice) {
  state.branchOffices = branchOffice;
}
export function setBrands(state, brands) {
  state.brands = brands;
}
export function setList(state, list) {
  state.list = list.sort((a, b) => a.code_parameter - b.code_parameter);
}
export function setPrices(state, prices) {
  state.prices = prices.map(p => ({
    ...p,
    new_price: p.new_price || p.price.price
  }));
}
export function setparamsUpdatePrices(state) {
  state.paramsUpdatePrices = [];
}

export function setLines(state, lines) {
  state.lines = lines;
}
export function setMsgErrorLoadExcel(state, value) {
  state.msgErrorLoadExcel = value;
}
