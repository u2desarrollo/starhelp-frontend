export default {
  params: {
    branchOffice_id: null,
    brand: null,
    list: null,
    product: null,
    line_id: null
  },
  paramsUpdatePrices: [],
  branchOffices: [],
  brands: [],
  list: [],
  prices: [],
  lines: [],
  msgErrorLoadExcel: ""
};
