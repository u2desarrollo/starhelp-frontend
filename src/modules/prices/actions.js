import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";

export async function getPrices({ commit, state }) {
  await $http
    .get("/api/price", { params: state.params })
    .then(res => {
      commit("setPrices", res.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("priceError", error.message);
    });
}

export async function updatedPrice({ commit, state }) {
  await $http
    .put(`/api/price-update`, state.paramsUpdatePrices)
    .then(res => {
      Notification.success({
        title: "Hecho!",
        message: "Se Ha actualizado el precio",
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
export async function updatedPriceExcel({ state }, data) {
  await $http
    .put(`/api/price-update-excel`, data)
    .then(res => {
      Notification.success({
        title: "Hecho!",
        message: "Se ha cargado la informacion correctamente.",
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
export async function uploadExcel({ commit, state }, excel) {
  await $http
    .post("/api/import-excel", excel)
    .then(res => {})
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("updatePriceError", error.message);
    });
}

//Obtener las sedes
export async function fetchBranchOffices({ commit, state }) {
  await $http
    .get("/api/sedes")
    .then(response => {
      commit("setBranchOffices", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function fetchBrand({ dispatch, state, rootState, commit }) {
  await dispatch(
    "brands/fetchBrands",
    { params: { paginate: false, lines: state.params.lines } },
    { root: true }
  );
  commit("setBrands", rootState.brands.brands);
}

export async function fetchList({ commit }) {
  await $http
    .get("/api/parametros-por-tabla/14")
    .then(response => {
      commit("setList", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function fetchLines({ dispatch, state, rootState, commit }) {
  await dispatch(
    "lines/fetchLines",
    { params: { paginate: false } },
    { root: true }
  );
  commit("setLines", rootState.lines.lines);
}

export async function uploadFilePrice({ commit }, data) {
  await $http
    .post("/api/price-format-excel", data)
    .then(response => {
      commit("setPrices", response.data.data);
    })
    .catch(error => {
      commit("setMsgErrorLoadExcel", error.response.data.message);
      // Notification.error({
      //   title: "Error!",
      //   message: error.response.data.message,
      //   type: "error",
      //   duration: 2000,
      //   customClass: "notification-box"
      // });
    });
}
