import {defaultContacts} from "./state";

export function setBranchOffices(state, branchOffices){
    state.branchOffices = branchOffices;
}
export function setVouchersTypes(state, vouchertype) {
    state.voucherTypes = vouchertype
}
export function setGroups (state, groups){
    state.groups = groups
  }
export function setContacts (state, contacts){
    state.contacts = contacts
}
export function setContactsdt (state, contactsdt){
    state.contactsdt = contactsdt
}
export function setContactdt(state, contactsdt) {
    state.contactdt = contactsdt
    state.contactsdt.push(contactsdt)
}

export function setContacts_filterdt(state, contactsdt) {
    // state.contactsdt = contactsdt
    let dt = state.params.document_transactions.find(el => el.order == state.order_account_select)
    console.log(dt);
    dt.contact_select = contactsdt
    // contactsdt.map((item)=>{
    //     let validate_id = state.contactsdt.find(el => el.id == item.id)
    //     if (validate_id == undefined) {
    //         state.contactsdt.push(item)
    //     }
    // })
}

export function setTemplates(state , templates) {
    state.templates = templates
}
export function setcost_center_1 (state, cost_center_1){
    state.cost_center_1 = cost_center_1
}
export function setCities(state, cities) {
    state.cities = cities
}
export function setSellers(state, sellers) {
    state.sellers = sellers
}
export function setProjects(state, projects) {
    state.projects = projects
}
export function setProjectsdt(state, projects) {
    state.projectsdt = projects
}
export function setConsecutive(state, consecutive) {
    let consecutive_number = consecutive.consecutive_number+1
    state.params.num_doc = consecutive_number.toString()
    state.params.prefix = consecutive.prefix
    state.code_voucherType = consecutive.code_voucher_type
}
export function setselected_document(state, selected_document){
    state.selected_document = selected_document
}
export function setbranchOffice(state, branchOffice){
    state.branchOffice= branchOffice
}

export function setprojects_column_visible(state, projects_column_visible){
    state.projects_column_visible = projects_column_visible
}
export function setcontact_column_visible (state, contact_column_visible){
    state.contact_column_visible = contact_column_visible
}
export function setcost_center_column_visible (state, cost_center_column_visible){
    state.cost_center_column_visible = cost_center_column_visible
}
export function setpayment_column_visible (state, payment_column_visible){
    state.payment_column_visible = payment_column_visible
}
export function setmanage_bank_document (state, manage_bank_document){
    state.manage_bank_document = manage_bank_document
}
export function setcross_date_visible (state, cross_date_visible){
    state.cross_date_visible = cross_date_visible
}
export function setmanage_document(state, manage_document) {
    state.manage_document = manage_document
}
export function setcommissionable_concept(state, commissionable_concept) {
    state.commissionable_concept = commissionable_concept
}
export function setcommissionable_concepts(state, commissionable_concepts){
    state.commissionable_concepts = commissionable_concepts
}
export function setaccount_selects(state, account_selects){
    // state.account_selects = account_selects
    // state.account_selects = []
    let data =[]
    account_selects.map((item)=>{
        if (item.level == 24575) {
            data.push(item)
        }
    })

    state.params.document_transactions.map((item)=>{
        if (item.order == state.order_account_select) {
            item.account_select = data
        }
    })
}

export function settotal_debit(state, total_debit){
    state.total_debit = total_debit
}
export function settotal_credit(state, total_credit){
    state.total_credit = total_credit
}
export function setParamsdocumentid(state, document_transaction) {
    state.params.document_id = state.params.document_id == null ? document_transaction.document_id : state.params.document_id

    state.selected_document_title = {
        id:document_transaction.document.id,
        // in_progress:document_transaction.document.in_progress
    }

    let index = state.params.document_transactions.findIndex( element => element.order == state.params.document_transaction.order)
    state.params.document_transactions[index].id_document_transactions = state.params.document_transactions[index].id_document_transactions == null ? document_transaction.id : state.params.document_transactions[index].id_document_transactions
    // console.log(state.params.document_transactions[index].id_document_transactions);
}
export function setOrderForAccountSelectToChange(state, order) {
    state.order_account_select = order
}
export function setshowInputs(state, model_id){
    let model = state.templates.find(el => el.id == model_id)
    if (model) {
        state.showInputs.center_cost = model.select_cost_center == 1 || model.select_cost_center == 9 ? true : false
        state.showInputs.seller = model.see_seller
        state.showInputs.observation1 = model.observation_tittle_1  != null ? true : false
        state.showInputs.observation2 = model.observation_tittle_2 != null ? true : false
        state.showInputs.projects = model.projects == 1 || model.projects == 1 ? true : false
        state.showInputs.cross_doc = model.doc_cruce_management == 1 || model.doc_cruce_management == 9 ? true : false
        state.showInputs.cross_doc_date = model.crossing_document_dates == 1 || model.crossing_document_dates == 9 ? true : false
        state.cross_title = model.doc_cruce_management == 1 || model.doc_cruce_management == 9 ? model.tittle : null
        state.validations_model.cross_doc = model.doc_cruce_management
        state.validations_model.cross_doc_date = model.crossing_document_dates
    }
}
export function setaccountselect(state, account_select){
    state.account_select = account_select
}
export function setpayment_methods(state, payment_methods){
    state.payment_methods = payment_methods
}
export function setmodel_code(state , model_code){
    state.model_code = model_code
}
export function setmodel_accounting_generate(state , model_accounting_generate){
    state.model_accounting_generate = model_accounting_generate
}
export function setShowPdf(state, show_pdf){
    state.file_name = show_pdf.accounting_link
    state.show_pdf = true
}
export function setloading_form(state, loading_form){
    state.loading_form = loading_form
}
export function setloading(state, loading){
    state.loading= loading
}
export function setorigin_document(state, origin_document){
    state.origin_document = origin_document
}
export function listTransactions(state, transactions){
    state.params.document_transactions = []
    transactions.map((el, index) =>{
        state.params.document_transactions.push({
            //datos que se muetran en la grilla
            order 						: 	el.order,
            payment_method              :   null,
            account_id 					:	el.account_id,
            observation 				:	el.observations,
            contact_id 					: 	el.contact_id,
            projects_id                 :   el.projects_id,
            value 						:	el.operation_value,
            debit_credit                :   el.transactions_type,
            base_value 					: 	el.base_value,
            bank_document_number        :   el.bank_document_number,
            commissionable_concept_id   :   el.commissionable_concept_id,

            //datos para mostrar u ocultar columnas
            contact_column_visible 		: 	el.account.manage_contact_balances,
            projects_column_visible 	: 	el.account.manage_projects != 0 ? true : false,
            payment_column_visible 		: 	el.manage_way_to_pay,
            cost_center_column_visible 	: 	el.account.cost_center_1 || el.account.cost_center_2 || el.account.cost_center_3 ? true : false,
            base_value_column_visible 	: 	el.manage_base_value,
            commissionable_concept      :   el.account.commissionable_concept,
            manage_document             :   el.account.manage_document_balances,

            //datos para cargar los selects
            account_select 				:	[el.account],
            contact_select 				: 	[el.contact],

            //ids para grilla y para manejo en back(crear o actualizar)
            id_document_transactions    :   el.id,
            id                          :   index,

            // verificar
            documet_id 					: 	el.document_number,
            no_doc 						:   null,
            affects_prefix_document     :   el.affects_prefix_document,
            crossing_number             :   el.crossing_consecutive,
            crossing_date               :   el.crossing_due_date_document
        })
    })
}
export function setloading_transactions(state, loading_transactions){
    state.loading_transactions = loading_transactions;
}
export function setParamsEdit(state, doc){
    console.log(doc.modify_document_firm_date);
    if (!doc.modify_document_firm_date) {
        state.Watch_documument = !doc.modify_document_firm_date
    }
    state.edit_document = true;
    state.loading_totals = true
    state.selected_document_title.id            = doc.id
    state.selected_document_title.in_progress   = doc.in_progress
    state.params.branchoffice_id                = doc.branchoffice_id
    state.params.voucherType_id                 = doc.vouchertype_id
    state.params.num_doc                        = doc.consecutive.toString()
    state.params.prefix                         = doc.prefix
    state.params.date                           = doc.document_date
    state.params.template_id                    = doc.model_id
    state.templates                             = [doc.template]
    state.params.contact_id                     = doc.contact_id
    state.params.document_id                    = doc.id
    state.params.observation                    = doc.observation
    state.params.observation2                   = doc.observation_two
    state.params.city_id                        = doc.city
    state.params.seller_id                      = doc.seller_id
    state.sellers                               = [doc.seller]
    state.params.affects_prefix_document        = doc.affects_prefix_document
    state.params.affects_number_document        = doc.affects_number_document
    state.params.affects_date_document          = doc.affects_date_document
    state.params.due_date                       = doc.due_date
    state.params.term_days                      = doc.term_days

    state.params.documents_products = doc.documents_products;

    state.params.document_transactions = []

    doc.accounting_transactions.map((el, index) =>{
        state.params.document_transactions.push({
            //datos que se muetran en la grilla
            order 						: 	el.order,
            payment_method              :   null,
            account_id 					:	el.account_id,
            observation 				:	el.observations,
            contact_id 					: 	el.contact_id,
            projects_id                 :   el.projects_id,
            value 						:	el.operation_value,
            debit_credit                :   el.transactions_type,
            base_value 					: 	el.base_value == null ? 0 : el.base_value,
            bank_document_number        :   el.bank_document_number,
            commissionable_concept_id   :   el.commissionable_concept_id,
        
            //datos para mostrar u ocultar columnas
            projects_column_visible 	: 	el.account.manage_projects != 0 ? true : false,
            payment_column_visible 		: 	el.manage_way_to_pay,
            contact_column_visible 		: 	el.account.manage_contact_balances,
            cost_center_column_visible 	: 	el.account.cost_center_1 || el.account.cost_center_2 || el.account.cost_center_3 ? true : false,
            base_value_column_visible 	: 	el.manage_base_value,
            commissionable_concept      :   el.account.commissionable_concept,
            manage_document             :   el.account.manage_document_balances,

            //datos para cargar los selects
            account_select 				:	[el.account],
            contact_select 				: 	[el.contact],
            account_code                :   el.account != null ? el.account.code : null,
            account_name                :   el.account != null ? el.account.name : null,
        
            //ids para grilla y para manejo en back(crear o actualizar)
            id_document_transactions    :   el.id,
            id                          :   index,
        
            // verificar
            affects_document_id         :   el.affects_document_id,
            documet_id 					: 	el.document_number,
            no_doc 						:   el.affects_document ? (el.affects_document.prefix != null ? el.affects_document.prefix +' - '+ el.affects_document.consecutive : el.affects_document.consecutive) : null,
            affects_prefix_document     :   el.affects_prefix_document,
            crossing_number             :   el.crossing_consecutive,
            crossing_date               :   el.crossing_due_date_document
        })
    })

    state.totals = {
        // Totales de los productos informativo
        total_quantity   : doc.total_quantity,
        total_discount   : doc.total_discount,
        total_vr_brut    : doc.total_vr_brut,
        total_vr_iva     : doc.total_vr_iva,
        total_           : doc.total_,
        total_items      : doc.total_items,
        total_inventory_cost_value : doc.total_inventory_cost_value,

        //totales documento
        total_value_brut : doc.total_value_brut,
        ret_fue_total    : doc.ret_fue_total,
        ret_iva_total    : doc.ret_iva_total,
        ret_ica_total    : doc.ret_ica_total,
        total_value      : doc.total_value
    }
    state.templates = state.params.template_id == null ? [] : state.templates
    state.sellers = state.params.seller_id == null ? [] : state.sellers

    state.loading_totals = false
}
export function setloading_totals(state, loading_totals){
    state.loading_totals = loading_totals
}
export function setloading_accounting_document(state, loading_accounting_document){
    state.loading_accounting_document = loading_accounting_document
}
export function settotal_difference(state, total_difference){
    state.total_difference = total_difference
}
export function setbase_value_column_visible(state, base_value_column_visible){
    state.base_value_column_visible = base_value_column_visible
}
export function setWatchDocumument(state, Watch_documument){
    state.Watch_documument = Watch_documument
}
export function clearParams(state){
    state.Watch_documument = false;
    state.edit_document = false;
    state.loading_totals = false
    state.show_pdf = false
    state.file_name = null


    state.selected_document_title.id            = null
    state.selected_document_title.in_progress   = null

    // state.params.branchoffice_id                = null
    state.params.voucherType_id                 = null
    state.params.num_doc                        = null
    state.params.date                           = null

    let date = new Date()
    let year = date.toLocaleDateString('COT', {year:'numeric'}).replace(/\//g, '-')
    let day = date.toLocaleDateString('COT', {day:'2-digit'}).replace(/\//g, '-')
    let month = date.toLocaleDateString('COT', {month:'2-digit'}).replace(/\//g, '-')
    state.params.date = `${year}-${month}-${day}`

    state.params.template_id                    = null
    state.params.contact_id                     = null
    state.params.document_id                    = null
    state.params.observation                    = null
    state.params.city_id                        = null
    state.params.seller_id                      = null

    state.params.affects_number_document        = null
    state.params.affects_prefix_document        = null
    state.params.affects_date_document          = null
    state.params.term_days_crossing             = null
    state.params.crossing_due_date_document     = null
    state.params.due_date                       = null
    state.params.term_days                      = null
    state.params.documents_products=[]
    state.totals={},
    state.params.document_transactions =[]

    state.params.document_transactions = [
        {
            id : 1,
            account_id : null,
            observation : '',
            contact_id : null,
            no_doc : null,
            value : 0,
            base_value : 0,
            order : 1,
            payment_method : null,
            projects_column_visible : null,
            contact_column_visible : null,
            cost_center_column_visible : null,
            account_select : [],
            contact_select : [],
            projects_id : null,
        }
    ]

    state.total_debit = 0;
    state.total_credit = 0;
    state.total_difference = 0;



}
