import $http from "../../axios";
import {
  Notification,
  MessageBox
} from 'element-ui';

export async function downloadAccountingDocument({}, documentToPdf) {
  let name = documentToPdf.vouchertype.name_voucher_type + ' No. ' + documentToPdf.consecutive + '.pdf'
  await $http({
    url: '/api/descargar-pdf-contabilidad/'+documentToPdf.id,
    method: 'GET',
    responseType: 'blob',
  }).then((response) => {
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', name);
    document.body.appendChild(link);
    link.click();
  }).catch((error) => {
    Notification.error({
      title: 'Error!',
      message: error.message,
      type: 'error',
      duration: 3000,
      customClass: 'notification-box',
    });

  })
}


//recontabilizar documento
export async function accountingDoc({commit, state, dispatch}, document_id){
  await $http.get(`/api/documento/contabilizar/${document_id}`)
  .then(response => {
    dispatch('listTransactions', document_id);
  })
  .catch(error => {
    let data = error.response.data
    Notification.error({
      title: "Error!",
      message: data.message,
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  });
}

export async function listTransactions({commit, state, dispatch}, document_id){
  await $http.get(`/api/comprobantes-documento/${document_id}`)
  .then(response => {
    commit('listTransactions', response.data.data);
  })
  .catch(error => {
    // let data = error.response.data
    Notification.error({
      title: "Error!",
      message: 'error',
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  });
}

//Obtener las sedes
export async function fetchBranchOffices({commit, state}) {
    await $http.get('/api/sedes')
      .then((response) => {
        commit('setBranchOffices', response.data)
      })
      .catch((error) => {
        Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 2000,
          customClass: 'notification-box',
        });
        commit('BranchOfficeError', error.message)
      })
  }

export async function fetchVouchertype({commit, state}) {
    await $http.get('/api/tipos-de-comprobantes', { params : state.paramsVoucherType})
        .then((response) => {
            commit('setVouchersTypes', response.data.data)
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}

export async function fetchpayment_methods(
  { commit, state },
) {
  await $http
    .get("/api/parametros-por-tabla/111")
    .then(response => {
      commit("setpayment_methods", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function getcommissionable_concepts(
  { commit, state },
) {
  await $http
    .get("/api/parametros-por-tabla/115")
    .then(response => {
      commit("setcommissionable_concepts", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}
export async function fetchGroups(
    { commit, state },
  ) {
    await $http
      .get("/api/parametros-por-tabla/97")
      .then(response => {
        commit("setGroups", response.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        commit("modelError", error.message);
      });
  }
  export async function fetchContacts({commit,state}, requestParameters = {params: state.paramsquery}) {
    await $http.get('/api/terceros', requestParameters)
        .then((response) => {
            commit('setContacts', response.data.data.data)
  
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  }

export async function fetchTemplates({commit, state}) {
    await $http.get('/api/modelos', { params : state.paramstemplates})
        .then((response) => {
            commit('setTemplates', response.data.data.data)
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}
export async function fetchcost_center_1(
    { commit, state },
  ) {
    await $http
      .get("/api/parametros-por-tabla/102")
      .then(response => {
        commit("setcost_center_1", response.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        commit("modelError", error.message);
      });
  }

export async function fetchCities({commit, state}, text) {
    let params = { department:state.params.departament, filter:text}
    await $http.get('/api/ciudades-departamentos', {params} )
        .then((response) => {
            commit('setCities', response.data.data)

        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
}
export async function fetchsellers({commit, state}, text) {
    let params = { filter:text}
    await $http.get('/api/lista-vendedores', {params} )
        .then((response) => {
            commit('setSellers', response.data.data)

        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
}
export async function fetchProjects({commit, state}, text) {
    let params = { filter:text, status:1, paginate: false,}
    await $http.get('/api/proyectos', {params} )
        .then((response) => {
            commit('setProjects', response.data.data)

        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
}
export async function fetchProjectsdt({commit, state}, text) {
    let params = { filter:text, status:1, paginate: false,}
    await $http.get('/api/proyectos-activos', {params} )
        .then((response) => {
            commit('setProjectsdt', response.data.data)

        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
}
export async function fetchconsecutive({commit, state}, id) {
  await $http.get('/api/tipos-de-comprobantes/'+id)
      .then((response) => {
        
        commit('setConsecutive', response.data.data)
      })
      .catch((error) => {
          Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
      });
  })
}

export async function fetchContact({commit,state}, id) {
  await $http.get('/api/terceros/'+id)
      .then((response) => {
          commit('setContacts', [response.data.data])

      })
      .catch((error) => {
          Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
      });
  })
}

export async function fetchContactdt({commit,state}, id) {
  await $http.get('/api/terceros/'+id)
      .then((response) => {
          commit('setContactdt', response.data.data)
      })
      .catch((error) => {
          Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
      });
  })
}

export async function fetchContactsdt({commit,state}, requestParameters = {params: state.paramsquerydt}) {
  await $http.get('/api/terceros', requestParameters)
      .then((response) => {
          commit('setContactsdt', response.data.data.data)

      })
      .catch((error) => {
          Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
      });
  })
}

export async function fetchContacts_filterdt({commit,state}, requestParameters = {params: state.paramsquerydt}) {
  await $http.get('/api/terceros', requestParameters)
      .then((response) => {
          commit('setContacts_filterdt', response.data.data.data)

      })
      .catch((error) => {
          Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
      });
  })
}

//Obtener las sedes
export async function fetchBranchoffice({commit, state}, id) {
  await $http.get('/api/sedes/'+id)
    .then((response) => {
      commit('setbranchOffice', response.data.data)
    })
    .catch((error) => {
      Notification.error({
        title: 'Error!',
        message: error.message,
        type: 'error',
        duration: 2000,
        customClass: 'notification-box',
      });
      commit('BranchOfficeError', error.message)
    })
}

export async function createVariousVouchers({commit , state}){
  await $http.post('/api/comprobantes-varios', state.params)
  .then((response)=>{
    // console.log(response.data.data);
    commit('setParamsdocumentid', response.data.data)
  })
  .catch((error)=>{
    Notification.error({
      title: 'Error!',
      message: error.message,
      type: 'error',
      duration: 2000,
      customClass: 'notification-box',
    });
  })
}

export async function fetchContactselect({commit, state}, id){
  let contats =[]
  await $http.get('/api/terceros/'+id)
      .then((response) => {
          contats= [response.data.data]
          // commit('setContactdt', response.data.data)
      })
      .catch((error) => {
          Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
      });
  })
  return contats
}

export async function fetchAccountFromRoot({commit, state}, id){
  let accounts =[]
  await $http
  .get(`/api/cuentas-raiz/${id}`)
  .then(response => {
    accounts = response.data.data
    // commit("setaccountsselect", );
  })
  .catch(error => {
    let data = error.response.data
    Notification.error({
      title: "Error!",
      message: data.message,
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  });
  return accounts
}

export async function fetchAccount({commit , state}, id){
  let account =[];
  await $http
  .get(`/api/cuentas/${id}`)
  .then(response => {
    account = response.data.data
    // commit("setaccountselect", response.data.data);
  })
  .catch(error => {
    let data = error.response.data
    Notification.error({
      title: "Error!",
      message: data.message,
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  });
  return account
}

export async function finalizeAccountingDocument({commit , state}){
  await $http.post('/api/finalizar-documento-contable', state.params)
  .then((response)=>{
    commit('setShowPdf', response.data.data)
  })
  .catch((error)=>{
    Notification.error({
      title: 'Error!',
      message: error.message,
      type: 'error',
      duration: 2000,
      customClass: 'notification-box',
    });
  })
}

export async function fetchDocumentransactions({commit , state}, id){
  await $http
  .get(`/api/comprobantes-documento/${id}`)
  .then(response => {
    // commit("setaccountselect", response.data.data);
  })
  .catch(error => {
    let data = error.response.data
    Notification.error({
      title: "Error!",
      message: data.message,
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  });
}

export async function searchDocument({commit , state}, data){
  let requestParameters = {params: data}, docu
  await $http.get('/api/buscar-documento', requestParameters)
      .then((response) => {
          // commit('setContacts_filterdt', data)
          docu = response.data.data
      })
      .catch((error) => {
          Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
      });
  })
  return docu
  // await $http
  // .get(`/api/buscar-documento`, data)
  // .then(response => {
  //   console.log(response);
  //   // commit("setaccountselect", response.data.data);
  // })
  // .catch(error => {
  //   let data = error.response.data
  //   Notification.error({
  //     title: "Error!",
  //     message: data.message,
  //     type: "error",
  //     duration: 1500,
  //     customClass: "notification-box"
  //   });
  // });
}


export async function editDocument({commit , state}, id){
  await $http
  .get(`/api/editar-documento/${id}`)
  .then(response => {
    commit("setParamsEdit", response.data.data);
  })
  .catch(error => {
    let data = error.response.data
    Notification.error({
      title: "Error!",
      message: data.message,
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  });
}
export async function FinalizeUpdate({commit , state}, id){
  state.params.total_debit = state.total_debit
  state.params.total_credit = state.total_credit
  await $http
  .post(`/api/actualizar-documento-contable`, state.params)
  .then(response => {
    commit('setShowPdf', response.data.data)
  })
  .catch(error => {
    let data = error.response.data
    Notification.error({
      title: "Error!",
      message: data.message,
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  });
}
export async function deleteAccountFromDocumentTransaction({commit, state}, $id){
  await $http
  .delete(`/api/eliminar-partida/${$id}`)
  .then(response => {
    // commit('setShowPdf', response.data.data)
  })
  .catch(error => {
    let data = error.response.data
    Notification.error({
      title: "Error!",
      message: data.message,
      type: "error",
      duration: 1500,
      customClass: "notification-box"
    });
  });
}