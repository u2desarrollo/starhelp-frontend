import {getApiDomain} from "../../util/domains";

export function resetParamsGeneratePdf(state) {
  state.paramsGeneratePdf = {
    dateFrom: "",
    timeFrom: "",
    dateTo: "",
    timeTo: "",
    branchoffice_id: "",
    note: ""
  };

  setInitialValuesDates(state);
}

export function resetFileNamePdfCashRegisterSquare(state) {
  state.fileNamePdfCashRegisterSquare = "";
  state.fileNamePdfCashRegisterSquareEmail = "";
}

export function setFileNamePdfCashRegisterSquare(state, value) {
  state.fileNamePdfCashRegisterSquare = getApiDomain() + "/storage/" + value;
  state.fileNamePdfCashRegisterSquareEmail = "/" + value;
}

export function setShowPdfCashRegisterSquare(state, value) {
  state.showPdfCashRegisterSquare = value;
}

export function setItemsHistoryTable(state, data) {
  state.itemsHistoryTable = data;
}

export function setItemHistoryTableSelected(state, data) {
  state.itemHistoryTableSelected = data;
}

export function setLoadingPdf(state, value) {
  state.loadingPdf = value;
}

export function setViewSquareCashRegister(state, data) {
  let dateFrom = data.dateFrom.split(" ");
  let dateTo = data.dateTo.split(" ");

  state.paramsGeneratePdf = {
    dateFrom: dateFrom[0],
    timeFrom: dateFrom[1],
    dateTo: dateTo[0],
    timeTo: dateTo[1],
    branchoffice_id: data.branchoffice_id,
    note: data.note
  };
}

export function setInitialValuesDates(state) {
  var dt = new Date();

  // Dia
  state.paramsGeneratePdf.dateFrom = dt.toISOString().substring(0, 10);
  state.paramsGeneratePdf.dateTo = dt.toISOString().substring(0, 10);

  // Hora
  state.paramsGeneratePdf.timeFrom = "00:00:00";
  state.paramsGeneratePdf.timeTo = "23:59:59";

  // Fecha Tabla
  dt.setDate(dt.getDate() - 60);
  state.paramsHistoryTable.dateFrom = dt.toISOString().substring(0, 10);

  state.paramsGeneratePdf.branchoffice_id = parseInt(
    localStorage.selected_branchoffice
  );
}
