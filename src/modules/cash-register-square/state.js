export default {
  showPdfCashRegisterSquare: false,
  fileNamePdfCashRegisterSquare: "",
  fileNamePdfCashRegisterSquareEmail: "",
  itemHistoryTableSelected: {},
  loadingPdf: false,
  itemsHistoryTable: [],
  fieldsHistoryTable: [
    {
      key: "dateFrom",
      label: "Fecha/Hora Desde"
    },
    {
      key: "dateTo",
      label: "Fecha/Hora Hasta"
    },
    {
      key: "actions",
      label: "Acciones"
    }
  ],
  paramsGeneratePdf: {
    dateFrom: "",
    timeFrom: "",
    dateTo: "",
    timeTo: "",
    branchoffice_id: "",
    note: ""
  },
  paramsHistoryTable: {
    dateFrom: ""
  }
};
