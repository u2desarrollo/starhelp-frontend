import $http from "../../axios";
import { Notification } from "element-ui";

export async function generateSquareCashRegisterPdf({ commit, state }) {
  commit("setLoadingPdf", true);
  commit("resetFileNamePdfCashRegisterSquare");

  var params = {
    params: state.paramsGeneratePdf
  };

  $http
    .get("/api/cuadre-de-caja", params)
    .then(response => {
      commit("setFileNamePdfCashRegisterSquare", response.data);
      commit("setShowPdfCashRegisterSquare", true);
      commit("setLoadingPdf", false);
    })
    .catch(error => {
      commit("setLoadingPdf", false);
      Notification.error({
        title: "¡Error!",
        message: "Error al crear el cuadre de caja.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function saveSquareCashRegister({ commit, state, dispatch }) {
  await $http
    .post("/api/historial-cuadre-de-caja", state.paramsGeneratePdf)
    .then(response => {
      commit("resetFileNamePdfCashRegisterSquare");
      dispatch("getHistorySquareCashRegister");
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error al crear el cuadre de caja.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function updateSquareCashRegister(
  { commit, state, dispatch },
  id
) {
  await $http
    .put(`/api/historial-cuadre-de-caja/${id}`, state.paramsGeneratePdf)
    .then(response => {
      commit("resetFileNamePdfCashRegisterSquare");
      dispatch("getHistorySquareCashRegister");
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error al crear el cuadre de caja.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function getHistorySquareCashRegister({ commit, state }) {
  var params = {
    params: state.paramsHistoryTable
  };
  $http
    .get("/api/historial-cuadre-de-caja", params)
    .then(response => {
      commit("setItemsHistoryTable", response.data);
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "¡Error!",
        message: "Error al traer los cuadres de caja.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function deleteHistorySquareCashRegister(
  { commit, state, dispatch },
  id
) {
  await $http
    .delete(`/api/historial-cuadre-de-caja/${id}`)
    .then(response => {
      dispatch("getHistorySquareCashRegister");
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error al eliminar el cuadre de caja.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
