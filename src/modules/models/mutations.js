import { defaultSelectedModel } from "./state";
import shortid from "shortid";

export function setModels(state, models) {
  state.models = models;
}

export function setModel(state, model) {
  state.selectedModel = model;
}

export function setAction(state, action) {
  state.action = action;
}
export function setParamTables(state, paramTables) {
  state.paramTables = paramTables;
}

export function modelError(state, payload) {
  state.error = true;
  state.errorMessage = payload;
  state.models = [];
}

export function setGetModelEdit(state, getModelEdit) {
  state.getModelEdit = getModelEdit;
}

//Establece el valor por defecto de selectedModel
export function resetSelectedModel(state) {
  //let id = state.selectedModel.id
  Object.assign(state.selectedModel, defaultSelectedModel());
  //state.selectedModel.id = id
}

//------------------------ PAGINACIÓN ------------------------//

export function setFilter(state, filter) {
  state.params.page = 1;
  state.params.filter = filter;
}

export function setPerPage(state, perPage) {
  state.params.page = 1;
  state.params.perPage = perPage;
}

export function setPage(state, page) {
  state.params.page = page;
}

export function setSortBy(state, sortBy) {
  state.params.sortBy = sortBy;
}

export function setSort(state, sort) {
  state.params.sort = sort;
}

//-------------------Suscriptores---------------------//
export function setBranchOffices(state, branchoffices) {
  state.branchoffices = branchoffices.map(p => {
    return { value: p.id, label: p.name };
  });
}
//-------------------Suscriptores---------------------//
export function setLines(state, lines) {
  state.lines = lines.map(p => {
    return { value: p.id, label: p.line_description };
  });
}

//-------------------Suscriptores---------------------//
export function setUsers(state, users) {
  state.users = users.map(p => {
    return { value: p.id, label: p.name + " " + p.surname };
  });
}
//-------------------Suscriptores---------------------//
export function setBranchOfficeWarehouses(state, warehouses) {
  state.warehouses = warehouses.map(p => {
    return { value: p.id, label: p.warehouse_description };
  });
}
// Carga el select de tipos de documento
export function setVoucherTypes(state, voucherTypes) {
  state.voucherTypes = voucherTypes.map(p => {
    return {
      value: p.id,
      label: p.name_voucher_type,
      code: p.code_voucher_type,
      consecutive_number: p.consecutive_number
    };
  });
}
// Carga el select de centro de costos
export function setCosts(state, costs) {
  state.costs = costs;
}

// Carga el select de las monedas
export function setCurrencys(state, currencys) {
  state.currencys = currencys;
}

// Carga el select de las formas de pago
export function setPaymentMethods(state, paymentMethods) {
  state.paymentMethods = paymentMethods;
}
// Carga el select de las formas de pago
export function setCategoryContact(state, contactCategories) {
  state.contactCategories = contactCategories;
}
export function setPriceLists(state, priceLists) {
  state.priceLists = priceLists;
}
export function setContacts(state, contacts) {
  state.contacts = contacts.map(p => {
    return { value: p.id, label: p.name + " " + p.surname };
  });
}

export function typesOperationError(state, params) {
  state.errorMessage = params;
}

export function setTypesOperation(state, params) {
  state.typesOperations = params;
}

export function setContactSelected(state, contact) {
  state.contactSelected = contact;
}

export function setList(state, list) {
  state.list = list;
}

export function resetBranchofficeWarehouseId(state) {
  state.selectedModel.branchoffice_warehouse_id = null;
}

// ======================================== //
// CONTABILIDAD
// ======================================== //

/**
 * Guardar los conceptos de contabilidad.
 *
 * @author Kevin Galindo
 */
export function setAccountingConcept(state, data) {
  state.accountingConcept = data;
}

/**
 * Guardar el listado de según de contabilidad.
 *
 * @author Kevin Galindo
 */
export function setAccountingAccording(state, data) {
  state.accountingAccording = data;
}

/**
 * Guardar el listado de cuentas de contabilidad.
 *
 * @author Kevin Galindo
 */
export function setAccountingAccount(state, data) {
  state.accountingAccount = data;
}

export function setRegisterAccounting(state) {
  // Validamos que los campos no esten vacios.
  
  if (
    !state.accounting.accounting_concept ||
    !state.accounting.accounting_according ||
    !state.accounting.debit_credit 
    //!state.accounting.order_accounting
    // !state.accounting.accounting_account
  ) {
    return;
  }

  let concept = state.accountingConcept.find(
    concept => concept.value === state.accounting.accounting_concept
  );

  let according = state.accountingAccording.find(
    according => according.value === state.accounting.accounting_according
  );

  let data = {
    model_id: state.selectedModel.id,
    concept_id: state.accounting.accounting_concept,
    according_id: state.accounting.accounting_according,
    account_id: state.accounting.accounting_account,
    debit_credit: state.accounting.debit_credit,
    departure_square: state.accounting.departure_square,
    order_accounting: state.accounting.order_accounting,

    //
    concept: {
      name_parameter: concept.label,
      id: concept.id
    },
    accounting: {
      order_accounting: state.accounting.order_accounting,
    },
    according: {
      name_parameter: according.label,
      id: according.id
    },
    account: state.accountingAccount.find(
      account => account.id === state.accounting.accounting_account
    )

  };

  // Validar si ya existe en el arreglo
  let exists = state.selectedModel.template_accountings.find(
    ta =>
      ta.concept_id == data.concept_id &&
      ta.according_id == data.according_id &&
      ta.account_id == data.account_id &&
      ta.debit_credit == data.debit_credit &&
      //ta.departure_square == data.departure_square &&
      ta.order_accounting == data.order_accounting
  );


  let update = state.selectedModel.template_accountings.find(
    ta =>
    ta.concept_id == data.concept_id &&
    ta.according_id == data.according_id &&
    ta.account_id == data.account_id &&
    ta.debit_credit == data.debit_credit &&
    //ta.departure_square == data.departure_square &&
    ta.order_accounting != data.order_accounting
  );

  

  if (exists) {  
    return;
  }

  console.log(update);
  if(update){
    state.selectedModel.template_accountings.forEach(function callback(currentValue, index, array) {
      if(update.concept_id == state.selectedModel.template_accountings[index]['concept_id'] 
      && update.according_id == state.selectedModel.template_accountings[index]['according_id']
      && update.debit_credit == state.selectedModel.template_accountings[index]['debit_credit'] 
      && update.account_id == state.selectedModel.template_accountings[index]['account_id']
      && update.departure_square == state.selectedModel.template_accountings[index]['departure_square'])
      {
        /* state.selectedModel.template_accountings.forEach(function callback(currentValue1, index1, array1) {
          if(state.selectedModel.template_accountings[index1]['order_accounting'] == data.order_accounting){
            state.selectedModel.template_accountings[index1]['order_accounting'] = parseInt(data.order_accounting)+1;
            state.selectedModel.template_accountings[index]['order_accounting'] = data.order_accounting;
          }else{
            state.selectedModel.template_accountings[index]['order_accounting'] = data.order_accounting;
          }
        }); */
        state.selectedModel.template_accountings[index]['order_accounting'] = data.order_accounting;
      }
      
      if(state.selectedModel.template_accountings[index]['order_accounting']==''){
        state.selectedModel.template_accountings[index]['order_accounting'] = 0;
      }
    });
  }else{
    state.selectedModel.template_accountings.push(data);

  }
  
/*   state.selectedModel.template_accountings.forEach(function callback(currentValue2, index2, array2) {

    let pos_prev = parseInt(index2)-1;
    if(parseInt(state.selectedModel.template_accountings[index2]['order_accounting'])>0){
      if(state.selectedModel.template_accountings[index2]['order_accounting'] == state.selectedModel.template_accountings[pos_prev]['order_accounting']){
        state.selectedModel.template_accountings[index2]['order_accounting'] =parseInt(state.selectedModel.template_accountings[index2]['order_accounting'])+1;
      }
    }
  }); */

  // Guardamos info
  state.selectedModel.template_accountings = state.selectedModel.template_accountings.sort((a, b) => a.order_accounting - b.order_accounting);
  // Resetear el formulario
  resetRegisterAccounting(state);
}

export function resetRegisterAccounting(state) {
  state.accounting.accounting_concept = "";
  state.accounting.accounting_according = "";
  state.accounting.accounting_account = "";
  state.accounting.debit_credit = "";
  state.accounting.departure_square = false;
  state.accounting.order_accounting = "";
}

export function deleteRegisterAccounting(state, data) {
  // Eliminamos del listado
  let template_accountings = state.selectedModel.template_accountings.filter(
    ta =>
      ta.concept_id != data.concept_id ||
      ta.according_id != data.according_id ||
      ta.account_id != data.account_id ||
      ta.debit_credit != data.debit_credit ||
      ta.departure_square != data.departure_square ||
      ta.order_accounting != data.order_accounting
  );

  // Actualizamos.
  state.selectedModel.template_accountings = template_accountings;
}

export function editRegisterAccounting(state, data){
  console.log(data);
  state.accounting.accounting_concept = data.concept_id;
  state.accounting.accounting_according = data.according_id;
  state.accounting.accounting_account = data.account_id;
  state.accounting.debit_credit = data.debit_credit;
  state.selectedModel.accounting_generate = data.departure_square;
  state.accounting.order_accounting = data.order_accounting;
}

export function setTemplateTransaction(state) {
  if (state.selectedModel && state.selectedModel.transactions.length > 0) {
    state.templateTransaction.data = JSON.parse(
      JSON.stringify(state.selectedModel.transactions)
    );
  }

  return state.templateTransaction;
}
export function resetTemplateTransaction(state) {
  state.templateTransaction.data = [];
  return state.templateTransaction;
}

export function addNewRowTemplateTransaction(state) {
  let last_data = null;
  if (state.templateTransaction.data.length > 0) {
    last_data = JSON.parse(
      JSON.stringify(
        state.templateTransaction.data[
          state.templateTransaction.data.length - 1
        ]
      )
    );
  }

  let row_create = JSON.parse(
    JSON.stringify(state.templateTransaction.row_create)
  );

  let order = 10;

  if (last_data) {
    order = parseInt(last_data.order) + 10;
  }

  row_create.order = order;
  row_create.id = shortid.generate();

  state.templateTransaction.data.push(row_create);
}

export function deleteRowTemplateTransaction(state, id) {
  let index = state.templateTransaction.data.findIndex(d => d.id === id);
  state.templateTransaction.data.splice(index, 1);
}

export function deleteAccountTemplateTransaction(state, data) {
  let transaction = state.templateTransaction.data.find(d => d.id === data.id);
  switch (data.type) {
    case "account_root":
      transaction.account_root_id = "";
      transaction.account_root = "";
      break;
    case "account_aux":
      transaction.account_aux_id = "";
      transaction.account_aux = "";
      break;
  }
}

export function setTemplateTransactionOnlyAux(state, value) {
  state.templateTransaction.params.onlyAux = value;
}

export function setTemplateTransactionNotAux(state, value) {
  state.templateTransaction.params.notAux = value;
}

export function setTemplateTransactionRowSelectedType(state, value) {
  state.templateTransaction.row_selected_type = value;
}

export function setTemplateTransactionRowSelected(state, data) {
  state.templateTransaction.row_selected = data;
}

export function resetTemplateTransactionRowSelected(state) {
  state.templateTransaction.row_selected = {};
}
