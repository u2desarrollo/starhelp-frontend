import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";

//Obtener los modelos
export async function fetchModels({ commit, state },requestParameters = { params: state.params }) {
  await $http
    .get("/api/modelos", requestParameters)
    .then(response => {
      commit("setModels", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchContacts({ commit, state }, filter) {
  await $http
    .get("/api/contactos-categoria", {
      params: {
        type: state.selectedModel.contact_type,
        category: state.selectedModel.contact_category,
        filter: filter
      }
    })
    .then(response => {
      commit("setContacts", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

//Crear tercero
export async function addModel({ commit, state }) {
  console.log(state.selectedModel);
  await $http
    .post("/api/modelos", state.selectedModel)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "El modelo ha sido creado",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setModel", response.data.data);

      state.error = false;
    })
    .catch(error => {
      let message;

      if (error.response.status == 422) {
        message = "El código de El modelo ya existe";
      } else {
        message = error.message;
      }

      MessageBox.alert(message, "Error", {
        dangerouslyUseHTMLString: true,
        confirmButtonText: "Aceptar",
        type: "error",
        customClass: "notification-error"
      });

      commit("modelError", error.message);
    });
}

//Consultar tercero por id
export async function getModel({ commit, state }, id) {
  // if (state.getModelEdit) {
  await $http
    .get(`/api/modelos/${id}`)
    .then(response => {
      commit("setModel", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });

  commit("setGetModelEdit", false);
  // }
}

//Actualizar tercero
export async function updateModel({ commit, state }) {
  let data = state.selectedModel;

  state.selectedModel.templateTransaction = state.templateTransaction.data;

  await $http
    .put(`/api/modelos/${state.selectedModel.id}`, data)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "El modelo ha sido actualizado",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      //commit("setModel", response.data.data);
    })
    .catch(error => {
      let message;

      if (error.response.status == 422) {
        message = "El código de El modelo ya existe";
      } else {
        message = error.message;
      }

      MessageBox.alert(message, "Error", {
        confirmButtonText: "Aceptar",
        type: "error"
      });

      commit("modelError", error.message);
    });
}

//Eliminar tercero
export async function removeModel({ commit, dispatch }, id) {
  await $http
    .delete(`/api/modelos/${id}`)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "El modelo ha sido eliminado",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      dispatch("fetchModels");
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}
//------------------------ PAGINACIÓN ------------------------//

export function setFilterPagination({ commit, dispatch }, filter) {
  commit("setFilter", filter.target.value);
  dispatch("fetchModels");
}

export function setPerPagePagination({ commit, dispatch }, perPage) {
  commit("setPerPage", parseInt(perPage.target.value));
  dispatch("fetchModels");
}

export function setPagePagination({ commit, dispatch }, page) {
  commit("setPage", parseInt(page));
  dispatch("fetchModels");
}

export function setSortByPagination({ commit, dispatch }, sortBy) {
  commit("setSortBy", sortBy);
  // dispatch('fetchModels')
}

export function setSortPagination({ commit, dispatch }, sort) {
  commit("setSort", sort);
  // dispatch('fetchModels')
}

//Función para mapear los parametros y retornarlos en un mismo formato para todos los select
function mapParameters(parameters) {
  return parameters.map(p => {
    return { value: p.id, label: p.name_parameter, code: p.code_parameter };
  });
}

//-------Parametros-----------//
// Obtiene los Centros de Costo
export async function fetchCostCenter({ dispatch, state, rootState, commit }) {
  if (!state.costs.length) {
    await dispatch(
      "parameters/listParameter",
      { params: { idParamTable: 9, paginate: false } },
      { root: true }
    ); //TODO: Determinar el idParamTable
    commit("setCosts", mapParameters(rootState.parameters.parameters));
  }
}
export async function fetchCategoryContact({
  dispatch,
  state,
  rootState,
  commit
}) {
  await $http
    .get("/api/categoria-cliente/" + state.selectedModel.contact_type, {
      params: { paginate: false }
    })
    .then(response => {
      commit("setCategoryContact", mapParameters(response.data));
    });

  //await dispatch('parameters/listParameter', { params: { idParamTable: state.selectedModel.contact_type, paginate: false } }, { root: true })
  //commit('setCategoryContact', mapParameters(rootState.parameters.parameters))
}

// Obtiene las Monedas
export async function fetchCurrencys({ dispatch, state, rootState, commit }) {
  if (!state.currencys.length) {
    await dispatch("parameters/listParameter",
      { params: { idParamTable: 66, paginate: false } },
      { root: true }
    );
    commit("setCurrencys", mapParameters(rootState.parameters.parameters));
  }
}
// Obtiene las Monedas
export async function fetchPriceLists({ dispatch, state, rootState, commit }) {
  if (!state.priceLists.length) {
    await dispatch("parameters/listParameter", {params: {idParamTable: 14, paginate: false}}, {root: true});
    commit("setPriceLists", mapParameters(rootState.parameters.parameters));
  }
}

// Obtiene las Formas de Pago
export async function fetchPaymentMethods({dispatch,state,rootState,commit}) {
  if (!state.paymentMethods.length) {
    await dispatch("parameters/listParameter",{ params: { idParamTable: 69, paginate: false } },{ root: true });
    commit("setPaymentMethods", mapParameters(rootState.parameters.parameters));
  }
}

// Obtiene los tipos de documentos
export async function fetchVoucherTypes({dispatch, state, rootState, commit}) {
  if (!state.voucherTypes.length) {
    await dispatch("vouchersTypes/fetchVouchersTypes", {params: {paginate: false, branchoffice: '010'}}, {root: true});
    commit("setVoucherTypes", rootState.vouchersTypes.vouchersTypes);
  }
}

// Obtiene las lineas
export async function fetchLines({dispatch, state, rootState, commit}) {
  if (!state.lines.length) {
    await dispatch("lines/fetchLines", {params: {paginate: false}}, {root: true});
    commit("setLines", rootState.lines.lines);
  }
}

//Obtiene los usuarios
export async function fetchUsers({dispatch, state, rootState, commit}) {

  if (!state.users.length) {
    await dispatch("users/fetchUsers", {
        params: {
          subscriber_id: state.selectedModel.subscriber_id,
          paginate: false
        }
      },
      {root: true}
    );
    commit("setUsers", rootState.users.users);
  }
}

export async function fetchTypesOperation({commit, state}, requestParameters = {params: state.params}) {
  let params = {
    params: {
      paginate: false
    }
  };
  console.log(state.typesOperations)
  if (!state.typesOperations.data.length) {
    await $http.get("/api/types_operations", params).then(res => {
      console.log(res.data)
      commit("setTypesOperation", res.data);
    }).catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("typesOperationError", error.message);
    });
  }
}

export async function getContactById({ commit, state }, id) {
  await $http.get(`/api/contactos/${id}`).then(response => {
    commit("setContactSelected", response.data.data);
  });
}

//Obtener las tablas de parametros
export async function fetchParamTables({ commit, state }) {
  // options += `&search=${state.tableParamTable.options.search}`;
  const params = {
    params: {
      paginate: false
    }
  };

  if (!state.paramTables.length) {
    await $http
      .get("/api/tablas-parametros", params)
      .then(response => {
        commit("setParamTables", response.data.data);
      })
      .catch(error => {
      });
  }
}

export async function fetchList({ commit }) {
  await $http
    .get("/api/parametros-por-tabla/14")
    .then(response => {
      commit("setList", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

// ======================================== //
// CONTABILIDAD
// ======================================== //

/**
 * Obtener los conceptos de contabilidad.
 *
 * @author Kevin Galindo
 */
export async function fetchAccountingConcept({dispatch,state,rootState,commit}) {

  if (!state.accountingConcept.length) {
    await dispatch("parameters/listParameter", {
        params: {idParamTable: 105, paginate: false}
      },
      {root: true}
    );
    commit("setAccountingConcept", mapParameters(rootState.parameters.parameters));
  }
}
/**
 * Obtener el listado de según de contabilidad.
 *
 * @author Kevin Galindo
 */
export async function fetchAccountingAccording({dispatch,state,rootState,commit}) {

  if (!state.accountingAccording.length) {
    await dispatch("parameters/listParameter", {
        params: {idParamTable: 106, paginate: false}
      },
      {root: true}
    );
    commit("setAccountingAccording", mapParameters(rootState.parameters.parameters));
  }
}
/**
 * Obtener el listado de cuentas de contabilidad.
 *
 * @author Kevin Galindo
 */
export async function fetchAccountingAccount({ commit, state }) {
  const options = {
    data: {
      paginate: false
    }
  };

  if (!state.accountingAccount.length) {

    await $http
      .get("/api/cuentas", options)
      .then(response => {
        commit("setAccountingAccount", response.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 2000,
          customClass: "notification-box"
        });
      });
  }
}

/**
 * Guardar los datos de contabilizacion en el modelo
 *
 * @author Kevin Galindo
 */
export async function saveTemplateAccounting({ commit, state }) {
  const data = {
    model_id: state.selectedModel.id,
    templateAccountings: state.selectedModel.template_accountings

  };

  await $http
    .post("/api/modelos-contabilidad", data)
    .then(response => {
      // commit("setAccountingAccount", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function fetchVoucherTypesByBranchofficeId(
  { dispatch, rootState, commit },
  branchoffice_id
) {
  await dispatch(
    "vouchersTypes/fetchVouchersTypes",
    { params: { paginate: false, branchoffice: branchoffice_id } },
    { root: true }
  );
  commit("setVoucherTypes", rootState.vouchersTypes.vouchersTypes);
}
