export const templateTransaction = () => {
  return {
    columns: [
      {
        name: "Orden",
        field: "order",
        format: "number",
        style: {backgroundColor: "#d5f3d3"}
      },
      {
        name: "Descripción",
        field: "description",
        format: "text",
        style: {backgroundColor: "#d5f3d3"}
      },
      {
        name: "Cuenta Aux",
        field: "account_aux_id",
        style: {width: "600px !important", backgroundColor: "#d5f3d3"}
      },
      {
        name: "Cuenta Raiz",
        field: "account_root_id",
        style: {width: "600px !important", backgroundColor: "#d5f3d3"}
      },
      {
        name: "DB/CR",
        field: "db_cr",
        style: {backgroundColor: "#d5f3d3"}
      },
      {
        name: "Cap Tercero",
        field: "cap_contact",
        style: {backgroundColor: "#d5f3d3"}
      },
      {
        name: "Tercero por defecto",
        field: "default_contact_id",
        style: {backgroundColor: "#d5f3d3"}
      },
      {
        name: "Crear Tercero",
        field: "create_contact",
        style: {backgroundColor: "#d5f3d3"}
      },
      {
        name: "Doc Banco",
        field: "doc_bank",
        style: {backgroundColor: "#d5f3d3"}
      },
      {
        name: "Detalle por defecto",
        field: "default_detail",
        format: "text",
        width: 400,
        style: {backgroundColor: "#d5f3d3"}
      },
      {
        name: "Partida Cuadre",
        field: "square_match",
        style: {backgroundColor: "#d5f3d3"}
      },
      {
        name: "Acciones",
        field: "actions"
      }
    ],
    data: [],
    row_create: {
      id: null,
      template_id: "",
      order: "",
      description: "",
      account_aux_id: "",
      account_root_id: "",
      db_cr: "",
      cap_contact: "",
      default_contact_id: "",
      create_contact: "",
      default_detail: "",
      square_match: "",
      doc_bank: "",
      new: true
    },
    row_selected: {},
    row_selected_type: {},
    params: {
      notAux: false,
      onlyAux: false
    }
  };
};

export const defaultSelectedModel = () => {
  return {
    condition: null,
    price_list: null,
    method: null,
    subscriber_id: 1,
    code: "",
    description: "",

    operationType_id: "",
    voucherType_id: "",
    consecutive_handling: "",
    return_model_id: "",
    select_branchoffice: 0,
    select_warehouse: 0,
    quantity_decimals: 0,
    select_validate_wms: 0,
    select_date_entry: 0,
    select_import: 0,
    select_cost_center: 2,
    seller_mode: "",
    currency: "",
    observation_tittle_1: "",
    observation_tittle_2: "",
    footnotes: "",
    user_id: "",
    select_conveyor: 2,
    document_name: 2,
    capt_delivery_address: 2,
    capt_license: 2,
    final_function: "",

    contact_capture: 0,
    contact_type: "",
    contact_category: "",
    contact_id: "",

    basic_contact_information: "",
    financial_info: 0,
    button_360: 0,
    send_contact_email: "",
    "days_number": null,

    doc_base_management: 2,
    voucher_type_id: "",
    model_id: "",
    see_backorder: 0,
    see_seller_doc: 0,

    doc_cruce_management: 2,
    tittle: "",
    valid_exist: "",
    value_control: 0,
    quantity_control: 0,
    crossing_document_dates: 2,

    add_products: 0,
    price_list_id: "",
    add_promotions_button: 0,
    recalc_iva_less_cost: 0,
    recalc_iva_base: "",
    up_excel_button: 0,
    down_excel_button: 0,

    line_id: "",
    see_location: 0,

    modify_prices: 0,
    see_discount: 0,
    edit_discount: 0,
    see_bonus_amount: 0,
    edit_bonus_amount: 0,
    see_vrunit_base: 0,
    see_cant_backorder: 0,
    backorder_quantity_control: "",
    see_physical_cant: 0,
    edit_physical_cant: 0,
    see_lots: 0,
    edit_lots: 0,
    edit_unit_value: 0,
    see_applied_promotion: 0,
    see_seller: 0,
    edit_seller: 0,
    see_product_observation: 0,
    edit_product_observation: 0,
    hide_value: 0,
    cents_values: 0,
    hands_free_operation: 0,
    hide_values: 0,
    pay_taxes: 0,

    retention_management: "",
    discount_foot_bill: 0,
    handle_payment_methods: 2,
    payment_methods_allowed_id: "",
    purchase_difference_management: 0,

    print: 0,
    print_template: "",
    print_tittle: "",
    product_order: "",
    total_letters: 0,
    cash_drawer_opens: 0,

    branchoffice_id: null,
    branchoffice_warehouse_id: null,
    TRM_value: null,
    edit_value_iva: null,
    import_number: null,
    branchoffice_vouchertype_id: null,
    branchoffice_change: null,
    business: 0,
    cost_center: 0,
    inherit_products: 0,
    multiple_documents: 0
  };
};

export default {
  list: [],
  models: [],
  typesOperations: {data:[]},
  voucherTypes: [],
  currencys: [],
  users: [],
  paymentMethods: [],
  contactCategories: [],
  contacts: [],
  lines: [],
  priceLists: [],
  templates: [],
  selectedModel: defaultSelectedModel(),
  error: false,
  errorMessage: "",
  action: "create",
  lineEdit: null,
  contactSelected: null,
  getModelEdit: true,

  params: {
    paginate: true,
    page: 1,
    perPage: 15,
    filter: "",
    sortBy: "code",
    sort: "ASC"
  },

  contactParams: {
    type: "",
    category: ""
  },

  paramTables: [],
  accountingConcept: [],
  accountingAccording: [],
  accountingAccount: [],
  debitCreditArray: [
    {
      label: "DÉBITO",
      value: "DEBITO"
    },
    {
      label: "CRÉDITO",
      value: "CREDITO"
    }
  ],
  accounting: {
    order_accounting: "",
    accounting_concept: "",
    accounting_according: "",
    accounting_account: "",
    debit_credit: "",
    departure_square: false
  },
  templateTransaction: templateTransaction()
};
