
export function setBranchOffices(state, branchOffices) {
    state.branchOffices = branchOffices;
}

export function setAccountsFrom(state, accounts_from) {
    state.accounts_from = accounts_from
}

export function setAccountsUp(state, accounts_up) {
    state.accounts_up = accounts_up
}

export function setContacts(state, contacts) {
    state.contacts = contacts;
}

export function setVouchersTypes(state, vouchertype) {
    state.voucherTypes = vouchertype    
}

export function setContactWarehouses(state, contact_warehouses){
    state.contact_warehouses = contact_warehouses
}
export function setaccount_balance(state, account_balance) {
    state.account_balance = account_balance
}

export function setcontact_balance(state, contact_balance) {
    state.contact_balance = contact_balance
}

export function setdocument_balance(state, document_balance) {
    state.document_balance = document_balance
    //console.log(state.document_balance);

}

export function setParams(state, params){
    state.params = params
}