export default{
    inventories: [],
    error: false,
    errorMessage: '',
    params: {
        paginate: true,
        page: 1,
        perPage: 15,
        filter: '',
        sortBy: 'id',
        sort: 'ASC'
    }

}
