export function setInventories(state,inventary){
    state.inventories=inventary;
}
export function inventoriesError(state,payload){
        state.error = true
        state.errorMessage = payload
        state.inventories = []
}
export function setFilter(state, filter){
    state.params.page = 1
    state.params.filter = filter
}

export function setPerPage(state, perPage){
    state.params.page = 1
    state.params.perPage = perPage
}

export function setPage(state, page){
    state.params.page = page
}

export function setSortBy(state, sortBy){
    state.params.sortBy = sortBy
}

export function setSort(state, sort){
    state.params.sort = sort
}
