import $http from "../../axios"
import { Notification,  MessageBox } from 'element-ui'

export async function fetchInventories({commit,state},requestParameters={params:state.params}){
    await $http.get('api/inventories',requestParameters).then((res)=>{
        commit('setInventories',res.data.data);
    }).catch((error)=>{
        Notification.error({
			title: '¡Error!',
			message: error.message,
			type: 'error',
			duration: 1500,
			customClass: 'notification-box'
		})
        commit('inventoriesError', error.message)
    });
}
export function setFilterPagination({commit, dispatch}, filter) {
    commit('setFilter', filter.target.value)
    dispatch('fetchInventories')
}

export function setPerPagePagination({commit, dispatch}, perPage) {
    commit('setPerPage', parseInt(perPage.target.value))
    dispatch('fetchInventories')
}

export function setPagePagination({commit, dispatch}, page) {
    commit('setPage', parseInt(page))
    dispatch('fetchInventories')
}

export function setSortByPagination({commit, dispatch}, sortBy) {
    commit('setSortBy', sortBy)
}

export function setSortPagination({commit, dispatch}, sort) {
    commit('setSort', sort)
}
