import $http from "../../axios";
import {Notification} from "element-ui";

export async function executeClosing({state, commit}) {
  commit('setPreloader', true)
  await $http.post("/api/cierre-mensual", state.params)
    .then(response => {
      Notification.success({
        title: "Éxito!",
        message: 'Cierre ejecutado exitosamente.',
        type: "success",
        duration: 4000,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
  commit('setPreloader', false)
}
