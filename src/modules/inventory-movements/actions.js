import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";
import {getApiDomain} from "../../util/domains";

/**
 * Obtiene el modelo de acuerdo al id del mismo
 * @param commit
 * @param state
 * @param requestParameters
 * @returns {Promise<void>}
 */
export async function fetchModel({ commit, state, rootState }, model_id) {
  let params = {
    params: {
      branchoffice_id: localStorage.selected_branchoffice
    }
  };

  if (state.selected_model.id !== model_id) {
    let modelTemp = rootState.baseDocuments.model;
    if (modelTemp && modelTemp.id && modelTemp.id === model_id) {
      commit("setSelectedModel", modelTemp);
      commit("setTypeContact", modelTemp);
      commit("setVoucherTypeToSelectedDocument", modelTemp);
    } else {
      await $http
        .get("/api/obtener-modelo/" + model_id, params)
        .then(response => {
          commit("setSelectedModel", response.data.data);
          commit("setTypeContact", response.data.data);
          commit("setVoucherTypeToSelectedDocument", response.data.data);
        })
        .catch(error => {
          Notification.error({
            title: "¡Error!",
            message: error.message,
            type: "error",
            duration: 1500,
            customClass: "notification-box"
          });
        });
    }
  } else {
    commit("setSelectedModel", state.selected_model);
    commit("setTypeContact", state.selected_model);
    commit("setVoucherTypeToSelectedDocument", state.selected_model);
  }
}
export async function getBranchofficeWarehouses({ commit }) {
  let params = {
    params: {
      paginate: false
    }
  };

  await $http
    .get("/api/sedes/", params)
    .then(response => {
      commit("setBranchofficeWarehouses", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error al traer las bodegas.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function getDocumentsProductsImport({commit, state}, params){
  if (!state.selected_model.select_import) return;
  await $http.get(`/api/productos-importacion/${params.document_id}`, {params})
  .then(response => {

    commit('setdocumentsProductsImport', response.data.data)
    commit('setexpenses', response.data.data)
  })
  .catch(error=>{
    console.log('Error al obtener productos');
  })
}


/**
 * Obtiene el modelo de acuerdo al id del mismo
 * @param commit
 * @param state
 * @param requestParameters
 * @returns {Promise<void>}
 * @author Kevin Galindo
 */
export async function fetchModels({ commit, state }) {
  await $http
    .get("/api/modelos/")
    .then(response => {
      commit("setModels", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Obtiene los documentos base
 * @param commit
 * @param state
 * @param voucher_type
 * @returns {Promise<void>}
 */
export async function fetchBaseDocuments({ commit, state }, voucher_type) {
  await $http
    .get("/api/documentos-base", {
      params: {
        voucher_type: voucher_type,
        filter: state.params.filter,
        date_start: state.params.filterDate[0],
        date_end: state.params.filterDate[1],
        model_id: selected_model.id
      }
    })
    .then(response => {
      commit("setBaseDocuments", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("errorMessage", error.message);
    });
}

/**
 * Obtiene un documento nuevo creado con base en el documento base
 * @param commit
 * @param state
 * @param document_id
 * @returns {Promise<void>}
 */
export async function createDocumentFromBase({ commit, state }, document_id) {
  let params = {
    params: {
      document_id: document_id,
      vouchertype_id: state.selected_model.voucherType_id,
      model_id: state.selected_model.id,
      branchoffice_warehouse_id: localStorage.selected_branchoffice_warehouse,
      additional_documents: state.additional_documents
    }
  };

  if (state.action === "update-document") {
    params.params.update_doc = true;
  }

  await $http
    .get("/api/documentos-inventario", params)
    .then(response => {
      commit("setSelectedDocument", response.data);
      commit("setBackupDocumentsProducts");
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.response.data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Obtiene un documento nuevo creado con base en el documento base
 * @param commit
 * @param state
 * @param document_id
 * @returns {Promise<void>}
 */
export async function fetchDocument({ commit, state }, document_id) {
  let params = {
    params: {
      vouchertype_id: state.selected_model.voucherType_id,
      model_id: state.selected_model.id,
      branchoffice_warehouse_id: localStorage.selected_branchoffice_warehouse,
      authorize_documents: state.authorize_documents ? true : undefined
    }
  };

  await $http
    .get(`/api/documentos-inventario/${document_id}`, params)
    .then(response => {
      commit("setSelectedDocument", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Crea el nuevo documento
 * @param commit
 * @param state
 * @param document_id
 * @returns {Promise<void>}
 */
export async function createDocument({ commit, state }) {
  let data = state.selected_document;
  data.model_id = state.selected_model.id;

  await $http
    .post("/api/carrito", state.selected_document)
    .then(response => {
      commit("setSelectedDocument", response.data.data);
      commit("setBackupDocumentsProducts");
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Obtiene todos los documentos relacionados al mismo id para calcular el backorder
 * @param commit
 * @param state
 * @param document_id
 * @returns {Promise<void>}
 */
export async function fetchDocumentsForBackOrder({ commit, state }, thread_id) {
  await $http
    .get(`/api/back-order/${thread_id}`)
    .then(response => {
      commit("setDocumentsForBackOrder", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Obtiene todas las sedes del suscriptor actual
 * @param dispatch
 * @param state
 * @param rootState
 * @param commit
 * @returns {Promise<void>}
 */
export async function fetchBranchOffices({
  dispatch,
  state,
  rootState,
  commit
}) {
  if (!state.branchoffices.length) {
    let params = {
      params: {
        subscriber_id: state.subscriber_id,
        paginate: false
      }
    };

    await $http
      .get("/api/sedes", params)
      .then(response => {
        commit("setBranchOffices", response.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 2000,
          customClass: "notification-box"
        });
      });
  }
}

/**
 * Obtiene todas las bodegas de la sede seleccionada
 * @param dispatch
 * @param state
 * @param rootState
 * @param commit
 * @returns {Promise<void>}
 */
export async function fetchBranchOfficesWarehouses({
  dispatch,
  state,
  rootState,
  commit
}) {
  if (!state.branchoffices_warehouses.length) {
    let params = {
      params: {
        branchoffice_id: state.selected_document.branchoffice_id,
        paginate: false
      }
    };

    await $http
      .get("/api/bodegas-sedes", params)
      .then(response => {
        commit("setBranchOfficesWarehouses", response.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 2000,
          customClass: "notification-box"
        });
      });
  }
}

/**
 * Obtiene los terceros de acuerdo al tipo de tercero
 * @param commit
 * @param state
 * @param rootState
 * @returns {Promise<void>}
 */
export async function fetchContacts({ commit, state, rootState }) {
  let params = {
    params: state.params_contacts
  };

  if (state.selected_document.contact && state.action === "view") {
    if (state.selected_document.contact.identification) {
      params.params.filter = state.selected_document.contact.identification;
    }
  }

  await $http
    .get("/api/terceros", params)
    .then(response => {
      commit("setContacts", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Obtiene las sucursales de acuerdo al tercero seleccionado
 * @param commit
 * @param state
 * @param rootState
 * @returns {Promise<void>}
 */
export async function fetchContactsWarehouses({ commit, state }) {
  let params = {
    params: {
      contact_id: state.selected_document.contact_id
    }
  };

  await $http
    .get("/api/establecimientos", params)
    .then(response => {
      commit("setContactsWarehouses", response.data.data);

      let contact_warehouse_id = -1;

      if (state.contacts_warehouses.length == 1) {
        contact_warehouse_id = state.contacts_warehouses[0].id;
      }

      if (state.action !== "view") {
        commit("setDataContactWarehouse", contact_warehouse_id);
      }
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
/**
 * Obtiene una sucursal por ID
 * @param commit
 * @param state
 * @param rootState
 * @returns {Promise<void>}
 */
export async function getContactsWarehousesById({ commit, state }, id) {
  await $http
    .get(`/api/establecimientos/${id}`)
    .then(response => {
      commit("setContactsWarehouseById", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Actualiza o crea un producto en el documento
 * @param commit
 * @param state
 * @param rootState
 * @returns {Promise<void>}
 */
export async function updateOrCreateDocumentProduct(
  { commit, state, dispatch },
  product
) {
  var data = {
    // Datos del producto
    product_id: product.id,
    promotion_id: product.promotion_id,
    quantity: product.quantity,
    is_benefit: product.is_benefit,
    physical_unit: product.physical_unit,
    base_unit_value: product.base_unit_value,
    unit_value_before_taxes: product.unit_value_before_taxes,
    discount_value: product.discount_value,
    description: product.description,
    total_value_iva: product.total_value_iva,
    inventory_cost_value: product.inventory_cost_value,
    inventory_cost_value_unit: product.inventory_cost_value_unit,
    discount_value_us: product.discount_value_us,
    total_value_brut: product.total_value_brut,

    // Datos del documento
    seller_id: state.selected_document.seller_id,
    contact_id: state.selected_document.contact_id,
    warehouse_id: state.selected_document.warehouse_id,
    document_id: state.selected_document.id,
    model_id: state.selected_model.id,

    // Otros datos
    branchoffice_warehouse_id: localStorage.selected_branchoffice_warehouse,
    branchoffice_id: localStorage.selected_branchoffice,
    account_id: product.account_id
  };

  if (product.document_product_id) {
    data.document_product_id = product.document_product_id;
    data.branchoffice_warehouse_id_product = product.branchoffice_warehouse_id;
  } else {
    data.branchoffice_warehouse_id_product =
      state.doc_product_default_branchoffice_warehouse_id;
  }

  await $http
    .post("/api/items-carrito", data)
    .then(response => {
      //Guardamos la info
      commit("addDocumentProduct", response.data.data);
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Finaliza el documento activo
 * @param commit
 * @param state
 * @param rootState
 * @returns {Promise<void>}
 */
export async function finalizeDocument({ commit, state, getters }) {
  const user = JSON.parse(localStorage.user);

  let formData = new FormData();
   // Informacion general
  if (state.authorize_documents) {
    formData.append("authorize_documents", state.authorize_documents);
  }

  formData.append("contact_id", state.selected_document.contact_id);
  formData.append("warehouse_id", state.selected_document.warehouse_id);
  formData.append("save_document", state.saveDocument);
  formData.append("document_id", state.selected_document.id);
  formData.append("observation", state.selected_document.observation || "");
  formData.append("seller_id", state.selected_document.seller_id || "");
  formData.append("model_id", state.selected_model.id);
  formData.append("projects_id", state.selected_document.projects_id);
  formData.append(
    "branchoffice_warehouse_id",
    localStorage.selected_branchoffice_warehouse
  );
  formData.append("user_id", user.id);

  // Porcentaje de comision
  if (state.selected_model.select_business) {
    formData.append("business_id", state.selected_document.business_id);
  }

  // Porcentaje de comision
  if (state.selected_model.capture_commission_percentage) {
    formData.append(
      "commission_percentage_applies",
      state.selected_document.documents_information
        .commission_percentage_applies
    );

    formData.append(
      "commission_percentage",
      state.selected_document.documents_information
        .commission_percentage_applies
        ? state.selected_document.documents_information.commission_percentage ||
            ''
        : ''
    );
  }

  // Diferencias
  if (state.selected_model.purchase_difference_management) {
    getters.getPurchaseDifference.map(pd => {
      formData.append("documentProductsDifferences[]", JSON.stringify(pd));
    });
  }

  // Otras opciones
  if (state.selected_model.handling_other_parameters_state != 2) {
    // Validamos si es obligatorio
    formData.append(
      "handling_other_parameters_value",
      state.selected_document.handling_other_parameters_value
    );
  }

  // Documento Cruce
  if (state.selected_model.doc_cruce_management != 2) {
    formData.append(
      "affects_prefix_document",
      state.selected_document.affects_prefix_document || ""
    );
    formData.append(
      "affects_number_document",
      state.selected_document.affects_number_document || ""
    );
    formData.append(
      "affects_date_document",
      state.selected_document.affects_date_document || ""
    );
  }

  // Numero de importacion
  if (state.selected_model.import_number != 2) {
    formData.append(
      "import_number",
      state.selected_document.import_number || ""
    );
  }

  if (state.selected_model.handle_payment_methods !== 2) {
    formData.append(
      "paymentMethods",
      JSON.stringify(state.modalPaymentMethods.data)
    );
  }

  if ([1,9].includes(state.selected_model.select_conveyor)) {
    formData.append("protractor",state.selected_document.documents_information.protractor || null);
    formData.append("guide_number",state.selected_document.documents_information.guide_number || null);
  }

  if (!state.saveDocument && state.selected_document.state_ending_id) {
    formData.append("consecutive", state.selected_document.consecutive);
  }

  // Archivos
  formData.append("document_files_delete", state.document_files_delete);

  formData.append(
    "document_name",
    state.selected_document.documents_information
      .document_name
  );

  formData.append(
    "date_entry",
    state.selected_document.documents_information
      .date_entry
  );

  state.document_files_new.map(fn => {
    formData.append("filesUpload[]", fn);
  });


  return await new Promise(async (resolve, reject) => {
    await $http
      .post("/api/finalizar-documento", formData)
      .then(async response => {
        if (response.status == 200) {
          await commit("setFilenameSelectedDocument", response.data);
          await commit("setError", false);
          await commit("setEditRetention", false);
          Notification.success({
            title: "Exito",
            message: "Documento finalizado exitosamente",
            type: "success",
            duration: 1500,
            customClass: "notification-box"
          });
          resolve(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(error => {
        console.error(error);
        commit("setError", true);
        reject(error.response.data);
      });
  });
}

/**
 * Este metodo actualiza los datos generales del documento.
 *
 * @author Kevin Galindo
 */
export async function updateGeneralDataDocument(
  { commit, state },
  document_id
) {
  let formData = new FormData();

  // Informacion general
  formData.append("contact_id", state.selected_document.contact_id || "");
  formData.append("warehouse_id", state.selected_document.warehouse_id || "");
  formData.append("seller_id", state.selected_document.seller_id || "");
  formData.append(
    "affects_prefix_document",
    state.selected_document.affects_prefix_document || ""
  );
  formData.append("import_number", state.selected_document.import_number || "");
  formData.append(
    "affects_number_document",
    state.selected_document.affects_number_document || ""
  );
  formData.append(
    "affects_date_document",
    state.selected_document.affects_date_document || ""
  );
  formData.append("observation", state.selected_document.observation || "");

  // Retenciones
  formData.append("ret_fue_total", state.selected_document.ret_fue_total || "");
  formData.append("ret_iva_total", state.selected_document.ret_iva_total || "");
  formData.append("ret_ica_total", state.selected_document.ret_ica_total || "");

  state.document_files_new.map(fn => {
    formData.append("filesUpload[]", fn);
  });

  state.document_files_delete.map(fn => {
    formData.append("filesDelete[]", JSON.stringify(fn));
  });

  formData.append("_method", "PUT");
  formData.append("date_entry", state.selected_document.documents_information.date_entry);
  formData.append("name_document", state.selected_document.documents_information.name_document);

  await $http
    .post(`/api/documento/${document_id}/actualizar-datos-generales`, formData)
    .then(async response => {
      await commit("setAction", "create");
      await commit("setFilenameSelectedDocument", response.data);
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "¡Error!",
        message: "Error al actualizar los valores del documento.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Calcula los valores del documento.
 * @param commit
 * @param state
 * @param rootState
 * @returns {Promise<void>}
 */
export async function updateDocumentValues({ commit, state }) {
  let data = {
    document_id: state.selected_document.id,
    model_id: state.selected_model.id,
    warehouse_id: state.selected_document.warehouse_id,
    edit_base_retention: state.edit_base_retention,
    document_products_edit_iva: state.document_products_edit_iva,
    date_entry: state.selected_document.documents_information.date_entry,
    document_name: state.selected_document.documents_information.document_name
  };

  // Valor total de la retencion.
  if (state.edit_retention) {
    data.edit_retention = state.edit_retention;
    data.ret_fue_total = state.selected_document.ret_fue_total;
    data.ret_iva_total = state.selected_document.ret_iva_total;
    data.ret_ica_total = state.selected_document.ret_ica_total;
  }

  await $http
    .post("/api/documento/actualizar-valores/", data)
    .then(response => {
      commit("setUpdateDocumentValues", response.data);
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "¡Error!",
        message: "Error al actualizar los valores del documento.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function updateDocumentProductsValues({ commit }, products) {
  await $http
    .post("/api/documento/actualizar-valores-productos/", {
      products
    })
    .then(response => {
      response.data.data.map(product => {
        commit("addDocumentProduct", product);
      });
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "¡Error!",
        message:
          "Error al actualizar los valores de los productos del documento.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Editar las retenciones del documento.
 * @param commit
 * @param state
 * @param rootState
 * @returns {Promise<void>}
 */
export async function editRetentioDocument({ commit, state }) {
  let data = {
    // datos necesarios.
    document_id: state.selected_document.id,
    model_id: state.selected_model.id,
    warehouse_id: state.selected_document.warehouse_id
  };

  // Valor total de la retencion.
  if (state.edit_retention) {
    data.edit_retention = state.edit_retention;
    data.ret_fue_total = state.selected_document.ret_fue_total;
    data.ret_iva_total = state.selected_document.ret_iva_total;
    data.ret_ica_total = state.selected_document.ret_ica_total;
  }

  // Valor base de la retencion.
  if (state.edit_base_retention) {
    data.edit_base_retention = state.edit_base_retention;
    data.ret_fue_base_value = state.selected_document.ret_fue_base_value;
    data.ret_iva_base_value = state.selected_document.ret_iva_base_value;
    data.ret_ica_base_value = state.selected_document.ret_ica_base_value;
    data.ret_fue_serv_base_value =
      state.selected_document.ret_fue_serv_base_value;
    data.ret_iva_serv_base_value =
      state.selected_document.ret_iva_serv_base_value;
    data.ret_ica_serv_base_value =
      state.selected_document.ret_ica_serv_base_value;
  }

  await $http
    .post("/api/documento/actualizar-retenciones/", data)
    .then(response => {
      commit("setUpdateDocumentValuesRetention", response.data);
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "¡Error!",
        message: "Error al actualizar los valores del documento.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Actualizar el documento activo
 * @param commit
 * @param state
 * @param rootState
 * @returns {Promise<void>}
 */
export async function finalizeUpdateDocument({ commit, state, getters }) {
  const user = JSON.parse(localStorage.user);

  let data = {
    save_document: state.saveDocument,
    document: {
      id: state.selected_document.id,
      observation: state.selected_document.observation
    },
    model_id: state.selected_model.id,
    user_id: user.id
  };

  if (state.selected_model.handle_payment_methods !== 2) {
    data.paymentMethods = state.modalPaymentMethods.data;
  }
  console.log(data);

  return await new Promise(async (resolve, reject) => {
    await $http
      .post("/api/finalizar-documento-actualizar", data)
      .then(async response => {
        if (response.status == 200) {
          await commit("setAction", "create");
          await commit("setFilenameSelectedDocument", response.data.document);
          resolve(response.data);
          console.log(response.data);
        } else {
          reject(response.data);
        }
      })
      .catch(error => {
        console.error(error);
        Notification.error({
          title: "¡Error!",
          message: "Error al Actualizar el documento.",
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        reject(error.response.data);
      });
  });
}

/**
 * Obtiene las promociones activas
 * @param commit
 * @param state
 * @returns {Promise<void>}
 */
export async function getPromotions({ commit, state }) {
  await $http
    .get("/api/promociones-activas")
    .then(response => {
      commit("setPromotions", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 *
 * @param state
 * @param commit
 * @param dispatch
 * @param rootState
 * @returns {Promise<void>}
 */
export async function validateAllPromotionRules({
  state,
  commit,
  dispatch,
  rootState
}) {
  state.promotions.map(promotion => {
    dispatch("validatePromotionRules", promotion.id);
  });
}

/**
 *
 * @param state
 * @param commit
 * @param dispatch
 * @param rootState
 * @param promotion_product
 * @returns {Promise<void>}
 */
export async function changePromotionProduct(
  { state, commit, dispatch, rootState },
  promotion_product
) {
  dispatch("validatePromotionRules", promotion_product.promotion_id);
}

/**
 *
 * @param state
 * @param commit
 * @param dispatch
 * @param rootState
 * @param promotion_id
 * @returns {Promise<void>}
 */
export async function validatePromotionRules(
  { state, commit, dispatch, rootState },
  promotion_id
) {
  let promotions = JSON.parse(JSON.stringify(state.promotions));

  promotions.map(promotion => {
    if (promotion.id == promotion_id) {
      let disable_button = false;
      let quantity = 0;
      let amount = 0;

      if (promotion.minimum_amount) {
        let minimum_amount = promotion.minimum_amount;

        promotion.products_promotions.map(ppp => {
          amount += parseInt(ppp.product.quantity);
        });

        disable_button = amount < minimum_amount;
        quantity = Math.floor(amount / minimum_amount);
      } else if (promotion.minimum_gross_value) {
        let minimum_gross = promotion.minimum_gross_value;

        promotion.products_promotions.map(ppp => {
          amount +=
            parseInt(ppp.product.price.price) * parseInt(ppp.product.quantity);
        });

        disable_button = amount < minimum_gross;
        quantity = Math.floor(amount / minimum_gross);
      } else {
        promotion.products_promotions.map(ppp => {
          let minimum_unit = ppp.minimum_amount;
          amount = ppp.product.quantity;

          disable_button = amount < minimum_unit;
          quantity = Math.floor(amount / minimum_unit);
        });
      }

      promotion.disable_button = disable_button;

      if (promotion.specific_product) {
        promotion.specific_product.quantity = quantity;
      }
    }
  });

  commit("setPromotions", promotions);
}

/** Buscar productos según los criterios de clasificación */
export function fetchProductsByClasification({ commit, state }, params) {
  $http.get("/api/productos-clasificacion", params).then(response => {
    commit("setProductsFiltered", response.data.data);
  });
}

/**
 *
 * @param {*} param0
 * @param {*} params
 */
export async function uploadFileProductsDocument(
  { commit, state, dispatch },
  params
) {
  // let params = {
  //   params: {
  //     branchoffice_id: localStorage.selected_branchoffice,
  //   model_id: state.selected_model.id
  //   }
  // }
  await $http
    .post(
      `/api/documento/cargar-productos-excel/${params.document_id}?model_id=${state.selected_model.id}&branchoffice_id=${localStorage.selected_branchoffice}&branchoffice_storage=${localStorage.selected_branchoffice}&warehouse_id=${state.selected_document.warehouse_id}&branchoffice_warehouse_id=${state.doc_product_default_branchoffice_warehouse_id}&branchoffice_warehouse_id_product=${state.doc_product_default_branchoffice_warehouse_id}`,
      params.file
    )
    .then(async response => {
      Notification.success({
        title: "Exito!",
        message: "Registro actualizado.",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });

      let products = response.data.message;
      let document_id = null;

      products.map(p => {
        //Guardamos la info
        commit("addDocumentProduct", p);
        document_id = p.document_id;
      });
    })
    .catch(error => {
      console.error(error);

      var dataError = error.response.data.message;

      var data = dataError.data;

      if (data) {
        var csv = "REFERENCIA\n";
        data.map((v, i) => {
          csv += `${v}\n`;
        });

        var hiddenElement = document.createElement("a");
        hiddenElement.href = "data:text/csv;charset=utf-8," + encodeURI(csv);
        hiddenElement.target = "_blank";
        hiddenElement.download = "productos que no existen.csv";
        hiddenElement.click();
      }

      Notification.error({
        title: "Error!",
        message: dataError.text ? dataError.text : "Erro al actualizar",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Este metodo trae los documentos segun los parametros
 *
 * @author Kevin Galindo
 */

export async function searchDocumentGlobal({ commit, state }) {
  state.TableSearchGlobal.loading = true;
  var params = {
    params: state.TableSearchGlobal.params
  };

  await $http
    .get("/api/documento/buscar/", params)
    .then(response => {
      commit("setSearchDocumentGlobal", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al buscar el documento",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });

  state.TableSearchGlobal.loading = false;
}

/**
 * Este metodo retorna la traza del backorder.
 *
 * @author Kevin Galindo
 */

export async function getTraceDocumentProduct({ commit, state }, data) {
  let params = {
    params: {
      model_id: state.selected_model.id,
      document_id: data.document_id
    }
  };
  await $http
    .get(`/api/traza-producto-documento/${data.document_product_id}`, params)
    .then(response => {
      commit("setTraceDocumentProduct", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al traer la traza del documento.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 * Actualizar las retenciones
 *
 * @author Kevin Galindo
 */

export async function setConfigurationRetention({ commit, state }, data) {
  await $http
    .post(`/api/sucursal-contacto/actualizar-retencion`, data)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "Registro actualizado.",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setEditRetentionDocument", true);
    });
}

/**
 *
 * Este metodo se encarga de calcular el valor de
 * las retenciones
 *
 * @param {*} data
 * @author Kevin Galindo
 */
export async function calculateRetention({ state, getters }, data) {
  if (state.action === "view") return;

  let retentions = state.tableRetention;

  let valueToCalculate = 0;
  let totalValueRetention = 0;

  retentions.map((retention, index) => {
    // Seteamos las variables
    valueToCalculate = 0;
    totalValueRetention = 0;

    // Recorremos el arreglo
    retention.fields.map((field, inxf) => {
      // Seteamos los valores
      field.total = 0;

      // IVA
      if (retention.prefix === "IVA") {
        if (field.prefix == "product") {
          valueToCalculate = Math.round(retention.base_value);
        }

        if (field.prefix == "service") {
          valueToCalculate = Math.round(retention.base_serv_value);
        }
      }

      // VR BRUTO
      else {
        if (field.prefix == "product") {
          valueToCalculate = Math.round(retention.base_value);
        }

        if (field.prefix == "service") {
          valueToCalculate = Math.round(retention.base_serv_value);
        }
      }

      if (
        state.selected_model.operation_type.code == "155" ||
        // state.selected_model.operation_type.code == "156" ||
        state.selected_model.operation_type.code == "270"
      ) {
        if (retention.prefix === "FUE") {
          if (field.prefix == "product") {
            if (
              parseInt(state.selected_document.document.ret_fue_base_value) >=
              parseInt(field.vr_limit)
            ) {
              field.total = Math.round(
                (valueToCalculate * field.percentage) / 100
              );
              totalValueRetention += field.total;
            }
          }

          if (field.prefix == "service") {
            if (
              parseInt(
                state.selected_document.document.ret_fue_serv_base_value
              ) >= parseInt(field.vr_limit)
            ) {
              field.total = Math.round(
                (valueToCalculate * field.percentage) / 100
              );
              totalValueRetention += field.total;
            }
          }
        }

        if (retention.prefix === "IVA") {
          if (field.prefix == "product") {
            if (
              parseInt(state.selected_document.document.ret_iva_base_value) >=
              parseInt(field.vr_limit)
            ) {
              field.total = Math.round(
                (valueToCalculate * field.percentage) / 100
              );
              totalValueRetention += field.total;
            }
          }

          if (field.prefix == "service") {
            if (
              parseInt(
                state.selected_document.document.ret_iva_serv_base_value
              ) >= parseInt(field.vr_limit)
            ) {
              field.total = Math.round(
                (valueToCalculate * field.percentage) / 100
              );
              totalValueRetention += field.total;
            }
          }
        }

        if (retention.prefix === "ICA") {
          if (field.prefix == "product") {
            if (
              parseInt(state.selected_document.document.ret_ica_base_value) >=
              parseInt(field.vr_limit)
            ) {
              field.total = Math.round(
                (valueToCalculate * field.percentage) / 100
              );
              totalValueRetention += field.total;
            }
          }

          if (field.prefix == "service") {
            if (
              parseInt(
                state.selected_document.document.ret_ica_serv_base_value
              ) >= parseInt(field.vr_limit)
            ) {
              field.total = Math.round(
                (valueToCalculate * field.percentage) / 100
              );
              totalValueRetention += field.total;
            }
          }
        }
      } else {
        if (valueToCalculate != 0 && valueToCalculate >= field.vr_limit) {
          field.total = Math.round((valueToCalculate * field.percentage) / 100);
          totalValueRetention += field.total;
        } else {
          field.total = 0;
          totalValueRetention += field.total;
        }
      }

      // Seteamos las variables del documento
      if (retention.prefix === "FUE") {
        if (field.prefix == "product") {
          state.selected_document.ret_fue_prod_vr_limit = field.vr_limit;
          state.selected_document.ret_fue_prod_percentage = field.percentage;
          state.selected_document.ret_fue_prod_total = field.total;

          //base
          state.selected_document.ret_fue_base_value = retention.base_value;
        }

        if (field.prefix == "service") {
          state.selected_document.ret_fue_serv_vr_limit = field.vr_limit;
          state.selected_document.ret_fue_serv_percentage = field.percentage;
          state.selected_document.ret_fue_serv_total = field.total;

          //base
          state.selected_document.ret_fue_serv_base_value =
            retention.base_serv_value;
        }
      }

      if (retention.prefix === "IVA") {
        if (field.prefix == "product") {
          state.selected_document.ret_iva_prod_vr_limit = field.vr_limit;
          state.selected_document.ret_iva_prod_percentage = field.percentage;
          state.selected_document.ret_iva_prod_total = field.total;

          // base
          state.selected_document.ret_iva_base_value = retention.base_value;
        }

        if (field.prefix == "service") {
          state.selected_document.ret_iva_serv_vr_limit = field.vr_limit;
          state.selected_document.ret_iva_serv_percentage = field.percentage;
          state.selected_document.ret_iva_serv_total = field.total;

          //base
          state.selected_document.ret_iva_serv_base_value =
            retention.base_serv_value;
        }
      }

      if (retention.prefix === "ICA") {
        if (field.prefix == "product") {
          state.selected_document.ret_ica_prod_vr_limit = field.vr_limit;
          state.selected_document.ret_ica_prod_percentage = field.percentage;
          state.selected_document.ret_ica_prod_total = field.total;

          //base
          state.selected_document.ret_ica_base_value = retention.base_value;
        }

        if (field.prefix == "service") {
          state.selected_document.ret_ica_serv_vr_limit = field.vr_limit;
          state.selected_document.ret_ica_serv_percentage = field.percentage;
          state.selected_document.ret_ica_serv_total = field.total;

          //base
          state.selected_document.ret_ica_serv_base_value =
            retention.base_serv_value;
        }
      }
    });

    // Asignamos el valor total
    retention.total = totalValueRetention;

    //Valores totales
    //Seteamos las variables del documento
    if (retention.prefix === "FUE") {
      state.selected_document.ret_fue_total = retention.total;
    }

    if (retention.prefix === "IVA") {
      state.selected_document.ret_iva_total = retention.total;
    }

    if (retention.prefix === "ICA") {
      state.selected_document.ret_ica_total = retention.total;
    }
  });
}

/**
 *
 * Este metodo valida que tipo de retencion es y se
 * encarga de traer la informacion correspodiente.
 *
 * @param {*} data
 * @author Kevin Galindo
 */
export async function validateRetentionData({ state, commit }) {
  // RETENCION NORMAL
  if (state.selected_model.retention_management == 1) {
    commit("getParamsWarehouseRetention");
  }

  // RETENCION CON DOCUMENTO BASE
  if (state.selected_model.retention_management == 3) {
    commit("getParamsBaseDocumentRetention");
  }
}

export async function showPdfDocumentById({ state, commit }, id) {
  await $http
    .get(`/api/imprimir-pedido/${id}`)
    .then(response => {
      let fileName = response.data.data.pdf;
      let appLink = getApiDomain();
      //let appLink = 'http://localhost:8085'

      window.open(
        `https://docs.google.com/viewerng/viewer?url=${appLink}${fileName}`,
        "_blank"
      );
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al generar el pdf",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

/**
 *
 * Este metodo recalcula todo el documento
 *
 * @author Kevin Galindo
 */
export async function getDocumentByIdUpdatedValues(
  { state, commit, getters, dispatch },
  id
) {
  let data = {
    params: {
      model_id: state.selected_model.id
    }
  };

  await $http
    .get("/api/carrito/" + id, data)
    .then(response => {
      let dataCommit = {
        document: response.data,
        getTotalIvaProducts: Math.round(getters.getTotalIvaProducts),
        getTotalIvaServices: Math.round(getters.getTotalIvaServices),
        getValBrutInProducts: Math.round(getters.getValBrutInProducts),
        getValBrutInServices: Math.round(getters.getValBrutInServices)
      };
      commit("setUpdateValuesDocument", dataCommit);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error al actualizar el documento",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });

  // Calculamos la retencion
  if (state.selected_model.retention_management != 0) {
    await dispatch("validateRetentionData");
    await dispatch("calculateRetention");
  }
}

// Obtener cartera del cliente
export async function getReceivablesByContactId({ commit, state }, contact_id) {
  // Validmos que el id no sea 'undefined'
  if (!contact_id) return;

  await $http
    .get(`/api/cartera/${contact_id}`)
    .then(response => {
      commit("setValuesReceivablesByContactId", response.data);
    })
    .catch(error => {
      /*Notification.error({
				title: '¡Error!',
				message: error.response.data.message,
				type: 'error',
				duration: 1500,
				customClass: 'notification-box'
			})*/
    });
}

// Obtener cartera del cliente
export async function deleteDocumentProduct({ commit, state, dispatch }, document_product_id) {
  // Validmos que el id no sea 'undefined'
  if (!document_product_id) return;

  state.updating = state.action === "update-document";

  let params = {
    params: {
      forceDelete: state.updating ? 0 : 1
    }
  };

  await $http.delete(`/api/items-carrito/${document_product_id}`, params)
    .then(response => {
      commit("deleteItemDocumentsProducts", document_product_id);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error al eliminar el elemento.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function searchmails({ state, commit }, text) {
  let params = {
    params: {
      filter: text,
      contact: state.selected_document.contact.id
    }
  };

  await $http
    .get("/api/buscar-correo", params)
    .then(response => {
      commit("setMails", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function updatemail({ state, commit }, to, contact) {
  let params = {
    to: to,
    contact: state.selected_document.contact.id
  };

  await $http
    .put(`/api/actualizar-correo`, params)
    .then(response => {
      commit("setMails", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error al eliminar el elemento.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function sendmail({ state, commit }, params) {
  await $http
    .post("/api/enviar-correo", params)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "El correo ha sido enviado exitosamente.",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

//Obtener los parametros de una tabla
export async function fetchParametersTable({ commit, state }, paramtable_id) {
  if (paramtable_id) {
    const params = {
      params: {
        paramtable_id: paramtable_id
      }
    };
    await $http
      .get("/api/parametros", params)
      .then(response => {
        commit("setParametersTable", response.data.data);
      })
      .catch(error => {});
  }
}

export async function downloadDocument({ state, commit }, data) {
  let params = {
    params: {
      branchoffice_id: data.branchoffice_id,
      model_id: data.model_id,
      fromDate: data.filterDate[0],
      toDate: data.filterDate[1]
    }
  };

  await $http
    .get(`/api/descargar-documentos-con-estado`, params)
    .then(response => {
      let objArray = response.data.data;
      let nameDoc = response.data.name_excel;

      const array =
        typeof objArray !== "object" ? JSON.parse(objArray) : objArray;
      let str = "",
        dataCsv = "";
      if (array.length > 0) {
        str =
          `${Object.keys(array[0])
            .map(value => `"${value}"`)
            .join(";")}` + "\r\n";

        dataCsv = array.reduce((str, next) => {
          str +=
            `${Object.values(next)
              .map(value => `"${value}"`)
              .join(";")}` + "\r\n";
          return str;
        }, str);
      }

      var hiddenElement = document.createElement("a");
      hiddenElement.href = "data:text/csv;charset=utf-8," + encodeURI(dataCsv);
      hiddenElement.target = "_blank";
      hiddenElement.download = nameDoc + ".csv";
      hiddenElement.click();
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error al descargar los docuemntos.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function updateDocCruce({ state, commit }) {
  let data = {
    document_id: state.selected_document.id,
    affects_prefix_document: state.selected_document.affects_prefix_document,
    affects_number_document: state.selected_document.affects_number_document,
    affects_date_document: state.selected_document.affects_date_document
  };
  await $http
    .post("/api/actualizar-doc-cruce", data)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "Se ha actualizado el documento cruce..",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al actualizar el docuemnto cruce.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

// Actualizar Documento
export async function updateDocument({ state, commit }, data) {
  let document_id = state.selected_document.id;

  if (document_id) {
    await $http
      .put(`/api/carrito/${document_id}`, data)
      .then(response => {})
      .catch(error => {
        console.error(error);
      });
  }
}

export async function createPDf({ commit, state }, id) {
  await $http
    .get(`/api/pdf/${id}`)
    .then(response => {
      // console.log(response.data.data.pdf);
      commit("setNamePdf", response.data.data.pdf);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al traer el pdf del documento.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function duplicateDocumentProduct(
  { commit, state, dispatch },
  dp
) {
  let data = {
    product_id: dp.product_id,
    quantity: 0,
    is_benefit: false,
    description: dp.product.description,
    contact_id: state.selected_document.contact_id,
    document_id: state.selected_document.id,
    model_id: state.selected_model.id,
    branchoffice_warehouse_id: dp.branchoffice_warehouse_id
      ? dp.branchoffice_warehouse_id
      : state.selected_document.branchoffice_warehouse_id,
    branchoffice_id: state.selected_document.branchoffice_id,
    base_unit_value: dp.base_unit_value,
    original_quantity: dp.original_quantity,
    unit_value_before_taxes: dp.unit_value_before_taxes,
    physical_unit: dp.physical_unit
  };

  await $http
    .post("/api/items-carrito", data)
    .then(response => {
      //Guardamos la info
      commit("addDocumentProduct", response.data.data);
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function cancelDocument({ commit, state }) {
  const user = JSON.parse(localStorage.user);
  let data = {
    user_id: user.id
  };

  let document_id =
    state.action === "update-document"
      ? state.selected_document.ref_update_doc
      : state.selected_document.id;

  await $http
    .post(`/api/anular-documento/${document_id}`, data)
    .then(async response => {
      await commit("setAction", "create");
      await commit("setFilenameSelectedDocument", response.data);
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

// MODAL CREAR TERCERO
export async function fetchCities({ commit, state }) {
  let department_id = state.newContact.department_id;

  await $http
    .get("/api/ciudades?department_id=" + department_id)
    .then(response => {
      commit("setCities", response.data.data);
    });
}

export async function fetchDepartments({ commit, state }) {
  if (!state.departments.length) {
    await $http.get("/api/departamentos").then(response => {
      commit("setDepartments", response.data.data);
    });
  }
}

//Obtener tipos de identificación
export async function fetchDocumentTypes({ commit, state }) {
  if (!state.identificationTypes.length) {
    await $http.get(`/api/parametros?paramtable_id=1`).then(response => {
      commit("setIdentificationTypes", response.data.data);
    });
  }
}

export async function storeContact({ commit, state }) {
  return await new Promise(async (resolve, reject) => {
    commit("setTypeNewContact");

    let data = state.newContact;
    data.branchoffice_id = localStorage.selected_branchoffice;
    data.direct_invoice_blocking = 0;

    await $http
      .post(`/api/creacion-basica-tercero`, data)
      .then(response => {
        Notification.success({
          title: "Éxito",
          message: "Cliente registrado exitosamente",
          type: "success",
          duration: 2000,
          customClass: "notification-box"
        });
        commit("setModalNewContact", false);
        resolve(response.data.data);
      })
      .catch(error => {
        reject(error.response.data.message);
      });
  });
}

// Calcular el digito de verificación
export function calculateCheckDigit({ state }) {
  return new Promise((resolve, reject) => {
    let vpri, x, y, dv, z;

    // Se limpia el Nit
    state.newContact.identification = state.newContact.identification.replace(
      /\s/g,
      ""
    ); // Espacios
    state.newContact.identification = state.newContact.identification.replace(
      /,/g,
      ""
    ); // Comas
    state.newContact.identification = state.newContact.identification.replace(
      /\./g,
      ""
    ); // Puntos
    state.newContact.identification = state.newContact.identification.replace(
      /-/g,
      ""
    ); // Guiones

    // Se valida el nit
    if (isNaN(state.newContact.identification)) {
      reject(
        "El nit/cédula '" +
          state.newContact.identification +
          "' no es válido(a)."
      );
    }
    // Procedimiento
    vpri = new Array(16);
    z = state.newContact.identification.length;

    vpri[1] = 3;
    vpri[2] = 7;
    vpri[3] = 13;
    vpri[4] = 17;
    vpri[5] = 19;
    vpri[6] = 23;
    vpri[7] = 29;
    vpri[8] = 37;
    vpri[9] = 41;
    vpri[10] = 43;
    vpri[11] = 47;
    vpri[12] = 53;
    vpri[13] = 59;
    vpri[14] = 67;
    vpri[15] = 71;

    x = 0;
    y = 0;
    for (let i = 0; i < z; i++) {
      y = state.newContact.identification.substr(i, 1);
      // console.log ( y + "x" + vpri[z-i] + ":" ) ;
      x += y * vpri[z - i];
      // console.log ( x ) ;
    }

    y = x % 11;
    resolve(+(y > 1 ? 11 - y : y));
  });
}

//Obtener tipos de vehiculo
export async function fetchVehicles({ commit, state }, filter) {
  if (filter.length >= 3) {
    let url = "/api/tipos-de-vehiculo?filter=" + filter;

    await $http.get(url).then(response => {
      commit("setVehicles", response.data.data);
    });
  }
}

export async function getContactForIdentification(
  { commit, state, dispatch },
  identification
) {
  await $http
    .get(`/api/terceros/id/${identification}`)
    .then(async response => {
      if (response.data.success) {
        commit("setNewContact", response.data.data);
        await dispatch("fetchDocumentTypes");
        await dispatch("fetchDepartments");
        dispatch("fetchCities");
        await dispatch("fetchPersonClasses");
        await dispatch("fetchTaxpayerTypes");
        await dispatch("fetchTaxRegimes");
        await dispatch("fetchFiscalResponsibilitys");
        await dispatch("getVehiclesContact", state.newContact.id);
        if (state.vehiclesContact.length) {
          await dispatch(
            "validateLicensePlate",
            state.vehiclesContact[0].license_plate
          );
        }
      } else {
        commit("resetNewContact", true);
      }
    })
    .catch(error => {
      commit("resetNewContact", true);
    });
}

export async function validateLicensePlate({ state, commit }) {
  if (state.newContact.id) {
    await $http
      .get(
        "/api/vehiculos-terceros?license_plate=" +
          state.newContact.vehicle.license_plate +
          "&contact_id=" +
          state.newContact.id
      )
      .then(async response => {
        if (response.data.data.length) {
          commit("setVehicles", [response.data.data[0].vehicle_type]);
          commit("setNewContactVehicle", response.data.data[0]);
          commit("setDataVehicle", response.data.data[0].vehicle_type_id);
        }
      });
  }
}

export async function fetchPersonClasses({ state, commit }) {
  if (!state.personClasses.length) {
    await $http.get(`/api/parametros?code_table=055`).then(response => {
      commit("setPersonClasses", response.data.data);
    });
  }
}

export async function fetchTaxpayerTypes({ state, commit }) {
  if (!state.taxpayerTypes.length) {
    await $http.get(`/api/parametros?code_table=056`).then(response => {
      commit("setTaxpayerTypes", response.data.data);
    });
  }
}

export async function fetchTaxRegimes({ state, commit }) {
  if (!state.taxRegimes.length) {
    await $http.get(`/api/parametros?code_table=057`).then(response => {
      commit("setTaxRegimes", response.data.data);
    });
  }
}

export async function fetchFiscalResponsibilitys({ state, commit }) {
  if (!state.fiscalResponsibilitys.length) {
    await $http.get(`/api/parametros?code_table=061`).then(response => {
      commit("setFiscalResponsibilitys", response.data.data);
    });
  }
}

//Obtener los datos del tercero seleccionado
export async function getNewContact({ commit, state }, contactId) {
  if (!state.brandsVehicles.length) {
    await $http.get(`/api/terceros/` + contactId).then(response => {
      commit("setNewContact", response.data.data);
    });
  }
}

//-------------------

// Metodos de pagos
export async function getPaymentMethods({ commit, state }) {
  await $http
    .get(`/api/parametros?paramtable_id=109&paginate=false`)
    .then(response => {
      commit("setPaymentMethods", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function documentsThreadHistoryByDocumentId(
  { commit },
  document_id
) {
  let params = {
    params: {
      document_id
    }
  };

  await $http
    .get("/api/documentos-historial", params)
    .then(response => {
      commit("setDocumentsThreadHistory", response.data.documents);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.response.data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchConveyors({commit}) {
  await $http.get("/api/obtener-transportadoras")
    .then(response => {
      commit("setConveyors", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error al obtener las transportadoras",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
