/**
 * Calcula y obtiene el subtotal del documento (total antes de impuestos)
 * @param state
 * @returns {number}
 */
export function getSubTotal(state) {
  let sub_total = 0;

  state.selected_document.documents_products.map(dp => {
    sub_total += dp.total_value_brut;
  });

  return sub_total;
}

/**
 * Calcula y obtiene el valor bruto de los productos (total antes de impuestos)
 * @param state
 * @returns {number}
 */
export function getValBrutInProducts(state) {
  let vr_brut = 0;

  state.selected_document.documents_products.map(dp => {
    if (dp.product.line) {
      if (dp.product.line.tipe != 2) {
        vr_brut += parseInt(dp.total_value_brut);
      }
    }
  });

  return vr_brut;
}

/**
 * Calcula y obtiene el valor bruto de los Servicios (total antes de impuestos)
 * @param state
 * @returns {number}
 */
export function getValBrutInServices(state) {
  let vr_brut = 0;

  state.selected_document.documents_products.map(dp => {
    if (dp.product.line) {
      if (dp.product.line.tipe == 2) {
        vr_brut += parseInt(dp.total_value_brut);
        //(dp.unit_value_before_taxes - dp.discount_value) * dp.quantity;
      }
    }
  });

  return vr_brut;
}

/**
 * Calcula y obtiene el total del iva de los productos del documento
 * @param state
 * @returns {number}
 */
export function getTotalIvaProducts(state) {
  let iva = 0;
  state.selected_document.documents_products.map(dp => {
    dp.document_product_taxes.map(dpt => {
      if (dp.product.line) {
        if (dp.product.line.tipe != 2) {
          if (dpt.tax.code == "10") {
            iva += parseInt(dp.total_value_iva);
          }
        }
      }
    });
  });

  return iva;
}
/**
 * Calcula y obtiene el total del iva de los productos del documento
 * @param state
 * @returns {number}
 */
export function getTotalIvaServices(state) {
  let iva = 0;
  state.selected_document.documents_products.map(dp => {
    dp.document_product_taxes.map(dpt => {
      if (dp.product.line) {
        if (dp.product.line.tipe == 2) {
          if (dpt.tax.code == "10") {
            iva += parseInt(dp.total_value_iva);
          }
        }
      }
    });
  });

  return iva;
}

/**
 * Calcula y obtiene el total del iva del documento
 * @param state
 * @returns {number}
 */
export function getTotalIva(state) {
  let iva = 0;
  state.selected_document.documents_products.map(dp => {
    /*dp.document_product_taxes.map(dpt => {
      if (dpt.tax.code == '10') {
        iva += (dp.unit_value_before_taxes - dp.discount_value) * dp.quantity * dpt.tax_percentage / 100
      }
    })*/
    iva += dp.total_value_iva;
  });
  return iva;
}

/**
 * Calcula y obtiene el total de cada uno de los impuestos del documento
 * @param state
 * @returns {[]}
 */
export function getTotalTaxes(state) {
  let sub_total = getSubTotal(state);
  let iva = getTotalIva(state);

  let iva_retention = true;
  let ica_retention = true;
  let source_retention = true;

  let contact = state.selected_document.contact;

  if (contact) {
    let type = state.params_contacts.type;

    switch (type) {
      case "is_customer":
        if (contact.customer) {
          iva_retention = contact.customer.retention_iva;
          ica_retention = contact.customer.retention_iva;
          source_retention = contact.customer.retention_source;
        }
        break;
      case "is_provider":
        if (contact.provider) {
          iva_retention = contact.customer.retention_iva;
          ica_retention = contact.customer.retention_iva;
          source_retention = contact.customer.retention_source;
        }
        break;
    }
  }

  let taxes = [];

  state.selected_document.documents_products.map(dp => {
    dp.document_product_taxes.map(dpt => {
      let tax = {
        description: dpt.tax.description,
        percentage: dpt.tax_percentage,
        total: 0,
        increase: dpt.tax.increase
      };

      if (dpt.tax.base == 1) {
        if (sub_total >= dpt.base_value) {
          tax.total = iva; //sub_total * dpt.tax_percentage / 100
        }
      } else if (dpt.tax.base == 2) {
        if (iva >= dpt.base_value) {
          tax.total = (iva * dpt.tax_percentage) / 100;
        }
      }

      if (tax.total != 0) {
        let found_tax = taxes.find(
          t =>
            t.description == tax.description && t.percentage == tax.percentage
        );
        if (!found_tax) {
          let add = true;
          switch (tax.description) {
            case "RETE-IVA":
              add = iva_retention;
              break;
            case "RETE-ICA":
              add = ica_retention;
              break;
            case "RETE-FUENTE":
              add = source_retention;
              break;
          }

          if (add) {
            taxes.push(tax);
          }
        }
      }
    });
  });

  return taxes;
}

/**
 * Calcula el total del documento incluyendo impuestos
 * @param state
 * @returns {number}
 */
export function getTotal(state) {
  let sub_total = getSubTotal(state);
  let taxes = getTotalTaxes(state);
  let total_taxes = 0;

  taxes.map(tax => {
    if (tax.increase) {
      total_taxes += tax.total;
    } else {
      total_taxes -= tax.total;
    }
  });

  return sub_total + total_taxes;
}

/**
 *
 * Traer las diferencias
 */
export function getPurchaseDifference(state) {
  let documentsProduct = state.selected_document.documents_products;
  let productPurchaseDifference = [];

  if (state.selected_document.id === state.selected_document.document.id) {
    return productPurchaseDifference;
  }

  documentsProduct.map((dp, indexdp) => {
    // DIFERENCIA POR CANTIDAD
    if (parseInt(dp.quantity) != parseInt(dp.physical_unit)) {
      productPurchaseDifference.push({
        id: dp.id,
        description: `${dp.product.code} - ${dp.product.description}`,
        difference_quantity: dp.physical_unit - dp.quantity, //Math.abs(dp.quantity - dp.physical_unit),
        difference_value:
          dp.unit_value_before_taxes * (dp.physical_unit - dp.quantity),
        type_difference: 0
      });
    }

    // DIFERENCIA POR VALORES
    if (dp.unit_value_before_taxes != dp.base_unit_value) {
      let dpRepeat = productPurchaseDifference.find(ppd => ppd.id == dp.id);
      let valDif =
        (dp.base_unit_value - dp.unit_value_before_taxes) * dp.quantity;

      if (dpRepeat) {
        if (dpRepeat.difference_quantity != 0) {
          dpRepeat.difference_value += valDif;
        } else {
          dpRepeat.difference_value = valDif;
        }
      } else {
        productPurchaseDifference.push({
          id: dp.id,
          description: `${dp.product.code} - ${dp.product.description}`,
          difference_quantity: 0,
          difference_value: valDif,
          type_difference: 0
        });
      }
    }
  });

  return productPurchaseDifference;
}

export function getBranchofficeWarehousesSelect(state) {
  const branchoffice_change = state.selected_model.branchoffice_change;
  const selected_branchoffice = localStorage.selected_branchoffice;
  let branchofficeWarehousesArray = [];

  if (branchoffice_change == 2) {
    state.branchofficeWarehouses.map((branchoffice, index) => {
      let data = {
        label: `${branchoffice.code} - ${branchoffice.name}`,
        name: branchoffice.name,
        options: []
      };

      branchoffice.warehouses.map(warehouse => {
        let dataWarehouse = {
          value: warehouse.id,
          text: `${warehouse.warehouse_code} - ${warehouse.warehouse_description}`
        };
        data.options.push(dataWarehouse);
      });

      branchofficeWarehousesArray.push(data);
    });

    return branchofficeWarehousesArray;
  } else if (branchoffice_change == 1) {
    let branchoffice = state.branchofficeWarehouses.find(
      bw => bw.id == selected_branchoffice
    );

    let data = {};

    if (branchoffice) {
      data = {
        label: `${branchoffice.code} - ${branchoffice.name}`,
        name: branchoffice.name,
        options: []
      };

      branchoffice.warehouses.map(warehouse => {
        let dataWarehouse = {
          value: warehouse.id,
          text: `${warehouse.warehouse_code} - ${warehouse.warehouse_description}`
        };
        data.options.push(dataWarehouse);
      });

      branchofficeWarehousesArray.push(data);
    }

    return branchofficeWarehousesArray;
  } else {
    return branchofficeWarehousesArray;
  }
}

export function footerTotalTotalValueIva(state) {
  let documents_products = state.selected_document.documents_products;
  let total_value_iva = 0;
  let total_value_brut = 0;

  if (documents_products) {
    documents_products.map(dp => {
      let total_value_iva_dp = dp.total_value_iva || 0;
      total_value_iva += parseFloat(total_value_iva_dp);
    });

    documents_products.map(
      dp => (total_value_brut += parseFloat(dp.total_value_brut))
    );
  }

  return total_value_iva;
}
