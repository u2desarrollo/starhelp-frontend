import {
  defaultNewContact,
  defaultNewVehicle,
  defaultSelectedDocument,
  defaultSelectedDocumentProduct
} from "./state";

/**
 * Asigna un valor a la variable de state error
 * @param state
 * @param error
 */
export function setError(state, error) {
  state.error = error;
}

/**
 * Asigna un valor a la variable de state action
 * @param state
 * @param action
 */
export function setAction(state, action) {
  state.action = action;
}

export function setdocumentsProductsImport(state , documents_products_import){
  state.documents_products_import = documents_products_import.documents_product_import
}

export function setexpenses(state , expenses){
  state.expenses = expenses.expenses
}
/**
 * Asigna valores por defecto a branchoffice_id y branchoffice_warehouse_id de selected_document
 * Los valores por defecto son los que se encuentran en el localStorage
 * @param state
 */
export function setDefaultBranchOfficeAndBranchOfficeWarehouse(state) {
  let branchoffice_id = localStorage.getItem("selected_branchoffice");
  let branchoffice_warehouse_id = localStorage.getItem(
    "selected_branchoffice_warehouse"
  );

  state.selected_document.branchoffice_id = Number(branchoffice_id);
  state.selected_document.branchoffice_warehouse_id = Number(
    branchoffice_warehouse_id
  );
}

/**
 * Asigna un valor a la variable de state selected_model
 * @param state
 * @param selected_model
 */
export function setSelectedModel(state, selected_model) {
  state.selected_model = selected_model;
}

/**
 *
 * @param state
 * @param data
 */
export function setModels(state, data) {
  state.models = data;
}

export function setTableSearchGlobalcolumns(state, erp){

  let user = JSON.parse(localStorage.getItem("user"));
    let manager_wms = null;
    if (user) {
      manager_wms = user.subscribers[0].manager_wms;
    }

  state.TableSearchGlobal.columns= [
      {
        label: "Fecha",
        key: "document_date"
      },
      {
        label: "Tipo Documento",
        key: "vouchertype.name_voucher_type"
      },
      {
        label: "Número",
        key: "consecutive",
        sortable: true
      },
      {
        label: "Tercero",
        key: "warehouse"
      },
      {
        label: "Vendedor",
        key: "seller.name"
      },
      {
        label: "Valor Total",
        key: "total_value"
      },
      erp ? 
      {
        label: "Erp",
        key: "erp"
      }
      :{},
      ,
      ,
      manager_wms ? 
      {
        label: "Estado WMS",
        key: "error_wms",
      }
      :{},
      ,
      {
        label: "Acciones",
        key: "actions"
      }
    ]
}

/**
 *
 * @param state
 * @param data
 */
export function setSearchDocumentGlobal(state, data) {
  // Agregamos los datos de la tabla
  state.TableSearchGlobal.data = data.data;

  // Quitamos los valores de la tabla del arreglo
  delete data.data;

  // Agregamos los datos de la paginacion
  state.TableSearchGlobal.pagination = data;
}

/**
 * Restablece el valor por defecto de la variable de state selected_document
 */
export function resetSelectedDocument(state) {
  state.selected_document = defaultSelectedDocument();
}

/**
 * Asigna un valor a la variable de state fetch_data
 * @param state
 * @param fetch_data
 */
export function setFetchData(state, fetch_data) {
  state.fetch_data = fetch_data;
}

/**
 * Asigna un valor al atributo type de la variable de state params_contacts
 * @param state
 * @param selected_model
 */
export function setTypeContact(state, selected_model) {
  let contact_type = selected_model.contact_type;

  let type = "is_customer";

  switch (contact_type) {
    case 1:
      type = "is_customer";
      break;
    case 2:
      type = "is_provider";
      break;
    case 3:
      type = "is_employee";
      break;
  }

  state.params_contacts.type = type;
}

/**
 * Asigna un valor al atributo vouchertype_id de la variable de state selected_document
 * @param state
 * @param selected_model
 */
export function setVoucherTypeToSelectedDocument(state, selected_model) {
  let voucherType_id = selected_model.voucherType_id;

  state.selected_document.vouchertype_id = voucherType_id;
}

export function setFilenameSelectedDocument(state, selected_document) {
  state.selected_document.filename = selected_document.filename;
}

/**
 * Asigna un valor a la variable de state selected_document
 * @param state
 * @param branchoffices
 */
export function setSelectedDocument(state, selected_document) {
  //productos con promo
  selected_document.documents_products.map((dp, indexdp) => {
    if (dp.promotion) {
      dp.applied_promotion = dp.promotion.description;
    }

    // if (dp.is_benefit) {
    //   dp.unit_value_before_taxes = 0;
    // }
  });

  state.selected_document = selected_document;
}

/**
 * Corregimos las existencias
 *
 * @param state
 * @param branchoffices
 */
export function setStockDocumentProducts(state, document_products) {
  document_products.map((dp, index) => {
    let key_p = state.selected_document.documents_products.findIndex(
      sdp => sdp.id == dp.document_product_id
    );

    if (key_p >= 0) {
      state.selected_document.documents_products[
        key_p
      ].product.inventory[0].stock = dp.stock;
      state.selected_document.documents_products[key_p].quantity = dp.stock;
    }
  });
}

/**
 * Asigna un valor a la variable de state backup_documents_products
 * @param state
 * @param branchoffices
 */
export function setBackupDocumentsProducts(state) {
  state.backup_documents_products = JSON.parse(
    JSON.stringify(state.selected_document.documents_products)
  );
}

/**
 * Restaura un valor a la variable de state documents_products
 * @param state
 * @param branchoffices
 */
export function restoreDocumentsProductsFromBackup(state) {
  state.selected_document.documents_products.map(dp => {
    state.backup_documents_products.map(bdp => {
      if (dp.product_id == bdp.product_id) {
        dp.quantity = bdp.quantity;
      }
    });
  });
}

/**
 * Asigna un valor a la variable de state documents_for_back_order que sirve para calcular el back_order
 * @param state
 * @param branchoffices
 */
export function setDocumentsForBackOrder(state, documents_for_back_order) {
  state.documents_for_back_order = documents_for_back_order;
}

/**
 * Asigna un valor por defecto a la variable de state selected_document_product
 * @param state
 * @param branchoffices
 */
export function setDefaultSelectedDocumentProduct(state) {
  state.selected_document_product = defaultSelectedDocumentProduct();
}

/**
 * Asigna un valor a la variable de state selected_document_product
 * @param state
 * @param branchoffices
 */
export function setSelectedDocumentProduct(state, selected_document_product) {
  state.selected_document_product = selected_document_product;
}

/**
 * Asigna un valor a la variable de state branchoffices
 * @param state
 * @param branchoffices
 */
export function setBranchOffices(state, branchoffices) {
  state.branchoffices = branchoffices.map(b => {
    return { value: b.id, label: b.name };
  });
}

/**
 * Asigna un valor a la variable de state branchoffices_warehouses
 * @param state
 * @param branchoffices_warehouses
 */
export function setBranchOfficesWarehouses(state, branchoffices_warehouses) {
  state.branchoffices_warehouses = branchoffices_warehouses.map(bw => {
    return { value: bw.id, label: bw.warehouse_description };
  });
}

/**
 * Asigna un valor a la variable de state contacts
 * @param state
 * @param contacts
 */
export function setContacts(state, contacts) {
  state.contacts = contacts;
}

/**
 * Asigna un valor a la variable de state contacts_warehouses
 * @param state
 * @param contacts
 */
export function setContactsWarehouses(state, contacts_warehouses) {
  state.contacts_warehouses = contacts_warehouses;
}

/**
 * Asigna un valor al atributo filter de params_contacts
 * @param state
 * @param filter
 */
export function setParamsContacts(state, filter) {
  state.params_contacts.filter = filter;
}

/**
 * Asigna valores a los datos del tercero en selected_document
 * @param state
 * @param contact_id
 */
export function setDataContact(state, contact_id) {
  let contact = state.contacts.find(c => c.id == contact_id);

  if (contact) {
    state.selected_document.contact = contact;
    state.selected_document.address = contact.address;
    state.selected_document.telephone = contact.main_telephone;
    state.selected_document.city = contact.city.city;
  }
}

/**
 * Asigna valores a los datos de la sucursal en selected_document
 * @param state
 * @param contact_id
 */
export function setDataContactWarehouse(state, contact_warehouse_id) {
  let contact_warehouse = state.contacts_warehouses.find(
    cw => cw.id == contact_warehouse_id
  );

  if (!contact_warehouse) {
    contact_warehouse = {};
  } else {
    state.selected_document.warehouse_id = contact_warehouse_id;
  }

  state.selected_document.warehouse = contact_warehouse;
  state.selected_document.warehouse.address = contact_warehouse.address;
  if (state.selected_document.documents_information != null) {
    state.selected_document.documents_information.price_list =
      contact_warehouse.price_list_id;
  } else {
    state.selected_document.documents_information = {
      price_list: contact_warehouse.price_list_id
    };
  }
}

/**
 * Agrega un document_product al array de documents_products, atributo de selected_document
 * @param state
 * @param contact_id
 */
export function addDocumentProduct(state, document_product) {
  // Buscamos el producto en la grilla.
  let temp_document_product = state.selected_document.documents_products.find(
    dp => dp.id == document_product.id
  );

  // Creamos
  if (temp_document_product === undefined) {
    if (state.selected_model.pay_taxes) {
      if (document_product.document_product_taxes.length > 0) {
        document_product.total_value_iva = document_product.document_product_taxes.find(
          dpt => dpt.tax.code == "10"
        ).value;
      }
    }
    state.selected_document.documents_products.push(document_product);
  }
  // Actualizamos
  else {
    // TRM
    temp_document_product.total_value_brut_us =
      document_product.total_value_brut_us;
    temp_document_product.total_value_us = document_product.total_value_us;
    temp_document_product.discount_value_us =
      document_product.discount_value_us;

    temp_document_product.unit_value_before_taxes =
      document_product.unit_value_before_taxes;
    temp_document_product.total_value_brut = document_product.total_value_brut;
    temp_document_product.total_value = document_product.total_value;
    temp_document_product.physical_unit = document_product.physical_unit;
    temp_document_product.discount_value = document_product.discount_value;
    temp_document_product.discount_value_unit =
      document_product.discount_value_unit;
    temp_document_product.description = document_product.description;
    temp_document_product.quantity = document_product.quantity;
    temp_document_product.is_benefit = document_product.is_benefit;
    temp_document_product.product.inventory =
      document_product.product.inventory;
    temp_document_product.inventory_cost_value_unit =
      document_product.inventory_cost_value_unit;
    temp_document_product.inventory_cost_value =
      document_product.inventory_cost_value;

    if (state.selected_model.pay_taxes) {
      if (document_product.document_product_taxes.length > 0) {
        temp_document_product.total_value_iva = document_product.document_product_taxes.find(
          dpt => dpt.tax.code == "10"
        ).value;
      }
    }
  }

  // Agregar al bk products
  let keyBackupDocuments = state.backup_documents_products.findIndex(
    bdp => bdp.id == document_product.id
  );
  state.backup_documents_products[keyBackupDocuments] = JSON.parse(
    JSON.stringify(document_product)
  );
}

/**
 * Establece los parametros para la busqueda de productos
 * @param state
 * @param params_list_products
 */
export function setParamsListProducts(state, params_list_products) {
  state.params_list_products = params_list_products;
}

/**
 * Asigna un valor a la variable de state show_promotions_modal
 * @param state
 * @param show_promotions_modal
 */
export function setShowPromotionsModal(state, show_promotions_modal) {
  state.show_promotions_modal = show_promotions_modal;
}

/**
 * Asigna un valor a la variable de state promotions
 * @param state
 * @param promotions
 */
export function setPromotions(state, promotions) {
  promotions.map(pr => {
    if (!pr.hasOwnProperty("disable_button")) {
      pr.disable_button = true;
    }
  });

  state.promotions = promotions;
}

/**
 * Asignar productos filtrados por criterios de clasificación
 * @param state
 * @param products_filtered
 */
export function setProductsFiltered(state, products_filtered) {
  state.products_filtered = products_filtered;
}

export function setDocProductDefaultBranchofficeWarehouseId(
  state,
  branchoffice_warehouse_id = null
) {
  if (branchoffice_warehouse_id) {
    state.doc_product_default_branchoffice_warehouse_id = parseInt(
      branchoffice_warehouse_id
    );
  } else {
    state.doc_product_default_branchoffice_warehouse_id = parseInt(
      localStorage.selected_branchoffice_warehouse
    );
  }
}

// Set column
export function setColumnTable(state) {
  state.columnsTable = [
    { name: "Codigo", field: "product.code", width: 150 },
    {
      name: "Descrip",
      field: "description",
      format: "text",
      width: 400,
      editable: state.action != "view"
    },
    {
      name: "Cuenta Contable",
      field: "account_id",
      visible: state.selected_model.capture_accounting_account
    },
    {
      name: "Bodega",
      field: "branchoffice_warehouse_id",
      visible: state.selected_model.branchoffice_change !== null
    },
    {
      name: "Exist",
      field: "existence",
      width: 140,
      format: "number",
      visible: state.action != "view"
    },
    {
      name: "Vr-Unit-Base",
      field: "base_unit_value",
      visible:
        state.selected_model.see_vrunit_base &&
        !state.selected_model.hide_values,
      width: 140,
      format: "currency",
      editable:
        state.selected_model.purchase_difference_management &&
        state.action != "view"
    },
    {
      name: "Cant Ord",
      field: "original_quantity",
      width: 140,
      format: "number",
      visible: state.selected_model.doc_base_management != 2
    },
    {
      name: "Cant BackO",
      field: "backorder_quantity",
      visible: state.selected_model.see_cant_backorder,
      width: 140,
      format: "number"
    },
    {
      name: "Cantidad",
      field: "quantity",
      editable:
        state.action != "view" &&
        state.selected_document.in_progress &&
        !state.selected_model.credit_note,
      format: "number"
    },
    {
      name: "Vr Brut Orig",
      field: "total_value_brut_orig",
      width: 140,
      visible: state.selected_model.credit_note
    },
    {
      name: "Cant Fisica",
      field: "physical_unit",
      visible: state.selected_model.see_physical_cant,
      format: "number",
      editable:
        state.selected_model.edit_physical_cant &&
        state.selected_document.in_progress,
      width: 140
    },
    {
      name: "Lote",
      field: "lot",
      visible: false,
      editable: false
    },
    {
      name: "Vr Unit",
      field: "unit_value_before_taxes",
      editable:
        state.selected_model.edit_unit_value &&
        state.selected_document.in_progress &&
        state.action != "view",
      format: "currency",
      width: 140,
      visible:
        !state.selected_model.hide_values && !state.selected_model.credit_note
    },
    {
      name: "Promo Aplic",
      field: "applied_promotion",
      visible: state.selected_model.see_applied_promotion
    },
    {
      name: "Vr Dcto Unit",
      field: "discount_value_unit",
      visible:
        state.action != "view" &&
        state.selected_model.see_discount &&
        (!state.selected_model.TRM_value ||
          state.selected_model.TRM_value == 2),
      editable:
        state.selected_model.edit_discount &&
        state.selected_document.in_progress,
      format: "currency"
    },
    {
      name: "Vr Dcto",
      field: "discount_value",
      visible:
        state.selected_model.see_discount &&
        (!state.selected_model.TRM_value ||
          state.selected_model.TRM_value == 2),
      editable: false,
      format: "currency"
    },
    {
      name: "Vr Dcto Unit",
      field: "discount_value_unit_us",
      format: "currency",
      type_currency: "USD",
      visible:
        state.selected_model.see_discount &&
        state.selected_model.TRM_value &&
        state.selected_model.TRM_value != 2,
      editable:
        state.selected_model.edit_discount &&
        state.selected_document.in_progress
    },
    {
      name: "Vr Dcto",
      field: "discount_value_us",
      format: "currency",
      type_currency: "USD",
      visible:
        state.selected_model.see_discount &&
        state.selected_model.TRM_value &&
        state.selected_model.TRM_value != 2,
      editable: false
    },
    {
      name: "Vr Bruto",
      field: "total_value_brut",
      width: 140,
      format: "currency",
      editable: state.selected_model.credit_note && state.action != "view",
      visible:
        !state.selected_model.hide_values &&
        (!state.selected_model.TRM_value || state.selected_model.TRM_value == 2)
    },
    {
      name: "Vr Bruto",
      field: "total_value_brut_us",
      width: 140,
      format: "currency",
      type_currency: "USD",
      visible:
        !state.selected_model.hide_values &&
        state.selected_model.TRM_value &&
        state.selected_model.TRM_value != 2
    },
    {
      name: "Vr IVA",
      field: "total_value_iva",
      width: 140,
      visible: !state.selected_model.hide_values && state.selected_model.pay_taxes,
      editable: state.selected_model.edit_value_iva && state.action != "view",
      format: "currency"
    },
    {
      name: "Vr Total",
      field: "total_value",
      width: 140,
      visible:
        !state.selected_model.hide_values &&
        (!state.selected_model.TRM_value || state.selected_model.TRM_value == 2)
    },
    {
      name: "Vr Total",
      field: "total_value_us",
      format: "currency",
      type_currency: "USD",
      width: 140,
      visible:
        !state.selected_model.hide_values &&
        state.selected_model.TRM_value &&
        state.selected_model.TRM_value != 2
    },
    {
      name: "Costo Unit",
      field: "inventory_cost_value_unit",
      width: 140,
      visible: state.selected_model.edit_cost,
      editable: state.selected_model.edit_cost && state.action != "view",
      format: "currency"
    },
    {
      name: "Costo Total",
      field: "inventory_cost_value",
      width: 140,
      visible: state.selected_model.edit_cost,
      editable: false,
      format: "currency"
    },
    /*{
      name: 'Vendedor',
      field: 'seller_id',
      visible: state.selected_model.see_seller,
      editable: state.selected_model.edit_seller && state.selected_document.in_progress,
    },*/
    {
      name: "Observaciones",
      field: "observation",
      width: 500,
      format: "long_text",
      visible: state.selected_model.see_product_observation,
      editable:
        state.selected_model.edit_product_observation &&
        state.selected_document.in_progress
    },
    {
      name: "Acciones",
      field: "actions",
      width: 150,
      visible: state.action != "view"
    }
  ];
}

export function setTraceDocumentProduct(state, data) {
  state.report_back_order = [];

  data.map((d, index_d) => {
    var item = {
      id: d.id,
      date: d.date,
      document: d.document_vouchertype_name,
      consecutive: d.consecutive,
      quantity: d.quantity,
      backorder: d.backorder,
      operation_type: d.operationType,
      prefix: d.prefix,
      invoice_link: d.invoice_link
    };

    state.report_back_order.push(item);
  });
}

export function setUpdatedContactWarehouses(state, data) {
  let warehousesUpdated = data.data;
  let warehouses = state.selected_document.contact.warehouses;

  warehouses.map((w, iw) => {
    if (w.id == warehousesUpdated.id) {
      warehouses.splice(iw, 1);
      warehouses.push(warehousesUpdated);
    }
  });
}

export function getParamsWarehouseRetention(state) {
  let warehouse_id = state.selected_document.warehouse_id;

  // Validamos que el dato no este vacio
  if (warehouse_id === undefined || warehouse_id == "" || warehouse_id == null)
    return;

  // Traemos el warehouse selecionado
  let selected_warehouse = state.selected_document.contact.warehouses.find(
    w => w.id == warehouse_id
  );

  if (selected_warehouse) {
    state.tableRetention.map(retention => {
      retention.fields.map(field => {
        if (retention.prefix == "FUE") {
          if (field.prefix == "product") {
            field.vr_limit = selected_warehouse.ret_fue_prod_vr_limit;
            field.percentage = selected_warehouse.ret_fue_prod_percentage;
          }
          if (field.prefix == "service") {
            field.vr_limit = selected_warehouse.ret_fue_serv_vr_limit;
            field.percentage = selected_warehouse.ret_fue_serv_percentage;
          }
        }

        if (retention.prefix == "IVA") {
          if (field.prefix == "product") {
            field.vr_limit = selected_warehouse.ret_iva_prod_vr_limit;
            field.percentage = selected_warehouse.ret_iva_prod_percentage;
          }
          if (field.prefix == "service") {
            field.vr_limit = selected_warehouse.ret_iva_serv_vr_limit;
            field.percentage = selected_warehouse.ret_iva_serv_percentage;
          }
        }

        if (retention.prefix == "ICA") {
          if (field.prefix == "product") {
            field.vr_limit = selected_warehouse.ret_ica_prod_vr_limit;
            field.percentage = selected_warehouse.ret_ica_prod_percentage;
          }
          if (field.prefix == "service") {
            field.vr_limit = selected_warehouse.ret_ica_serv_vr_limit;
            field.percentage = selected_warehouse.ret_ica_serv_percentage;
          }
        }
      });
    });
  }
}

export function setParamsWarehouseRetention(state, selected_warehouse) {
  if (selected_warehouse) {
    state.tableRetention.map(retention => {
      retention.fields.map(field => {
        if (retention.prefix == "FUE") {
          if (field.prefix == "product") {
            field.vr_limit = selected_warehouse.ret_fue_prod_vr_limit;
            field.percentage = selected_warehouse.ret_fue_prod_percentage;
          }
          if (field.prefix == "service") {
            field.vr_limit = selected_warehouse.ret_fue_serv_vr_limit;
            field.percentage = selected_warehouse.ret_fue_serv_percentage;
          }
        }

        if (retention.prefix == "IVA") {
          if (field.prefix == "product") {
            field.vr_limit = selected_warehouse.ret_iva_prod_vr_limit;
            field.percentage = selected_warehouse.ret_iva_prod_percentage;
          }
          if (field.prefix == "service") {
            field.vr_limit = selected_warehouse.ret_iva_serv_vr_limit;
            field.percentage = selected_warehouse.ret_iva_serv_percentage;
          }
        }

        if (retention.prefix == "ICA") {
          if (field.prefix == "product") {
            field.vr_limit = selected_warehouse.ret_ica_prod_vr_limit;
            field.percentage = selected_warehouse.ret_ica_prod_percentage;
          }
          if (field.prefix == "service") {
            field.vr_limit = selected_warehouse.ret_ica_serv_vr_limit;
            field.percentage = selected_warehouse.ret_ica_serv_percentage;
          }
        }
      });
    });
  }
}

export function getParamsBaseDocumentRetention(state) {
  let baseDocument = state.selected_document.document;
  let retentions = state.tableRetention;

  if (baseDocument) {
    retentions.map(retention => {
      retention.fields.map(field => {
        if (retention.prefix == "FUE") {
          if (field.prefix == "product") {
            field.vr_limit = baseDocument.ret_fue_prod_vr_limit;
            field.percentage = baseDocument.ret_fue_prod_percentage;
          }
          if (field.prefix == "service") {
            field.vr_limit = baseDocument.ret_fue_serv_vr_limit;
            field.percentage = baseDocument.ret_fue_serv_percentage;
          }
        }

        if (retention.prefix == "IVA") {
          if (field.prefix == "product") {
            field.vr_limit = baseDocument.ret_iva_prod_vr_limit;
            field.percentage = baseDocument.ret_iva_prod_percentage;
          }
          if (field.prefix == "service") {
            field.vr_limit = baseDocument.ret_iva_serv_vr_limit;
            field.percentage = baseDocument.ret_iva_serv_percentage;
          }
        }

        if (retention.prefix == "ICA") {
          if (field.prefix == "product") {
            field.vr_limit = baseDocument.ret_ica_prod_vr_limit;
            field.percentage = baseDocument.ret_ica_prod_percentage;
          }
          if (field.prefix == "service") {
            field.vr_limit = baseDocument.ret_ica_serv_vr_limit;
            field.percentage = baseDocument.ret_ica_serv_percentage;
          }
        }
      });
    });
  }
}

export function resetReportBackOrder(state) {
  state.report_back_order = [];
}

export function setParametersTable(state, data) {
  state.parametersTable = data;
}

export function setUpdateValuesDocument(state, data) {
  const retentionBase =
    state.selected_model.retention_management == 3 ? true : false;

  const {
    document,
    getTotalIvaProducts,
    getTotalIvaServices,
    getValBrutInProducts,
    getValBrutInServices
  } = data;

  state.selected_document.total_value = document.total_value;
  state.selected_document.total_value_brut = document.total_value_brut;
  state.selected_document.taxes = document.taxes;

  // Retenciones
  state.tableRetention.map(retencion => {
    if (retencion.prefix === "IVA") {
      retencion.base_value = getTotalIvaProducts;
      retencion.base_serv_value = getTotalIvaServices;
    } else {
      retencion.base_value = getValBrutInProducts;
      retencion.base_serv_value = getValBrutInServices;
    }
  });
}

export function setUpdateDocumentValues(state, data) {
  // Totales
  state.selected_document.total_value = data.total_value;
  state.selected_document.total_value_brut = data.total_value_brut;
  state.selected_document.taxes = data.taxes;

  // Totales TRM
  state.selected_document.total_value_us = data.total_value_us;
  state.selected_document.total_value_brut_us = data.total_value_brut_us;

  // Retenciones
  // # FUE
  // productos
  state.selected_document.ret_fue_prod_vr_limit = data.ret_fue_prod_vr_limit;
  state.selected_document.ret_fue_prod_percentage =
    data.ret_fue_prod_percentage;
  state.selected_document.ret_fue_prod_total = data.ret_fue_prod_total;
  state.selected_document.ret_fue_base_value = data.ret_fue_base_value;

  // servicios
  state.selected_document.ret_fue_serv_vr_limit = data.ret_fue_serv_vr_limit;
  state.selected_document.ret_fue_serv_percentage =
    data.ret_fue_serv_percentage;
  state.selected_document.ret_fue_serv_total = data.ret_fue_serv_total;
  state.selected_document.ret_fue_serv_base_value =
    data.ret_fue_serv_base_value;

  // total
  state.selected_document.ret_fue_total = data.ret_fue_total;

  // # IVA
  // productos
  state.selected_document.ret_iva_prod_vr_limit = data.ret_iva_prod_vr_limit;
  state.selected_document.ret_iva_prod_percentage =
    data.ret_iva_prod_percentage;
  state.selected_document.ret_iva_prod_total = data.ret_iva_prod_total;
  state.selected_document.ret_iva_base_value = data.ret_iva_base_value;

  // servicios
  state.selected_document.ret_iva_serv_vr_limit = data.ret_iva_serv_vr_limit;
  state.selected_document.ret_iva_serv_percentage =
    data.ret_iva_serv_percentage;
  state.selected_document.ret_iva_serv_total = data.ret_iva_serv_total;
  state.selected_document.ret_iva_serv_base_value =
    data.ret_iva_serv_base_value;

  // total
  state.selected_document.ret_iva_total = data.ret_iva_total;

  // # ICA
  // productos
  state.selected_document.ret_ica_prod_vr_limit = data.ret_ica_prod_vr_limit;
  state.selected_document.ret_ica_prod_percentage =
    data.ret_ica_prod_percentage;
  state.selected_document.ret_ica_prod_total = data.ret_ica_prod_total;
  state.selected_document.ret_ica_base_value = data.ret_ica_base_value;

  // servicios
  state.selected_document.ret_ica_serv_vr_limit = data.ret_ica_serv_vr_limit;
  state.selected_document.ret_ica_serv_percentage =
    data.ret_ica_serv_percentage;
  state.selected_document.ret_ica_serv_total = data.ret_ica_serv_total;
  state.selected_document.ret_ica_serv_base_value =
    data.ret_ica_serv_base_value;

  // total
  state.selected_document.ret_ica_total = data.ret_ica_total;
  data.documents_products.map(dp => {
    let dpdoc = state.selected_document.documents_products.find(
      dpd => dpd.id === dp.id
    );
    dpdoc.total_value_iva = dp.total_value_iva;
    dpdoc.total_value_brut = dp.total_value_brut;
    dpdoc.total_value = dp.total_value;
  });
}

export function setUpdateDocumentValuesRetention(state, data) {
  // Retenciones
  // # FUE
  // productos
  state.selected_document.ret_fue_prod_vr_limit = data.ret_fue_prod_vr_limit;
  state.selected_document.ret_fue_prod_percentage =
    data.ret_fue_prod_percentage;
  state.selected_document.ret_fue_prod_total = data.ret_fue_prod_total;
  state.selected_document.ret_fue_base_value = data.ret_fue_base_value;

  // servicios
  state.selected_document.ret_fue_serv_vr_limit = data.ret_fue_serv_vr_limit;
  state.selected_document.ret_fue_serv_percentage =
    data.ret_fue_serv_percentage;
  state.selected_document.ret_fue_serv_total = data.ret_fue_serv_total;
  state.selected_document.ret_fue_serv_base_value =
    data.ret_fue_serv_base_value;

  // total
  state.selected_document.ret_fue_total = data.ret_fue_total;

  // # IVA
  // productos
  state.selected_document.ret_iva_prod_vr_limit = data.ret_iva_prod_vr_limit;
  state.selected_document.ret_iva_prod_percentage =
    data.ret_iva_prod_percentage;
  state.selected_document.ret_iva_prod_total = data.ret_iva_prod_total;
  state.selected_document.ret_iva_base_value = data.ret_iva_base_value;

  // servicios
  state.selected_document.ret_iva_serv_vr_limit = data.ret_iva_serv_vr_limit;
  state.selected_document.ret_iva_serv_percentage =
    data.ret_iva_serv_percentage;
  state.selected_document.ret_iva_serv_total = data.ret_iva_serv_total;
  state.selected_document.ret_iva_serv_base_value =
    data.ret_iva_serv_base_value;

  // total
  state.selected_document.ret_iva_total = data.ret_iva_total;

  // # ICA
  // productos
  state.selected_document.ret_ica_prod_vr_limit = data.ret_ica_prod_vr_limit;
  state.selected_document.ret_ica_prod_percentage =
    data.ret_ica_prod_percentage;
  state.selected_document.ret_ica_prod_total = data.ret_ica_prod_total;
  state.selected_document.ret_ica_base_value = data.ret_ica_base_value;

  // servicios
  state.selected_document.ret_ica_serv_vr_limit = data.ret_ica_serv_vr_limit;
  state.selected_document.ret_ica_serv_percentage =
    data.ret_ica_serv_percentage;
  state.selected_document.ret_ica_serv_total = data.ret_ica_serv_total;
  state.selected_document.ret_ica_serv_base_value =
    data.ret_ica_serv_base_value;

  // total
  state.selected_document.ret_ica_total = data.ret_ica_total;
}

export function addDocumentProductAll(state, data) {
  state.selected_document.documents_products = data.message;
}

// Asignar valores de la cartera
export function setValuesReceivablesByContactId(state, data) {
  let total_wallet = 0;
  let credit_quota = state.selected_document.contact.customer.credit_quota;

  data.map((d, indexd) => {
    total_wallet += d.total_balance;
  });

  state.selected_document.contact.customer.total_wallet = total_wallet;
  state.selected_document.contact.customer.credit_quota_available = 0;

  if (total_wallet >= credit_quota) {
    state.selected_document.contact.customer.credit_quota_available = 0;
  } else {
    state.selected_document.contact.customer.credit_quota_available =
      credit_quota - total_wallet;
  }
}

export function deleteItemDocumentsProducts(state, document_product_id) {
  let key_document_product = state.selected_document.documents_products.findIndex(
    dp => dp.id == document_product_id
  );
  if (key_document_product != -1) {
    state.selected_document.documents_products.splice(key_document_product, 1);
  }
}

export function setMails(state, mails) {
  state.mails = mails;
}

export function setSellerIdByContactWarehouse(state, seller) {
  state.selected_document.seller_id = seller.id;
}
export function resetSellerId(state) {
  state.selected_document.seller_id = null;
}
export function setUpdating(state, data) {
  state.updating = data;
}

export function setNamePdf(state, name_pdf) {
  state.name_pdf = name_pdf;
}

export function setBranchofficeWarehouses(state, data) {
  state.branchofficeWarehouses = data;
}
export function setEditRetention(state, data) {
  state.edit_retention = data;
}
export function setEditBaseRetention(state, data) {
  state.edit_base_retention = data;
}

export function setContactsWarehouseById(state, data) {
  state.selected_contact_warehouse = data;
}

export function resetContacts(state, data) {
  state.contacts = [];
}

export function setEditRetentionDocument(state, value) {
  state.selected_document.edit_retention = value;
}
export function setFinalizingDocument(state, value) {
  state.finalizing_document = value;
}
export function setDocumentFile(state, data) {
  let extension = data.name.split(".");
  let file = {
    id: null,
    name: data.name,
    extension: extension[extension.length - 1],
    path: "",
    size: data.size,
    sent: false,
    document_id: state.selected_document.id
  };

  state.selected_document.files.push(file);

  state.document_files_new.push(data);
}
export function deleteDocumentFile(state, data) {
  if (data.id) {
    let key_file = state.selected_document.files.findIndex(
      f => f.id === data.id
    );
    state.selected_document.files.splice(key_file, 1);
    state.document_files_delete.push(data);
  } else {
    let key_file = state.selected_document.files.findIndex(
      f => f.size === data.size && f.name === data.name
    );
    state.selected_document.files.splice(key_file, 1);

    let key_file_new = state.document_files_new.findIndex(
      f => f.size === data.size && f.name === data.name
    );
    state.document_files_new.splice(key_file_new, 1);
  }
}

export function resetDataDocumentFIles(state) {
  state.document_files_new = [];
  state.document_files_delete = [];
}

export function resetRetentionStates(state) {
  state.edit_base_retention = false;
  state.edit_retention = false;
}

export function setDocumentProductsEditIva(state, document_product) {
  state.document_products_edit_iva.push({
    id: document_product.id
  });
}
export function removeDocumentProductsEditIva(state, document_product_id) {
  let key_document_product = state.document_products_edit_iva.findIndex(
    dp => dp.id == document_product_id
  );
  if (key_document_product != -1) {
    state.document_products_edit_iva.splice(key_document_product, 1);
  }
}
export function setAuthorizeDocuments(state, value) {
  state.authorize_documents = value;
}
export function resetAuthorizeDocuments(state) {
  state.authorize_documents = false;
}

// MODAL CREAR TERCERO
export function setCities(state, cities) {
  state.cities = cities;
}

export function setDepartments(state, departments) {
  state.departments = departments;
}

export function setIdentificationTypes(state, identificationTypes) {
  state.identificationTypes = identificationTypes;
}

export function setTypeNewContact(state, type) {
  let model = state.selected_model;

  if (model) {
    switch (model.contact_type) {
      case 1:
        state.newContact.is_customer = true;
        break;
      case 2:
        state.newContact.is_provider = true;
        break;
      case 3:
        state.newContact.is_employee = true;
        break;
    }
  }
}

export function setNewContact(state, newContact) {
  newContact.vehicle = defaultNewVehicle();
  newContact.department_id = newContact.city.department_id;
  state.newContact = newContact;
}

export function resetNewContact(state, preserveIdentification = false) {
  let identification;
  let identification_type;

  let city_id = state.newContact.city_id;
  let department_id = state.newContact.department_id;

  if (preserveIdentification) {
    identification = state.newContact.identification;
    identification_type = state.newContact.identification_type;
  }

  state.newContact = defaultNewContact();

  if (preserveIdentification) {
    state.newContact.identification = identification;
    state.newContact.identification_type = identification_type;
  }

  state.newContact.city_id = city_id;
  state.newContact.department_id = department_id;
}

export function setVehicles(state, vehicles) {
  state.vehicles = vehicles;
}

export function setNewContactVehicle(state, vehicle) {
  state.newContact.vehicle = vehicle;
}

export function setDataVehicle(state, vehicle_id) {
  let vehicle = state.vehicles.find(v => v.id === vehicle_id);

  if (vehicle) {
    state.dataVehicle = [
      { title: "Marca", value: vehicle.brand_vehicle.name_parameter },
      { title: "Tipo", value: vehicle.class_vehicle.name_parameter },
      {
        title: "Servicio",
        value: vehicle.servicio === 2 ? "Privado" : "Público"
      },
      { title: "Potencia", value: vehicle.power },
      { title: "Cilindraje", value: vehicle.cylinder_capacity },
      { title: "Peso", value: vehicle.weight },
      { title: "Tipo de caja", value: vehicle.box_type },
      { title: "Puertas", value: vehicle.doors },
      { title: "Combustible", value: vehicle.fuel },
      { title: "Nacionalidad", value: vehicle.nationality },
      { title: "Capacidad de pasajeros", value: vehicle.passenger_capacity },
      { title: "Transmisión", value: vehicle.transmission },
      { title: "Tipo de caja", value: vehicle.box_type }
    ];
  } else {
    state.dataVehicle = [];
  }
}

export function setPersonClasses(state, personClasses) {
  state.personClasses = personClasses;
}

export function setTaxpayerTypes(state, taxpayerTypes) {
  state.taxpayerTypes = taxpayerTypes;
}

export function setTaxRegimes(state, taxRegimes) {
  state.taxRegimes = taxRegimes;
}

export function setFiscalResponsibilitys(state, fiscalResponsibilitys) {
  state.fiscalResponsibilitys = fiscalResponsibilitys;
}

export function setModalNewContactStep(state, step) {
  state.modalNewContact.step = step;
}

export function setModalNewContact(state, modalNewContact) {
  state.modalNewContact.show = modalNewContact;
}

export function resetNewContactVehicle(state, preserveLicencePlate = false) {
  let licence_plate;

  if (preserveLicencePlate) {
    licence_plate = state.newContact.vehicle.id;
  }

  state.newContact.vehicle = defaultNewVehicle();

  if (preserveLicencePlate) {
    state.newContact.vehicle.id = licence_plate;
    state.newContact.vehicle.license_plate = licence_plate;
  }
}

export function setAddressNewContact(state, address) {
  if (address) {
    state.newContact.address = address;
  }
}

//--- METODOS DE PAGO
export function modalPaymentMethodsToggle(state) {
  state.modalPaymentMethods.showModal = !state.modalPaymentMethods.showModal;
}

export function resetModalPaymentMethodsData(state) {
  state.modalPaymentMethods.data = [];
}

export function setPaymentMethods(state, data) {
  state.modalPaymentMethods.paymentMethods = data;
}

export function deleteRowModalPaymentMethodsData(state, id) {
  let index = state.modalPaymentMethods.data.findIndex(d => d.id === id);
  state.modalPaymentMethods.data.splice(index, 1);
}

export async function validationsModalPaymentMethods(state) {
  // Eliminamos los registros vacios
  let voidModalPaymentMethods = state.modalPaymentMethods.data.filter(
    d =>
      !d.payment_method_id &&
      !d.authorization_number &&
      !d.check_number &&
      !d.entity &&
      !d.value
  );

  await Promise.all(
    voidModalPaymentMethods.map(d => {
      deleteRowModalPaymentMethodsData(state, d.id);
    })
  );

  if (state.selected_model.handle_payment_methods === 9) {
    if (state.modalPaymentMethods.data.length <= 0) {
      return false;
    }
  }
}
//
//
export function setDocumentsThreadHistory(state, documents) {
  state.modalDocumentThreadHistory.documents = documents;
}

export function setDocumentsThreadHistoryShowModal(state, value) {
  state.modalDocumentThreadHistory.showModal = value;
}

export function resetDocumentsThreadHistorys(state) {
  state.modalDocumentThreadHistory.documents = [];
}

export function setSaveDocument(state, value) {
  state.saveDocument = value;
}

export function setAdditionalDocuments(state, additional_documents){
  state.additional_documents = additional_documents
}

export function setConveyors(state, conveyors){
  state.conveyors = conveyors
}
