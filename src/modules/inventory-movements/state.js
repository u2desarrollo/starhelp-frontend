export const defaultModalDocumentThreadHistory = () => {
  return {
    documents: [],
    showModal: false
  };
};

export const defaultModalPaymentMethods = () => {
  return {
    showModal: false,
    data: [],
    // columns: [
    //   {
    //     name: "Metodo de pago",
    //     field: "payment_method_id",
    //     style: { "min-width": "200px !important", backgroundColor: "#d5f3d3" }
    //   },
    //   {
    //     name: "Numero de autorización",
    //     field: "authorization_number",
    //     style: { "min-width": "200px !important", backgroundColor: "#d5f3d3" }
    //   },
    //   {
    //     name: "Numero de cheque",
    //     field: "check_number",
    //     style: { "min-width": "200px !important", backgroundColor: "#d5f3d3" }
    //   },
    //   {
    //     name: "Entidad",
    //     field: "entity",
    //     style: { "min-width": "200px !important", backgroundColor: "#d5f3d3" }
    //   },
    //   {
    //     name: "Valor",
    //     field: "value",
    //     style: { "min-width": "200px !important", backgroundColor: "#d5f3d3" }
    //   },
    //   {
    //     name: "Acciones",
    //     field: "actions"
    //   }
    // ],
    row_create: {
      id: null,
      document_id: "",
      payment_method_id: "",
      authorization_number: "",
      check_number: "",
      entity: "",
      value: "",
      value_change: "",
      total_value: "",
      new: true
    },
    row_selected: {},
    params: {},
    paymentMethods: []
  };
};

export const defaultNewContact = () => {
  return {
    id: null,
    subscriber_id: 1,
    identification_type: "",
    identification: "",
    check_digit: "",
    name: "",
    surname: "",
    city_id: "",
    address: "",
    address_object: {
      type_of_road: "",
      way_number: "",
      generating_way_number: "",
      plate_number: "",
      complement: ""
    },
    main_telephone: "",
    cell_phone: "",
    email: "",
    is_customer: false,
    is_provider: false,
    is_employee: false,
    tradename: "",
    department_id: "",
    vehicle: defaultNewVehicle(),
    class_person: null,
    taxpayer_type: null,
    tax_regime: null,
    fiscal_responsibility_id: null,
    declarant: null,
    self_retainer: null
  };
};

export const defaultNewVehicle = () => {
  return {
    id: null,
    vehicle_type_id: "",
    contact_id: "",
    year: "",
    license_plate: "",
    serial_number: "",
    average_daily_kilometers: "",
    width: "",
    rin: "",
    profile: "",
    observations: ""
  };
};

export const defaultSelectedDocument = () => {
  return {
    trm_value: null,
    branchoffice_id: "",
    branchoffice_warehouse_id: "",
    contact_id: "",
    contact: {
      city: {
        city: ""
      }
    },
    warehouse_id: "",
    warehouse: {
      id: "",
      description: "",
      address: ""
    },
    address: "",
    city: "",
    telephone: "",
    credit_limit: "",
    total_receivable: "",
    available: "",
    term_days: "",
    due_date: "",
    purchase_order: "",
    document_date: "",
    user_id: "",
    delivery_warehouse: "",
    warehouse_address: "",
    observations_one: "",
    observationes_two: "",
    observations_shipping: "",
    documents_products: [],
    documents_information: {
      price_list: "",
      document_name: "",
      date_entry: "",
      protractor: null,
      guide_number: null
    },
    in_progress: true,
    user: {
      name: "",
      surname: ""
    },
    vouchertype: {
      name_voucher_type: ""
    },
    document: {},
    seller: null
  };
};

export const defaultSelectedModel = () => {
  return {
    add_products: 0,
    add_promotions_button: 0,
    backorder_quantity_control: null,
    basic_contact_information: null,
    button_360: 0,
    capt_delivery_address: 2,
    capt_license: 2,
    cash_drawer_opens: 0,
    cents_values: 0,
    code: "",
    consecutive_handling: 1,
    contact_capture: 0,
    contact_category: null,
    contact_id: null,
    contact_type: null,
    crossing_document_dates: 2,
    currency: 840,
    description: "",
    discount_foot_bill: 0,
    doc_base_management: 2,
    doc_cruce_management: 2,
    down_excel_button: 0,
    edit_bonus_amount: 0,
    edit_discount: 0,
    edit_lots: 0,
    edit_physical_cant: 0,
    edit_product_observation: 0,
    edit_seller: 0,
    edit_unit_value: 0,
    final_function: null,
    financial_info: 0,
    footnotes: null,
    handle_payment_methods: 2,
    hands_free_operation: 0,
    hide_value: 0,
    id: null,
    line_id: null,
    model_id: null,
    modify_prices: 0,
    observation_tittle_1: null,
    observation_tittle_2: null,
    operationType_id: null,
    payment_methods_allowed_id: null,
    price_list_id: null,
    print: 0,
    print_template: null,
    print_tittle: null,
    product_order: null,
    purchase_difference_management: 0,
    quantity_control: 0,
    recalc_iva_base: null,
    recalc_iva_less_cost: 0,
    retention_management: null,
    return_model_id: null,
    see_applied_promotion: 0,
    see_backorder: 0,
    see_bonus_amount: 0,
    see_cant_backorder: 0,
    see_discount: 0,
    see_location: 0,
    see_lots: 0,
    see_physical_cant: 0,
    see_product_observation: 0,
    see_seller: 0,
    see_seller_doc: 0,
    see_vrunit_base: 0,
    select_branchoffice: 0,
    select_conveyor: null,
    select_cost_center: 2,
    select_warehouse: 0,
    seller_mode: null,
    send_contact_email: null,
    tittle: null,
    total_letters: 0,
    up_excel_button: 0,
    valid_exist: null,
    value_control: 0,
    voucherType_id: 4,
    voucher_type_id: null,
    voucher_type: {
      code_voucher_type: null
    },
    voucher_type_selected_branchoffice: {
      id: null
    }
  };
};

export const defaultsubscriberId = () => {
  let user = JSON.parse(localStorage.getItem("user"));

  if (user) {
    return user.subscriber_id;
  }

  return "";
};

export const defaultSelectedDocumentProduct = () => {
  return {
    applies_inventory: false,
    base_quantity: 0,
    bonus_quantity: 0,
    brand_id: "",
    consumption_tax_percentage: null,
    discount_value: null,
    document_id: "",
    freight_value: null,
    iva_percentage: null,
    line_id: "",
    observation: null,
    original_quantity: 0,
    product_id: "",
    quantity: 0,
    seller_id: "",
    subbrand_id: null,
    subline_id: "",
    unit_value: 0
  };
};

export const defaultTableSearchGlobal = () => {
  return {
    data: [],
    columns:[],
    // columns: [
    //   {
    //     label: "Fecha",
    //     key: "document_date"
    //   },
    //   {
    //     label: "Tipo Documento",
    //     key: "vouchertype.name_voucher_type"
    //   },
    //   {
    //     label: "Número",
    //     key: "consecutive",
    //     sortable: true
    //   },
    //   {
    //     label: "Tercero",
    //     key: "warehouse"
    //   },
    //   {
    //     label: "Vendedor",
    //     key: "seller.name"
    //   },
    //   {
    //     label: "Valor Total",
    //     key: "total_value"
    //   },
    //   {
    //     label: "Erp",
    //     key: "erp"
    //   },
    //   // {
    //   //   label: "Estado",
    //   //   key: "statu"
    //   // },
    //   {
    //     label: "Acciones",
    //     key: "actions"
    //   }
    // ],
    pagination: {
      from: 0,
      to: 0,
      total: 0
    },
    loading: false,
    params: {
      //paginacion
      perPage: 15,
      currentPage: 1,
      page: 1,
      warehouse_id:null,
      branchoffice_id:null,
      dateFrom:null,
      dateTo:null,
      vouchertype_id:null,
      model_id:null,
      contact_id:null,
      filterText:null,

    }
  }
};

export default {
  import_params:{
    trm:null
  },
  authorize_documents: false,
  name_pdf: null,
  updating: false,
  mails: [],
  selected_document: defaultSelectedDocument(),
  backup_documents_products: [],
  selected_model: defaultSelectedModel(),
  selected_document_product: defaultSelectedDocumentProduct(),
  branchoffices: [],
  branchoffices_warehouses: [],
  contacts: [],
  contacts_warehouses: [],
  documents_for_back_order: [],
  subscriber_id: defaultsubscriberId(),
  action: "create",
  error: false,
  fetch_data: true,
  show_promotions_modal: false,
  promotions: [],
  products_filtered: [],
  params_list_products: {},
  params: {
    manager_wms: null,
  },
  params_contacts: {
    filter: "",
    type: "is_customer",
    paginate: false
  },

  //Kevin
  columnsTable: [],
  models: [],
  TableSearchGlobal: defaultTableSearchGlobal(),
  report_back_order: [],

  // Retenciones
  tableRetention: [
    {
      title: "Rete FTE",
      prefix: "FUE",
      base_value: 0,
      base_serv_value: 0,
      edit_base_value: false,
      edit_base_serv_value: false,
      fields: [
        {
          //Products
          prefix: "product",
          label: "Product",
          vr_limit: 0,
          percentage: 0,
          total: 0,
          edit_vr_limit: false,
          edit_percentage: false
        },
        {
          //Serv
          prefix: "service",
          label: "Service",
          vr_limit: 0,
          percentage: 0,
          total: 0,
          edit_vr_limit: false,
          edit_percentage: false
        }
      ],
      total: 0
    },
    {
      title: "Rete IVA",
      prefix: "IVA",
      base_value: 0,
      base_serv_value: 0,
      edit_base_value: false,
      edit_base_serv_value: false,
      fields: [
        {
          //Products
          prefix: "product",
          label: "Product",
          vr_limit: 0,
          percentage: 0,
          total: 0,
          edit_vr_limit: false,
          edit_percentage: false
        },
        {
          //Serv
          prefix: "service",
          label: "Service",
          vr_limit: 0,
          percentage: 0,
          total: 0,
          edit_vr_limit: false,
          edit_percentage: false
        }
      ],
      total: 0
    },
    {
      title: "Rete ICA",
      prefix: "ICA",
      base_value: 0,
      base_serv_value: 0,
      edit_base_value: false,
      edit_base_serv_value: false,
      fields: [
        {
          //Products
          prefix: "product",
          label: "Product",
          vr_limit: 0,
          percentage: 0,
          total: 0,
          edit_vr_limit: false,
          edit_percentage: false
        },
        {
          //Serv
          prefix: "service",
          label: "Service",
          vr_limit: 0,
          percentage: 0,
          total: 0,
          edit_vr_limit: false,
          edit_percentage: false
        }
      ],
      total: 0
    }
  ],
  // Otras opciones
  parametersTable: [],
  // branchoffice_warehouses
  branchofficeWarehouses: [],
  // Bodega por defcto en las bodegas
  doc_product_default_branchoffice_warehouse_id: null,
  // Editar retencion
  edit_retention: false,
  // Editar base retencion.
  edit_base_retention: false,
  // WarehouseById
  selected_contact_warehouse: null,
  finalizing_document: false,
  document_files_new: [],
  document_files_delete: [],
  document_products_edit_iva: [],
  newContact: defaultNewContact(),
  modalNewContact: {
    show: false,
    step: 1
  },
  cities: [],
  departments: [],
  identificationTypes: [],
  vehicles: [],
  brandsVehicles: [],
  vehiclesContact: [],
  vehicle: {},
  itemsInspection: [],
  documents_products_import:[],
  expenses:[],
  dataVehicle: {},
  personClasses: [],
  taxpayerTypes: [],
  taxRegimes: [],
  fiscalResponsibilitys: [],
  saveDocument: false,
  modalPaymentMethods: defaultModalPaymentMethods(),
  modalDocumentThreadHistory: defaultModalDocumentThreadHistory(),
  additional_documents: [],
  conveyors: []
};
