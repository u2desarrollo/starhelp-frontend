import {
    defaultSelectedSubline
} from "./state";

export function setSublines(state, sublines) {
    state.sublines = sublines;
}

export function setSubline(state, subline) {
    state.selectedSubline = JSON.parse(JSON.stringify(subline));
}

export function setActionSublines(state, action) {
    state.actionSubline = action;
}

export function sublineError(state, error) {
    state.error = true
    state.errorMessage = error
}

export function setAccountsInventory(state, accountsInventory) {
    state.accountsInventory = accountsInventory
}

export function setAccountsCost(state, accountsCost) {
    state.accountsCost = accountsCost
}

export function setAccountsSales(state, accountsSales) {
    state.accountsSales = accountsSales
}


//Establece el valor por defecto de selectedSubline
export function resetSelectedSubline(state) {
    let id = state.selectedSubline.id
    Object.assign(state.selectedSubline, defaultSelectedSubline())
    if (id) {
        state.selectedSubline.id = id
    }
}

export function setSublineList(state, data) {
    state.sublinesList = data;
}