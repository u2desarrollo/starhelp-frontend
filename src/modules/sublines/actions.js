import $http from '../../axios'
import {
    Notification,
    MessageBox
} from 'element-ui';


export async function fetchAccountsInventory(
    { commit, state },
    requestParameters = { params: state.paramsAccountInventory }
  ) {
    await $http
      .get("/api/cuentas", requestParameters)
      .then(response => {
        commit("setAccountsInventory", response.data.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        commit("modelError", error.message);
      });
  }

  export async function fetchAccountsCost(
    { commit, state },
    requestParameters = { params: state.paramsAccountCost }
  ) {
    await $http
      .get("/api/cuentas", requestParameters)
      .then(response => {
        commit("setAccountsCost", response.data.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        commit("modelError", error.message);
      });
  }

export async function fetchAccountsSales(
    { commit, state },
    requestParameters = { params: state.paramsAccountSales }
  ) {
    await $http
      .get("/api/cuentas", requestParameters)
      .then(response => {
        commit("setAccountsSales", response.data.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        commit("modelError", error.message);
      });
  }

//Obtener los sublineas
export async function fetchSublines({
    commit,
    state,
    rootState
}) {

    if (rootState.lines.selectedLine.id) {
        await $http.get(`/api/sublineas?lines=${rootState.lines.selectedLine.id}`)
            .then((response) => {
                commit('setSublines', response.data)
            })
            .catch((error) => {
                Notification.error({
                    title: 'Error!',
                    message: error.message,
                    type: 'error',
                    duration: 1500,
                    customClass: 'notification-box',
                });
                commit('sublineError', error.message)
            })
    }
}

//Crear funcionario
export async function addSubline({
    commit,
    state,
    dispatch,
    rootState
}, id) {
    state.selectedSubline.line_id = id;
    await $http.post('/api/sublineas', state.selectedSubline)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'La Sublinea ha sido creada',
                type: 'success',
                duration: 3000,
                customClass: 'notification-box',
            });
            commit('resetSelectedSubline')
            dispatch('fetchSublines')
            state.error = false
        })
        .catch((error) => {

            let message;

            if (error.response.status == 422) {
                message = 'El código de la sublinea ya existe'
            } else {
                message = error.message
            }

            MessageBox.alert(message, 'Error', {
                dangerouslyUseHTMLString: true,
                confirmButtonText: 'Aceptar',
                type: 'error',
                customClass: 'notification-error'
            })


            commit('sublineError', error.message)
        })
}

//Consultar funcionario por id
export async function getSubline({
    commit,
    state
}, id) {

    if (state.selectedSubline.id != id) {

        await $http.get(`/api/sublineas/${id}`)
            .then((response) => {
                commit('setSubline', response.data.data)
            })
            .catch((error) => {
                Notification.error({
                    title: 'Error!',
                    message: error.message,
                    type: 'error',
                    duration: 3000,
                    customClass: 'notification-box',
                });
                commit('sublineError', error.message)
            })
    }

}

//Actualizar funcionario
export async function updateSubline({
    commit,
    state,
    dispatch
}) {

    await $http.put(`/api/sublineas/${state.selectedSubline.id}`, state.selectedSubline)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'La Sublinea ha sido actualizada',
                duration: 3000,
                customClass: 'notification-box',
            });
            commit('setActionSublines', 'create')
            commit('resetSelectedSubline')
            dispatch('fetchSublines')
            state.error = false
        })
        .catch((error) => {
            let message;

            if (error.response.status == 422) {
                message = error.response.data.errors.subline_code[0]
            } else {
                message = error.message
            }

            MessageBox.alert(message, 'Error', {
                confirmButtonText: 'Aceptar',
                type: 'error'
            })

            commit('sublineError', error.message)
        })
}


//Eliminar funcionario
export async function removeSubline({
    commit,
    dispatch
}, subline) {

    await $http.delete(`/api/sublineas/${subline.id}`)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'La Sublinea ha sido eliminada',
                duration: 3000,
                customClass: 'notification-box',
            });
            dispatch('fetchSublines')
        })
        .catch((error) => {
            MessageBox.alert(error.message, 'Error', {
                confirmButtonText: 'Aceptar',
                type: 'error'
            })

            commit('sublineError', e.message)
        })
}

export async function listSublines({
	    commit,
	    state
	}, requestParameters = {
	    params: state.params
	}) {
    await $http.get('/api/lista-sublineas', requestParameters)
    .then((response) => {
        commit('setSublines', response.data.data)
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
        commit('sublineError', error.message)
    })
}

export async function getSublineByLineId({
    commit,
    state
}, line_id) {

    await $http.get(`/api/sublinea-por-linea?line_id=${line_id}`)
        .then((response) => {
            commit('setSublineList', response.data)
        })

}
