export const defaultSelectedMenu = () => {
  return {
    module_id: null,
    order: null,
    icon: null,
    url: "",
    menu_id: null,
    description: "",
    second_level: []
  };
};
export default {
  menu: [],
  selected_menu: defaultSelectedMenu(),
  permissions: [],
  modules: [],
  parents_menu: [],
  menus: [],
  menus_filtered: [],
  error: false,
  error_message: "",
  action: "create",
  get_menu_subscriber: true,
  left_menu: {
    selected_menu: null
  },
  menuVideoTutorial: []
};
