import { defaultSelectedMenu } from "./state";

export function setMenu(state, menu) {
  menu.map(module => {
    if (module.first_level) {
      module.first_level.map(fl => {
        if (fl.visible === undefined) {
          fl.visible = false;
        }
      });
    }
  });

  state.menu = menu;
}

export function setSelectedMenu(state, selected_menu) {
  state.selected_menu = JSON.parse(JSON.stringify(selected_menu));
}

export function setModules(state, modules) {
  state.modules = modules;
}

export function setMenus(state, menus) {
  state.menus = menus;
}

export function setMenusFiltered(state, menus_filtered) {
  state.menus_filtered = menus_filtered;
}

export function setParentsMenus(state, parents_menu) {
  state.parents_menu = parents_menu;
}

export function cleanMenuId(state) {
  state.selected_menu.menu_id = "";
}

export function setAction(state, action) {
  state.action = action;
}

export function menuError(state, payload) {
  state.error = true;
  state.errorMessage = payload;
}

export function setGetMenu(state, get_menu) {
  state.get_menu = get_menu;
}

export function resetSelectedMenu(state) {
  state.selected_menu = defaultSelectedMenu();
}

export function refactorMenu(state) {
  state.menu.map(module => {
    if (module.first_level) {
      module.first_level.map(fl => {
        fl.module_id = module.id;
        if (fl.second_level) {
          fl.second_level.map(sl => {
            sl.module_id = module.id;
            sl.menu_id = fl.id;
          });
        }
      });
    }
  });
}

export function setPermissions(state, permissions) {
  state.permissions = permissions;
}

export function setSelectedMenuUser(state, selected_menu_user) {
  state.left_menu.selected_menu = selected_menu_user;
}

export function setMenuVideoTutorial(state, notes) {
  state.menuVideoTutorial = notes;
}
