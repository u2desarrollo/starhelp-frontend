import $http from "../../axios";
import { Notification } from "element-ui";

export async function getMenu({ commit, dispatch }) {
  await $http
    .get("/api/menu")
    .then(response => {
      commit("setMenu", response.data.data);
      dispatch("fetchModules");
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function getPermissions({ commit, dispatch }) {
  await $http.get("/api/permisos").then(response => {
    commit("setPermissions", response.data);
  });
}

//Actualizar menu de suscriptor
export async function store({ commit, state }) {
  await $http
    .post(`/api/menu/`, state.menu)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: " El menu ha sido actualizado",
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });
      commit("setMenu", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
      commit("menuSubscriberError", error.message);
    });
}

export async function fetchParentsMenus({ state, commit, dispatch }) {
  let module_id = state.selected_menu.module_id;
  if (module_id) {
    let module = state.menu.find(m => m.id === module_id);

    let parents_menu = [];

    module.first_level.map(fl => {
      parents_menu.push(fl);
    });

    commit("setParentsMenus", parents_menu);
  }
}

export async function fetchModules({ commit, state }) {
  let menu = JSON.parse(JSON.stringify(state.menu));

  let modules = [];

  menu.map(module => {
    modules.push(module);
  });

  commit("setModules", modules);
}

export async function fetchMenus({ commit, state }) {
  if (!state.menus.length) {
    await $http
      .get("/api/menus")
      .then(response => {
        commit("setMenus", response.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 2000,
          customClass: "notification-box"
        });
        commit("menuSubscriberError", error.message);
      });
  }
}

export async function fetchSubscribers({ commit, state, rootState, dispatch }) {
  if (!state.subscribers.length) {
    await dispatch(
      "subscribers/fetchSubscribers",
      { params: { paginate: false } },
      { root: true }
    );
    commit("setSubscribers", rootState.subscribers.subscribers);
  }
}

export function copyMenuItem({ commit, state }, menu_copy) {
  let current_menu = JSON.parse(JSON.stringify(state.menu));

  menu_copy = JSON.parse(JSON.stringify(menu_copy));
  menu_copy.id = (Math.floor(Math.random() * (10000 - 1000)) + 1000) * -1;

  let copied = false;
  let menu_id = menu_copy.menu_id;

  current_menu.map(module => {
    if (menu_copy.module_id === module.id) {
      if (menu_id == null || menu_id === "") {
        if (module.id === menu_copy.module_id) {
          if (!module.first_level) {
            module.first_level = [];
          }
          module.first_level.push(menu_copy);
          copied = true;
        }
      }
      if (module.first_level && !copied) {
        module.first_level.map(fl => {
          if (fl.id === menu_id) {
            if (!fl.second_level) {
              fl.second_level = [];
            }
            fl.second_level.push(menu_copy);
            copied = true;
          }
          if (fl.second_level && !copied) {
            fl.second_level.map(sl => {
              if (sl.id === menu_id) {
                if (!sl.third_level) {
                  sl.third_level = [];
                }
                sl.third_level.push(menu_copy);
                copied = true;
              }
            });
          }
        });
      }
    }
  });

  commit("setMenu", current_menu);
  commit("refactorMenu");
}

export function deleteMenuItem({ commit, state }, menu_delete) {
  let menu = JSON.parse(JSON.stringify(state.menu));

  menu_delete = JSON.parse(JSON.stringify(menu_delete));

  let menu_id = menu_delete.id;

  let deleted = false;

  menu.map(module => {
    if (module.id === menu_delete.module_id) {
      if (module.first_level && !deleted) {
        module.first_level.map((fl, ifl) => {
          if (fl.id === menu_id) {
            module.first_level.splice(ifl, 1);
            deleted = true;
          }
          if (fl.second_level && !deleted) {
            fl.second_level.map((sl, isl) => {
              if (sl.id === menu_id) {
                fl.second_level.splice(isl, 1);
                deleted = true;
              }
              if (sl.third_level && !deleted) {
                sl.third_level.map((tl, itl) => {
                  if (tl.id == menu_id) {
                    sl.third_level.splice(itl, 1);
                    deleted = true;
                  }
                });
              }
            });
          }
        });
      }
    }
  });

  commit("setMenu", menu);
  commit("refactorMenu");
}

export function addMenuItem({ commit, state }) {
  let current_menu = JSON.parse(JSON.stringify(state.menu));

  let selected_menu = JSON.parse(JSON.stringify(state.selected_menu));
  selected_menu.id = (Math.floor(Math.random() * (10000 - 1000)) + 1000) * -1;

  let added = false;

  for (let module in current_menu) {
    if (module.id === selected_menu.module_id && !added) {
      if (selected_menu.menu_id && !added) {
      } else {
        module.first_level.push(selected_menu);
      }
    }
  }

  current_menu.map(module => {
    if (module.id === selected_menu.module_id && !added) {
      if (selected_menu.menu_id) {
        if (module.first_level) {
          module.first_level.map(first_level => {
            if (first_level.id === selected_menu.menu_id && !added) {
              if (!first_level.second_level) {
                first_level.second_level = [];
              }
              if (!selected_menu.third_level) {
                selected_menu.third_level = [];
              }
              first_level.second_level.push(selected_menu);
              added = true;
            }
          });
        }
      } else {
        module.first_level.push(selected_menu);
      }
    }
  });

  commit("setMenu", current_menu);
  commit("resetSelectedMenu");
}

export function updateMenuItem({ commit, state }) {
  let selected_menu = JSON.parse(JSON.stringify(state.selected_menu));

  let menu = JSON.parse(JSON.stringify(state.menu));

  let menu_id = selected_menu.id;

  let updated = false;

  menu.map(module => {
    if (module.id == selected_menu.module_id) {
      if (module.first_level && !updated) {
        module.first_level.map((fl, ifl) => {
          if (fl.id == menu_id) {
            module.first_level.splice(ifl, 1, selected_menu);
            updated = true;
          }
          if (fl.second_level && !updated) {
            fl.second_level.map((sl, isl) => {
              if (sl.id == menu_id) {
                fl.second_level.splice(isl, 1, selected_menu);
                updated = true;
              }
            });
          }
        });
      }
    }
  });

  commit("setMenu", menu);
  commit("resetSelectedMenu");
}

export function filterMenus({ state, commit }) {
  let menus = state.menus;

  let menus_filtered = [];

  let current_menu = JSON.parse(JSON.stringify(state.menu));

  if (
    state.selected_menu.menu_id == null ||
    state.selected_menu.menu_id == ""
  ) {
    menus_filtered = menus.filter(m => {
      if (m.level == 1) {
        return m;
      }
    });
  } else {
    let menu = null;

    current_menu.map(module => {
      if (module.id == state.selected_menu.module_id) {
        if (module.first_level) {
          module.first_level.map(fl => {
            if (fl.id == state.selected_menu.menu_id) {
              menu = fl;
            }
            if (fl.second_level) {
              fl.second_level.map(sl => {
                if (sl.id == state.selected_menu.menu_id) {
                  menu = sl;
                }
              });
            }
          });
        }
      }
    });

    if (menu) {
      if (menu.level === 1) {
        menus_filtered = menus.filter(m => {
          if (m.level == 2) {
            return m;
          }
        });
      } else if (menu.level === 2) {
        menus_filtered = menus.filter(m => {
          if (m.level === 3) {
            return m;
          }
        });
      }
    }
  }

  commit("setMenusFiltered", menus_filtered);
}

export function changeVisible({ commit, state }, menu_change) {
  let menu = JSON.parse(JSON.stringify(state.menu));

  let menu_id = menu_change.id;

  menu.map(module => {
    if (module.id === menu_change.module_id) {
      if (module.first_level) {
        module.first_level.map((fl, ifl) => {
          if (fl.id === menu_id) {
            fl.visible = !fl.visible;
          } else {
            fl.visible = false;
          }
        });
      }
    }
  });

  commit("setMenu", menu);
}

export async function findAllMenuVideoTutorial({ commit }, menu_id) {
  await $http
    .get("/api/menu-video-tutorial", {
      params: {
        menu_id
      }
    })
    .then(response => {
      commit("setMenuVideoTutorial", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function createMenuVideoTutorial({ dispatch }, data) {
  await $http
    .post("/api/menu-video-tutorial", data)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "Creado con exito",
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function updateMenuVideoTutorial({ dispatch }, data) {
  await $http
    .put(`/api/menu-video-tutorial/${data.id}`, data.data)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "Actualizado con exito",
        type: "success",
        duration: 2000,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function deleteMenuVideoTutorial({ dispatch }, id) {
  await $http
    .delete(`/api/menu-video-tutorial/${id}`)
    .then(response => {
      // dispatch("findAllMenuVideoTutorial");
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}
