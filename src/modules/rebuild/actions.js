import $http from "../../axios";
import { Notification } from "element-ui";

export async function searchProducts({ state, commit }, filter) {
    let params = {
      filter: filter,
      paginate: false,
      branchoffice_id: localStorage.selected_branchoffice
    };
    await $http.get("/api/lista-productos", { params }).then(response => {
      commit("setProducts", response.data.data.data);
    });
  }


export async function rebuild({ state, commit }, filter) {
let params = state.params
await $http.get("/api/reconstructor-global", { params }).then(response => {
    Notification.success({
        title: "Exito!",
        message:'El proceso a finalizado satisfactoriamente',
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
})
.catch(error => {
    Notification.error({
      title: "Error!",
      message: error.message,
      type: "error",
      duration: 3000,
      customClass: "notification-box"
    });
});
}
