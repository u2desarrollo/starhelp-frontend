import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";

export async function searchmails({ state, commit }, text) {
  let params = {
    params: {
      filter: text
    }
  };

  await $http
    .get("/api/buscar-correo", params)
    .then(response => {
      commit("setMails", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function updatemail({ state, commit }, to, contact_id) {
  let params = {
    to: to,
    contact: contact_id
  };

  await $http
    .put(`/api/actualizar-correo`, params)
    .then(response => {
      commit("setMails", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error al eliminar el elemento.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function sendmail({ state, commit }, params) {
  await $http
    .post("/api/enviar-correo", params)
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "El correo ha sido enviado exitosamente.",
        type: "success",
        duration: 1500,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}