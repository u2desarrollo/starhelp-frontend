export default {
    sendingMail: false,
    modalAttachment: {
        show: false
    },
    paramsmail: {
        to: [],
        body: null,
        subject: null,
        updateEmailContact: false,
    },
    mails: [],
    send_mail: false
}