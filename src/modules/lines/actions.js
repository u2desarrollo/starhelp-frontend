import $http from "../../axios";
import {
    Notification,
    MessageBox
} from 'element-ui';


export async function fetchAccountsInventory(
    { commit, state },
    requestParameters = { params: state.paramsAccountInventory }
  ) {
    await $http
      .get("/api/cuentas", requestParameters)
      .then(response => {
        commit("setAccountsInventory", response.data.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        commit("modelError", error.message);
      });
  }

  export async function fetchAccountsCost(
    { commit, state },
    requestParameters = { params: state.paramsAccountCost }
  ) {
    await $http
      .get("/api/cuentas", requestParameters)
      .then(response => {
        commit("setAccountsCost", response.data.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        commit("modelError", error.message);
      });
  }
  
export async function fetchAccountsSales(
    { commit, state },
    requestParameters = { params: state.paramsAccountSales }
  ) {
    await $http
      .get("/api/cuentas", requestParameters)
      .then(response => {
        commit("setAccountsSales", response.data.data.data);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        commit("modelError", error.message);
      });
  }
//Obtener los lineas
export async function fetchLines ({
    commit,
    state
}, requestParameters = {
    params: state.params
}) {

    await $http.get('/api/lineas', requestParameters)
        .then((response) => {
            commit('setLines', response.data.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('lineError', error.message)
        })
}

//Crear tercero
export async function addLine ({
    commit,
    state
}) {

    state.selectedLine.data_sheets = state.dataSheetsLine;
    await $http.post('/api/lineas', state.selectedLine)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'La linea ha sido creada',
                type: 'success',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('setLine', response.data.data)
            commit('resetSelectedLine')
            state.error = false
        })
        .catch((error) => {

            let message;

            if (error.response.status == 422) {
                message = 'El código de la linea ya existe'
            } else {
                message = error.message
            }

            MessageBox.alert(message, 'Error', {
                dangerouslyUseHTMLString: true,
                confirmButtonText: 'Aceptar',
                type: 'error',
                customClass: 'notification-error'
            })

            commit('lineError', error.message)
        })
}

//Consultar tercero por id
export async function getLine ({
    commit,
    state
}, id) {

    if (state.getLineEdit) {

        await $http.get(`/api/lineas/${id}`)
            .then((response) => {
                commit('setLine', response.data.data)

            })
            .catch((error) => {
                Notification.error({
                    title: 'Error!',
                    message: error.message,
                    type: 'error',
                    duration: 1500,
                    customClass: 'notification-box',
                });
                commit('lineError', error.message)
            })

        commit('setGetLineEdit', false)

    }
}

//Consultar tercero por identificación
export async function getLineForIdentification ({
    commit,
    state,
    rootState
}, identification) {

    await $http.get(`/api/lineas/id/${identification}`)
        .then((response) => {
            if (response.data.success) {
                state.lineEdit = response.data.data.id
            }
        })
        .catch((error) => {
            state.lineEdit = null
            commit('lineError', error.message)
        })
}


//Actualizar tercero
export async function updateLine ({
    commit,
    state
}) {

    state.selectedLine.data_sheets = state.dataSheetsLine;
    await $http.put(`/api/lineas/${state.selectedLine.id}`, state.selectedLine)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'La linea ha sido actualizada',
                type: 'success',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('setLine', response.data.data)
        })
        .catch((error) => {
            let message;

            if (error.response.status == 422) {
                message = 'El código de la linea ya existe'
            } else {
                message = error.message
            }

            MessageBox.alert(message, 'Error', {
                confirmButtonText: 'Aceptar',
                type: 'error'
            })

            commit('lineError', error.message)
        })
}


//Eliminar tercero
export async function removeLine ({
    commit,
    dispatch
}, line) {

    await $http.delete(`/api/lineas/${line.id}`)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'La linea ha sido eliminada',
                type: 'success',
                duration: 1500,
                customClass: 'notification-box',
            });
            dispatch('fetchLines')
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('LineError', error.message)
        })
}

//------------------------ PAGINACIÓN ------------------------//

export function setFilterPagination ({
    commit,
    dispatch
}, filter) {
    commit('setFilter', filter.target.value)
    dispatch('fetchLines')
}

export function setPerPagePagination ({
    commit,
    dispatch
}, perPage) {
    commit('setPerPage', parseInt(perPage.target.value))
    dispatch('fetchLines')
}

export function setPagePagination ({
    commit,
    dispatch
}, page) {
    commit('setPage', parseInt(page))
    dispatch('fetchLines')
}

export function setSortByPagination ({
    commit,
    dispatch
}, sortBy) {
    commit('setSortBy', sortBy)
    // dispatch('fetchLines')
}

export function setSortPagination ({
    commit,
    dispatch
}, sort) {
    commit('setSort', sort)
    // dispatch('fetchLines')
}

/** Obtener listado de impuestos */
export async function fetchTaxes ({
    commit,
    state
}) {
    await $http.get('/api/impuestos', {
        params: {
            paginate: false
        }
    }).then((response) => {
        commit('setTaxes', response.data.data)
    }).catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        })
    })
}

/** Obtener los impuestos a partir del id de la línea */
export async function fetchTaxesByLine ({
    state,
    commit
}, line_id) {
    await $http.get('/api/lineas-impuestos', {
        params: {
            id: line_id
        }
    }).then((response) => {
        commit('setTaxesByLine', response.data.data)
    }).catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        })
    })
}

/** Guardar impuesto en la línea */
export async function saveTax ({
    state,
    commit,
    dispatch
}, params) {
    await $http.post('/api/lineas-impuestos', params).then((response) => {
        Notification.success({
            title: 'Exito!',
            message: 'El impuesto  ha sido agaregado a la linea',
            type: 'success',
            duration: 1500,
            customClass: 'notification-box',
        });
        commit('resetTaxesForm')
        dispatch('fetchTaxesByLine', params.line_id)
    }).catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        })
    })
}

/** Eliminar el impuesto de la línea */
export async function deleteTax ({
    state,
    commit,
    dispatch
}, params) {
    await $http.delete(`/api/lineas-impuestos/${params.id}`).then((response) => {
        if (response) {
            dispatch('fetchTaxesByLine', params.line_id)
            Notification.success({
                title: 'Exito!',
                message: 'Se ha eliminado el impuesto',
                type: 'success',
                duration: 1500,
                customClass: 'notification-box',
            })
        }
    }).catch((error) => {
        Notification.error({
            title: '¡Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box'
        })
        commit('setError', error.message)
    })
}


/**
 * Acciones para la ficha de datos
 **/
//Obtener solo un registro segun el id
export async function getDataSheetLine ({
    commit,
    state
}, line_id) {
    await $http.get(`/api/linea/${line_id}/ficha-datos`)
        .then((response) => {
            commit('setDataSheetsLine', response.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
        });
}

export async function getDataSheetList ({
    commit,
    state
}, line_id) {
    await $http.get(`/api/ficha-datos-linea/${line_id}`)
        .then((response) => {
            commit('setDataSheetsList', response.data)
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
        });
}

//Agregamos una ficha de dato a la linea
export async function addDataSheetToLine ({
    commit,
    state
}, data) {

    await $http.post('/api/ficha-datos-linea', data)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: response.data.message,
                type: 'success',
                duration: 1500,
                customClass: 'notification-box',
            });
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.response.data.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
        })
}

//Remover una ficha de dato a la linea
export async function removeDataSheetToLine ({
    commit,
    state
}, id) {

    await $http.delete(`/api/ficha-datos-linea/${id}`)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: response.data.message,
                type: 'success',
                duration: 1500,
                customClass: 'notification-box',
            });
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.response.data.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
        })
}

//Actualizar una ficha de dato a la linea
export async function updateDataSheetToLine ({
    commit,
    state
}, data) {

    await $http.put(`/api/ficha-datos-linea/${data.id}`, data)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: response.data.message,
                type: 'success',
                duration: 1500,
                customClass: 'notification-box',
            });
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.response.data.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
        })
}
export async function create ({
    commit,
    state
}) {

    state.error = false;
    await $http.post('/api/ficha-datos', state.dataSheetLayout)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'La ficha de dato ha sido creada',
                type: 'success',
                duration: 1500,
                customClass: 'notification-box',
            });
            state.error = false;
        })
        .catch((error) => {
            Notification.error({
                title: 'Error!',
                message: error.response.data.message,
                type: 'error',
                duration: 1500,
                customClass: 'notification-box',
            });
            commit('setError', error.response);
        });
}


//Obtener todas las lineas
export async function getLinesList ({
    commit,
    state
}) {

    await $http.get('/api/lista-lineas')
        .then((response) => {
            commit('setLinesList', response.data)
        })
        .catch((error) => {

        })
}