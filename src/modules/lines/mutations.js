import {
    defaultSelectedClerk,
    defaultSelectedLine,
    defaultSelectedDocument,
    defaultSelectedWarehouse,
    defaultSelectedTax
} from "./state";

export function setLines(state, lines) {
    state.lines = lines;
}

//Agregamos las fichas de datos asociados a la linea.
export function setDataSheetsLine(state, data) {
    state.dataSheetsLine = data;
}

//Se agrega el listado de ficha de datos para el select de adicionar
export function setDataSheetsList(state, data) {
    state.dataSheetsList = data;
}

//Se agrega el listado de ficha de datos para el select de adicionar
export function clearDataSheetsList(state) {
    state.dataSheetsList = [];
}

export function setLine(state, line) {
    state.selectedLine = line;
}

export function setAccountsInventory(state, accountsInventory) {
    state.accountsInventory = accountsInventory
}

export function setAccountsCost(state, accountsCost) {
    state.accountsCost = accountsCost
}

export function setAccountsSales(state, accountsSales) {
    state.accountsSales = accountsSales
}

export function setAction(state, action) {
    state.action = action;
}

export function lineError(state, payload) {
    state.error = true
    state.errorMessage = payload
    state.lines = []
}

export function setGetLineEdit(state, getLineEdit) {
    state.getLineEdit = getLineEdit;
}

//Establece el valor por defecto de selectedLine
export function resetSelectedLine(state) {
    // let id = state.selectedLine.line_id
    Object.assign(state.selectedLine, defaultSelectedLine())
        //state.selectedLine.id = id
}

/** Asigna el valor al error, cuando ocurre uno */
export function setError(state, payload) {
    state.error = true
    state.errorMessage = payload
}

//------------------------ PAGINACIÓN ------------------------//

export function setFilter(state, filter) {
    state.params.page = 1
    state.params.filter = filter
}

export function setPerPage(state, perPage) {
    state.params.page = 1
    state.params.perPage = perPage
}

export function setPage(state, page) {
    state.params.page = page
}

export function setSortBy(state, sortBy) {
    state.params.sortBy = sortBy
}

export function setSort(state, sort) {
    state.params.sort = sort
}

/** Asignar valor a los impuestos */
export function setTaxes(state, taxes) {
    state.taxes = taxes
}

/** Asignar valor al impuesto seleccionado */
export function setSelectedTax(state, tax) {
    state.selectedTax = tax
}

/** Asignar valor a los impuestos que pertenecen a una línea específica */
export function setTaxesByLine(state, taxes) {
    state.lineTaxes = taxes
}

/** Limpiar el formulario de impuestos */
export function resetTaxesForm(state) {
    state.selectedTax = defaultSelectedTax();
}

// Agregamos la lista de lineas al atributo
export function setLinesList(state, data) {
    state.linesList = data;
}

//Se agrega el listado de ficha de datos para el select de adicionar
export function clearDataSheetsLine(state) {
    state.dataSheetsLine = [];
}

export function chageTableCreateData(state, val) {
    state.tableCreateData = val;
}