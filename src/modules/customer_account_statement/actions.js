import $http from "../../axios";
import { Notification, MessageBox } from "element-ui";


export async function  getPdf({state,commit}) {
    var response = await $http.get('/api/pdfCartera',
      {
          params:state.params,
       // responseType: "blob"
      }
      )

      commit('setPathPdf',response.data);
      // const url = window.URL.createObjectURL(new Blob([response.data]));
      // const link = document.createElement("a");
      // link.href = url;
      // link.setAttribute("download", "cartera.pdf"); //or any other extension
      // document.body.appendChild(link);
      // link.click();
  }


  export async function getAllClients({state,commit},nameClient) {

    await $http.get('/api/todo-clientes',{
        params: {
            nameClient: nameClient
        }
      }).then( async response=>{
         await commit('setClients',response.data)
    }).catch();

  }


  export async function  getPdfNegativeValue({state,commit}) {
    var response = await $http.get('/api/pdfReporteNegativo',
      {
          params:state.params,
       // responseType: "blob"
      }
      )

      commit('setPathPdf',response.data);
      // const url = window.URL.createObjectURL(new Blob([response.data]));
      // const link = document.createElement("a");
      // link.href = url;
      // link.setAttribute("download", "cartera.pdf"); //or any other extension
      // document.body.appendChild(link);
      // link.click();
  }


  export async function  pdfPrejuridico({state,commit}) {
    var response = await $http.get('/api/pdfPrejuridico',
      {
          params:state.params,
       // responseType: "blob"
      }
      )

      commit('setPathPdf',response.data);
      // const url = window.URL.createObjectURL(new Blob([response.data]));
      // const link = document.createElement("a");
      // link.href = url;
      // link.setAttribute("download", "cartera.pdf"); //or any other extension
      // document.body.appendChild(link);
      // link.click();
  }

export async function searchMails({ state, commit }, text) {
  await $http
    .get(
      `/api/buscar-correo?filter=${text}&contact=${state.params.client}`
    )
    .then(response => {
      commit("setMails", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
