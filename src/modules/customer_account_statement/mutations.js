import { paramsDefault } from "./state";
import {getApiDomain} from "../../util/domains";

export function setClients(state,clients) {
    state.clients=clients
}



export function setShowPdf(state, value) {
    state.show_pdf=value;
}


export function setPathPdf(state,pathPdf ) {
  state.paramsEmail.filename=pathPdf;
  state.pathPdf= getApiDomain() + '/storage/pdf/' + pathPdf;
}


export function setParamMailTo(state,mail) {
   state.paramsEmail.to=[];
   state.paramsEmail.to.push(mail);

}


export function setParamDefault(state) {
    state.params=paramsDefault();
}


export function setPreloader(state, preloader) {
    state.preloader=preloader;
}

export function setAuthenticatedRole(state,authenticatedRole){
    state.authenticatedRole=authenticatedRole;
}

export function setMails(state,mails){
  state.mails=mails;
}
