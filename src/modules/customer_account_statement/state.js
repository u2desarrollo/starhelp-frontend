export const paramsDefault= ()=>{
    return{
        client:null,
        observation:null,
        legalrepresentative:''
    }
}



export  default {


    show_pdf:false,
    clients:[],
    params:paramsDefault(),
    mails: [],
    file:null,
    paramsEmail:{
        to:[],
        subject:null,
        body:null,
        filename:null

    },
    pathPdf:null,
    preloader:false,
    authenticatedRole:''

}
