import $http from "../../axios";
import { Notification } from 'element-ui';

export async function fetchVouchertype({commit, state}) {
    await $http.get('/api/tipos-de-comprobantes', { params : state.paramsVoucherType})
        .then((response) => {
            commit('setVouchersTypes', response.data.data)
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}
//Obtener las sedes
export async function fetchBranchOffices ({ commit, state }) {
    await $http.get('/api/sedes')
        .then((response) => {
            commit('setBranchOffices', response.data)
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}

//Obtener las bodega
export async function fetchBranchOfficesWarehouse ({ commit, state }, branchOffice) {
    await $http.get(`/api/bodegas-sedes?branchoffice_id=${branchOffice}`)
        .then((response) => {
            commit("setBranchofficeWarehousesEntry", response.data.data);
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}

export async function fetchTemplates({commit, state}) {
    await $http.get('/api/modelos', { params : state.paramstemplates})
        .then((response) => {
            commit('setTemplates', response.data.data.data)
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}

export async function fetchTemplatesExclude({commit, state}) {
    await $http.get('/api/modelos', { params : state.paramstemplates_exclude})
        .then((response) => {
            commit('setTemplatesExclude', response.data.data.data)
        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 2000,
            customClass: 'notification-box',
        });
    })
}

export async function fetchLines ({ dispatch, state, rootState, commit }) {
    if (!state.lines.length) {
      await dispatch('lines/fetchLines', { params: { paginate: false } }, { root: true })
      commit('setLines', rootState.lines.lines)
    }
}

export async function fetchSublines ({ dispatch, state, rootState, commit }, requestParameters = { params: state.category_params }) {
    let params = {
      params: {
        lines: state.params.filter_lines,
        filter_brands: state.params.filter_brands,
        filter_groups: state.params.filter_groups
      }
    }

    await dispatch('sublines/listSublines', params, { root: true })
    commit('setGroups', rootState.sublines.sublines)
}

export async function fetchBrand ({ dispatch, state, rootState, commit }) {
    await dispatch('brands/fetchBrands', { params: { paginate: false, lines: state.params.lines } }, { root: true })
    commit('setBrands', rootState.brands.brands)
}

export async function fetchSubbrands ({ dispatch, state, rootState, commit }, requestParameters = { params: state.params }) {
    await dispatch('subbrands/listSubbrands', requestParameters, { root: true })
    commit('setSubbrands', rootState.subbrands.subbrands)
}

export async function searchProducts ({state,commit}, filter) {
    let params ={
        filter : filter,
        paginate : false,
        branchoffice_id: localStorage.selected_branchoffice,
        branchoffice_warehouse_id: localStorage.selected_branchoffice_warehouse
    }
    await $http.get('/api/lista-productos', {params}).then((response) => {
        commit('setProducts', response.data.data.data)
    })
}

export async function fetchContacts({commit,state}, requestParameters = {params: state.paramsquery}) {
    await $http.get('/api/terceros', requestParameters)
        .then((response) => {
            commit('setContacts', response.data.data.data)

        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
}

export async function fetchDepartaments({commit}) {
    await $http.get('/api/departamentos')
        .then((response) => {
            commit('setDepartaments', response.data.data)

        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
}

export async function fetchCities({commit, state}, text) {
    let params = { department:state.params.departament, filter:text}
    await $http.get('/api/ciudades-departamentos', {params} )
        .then((response) => {
            commit('setCities', response.data.data)

        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
}

export async function fetchTypesOperations({commit, state}){
    await $http.get('/api/types_operations?paginate=false')
        .then((response) => {
            commit('settypes_operations', response.data.data)

        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
}

export async function fetchsellers({commit, state}, text) {
    let params = { filter:text}
    await $http.get('/api/lista-vendedores', {params} )
        .then((response) => {
            commit('setSellers', response.data.data)

        })
        .catch((error) => {
            Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
}

export async function generateReport({commit, state}) {
    await $http({
        url: `/api/crear-informe-operaciones`,
        method: 'GET',
        params: state.params,
        responseType: 'blob', // important
      }).then(response => {

        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', state.params.name_file+'.xlsx'); //or any other extension
        document.body.appendChild(link);
        link.click();
        commit('setLoading', false)

      }).catch((error) => {
        Notification.error({
          title: "Error!",
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box'
        });
      })
}
