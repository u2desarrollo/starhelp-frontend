export function setVouchersTypes(state, vouchertype) {
    state.voucherTypes = vouchertype    
}
export function setBranchOffice(state , branchOffice) {
    state.paramsVoucherType.branchoffice = branchOffice
}
export function setBranchOffices(state , branchOffices) {
    state.branchOffices = branchOffices
}
export function setTemplates(state , templates) {
    state.templates = templates
}
export function setTemplatesExclude(state , templates) {
    state.templates_exlude = templates
}
export function setLines(state, lines){
    state.lines = lines
}
export function setGroups(state, sublines){
    state.sublines = sublines
}
export function setBrands(state, brands){
    state.brands = brands
}
export function setSubbrands(state, subbrands){
    state.subbrands = subbrands
}
export function setProducts(state, products){
    state.products = products
}
export function setContacts(state, contacts){
    state.contacts = contacts
}
export function setDepartaments(state, departaments) {
    state.departaments = departaments
}
export function setCities(state, cities) {
    state.cities = cities
}
export function setSellers(state, sellers) {
    state.sellers = sellers
}
export function settypes_operations(state,types_operations){
    state.types_operations = types_operations
}
export function setBranchofficeWarehousesEntry(state, branchofficeWarehouses){
    state.branchofficeWarehouses = branchofficeWarehouses
}
export function setLoading(state, loading){
    state.loading = loading
}