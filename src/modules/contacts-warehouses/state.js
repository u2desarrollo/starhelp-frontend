
export const defaultSelectedWarehouse = () => {
  return {
    id: null,
    city_id: "",
    code: "",
    description: "",
    address: "",
    latitude: "",
    length: "",
    telephone: "",
    email: "",
    seller_id: null,
    observation: "",
    state: 1,
    branchoffice_id: "",
    creation_date: new Date(),
    other_telephones: null,
    payment_type_id: null,
    discount_percentage: null,
    zone_id:null,
    types_negotiation_portfolio: null,
    zone_despatch_id:null
  };
};

export default {
  cities: [],
  warehouses: [],
  selectedWarehouse: defaultSelectedWarehouse(),
  error: false,
  errorMessage: "",
  actionWarehouse: "create",
  citiesWarehouses: [],
  sellers: [],
  paymentMethods: [],
  types_negotiation_portfolio: [],
  zones:[],
  zones_despatch:[]
};
