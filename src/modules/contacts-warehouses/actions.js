import $http from "../../axios";
import {Notification, MessageBox} from 'element-ui';

// Obtiene las ciudades
export async function fetchCities({
    dispatch,
    state,
    rootState,
    commit
}, payload) {

    let id = false;
    id = ''

    if (payload) {
        if (payload.idDepart != '') {
            id = payload.idDepart
        }
    }

    await dispatch('cities/fetchCities', {
        params: {
            department_id: id
        }
    }, {
        root: true
    })
    commit('setCities', rootState.cities.cities)

}

//Obtener los establecimientos
export async function fetchWarehouses({commit, state, rootState}) {

    // if (!state.warehouses.length) {
        await $http.get(`/api/establecimientos?contact_id=${rootState.contacts.selectedContact.id}`)
            .then((response) => {
                commit('setWarehouses', response.data.data)
            })
            .catch((error) => {
                Notification.error({
                    title: 'Error!',
                    message: error.message,
                    type: 'error',
                    duration: 1500,
                    customClass: 'notification-box',
                });
                commit('warehouseError', error.message)
            })
    // }
}

//Crear establecimiento
export async function addWarehouse({commit, state, dispatch, rootState}) {

    state.selectedWarehouse.contact_id = rootState.contacts.selectedContact.id;

    await $http.post('/api/establecimientos', state.selectedWarehouse)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'El establecimiento ha sido creado',
                type: 'success',
                duration: 3000,
                customClass: 'notification-box',
            });
            commit('resetSelectedWarehouse')
            dispatch('fetchWarehouses')
            state.error = false
        })
        .catch((error) => {

            let message;

            if (error.response.status == 422) {
                message = "Algunos campos no se registraron correctamente"
            } else {
                message = error.message
            }

            MessageBox.alert(message, 'Error', {
                confirmButtonText: 'Aceptar',
                type: 'error'
            })

            commit('warehouseError', error.message)
        })
}

//Actualizar establecimiento
export async function updateWarehouse({commit, state, dispatch}) {

    await $http.put(`/api/establecimientos/${state.selectedWarehouse.id}`, state.selectedWarehouse)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'El establecimiento ha sido actualizado',
                duration: 3000,
                customClass: 'notification-box',
            });
            commit('setActionWarehouses', 'create')
            commit('resetSelectedWarehouse')
            dispatch('fetchWarehouses')
            state.error = false
        })
        .catch((error) => {
            let message;

            if (error.response.status == 422) {
                message = "Algunos campos no se registraron correctamente"
            } else {
                message = error.message
            }

            MessageBox.alert(message, 'Error', {
                confirmButtonText: 'Aceptar',
                type: 'error'
            })

            commit('warehouseError', error.message)
        })
}


//Eliminar establecimiento
export async function removeWarehouse({commit, dispatch}, warehouse) {

    await $http.delete(`/api/establecimientos/${warehouse.id}`)
        .then((response) => {
            Notification.success({
                title: 'Exito!',
                message: 'El establecimiento ha sido eliminado',
                duration: 3000,
                customClass: 'notification-box',
            });
            dispatch('fetchWarehouses')
        })
        .catch((error) => {
            MessageBox.alert(error.response.data.message, 'Error', {
                confirmButtonText: 'Aceptar',
                type: 'error'
            })

            commit('warehouseError', e.message)
        })
}


export async function fetchsellers({commit, state}, text) {
  let params = { filter:text}
  await $http.get('/api/lista-vendedores', {params} )
      .then((response) => {
          commit('setSellers', response.data.data)
      })
      .catch((error) => {
          Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
      });
  })
}


export async function getTypePayment({commit}){
    await $http.get('/api/type-payment').then(response=>{
commit('setPaymentMethods',response.data.data);
    }).catch(error=>{

    })
}

export async function getTypeNegotiationPortfolio({commit}){
    await $http.get('/api/tipo-negociacion-cartera').then(response=>{
commit('setTypeNegotiationPortfolio',response.data.data);
    }).catch(error=>{

    })
}

function mapParameters(parameters) {
    return parameters.map(p => {
      return {
        value: p.id,
        label: p.name_parameter
      };
    });
  }



export async function fetchZones({ dispatch, state, rootState, commit }) {
    if (!state.zones.length) {
      await dispatch(
        "parameters/listParameter",
        {
          params: {
            idParamTable: 95,
            paginate: false
          }
        },
        {
          root: true
        }
      );
      commit("setZones", mapParameters(rootState.parameters.parameters));
    }
  }

export async function fetchZonesDespatch({ dispatch, state, rootState, commit }) {
  if (!state.zones_despatch.length) {
    await dispatch(
      "parameters/listParameter",
      {
        params: {
          idParamTable: 6,
          paginate: false
        }
      },
      {
        root: true
      }
    );
    commit("setZonesDespatch", mapParameters(rootState.parameters.parameters));
  }
}
