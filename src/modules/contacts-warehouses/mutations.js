import {defaultSelectedWarehouse} from "./state";

export function setWarehouses(state, warehouses) {
    state.warehouses = warehouses;
}

export function setWarehouse(state, warehouse) {
    state.selectedWarehouse = JSON.parse(JSON.stringify(warehouse));
}

export function setActionWarehouses(state, action) {
    state.actionWarehouse = action;
}

export function warehouseError(state, error) {
    state.error = true
    state.errorMessage = error
}

//Establece el valor por defecto de selectedWarehouse
export function resetSelectedWarehouse(state) {
    let id = state.selectedWarehouse.id
    Object.assign(state.selectedWarehouse, defaultSelectedWarehouse())
    if (id) {
        state.selectedWarehouse.id = id
    }
}


 export function setSellers(state,sellers) {
   state.sellers=sellers;
}

export function setCities(state, cities){
    state.cities =cities;
}

export function setPaymentMethods(state,data){
    state.paymentMethods=data;
}

export function setTypeNegotiationPortfolio(state,data){
    state.types_negotiation_portfolio=data;
}
export function setZones(state, zones) {
    state.zones = zones
  }

export function setZonesDespatch(state, zones_despatch) {
  state.zones_despatch = zones_despatch
}
