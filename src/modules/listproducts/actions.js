import $http from '../../axios'
import { Notification } from 'element-ui'
import { cpus } from 'os';

export async function getProducts({commit,state}){
	await $http.get(`api/lista-productos`).then((response)=>{
		commit('getProducts',response.data);
	})
}
export async function getBranchsWare({commit,state}){
	await $http.get('api/branchs-ware').then((response)=>{
		commit('getBranchsWare',response.data);
	}).catch((error)=>{
		Notification.error({
			title: '¡Error!',
			message: error.message,
			type: 'error',
			duration: 1500,
			customClass: 'notification-box'
		})
	})
}
