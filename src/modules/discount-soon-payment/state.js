export default {
    discount_soon_payment: null,
    discounts_soon_payment: [],
    lines: [],
    brands:[],
    sub_lines: [],
    branchoffices: [],
    params: {
        branchoffice_id: null,
        line: null,
        sub_line: null,
        days_up: null,
        percentage: null,
        brand_id:null
    }
}


