export function setLines (state, lines) {
    state.lines = lines 
}
export function setSubLines (state, sub_lines) {
    state.sub_lines = sub_lines 
}
export function setDiscounts (state, discounts_soon_payment) {
    state.discounts_soon_payment = discounts_soon_payment 
}
export function setDiscount (state, discount_soon_payment) {
    state.discount_soon_payment = discount_soon_payment 
}



export function setBranchoffices (state, branchoffices) {
    state.branchoffices = branchoffices 
}

export function setBrands(state, brands) {
    state.brands=brands;
    
}