import $http from "../../axios"
import { Notification, MessageBox } from 'element-ui'

export async function fetchLines ({ commit, state }) {
  
    await $http.get('/api/lista-lineas')
    .then((response) => {
        commit('setLines', response.data)
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function fetchSubLines ({ commit, state }, line) {
    await $http.get(`/api/lista-sublineas?lines=${line}`)
    .then((response) => {
        commit('setSubLines', response.data.data)
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function createDiscount ({ commit, state }, params) {
  
    await $http.post('/api/crear-descuento-pronto-pago', params)
    .then((response) => {     
        
        Notification.success({
            title: 'Hecho!',
            message: 'Se ha registrado correctamente',
            type: 'success',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function fetchDiscounts ({ commit, state }) {
  
    await $http.get('/api/lista-descuentos-pronto-pago')
    .then((response) => {
        commit('setDiscounts', response.data.data)
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function fetchDiscount ({ commit, state }, id) {
  
    await $http.get(`/api/descuento-pronto-pago/${id}`)
    .then((response) => {
        commit('setDiscount', response.data)
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function updateDiscount ({ commit, state }, params) {
  
    await $http.put('/api/actualizar-descuento-pronto-pago', params)
    .then((response) => {
        Notification.success({
            title: 'Hecho!',
            message: 'Se ha actualizado correctamente',
            type: 'success',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}

export async function deleteDiscount ({ commit, state }, id) {    
    await $http.delete('/api/eliminar-descuento-pronto-pago/'+id, )
    .then((response) => {        
        Notification.success({
            title: 'Hecho!',
            message: 'Se ha Eliminado correctamente',
            type: 'success',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
    .catch((error) => {
        Notification.error({
            title: 'Error!',
            message: error.message,
            type: 'error',
            duration: 1500,
            customClass: 'notification-box',
        });
    })
  
}


export async function getBranchs({commit}){
    await $http.get('/api/branchs').then((response)=>{
          commit('setBranchoffices',response.data.data);
    }).catch((error)=>{
   console.log(error);
    })
}

export async function getBrands({commit}){
  await $http.get('/api/lista-marcas').then((response)=>{
    
     commit('setBrands',response.data);
  })
}