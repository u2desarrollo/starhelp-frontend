import $http from "../../axios";
import { Notification } from "element-ui";

export async function fetchConsecutive ({ commit, state }, br) {
  await $http
    .get(`/api/consecutivo/143/${br}`)
    .then(response => {
      commit("setConsecutive", response.data.consecutive_number);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function fetchBranchOfficeWarehousesEntry (
  { commit, state },
  brancheoffice
) {
  await $http
    .get(`/api/bodegas-sedes?branchoffice_id=${brancheoffice}&code[]=01&code[]=17&code[]=02&code[]=11`)
    .then(response => {
      commit("setBranchofficeWarehousesEntry", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

//Obtener las bodegas
export async function fetchBranchOfficeWarehouses (
  { commit, state },
  brancheoffice
) {
  await $http
    .get(`/api/bodegas-sedes?branchoffice_id=${brancheoffice}&code[]=01&code[]=17&code[]=02&code[]=11`)
    .then(response => {
      commit("setBranchofficeWarehouses", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

//Obtener las sedes
export async function fetchBranchOffices ({ commit, state }) {
  await $http
    .get("/api/sedes")
    .then(response => {
      commit("setBranchOffices", response.data);
      commit("setBranchOffices_entry", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
      commit("BranchOfficeError", error.message);
    });
}

export async function createtransfer ({ commit, state }) {
  var data = {
    params: state.params,
    product: state.product
  };

  return await new Promise(async (resolve, reject) => {
    await $http
      .post("/api/tets-rot", data)
      .then(async response => {
        if (!response.data.data.pending_authorization) {
          await commit("setDocument", response.data.data);
          await commit("setShowPdf", true);
          await commit("setLoading_", false);
          Notification.success({
            title: "Hecho!",
            message: "Se Ha creado el traslado",
            type: "success",
            duration: 2000,
            customClass: "notification-box"
          });

        }else{
          Notification.success({
            title: "Hecho!",
            message: "Se Solicitado La Autorización Correctamente",
            type: "success",
            duration: 2000,
            customClass: "notification-box"
          });
        }

        resolve();
      })
      .catch(error => {
        let data = error.response.data

        if (data.type !== undefined && data.type == 'stock') {
          Notification.error({
            title: "Error!",
            message: data.message,
            type: "error",
            duration: 2000,
            customClass: "notification-box"
          });

          commit("setStockProducts", data.data)
          reject(data);
          return
        }

        Notification.error({
          title: "Error!",
          message: data.message,
          type: "error",
          duration: 2000,
          customClass: "notification-box"
        });
      });
      commit("clearParams", data.data)
      commit("setLoading_", false)
    });
}

export async function generatePdf ({ state }, documentid) {
  await $http({
    url: `/api/pdf-traslado/${documentid}`,
    method: "GET",
    responseType: "blob" // important
  })
    .then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "traslado.pdf"); //or any other extension
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchTransferDocument ({ commit, state }) {
  var data = {
    params: state.params,
    product: state.product
  };
  await $http
    .get(`/api/lista-tras`, data)
    .then(response => {
      commit("setTransferDocument", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function fetchVoucherType ({ commit, state }) {
  let params = {
    branchoffice: localStorage.selected_branchoffice,
    code_voucher_type: 140
  };
  await $http
    .get(`/api/tipo-comprobante?branchoffice=${localStorage.selected_branchoffice}&code_voucher_type=140`)
    .then(response => {
      // commit("setTransferDocument", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}


export async function fetchTransfer ({ commit, state }, id) {
  await $http
    .get(`/api/testt/${id}`)
    .then(response => {
      commit("setTransfer", response.data.data);
      commit("setParamsTransfer", response.data.data)
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.response.data.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function fetchTransferForUpdate ({ commit, state }, id) {
  await $http
    .get(`/api/testt/${id}`)
    .then(response => {
      commit("setTransfer", response.data.data);
      commit("setParamsTransferupdate", response.data.data)
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.response.data.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function deletedocumentproduct ({ commit, state }, id) {
  await $http
    .get(`/api/testt/${id}`)
    .then(response => {
      commit("setTransfer", response.data.data);
      commit("setParamsTransferupdate", response.data.data)
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.response.data.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
}

export async function fetchModel({ commit, state }, model_id) {
  let params = {
    params: {
      branchoffice_id: localStorage.selected_branchoffice
    }
  };

  await $http
    .get("/api/modelos/243")
    .then(response => {
      commit("setSelectedModel", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function getProductByCode({ commit, state }) {
  var params = {
    list_code_products: state.list_code_products,
    branchoffice_warehouse_id: state.params.bodega_salida
  };
  await $http
    .post('/api/producto-por-codigo', params)
    .then(response => {
      if (response.data.data.length === state.list_code_products.length) {
        // console.log(response.data.data);
        // console.log(state.list_code_products);
        commit('setProductsGridFromCsv', response.data.data)
      }else{
        Notification.error({
          title: "¡Error!",
          message: 'Algunos productos no existen',
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
      }
      state.list_code_products =[]
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });

}
export async function fetchContacts({
  commit,
  state
}, requestParameters = {
  params: state.paramsquery
}) {

  await $http.get('/api/terceros', requestParameters)
      .then((response) => {
          commit('setContacts', response.data.data.data)

      })
      .catch((error) => {
          Notification.error({
              title: 'Error!',
              message: error.message,
              type: 'error',
              duration: 1500,
              customClass: 'notification-box',
          });
          commit('contactError', error.message)
      })
}

export async function fetchContactsWarehouses({
  commit,
  state
},contact) {
  let  requestParameters = {
    params: {contact_id : contact}
  }
  await $http.get('/api/establecimientos', requestParameters)
      .then((response) => {
          commit('setContactsWarehouses', response.data.data)

      })
      .catch((error) => {
          Notification.error({
              title: 'Error!',
              message: error.message,
              type: 'error',
              duration: 1500,
              customClass: 'notification-box',
          });
          commit('contactError', error.message)
      })
}