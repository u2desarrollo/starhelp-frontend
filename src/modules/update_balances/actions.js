import $http from "../../axios";
import {Notification} from "element-ui";

export async function updateBalances({state, commit}) {
  commit('setPreloader', true)
  await $http.post("/api/actualizar-saldos", state.params)
    .then(response => {
      Notification.success({
        title: "Éxito!",
        message: 'La actualización de saldos a iniciado satisfactoriamente.',
        type: "success",
        duration: 4000,
        customClass: "notification-box"
      });
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 2000,
        customClass: "notification-box"
      });
    });
  commit('setPreloader', false)
}

export async function addNotification({state, commit, rootState}, notification) {

  let notifications = rootState.Notifications.notifications

  const today = new Date();

  const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

  const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

  let new_notification = {
    id: null,
    title: 'ACTUALIZACIÓN MASIVA DE SALDOS',
    text: [
      {key: 'Fecha', value: date + ' ' + time},
      {key: 'Mensaje', value: notification.message}
    ],
    type: 'update_balances',
    url: null,
    close: true
  }

  notifications.unshift(new_notification)

  commit('Notifications/setNotifications', notifications, {root: true})
}


export async function getSubscriber({commit}) {

  const user = JSON.parse(localStorage.getItem('user'))
  const subscriber_id = user.subscriber_id

  await $http.get("/api/suscriptores/" + subscriber_id)
    .then(response => {

      const subscriber = response.data.data

      commit('setSubscriber', subscriber)

      if (subscriber.document){
        commit('setParamsYearMonth', subscriber.document.document_date.substring(0, 9))
      }
    })
}



