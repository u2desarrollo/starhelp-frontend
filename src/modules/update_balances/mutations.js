export function setPreloader(state, preloader){
    state.preloader = preloader
}

export function setSubscriber(state, subscriber){
  state.subscriber = subscriber
}

export function setParamsYearMonth(state, year_month){
  state.params.year_month = year_month
}

export function setMessagePreloader(state, message_preloader){
  state.message_preloader = message_preloader
}

