export function setFixAssets(state, data) {
  // Agregamos los datos de la tabla
  state.tableFixAssets.items = data.data;

  // Quitamos los valores de la tabla del arreglo
  delete data.data;

  // Agregamos los datos de la paginacion
  state.tableFixAssets.pagination = data;
}

export function setFixAssetAdditions(state, data) {
  // Agregamos los datos de la tabla
  state.tableFixAssetAdditions.items = data.data;

  // // Quitamos los valores de la tabla del arreglo
  // delete data.data;

  // // Agregamos los datos de la paginacion
  // state.tableFixAssetAdditions.pagination = data;
}

export function setFixAssetMonthlyDepreciation(state, data) {
  state.tableFixAssetMonthlyDepreciation.items = data;
}

export function setTableFixAssetDocumentDepreciationLoading(state, value) {
  state.tableFixAssetDocumentDepreciationLoading = value;
}

export function setFixAssetDocumentDepreciation(state, data) {
  state.tableFixAssetDocumentDepreciation.items = data;
}

export function setActiveGroups(state, data) {
  state.activeGroups = data;
}

export function setClasses(state, data) {
  state.classes = data;
}

export function setCostCenters(state, data) {
  state.costCenters = data;
}

export function setCities(state, data) {
  state.cities = data;
}

export function setPolicyProviders(state, data) {
  state.policyProviders = data;
}

export function setMaintenanceProviders(state, data) {
  state.maintenanceProviders = data;
}

export function setProviders(state, data) {
  state.providers = data;
}

export function setContactResponsibles(state, data) {
  state.contactResponsibles = data;
}

export function setContactsSale(state, data) {
  state.contactsSale = data;
}

export function setDerecognizedTypes(state, data) {
  state.derecognizedTypes = data;
}

export function setErrorForm(state, value) {
  state.errorForm = value;
}

export function setAction(state, value) {
  state.action = value;
}

export function setFixAsset(state, data) {
  state.fixAsset = data;
}

export function setFixAssetAddition(state, data) {
  state.fixAssetAddition = data;
}

export function setAttributeFixAsset(state, data) {
  state.fixAsset = {
    ...state.fixAsset,
    ...data
  };
}

export function resetFixAsset(state, data) {
  state.fixAsset = {};
}

export function resetFixAssetAddition(state, data) {
  state.fixAssetAddition = {};
}
