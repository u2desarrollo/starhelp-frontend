import $http from "../../axios";
import { Notification } from "element-ui";

const apiName = "/api/activos-fijos";
const routePathAdditions = "/api/activos-fijos-adiciones";
const routePathMonthlyDepreciation = "/api/activos-fijos-depreciacion-mensual";

export async function getLastConsecutiveSuggested({ commit, state }) {
  await $http
    .get("/api/activos-fijos-siguiente-consecutivo-sugerido")
    .then(response => {
      commit("setAttributeFixAsset", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchFixAssets({ commit, state }) {
  await $http
    .get(apiName, {
      params: state.tableFixAssets.params
    })
    .then(response => {
      commit("setFixAssets", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchFixAssetById({ commit }, id) {
  await $http
    .get(`${apiName}/${id}`)
    .then(response => {
      commit("setFixAsset", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function createFixAsset({ commit, state }) {
  commit("setErrorForm", false);

  await $http
    .post(apiName, state.fixAsset)
    .then(response => {
      commit("setFixAsset", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setErrorForm", true);
    });
}

export async function updateFixAsset({ commit, state }) {
  commit("setErrorForm", false);

  await $http
    .put(`${apiName}/${state.fixAsset.id}`, state.fixAsset)
    .then(response => {
      commit("setFixAsset", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setErrorForm", true);
    });
}

export async function deleteFixAsset({ commit, dispatch, state }, id) {
  await $http
    .delete(`${apiName}/${id}`)
    .then(response => {
      dispatch("fetchFixAssets");
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("setErrorForm", true);
    });
}

export async function fetchCities({ commit, state }) {
  await $http
    .get("/api/ciudades")
    .then(response => {
      commit("setCities", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

function mapParameters(parameters) {
  return parameters.map(p => {
    return { value: p.id, label: p.name_parameter, code: p.code_parameter };
  });
}

export async function fetchActiveGroups({
  dispatch,
  rootState,
  commit,
  state
}) {
  if (state.activeGroups.length === 0) {
    await dispatch(
      "parameters/listParameter",
      { params: { codeParamTable: "C-015", paginate: false } },
      { root: true }
    );
    commit("setActiveGroups", mapParameters(rootState.parameters.parameters));
  }
}

export async function fetchClasses({ dispatch, rootState, commit, state }) {
  if (state.classes.length === 0) {
    await dispatch(
      "parameters/listParameter",
      { params: { codeParamTable: "C-016", paginate: false } },
      { root: true }
    );
    commit("setClasses", mapParameters(rootState.parameters.parameters));
  }
}

export async function fetchCostCenters({ dispatch, rootState, commit, state }) {
  if (state.costCenters.length === 0) {
    await dispatch(
      "parameters/listParameter",
      { params: { codeParamTable: "038", paginate: false } },
      { root: true }
    );
    commit("setCostCenters", mapParameters(rootState.parameters.parameters));
  }
}

export async function fetchDerecognizedTypes({
  dispatch,
  rootState,
  commit,
  state
}) {
  if (state.derecognizedTypes.length === 0) {
    await dispatch(
      "parameters/listParameter",
      { params: { codeParamTable: "C-017", paginate: false } },
      { root: true }
    );
    commit(
      "setDerecognizedTypes",
      mapParameters(rootState.parameters.parameters)
    );
  }
}

export async function fetchProviders({ dispatch, rootState, commit }, filter) {
  if (filter.length >= 3) {
    await dispatch(
      "contacts/listContact",
      {
        params: {
          type: "is_provider",
          filter
        }
      },
      {
        root: true
      }
    );
    commit("setProviders", rootState.contacts.contacts);
  }
}

export async function fetchMaintenanceProviders(
  { dispatch, rootState, commit },
  filter
) {
  if (filter.length >= 3) {
    await dispatch(
      "contacts/listContact",
      {
        params: {
          type: "is_provider",
          filter
        }
      },
      {
        root: true
      }
    );
    commit("setMaintenanceProviders", rootState.contacts.contacts);
  }
}

export async function fetchPolicyProviders(
  { dispatch, rootState, commit },
  filter
) {
  if (filter.length >= 3) {
    await dispatch(
      "contacts/listContact",
      {
        params: {
          type: "is_provider",
          filter
        }
      },
      {
        root: true
      }
    );
    commit("setPolicyProviders", rootState.contacts.contacts);
  }
}

export async function fetchContactResponsibles(
  { dispatch, rootState, commit },
  filter
) {
  if (filter.length >= 3) {
    await dispatch(
      "contacts/listContact",
      {
        params: {
          type: "is_employee",
          filter
        }
      },
      {
        root: true
      }
    );
    commit("setContactResponsibles", rootState.contacts.contacts);
  }
}

export async function fetchContactsSale(
  { dispatch, rootState, commit },
  filter
) {
  if (filter.length >= 3) {
    await dispatch(
      "contacts/listContact",
      {
        params: {
          type: "is_customer",
          filter
        }
      },
      {
        root: true
      }
    );
    commit("setContactsSale", rootState.contacts.contacts);
  }
}

export async function downloadFixAssets({ state }) {
  await $http({
    url: "/api/activos-fijos-descargar",
    method: "GET",
    responseType: "blob"
  })
    .then(response => {
      const url = window.URL.createObjectURL(
        new Blob(["\ufeff", response.data])
      );
      const link = document.createElement("a");
      link.href = url;

      link.setAttribute(
        "download",
        `Activos-Fijos-${new Date().toISOString().slice(0, 10)}.csv`
      );
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al Descargar",
        type: "error",
        duration: 3000,
        customClass: "notification-box"
      });
    });
}

/**
 * Adiciones
 *
 * @author Kevin Galindo
 */
export async function fetchFixAssetAdditions({ commit, state }) {
  await $http
    .get(routePathAdditions, {
      params: state.tableFixAssetAdditions.params
    })
    .then(response => {
      commit("setFixAssetAdditions", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function fetchFixAssetAdditionById({ commit }, id) {
  await $http
    .get(`${routePathAdditions}/${id}`)
    .then(response => {
      commit("setFixAssetAddition", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("modelError", error.message);
    });
}

export async function createFixAssetAddition({ dispatch, state }) {
  const data = {
    ...state.fixAssetAddition,
    fix_asset_id: state.fixAsset.id
  };

  await $http
    .post(routePathAdditions, data)
    .then(response => {
      dispatch("fetchFixAssetAdditions");
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function updateFixAssetAddition({ dispatch, state }) {
  const data = {
    ...state.fixAssetAddition,
    fix_asset_id: state.fixAsset.id
  };

  await $http
    .put(`${routePathAdditions}/${state.fixAssetAddition.id}`, data)
    .then(response => {
      dispatch("fetchFixAssetAdditions");
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function deleteFixAssetAddition({ commit, dispatch }, id) {
  await $http
    .delete(`${routePathAdditions}/${id}`)
    .then(() => {
      dispatch("fetchFixAssetAdditions");
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchFixAssetMonthlyDepreciation({ commit }, id) {
  await $http
    .get(`${routePathMonthlyDepreciation}/${id}`)
    .then(response => {
      commit("setFixAssetMonthlyDepreciation", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.respons ? error.response.data : error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchFixAssetDocumentDepreciation({ commit }) {
  await $http
    .get("/api/lista-comprobante-activos-fijos-depreciacion")
    .then(response => {
      commit("setFixAssetDocumentDepreciation", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.response ? error.response.data : error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });

      // throw Error(error);
    });
}

export async function regenerateFixAssetDocumentDepreciation(
  { commit, dispatch },
  document_id
) {
  commit("setTableFixAssetDocumentDepreciationLoading", true);

  await $http
    .post(
      `/api/regenerar-comprobante-activos-fijos-depreciacion/${document_id}`
    )
    .then(response => {
      Notification.success({
        title: "Exito!",
        message: "El documento se ha regenerado.",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      dispatch("fetchFixAssetDocumentDepreciation");
    })
    .catch(error => {
      console.log(error.response);
      Notification.error({
        title: "Error!",
        message: error.response ? error.response.data.message : error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });

      // throw Error(error);
    });

  commit("setTableFixAssetDocumentDepreciationLoading", false);
}

export async function createFixAssetDocumentDepreciation(
  { commit, dispatch },
  yearMonth
) {
  commit("setTableFixAssetDocumentDepreciationLoading", true);
  await $http
    .post(`/api/comprobante-activos-fijos-depreciacion/${yearMonth}`)
    .then(response => {
      dispatch("fetchFixAssetDocumentDepreciation");
    })
    .catch(error => {
      console.log(error.response);
      Notification.error({
        title: "Error!",
        message: error.response ? error.response.data.message : error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });

      // throw Error(error);
    });
  commit("setTableFixAssetDocumentDepreciationLoading", false);
}

export async function createAndFetchFixAssetMonthlyDepreciation(
  { commit },
  id
) {
  await $http
    .post(`${routePathMonthlyDepreciation}/${id}`)
    .then(response => {
      commit("setFixAssetMonthlyDepreciation", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.response.data,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function generateExcelFixAssetDocumentDepreciation(
  { commit, dispatch },
  document_id
) {
  commit("setTableFixAssetDocumentDepreciationLoading", true);

  await $http
    .get(`/api/excel-comprobante-activos-fijos/${document_id}`)
    .then(response => {
      const { data, header, nameFile } = response.data;

      const array = typeof data !== "object" ? JSON.parse(data) : data;

      let str =
        `${Object.values(header)
          .map(value => `"${value}"`)
          .join(",")}` + "\r\n";

      let dataCsv = array.reduce((str, next) => {
        str +=
          `${Object.values(next)
            .map(value => `"${value}"`)
            .join(",")}` + "\r\n";
        return str;
      }, str);

      var hiddenElement = document.createElement("a");
      hiddenElement.href = "data:text/csv;charset=utf-8," + encodeURI(dataCsv);
      hiddenElement.target = "_blank";
      hiddenElement.download = `${nameFile}.csv`;
      hiddenElement.click();
    })
    .catch(error => {
      console.log(error.response);
      Notification.error({
        title: "Error!",
        message: error.response ? error.response.data.message : error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });

      // throw Error(error);
    });

  commit("setTableFixAssetDocumentDepreciationLoading", false);
}
