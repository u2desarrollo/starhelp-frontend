export const defaultTableFixAssets = () => {
  return {
    items: [],
    fields: [
      {
        name: "Codigo",
        field: "consecut",
        format: "text",
        style: { width: 100 }
        // width: "10px"
      },
      {
        field: "description",
        name: "Descripcion",
        format: "text",
        style: { width: 350, short: true }
        // width: 250
      },
      {
        field: "plate_number",
        name: "Num Placa"
        // width: 150
      },
      {
        field: "caus_status",
        name: "Estado Depreciación",
        format: "text",
        style: { width: 50 }
        // width: 150
      },
      {
        field: "active_group",
        name: "Grupo activos"
        // width: 150
      },
      {
        field: "actions",
        name: "Opciones",
        format: "text",
        style: { width: 300, short: true }
        // width: 150
      }
    ],
    pagination: "",
    params: {
      perPage: 100,
      filterText: "",
      currentPage: 1,
      page: 1,
      pagination: true
    }
  };
};

export const defaultTableFixAssetAdditions = () => {
  return {
    items: [],
    fields: [
      {
        field: "plate_number",
        name: "Num Placa"
      },
      {
        field: "description",
        name: "Descripcion"
      },
      {
        field: "value",
        name: "Valor"
      },
      {
        field: "date_addition",
        name: "Fecha"
      },
      {
        field: "actions",
        name: "Opciones"
      }
    ],
    pagination: "",
    params: {
      perPage: 100,
      filterText: "",
      currentPage: 1,
      page: 1,
      pagination: true
    }
  };
};

export const defaultTableFixAssetMonthlyDepreciation = () => {
  return {
    items: [],
    fields: [
      { field: "year_month", name: "Año - Mes" },
      { field: "value_additions", name: "Vr Adiciones", format: "currency" },
      { field: "historical_cost", name: "Costo Historico", format: "currency" },
      {
        field: "balance_to_depreciated",
        name: "Saldo por Depreciar",
        format: "currency"
      },
      {
        field: "depreciate_value_month",
        name: "Vr Deprec Mes",
        format: "currency"
      },
      {
        field: "accumulated_depreciation_value",
        name: "Vr Deprec Acum",
        format: "currency"
      },
      { field: "months_accumulated", name: "Meses Acum" }
    ],
    pagination: "",
    params: {
      perPage: 100,
      filterText: "",
      currentPage: 1,
      page: 1,
      pagination: true
    }
  };
};

export const defaultTableFixAssetDocumentDepreciation = () => {
  return {
    items: [],
    fields: [
      { field: "document_date", name: "Año - Mes" },
      { field: "consecutive", name: "Num-Comp" },
      { field: "tot_act", name: "Tot Act" },
      { field: "tot_vrs", name: "Tot-Vrs" },
      {
        field: "actions",
        name: "Opciones"
      }
    ],
    pagination: "",
    params: {
      perPage: 100,
      filterText: "",
      currentPage: 1,
      page: 1,
      pagination: true
    }
  };
};

export default {
  tableFixAssets: defaultTableFixAssets(),
  tableFixAssetAdditions: defaultTableFixAssetAdditions(),
  tableFixAssetMonthlyDepreciation: defaultTableFixAssetMonthlyDepreciation(),
  tableFixAssetDocumentDepreciation: defaultTableFixAssetDocumentDepreciation(),
  tableFixAssetDocumentDepreciationLoading: false,
  fixAsset: {},
  fixAssetAddition: {},
  action: "create",
  errorForm: false,
  activeGroups: [],
  classes: [],
  costCenters: [],
  cities: [],
  providers: [],
  contactsSale: [],
  derecognizedTypes: [],
  maintenanceProviders: [],
  policyProviders: [],
  contactResponsibles: []
};
