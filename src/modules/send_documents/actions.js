import $http from "../../axios";
import {Notification} from 'element-ui';
import * as MessageBox from "element-ui";

//Obtener los tipos de comprobantes
export async function fetchDocuments({commit}) {
  commit('setMessageLoading', 'Obteniendo documentos')
  commit('setLoading', true)
  await $http.get('/api/documentos-finalizados-pendientes-por-enviar')
    .then((response) => {
      commit('setDocuments', response.data)
      commit('setLoading', false)
    })
    .catch((error) => {
      Notification.error({
        title: 'Error!',
        message: 'Error al obtener los documentos',
        type: 'error',
        duration: 5000,
        customClass: 'notification-box',
      });
      commit('setLoading', false)
    })
}

//Crear tipo de comprobante
export async function sendDocument({commit, dispatch}, document_id) {
  commit('setMessageLoading', 'Enviando documento')
  commit('setLoading', true)
  await $http.post('/api/enviar-documento-finalizado/' + document_id)
    .then((response) => {
      commit('setDocument', response.data)
      commit('setLoading', false)
      MessageBox.MessageBox.confirm('Documento emitido exitosamente', 'Exito', {
        confirmButtonText: "Aceptar",
        dangerouslyUseHTMLString: true,
        showCancelButton: false,
        type: "success",
        customClass: "notification-success"
      }).then(() =>{
        commit('setShowPdf', true)
        dispatch('fetchDocuments')
      });
    })
    .catch((error) => {
      commit('setLoading', false)
      MessageBox.MessageBox.confirm(error.response.data.message, 'Error', {
        confirmButtonText: "Aceptar",
        dangerouslyUseHTMLString: true,
        showCancelButton: false,
        type: "error",
        customClass: "notification-error"
      });
    })
}
