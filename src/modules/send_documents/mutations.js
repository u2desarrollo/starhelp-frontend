export function setDocuments(state, documents){
    state.documents = documents
}

export function setLoading(state, loading){
    state.loading = loading
}

export function setMessageLoading(state, messageLoading){
    state.messageLoading = messageLoading
}

export function setShowPdf(state, showPdf){
  state.showPdf = showPdf
}

export function setDocument(state, document){
  state.document = document
}
