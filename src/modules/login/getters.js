export function selectedCompany(state) {
  return JSON.parse(localStorage.getItem("selected_company"));
}
export function selectedArea(state) {
  return JSON.parse(localStorage.getItem("selected_area"));
}
export function user(state) {
  return JSON.parse(localStorage.getItem("user"));
}
export function isUserU2(state) {
  const user = JSON.parse(localStorage.getItem("user"));

  return !!user.get_roles.find(r => r.role === "u2");
}
