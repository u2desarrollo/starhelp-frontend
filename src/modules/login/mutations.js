import { defaultUserLogued } from "./state";

export function login(state, auth) {
  state.user_logued = auth.user;
  state.status = true;
}

export function loginFail(state, error) {
  state.user = false;
}

export function logout(state) {
  Object.assign(state.user_logued, defaultUserLogued());
  (state.authentication = { email: "", password: "" }), (state.status = false);
}

export function setUserLogued(state, user_logued) {
  state.user_logued = user_logued;
}

export function setUserLoguedB2b(state, user_b2b) {
  state.user_b2b = user_b2b;
}

export function setForgotPassword(state, forgot) {
  forgot.password = "";
  forgot.password_confirmation = "";
  state.forgot = forgot;
}

export function setLoguedType(state, logued_type) {
  state.logued_type = logued_type;
}

export function setLowerCaseAuthEmail(state) {
  state.auth.email = state.auth.email.toLowerCase();
}

export function setBranchOffices(state, branchoffices) {
  localStorage.setItem("branchoffices", JSON.stringify(branchoffices));
  state.branchoffices = branchoffices;
}

export function setPermissions(state, permissions) {
  state.permissions = permissions;
}

export function setBranchOfficesWarehouses(state, branchoffices_warehouses) {
  // Guardamos todos los "branchoffices_warehouses"
  if (localStorage.branchoffices_warehouses_all === undefined) {
    localStorage.branchoffices_warehouses_all = JSON.stringify(
      branchoffices_warehouses
    );
  }

  // Codificamos el JSON
  let branchoffices_warehouses_all = JSON.parse(
    localStorage.branchoffices_warehouses_all
  );

  // Selecionamos el "id" del "selected_branchoffice"
  let selected_branchoffice = state.selected_branchoffice;

  // Buscamos los "branchoffices_warehouses" segun el id del "selected_branchoffice"
  let branchoffices_warehouses_temp = branchoffices_warehouses_all.filter(
    bw => {
      return bw.branchoffice_id == selected_branchoffice;
    }
  );

  // Seteamos la variable del LocalStorage
  localStorage.setItem(
    "branchoffices_warehouses",
    JSON.stringify(branchoffices_warehouses_temp)
  );

  state.branchoffices_warehouses = branchoffices_warehouses_temp;
}

export function setSelectedBranchOffice(state, selected_branchoffice) {
  let found_branchoffice = state.branchoffices.find(
    b => b.id == selected_branchoffice
  );

  if (found_branchoffice === undefined) {
    found_branchoffice = state.branchoffices.find(b => b.main);
  }

  let branchoffice_id = found_branchoffice.id;

  state.selected_branchoffice = branchoffice_id;
  localStorage.setItem("selected_branchoffice", branchoffice_id);
}

export function setSelectedBranchOfficeWarehouse(
  state,
  selected_branchoffice_warehouse
) {
  // Traemos todos los "branchoffices_warehouses_all"
  let branchoffices_warehouses_all = JSON.parse(
    localStorage.branchoffices_warehouses_all
  );

  // Buscamos "branchoffice_warehouse" segun el id de "selected_branchoffice_warehouse"
  let found_branchoffice_warehouse = branchoffices_warehouses_all.find(
    bw => bw.id == selected_branchoffice_warehouse
  );

  // Si no se encuentra datos se busca la sede principal
  if (!found_branchoffice_warehouse) {
    found_branchoffice_warehouse = branchoffices_warehouses_all.find(
      bw =>
        bw.branchoffice_id == state.selected_branchoffice &&
        bw.warehouse_code == "01"
    );
  }

  // Si no encuentra ninguno se busca el primero de la lista
  if (!found_branchoffice_warehouse) {
    found_branchoffice_warehouse = branchoffices_warehouses_all[0];
  }

  // Seteamos los valores
  state.selected_branchoffice_warehouse = found_branchoffice_warehouse.id;
  localStorage.setItem(
    "selected_branchoffice_warehouse",
    found_branchoffice_warehouse.id
  );
}
export function resetSelectedBranchofficeWarehouse(state, value) {
  state.selected_branchoffice_warehouse = "";
  localStorage.setItem("selected_branchoffice_warehouse", "");
}

export function setCurrentBranchoffice(state) {
  state.current_branchoffice.selected_branchoffice = parseInt(
    localStorage.selected_branchoffice
  );

  state.current_branchoffice.selected_branchoffice_warehouse = parseInt(
    localStorage.selected_branchoffice_warehouse
  );
}

export function setSubscriber(state, subscriber) {
  state.subscriber = subscriber
}
