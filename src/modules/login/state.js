export const defaultUserLogued = () => {
  return {
    id: "",
    name: "",
    email: "",
    token: ""
  };
};

export const defaultUserB2b = () => {
  return {
    id: "",
    description: ""
  };
};

export default {
  auth: { email: "", password: "" },
  user_logued: defaultUserLogued(),
  user_b2b: defaultUserB2b(),
  status: false,
  logued_type: "",
  selected_branchoffice: "",
  selected_branchoffice_warehouse: "",
  branchoffices: [],
  branchoffices_warehouses: [],
  permissions: [],
  subscriber: {},
  forgot: {
    email: "",
    password: "",
    password_confirmation: "",
    token: ""
  },
  change: {
    email: "",
    password: "",
    password_confirmation: ""
  },
  current_branchoffice: {
    selected_branchoffice: "",
    selected_branchoffice_warehouse: ""
  }
};
