import $http from "../../axios";
import { Notification } from "element-ui";

export async function authentication({ commit, state }) {
  commit("setLowerCaseAuthEmail");

  return new Promise(async (resolve, reject) => {
    await $http
      .post("/api/login", state.auth)
      .then(response => {
        if (response.data.data.user) {
          commit("setLoguedType", response.data.data.user.type);
          commit("login", response.data.data);
          localStorage.setItem("user", JSON.stringify(response.data.data.user));
          localStorage.setItem(
            "subscriber",
            JSON.stringify(response.data.data.subscriber)
          );
          localStorage.setItem(
            "permissions",
            JSON.stringify(response.data.data.role_and_permisions)
          );
          commit("setSubscriber", response.data.data.subscriber);
          commit("setBranchOffices", response.data.data.branch_offices);
          commit(
            "setSelectedBranchOffice",
            response.data.data.user.branchoffice_id
          );
          commit(
            "setBranchOfficesWarehouses",
            response.data.data.branch_offices_warehouses
          );
          commit(
            "setSelectedBranchOfficeWarehouse",
            response.data.data.user.branchoffice_warehouse_id
          );
          commit("setPermissions", response.data.data.role_and_permisions);

          // compañias y areas
          localStorage.setItem(
            "selected_company",
            JSON.stringify(response.data.data.user.company)
          );

          localStorage.setItem(
            "selected_area",
            JSON.stringify(response.data.data.user.area)
          );

          resolve(response);
        }
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message:
            "La dirección de correo electrónico o contraseña proporcionada es incorrecta",
          type: "error",
          duration: 5000,
          customClass: "notification-box"
        });
        reject(error);
      });
  });
}

export async function forgotPassword({ state }) {
  return new Promise(async (resolve, reject) => {
    await $http
      .post("/api/contrasena/olvido", state.forgot)
      .then(response => {
        Notification.success({
          title: "Bien hecho",
          message:
            "¡Hemos enviado por correo electrónico el enlace para restablecer su contraseña!",
          type: "success",
          duration: 5000,
          customClass: "notification-box"
        });
        resolve(response);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message:
            "No podemos encontrar un usuario con esa dirección de correo electrónico.",
          type: "error",
          duration: 5000,
          customClass: "notification-box"
        });
        reject(error);
      });
  });
}

export async function getTokenResetPassword({ commit }, token) {
  return new Promise(async (resolve, reject) => {
    await $http
      .get("/api/contrasena/buscar/" + token)
      .then(response => {
        commit("setForgotPassword", response.data);
        resolve(response);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message: "Este token de restablecimiento de contraseña no es válido.",
          type: "error",
          duration: 5000,
          customClass: "notification-box"
        });
        reject(error);
      });
  });
}

export async function resetPassword({ commit, state }) {
  return new Promise(async (resolve, reject) => {
    await $http
      .post("/api/contrasena/restablecer", state.forgot)
      .then(response => {
        commit("setForgotPassword", {});
        Notification.success({
          title: "Bien hecho",
          message: "¡Contraseña restablecida exitosamente!",
          type: "success",
          duration: 5000,
          customClass: "notification-box"
        });
        resolve(response);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message:
            "Ocurrio un error durante el restablecimiento de la contraseña. Por favor intente nuevamente.",
          type: "error",
          duration: 5000,
          customClass: "notification-box"
        });
        reject(error);
      });
  });
}

export async function changePassword({ commit, state }) {
  return new Promise(async (resolve, reject) => {
    await $http
      .post("/api/cambiar/contrasena", state.change)
      .then(response => {
        let user = JSON.parse(localStorage.getItem("user"));
        user.password_changed = true;
        localStorage.setItem("user", JSON.stringify(user));

        Notification.success({
          title: "Bien hecho",
          message: "¡Contraseña actualizada exitosamente!",
          type: "success",
          duration: 5000,
          customClass: "notification-box"
        });
        resolve(response);
      })
      .catch(error => {
        Notification.error({
          title: "Error!",
          message:
            "Ocurrio un error durante la actualización de la contraseña. Por favor intente nuevamente.",
          type: "error",
          duration: 5000,
          customClass: "notification-box"
        });
        reject(error);
      });
  });
}

export async function fetchUserB2b({ dispatch, state }) {
  let user = JSON.parse(localStorage.getItem("user"));
  let params = {
    params: {
      ide: user.contact_warehouse_id
    }
  };

  return new Promise(async (resolve, reject) => {
    await $http
      .get("/api/establecimientos-vendedor", params)
      .then(async response => {
        dispatch("loginB2b", response.data.data);
        resolve(response);
      })
      .catch(error => {
        Notification.error({
          title: "¡Error!",
          message: error.message,
          type: "error",
          duration: 1500,
          customClass: "notification-box"
        });
        reject(error);
      });
  });
}

export async function loginB2b({ commit, state }, customer) {
  localStorage.setItem("user_b2b", JSON.stringify(customer));
  commit("setUserLoguedB2b", customer);
}

export async function logout({ commit }) {
  // Corregir el logout

  let user = JSON.parse(localStorage.getItem("user"));
  commit("logout");
  localStorage.clear();
}
