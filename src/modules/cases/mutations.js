export function setDataTable(state, data) {
  state.caseTable.data = data.data;

  state.caseTable.pagination = {
    ...data,
    data: undefined
  };
}

export function setListParameter(state, { key, data }) {
  state[key] = data;
}

export function setCase(state, data) {
  state.caseData = data;
}

export function setFormObj(state, data) {
  state.formObj = data;
}
