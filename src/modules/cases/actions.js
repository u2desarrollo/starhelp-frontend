import $http from "../../axios";
import { Notification } from "element-ui";

const apiName = "/api/casos";
const apiNameInteract = "/api/casos/interactuar";

function mapParameters(parameters) {
  return parameters.map(p => {
    return { value: p.id, label: p.name_parameter, code: p.code_parameter };
  });
}

export async function fetchCases({ commit, state }) {
  let params = {
    params: {
      ...state.caseTable.params
    }
  };

  const user = JSON.parse(localStorage.getItem("user"));

  if (!!user.get_roles.find(r => r.role === "u2")) {
    if (state.caseTable.params.company_id) {
      params.params.area_id = null;
    }
  } else {
    params.params.area_id = JSON.parse(
      localStorage.getItem("selected_area")
    ).id;
    params.params.company_id = JSON.parse(
      localStorage.getItem("selected_company")
    ).id;
  }

  await $http
    .get(apiName, params)
    .then(response => {
      commit("setDataTable", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al traer los casos",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function caseFindById({ commit }, id) {
  await $http
    .get(`${apiName}/${id}`)
    .then(response => {
      commit("setCase", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al traer el caso",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchCompanies({ commit, state }) {
  await $http
    .get("/api/companies")
    .then(response => {
      commit("setListParameter", {
        key: "companies",
        data: response.data
      });
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al traer los casos",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function createInteract({ commit, state }, data) {
  await $http
    .post(apiNameInteract, data)
    .then(response => {
      console.log(response);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al crear el caso",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function updateInteract({ commit, state }, data) {
  await $http
    .put(`${apiNameInteract}/${data.id}`, data)
    .then(response => {
      console.log(response);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al crear el caso",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function createCase({ commit, state }) {
  await $http
    .post(apiName, state.formObj)
    .then(response => {
      console.log(response);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: "Error al crear el caso",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchCasePriorities({
  dispatch,
  rootState,
  commit,
  state
}) {
  if (state.casePriorities.length === 0) {
    await dispatch(
      "parameters/listParameter",
      { params: { codeParamTable: "501", paginate: false } },
      { root: true }
    );
    commit("setListParameter", {
      key: "casePriorities",
      data: mapParameters(rootState.parameters.parameters)
    });
  }
}

export async function fetchCaseStates({ dispatch, rootState, commit, state }) {
  if (state.casePriorities.length === 0) {
    await dispatch(
      "parameters/listParameter",
      { params: { codeParamTable: "500", paginate: false } },
      { root: true }
    );
    commit("setListParameter", {
      key: "caseStates",
      data: mapParameters(rootState.parameters.parameters)
    });
  }
}
