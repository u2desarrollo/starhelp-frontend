export const statesCaseTable = () => ({
  data: [],
  columns: [
    {
      name: "Caso",
      field: "title",
      style: { width: 100 }
    },
    {
      name: "Fecha/Hora",
      field: "created_at",
      style: { width: 100 }
    },
    {
      name: "Prioridad",
      field: "priority",
      style: { width: 100 }
    },
    {
      name: "Estado",
      field: "state",
      style: { width: 100 }
    },
    {
      name: "Usuario",
      field: "user",
      style: { width: 100 }
    },
    {
      name: "Cliente",
      field: "company",
      style: { width: 100 },
      private: true
    },
    {
      name: "Usr Respons",
      field: "responsibles",
      style: { width: 100 },
      private: true
    },
    {
      name: "Opciones",
      field: "actions",
      style: { width: 200 }
    }
  ],
  pagination: null,
  loadingTable: false,
  params: {
    paginate: true,
    page: 1,
    perPage: 100,
    filter: "",
    company_id: "",
    responsiblesIds: ""
  }
});

export default {
  caseTable: statesCaseTable(),
  formObj: {},
  test: "active",
  casePriorities: [],
  caseStates: [],
  companies: [],
  caseData: null
};
