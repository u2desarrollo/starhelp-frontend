import $http from "../../axios"
import {Notification, MessageBox} from 'element-ui'

/**
 *
 * @returns {Promise<void>}
 */
export async function reportGenerate({state, commit}) {
  commit('setPreloader', true)
  await $http({
    url: "/api/reportes/sugerido-de-reposicion",
    method: "GET",
    responseType: "blob",
    params: state.params
  })
    .then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "Sugerido reposición.xlsx");
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 3000,
        customClass: "notification-box"
      });
    });
  commit('setPreloader', false)
}
