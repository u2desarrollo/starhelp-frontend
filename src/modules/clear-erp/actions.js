import $http from "../../axios";
import {
  Notification,
  MessageBox
} from 'element-ui';

export async function fetchTypesOperations({commit, state}){
  await $http.get('/api/types_operations?paginate=false')
      .then((response) => {
          commit('settypes_operations', response.data.data)

      })
      .catch((error) => {
          Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
      });
  })
}

export async function fetchTdateSubs({commit, state}){
    var user = JSON.parse(localStorage.user);
    let id = user.subscriber_id
  await $http.get(`/api/suscriptores/${id}`)
      .then((response) => {
          
          commit('setDateSusbs', response.data.data)

      })
      .catch((error) => {
          Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
      });
  })
}

export async function clearErp({commit , state}){
  await $http.put('/api/boorrar-erp', state.params)
  .then((response)=>{
    // console.log(response.data.data.data);
    commit('setloading_', false)
    Notification.success({
      title: '!Hecho!',
      message: response.data.data.message,
      type: 'error',
      duration: 1500,
      customClass: 'notification-box',
    })
  })
  .catch((error) => {
    Notification.error({
      title: 'Error!',
      message: error.message,
      type: 'error',
      duration: 1500,
      customClass: 'notification-box',
    })
  })
}