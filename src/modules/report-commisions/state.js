export default {
    loading: {
        module: true
    },
    params: {
        year_month_from: '',
        year_month_up: '',
        type_query: "1",
    },
    params_cotact:{
        is_customer: false,
        is_provider: false,
        is_employee: false,
        filter_customer: [],
        filter_provider: [],
        filter_employee: [],
        paginate: true,
        page: 1,
        perPage: 50,
        query: '',
        sortBy: 'id',
        sort: 'ASC',
    },
    accounts_from: [],
    contacts:[],
    reporte_comisiones:[],
    ag_grid:{
        selectedTheme:'ag-theme-balham'
    }
}