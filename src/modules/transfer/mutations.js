export function setBranchOffices (state, branchOffices) {
  state.branchOffices = branchOffices;
}

export function setBranchOffices_entry (state, branchOffices) {
  state.branchOffices_entry = branchOffices;
}

export function setBranchofficeWarehouses (state, branchofficeWarehouses) {
  // newbranchofficeWarehouses = []
  state.branchoffice_warehouses = []
  branchofficeWarehouses.map((branchoffice, index)=>{
    if(branchoffice.warehouse_code != '17' /*&& branchoffice.warehouse_code != '02' && branchoffice.warehouse_code != '11'*/){
      state.branchoffice_warehouses.push(branchoffice);
    }
  })
}

export function setBranchofficeWarehousesEntry (state, branchofficeWarehouses) {
  // state.branchoffice_warehouses_entry = branchofficeWarehouses;
  state.branchoffice_warehouses_entry = []
  branchofficeWarehouses.map((branchoffice, index)=>{
    if(branchoffice.warehouse_code != '17' /*&& branchoffice.warehouse_code != '02' && branchoffice.warehouse_code != '11'*/){
      state.branchoffice_warehouses_entry.push(branchoffice);
    }
  })
}
export function setContacts(state, contacts) {
  state.contacts = contacts;
}
export function setContactsWarehouses(state, contacts_warehouses) {
  state.contacts_warehouses = contacts_warehouses;
}
export function setSedeSalida (state, sede_salida) {
  state.params.sede_salida = sede_salida;
}
export function setBodegaSalida (state, bodega_salida) {
  state.params.bodega_salida = bodega_salida;
}
export function setDocument (state, document) {
  state.document = document;
}
export function setTransferDocument (state, transferDocument) {
  state.transferDocument = transferDocument;
}

export function setShowPdf (state, data) {
  state.showPdf = data;
}

export function clearParams (state, data) {
  state.product.products_transfer = [];
  state.params.sede_entrada = "";
  state.params.bodega_entrada = "";
  state.params.observation = "";
  state.params.additional_contact_id = null;
  state.params.additional_contact_warehouse_id = null;
}
export function setConsecutive (state, consecutive) {
  state.consecutive = consecutive;
}

export function setStockProducts (state, products) {
  products.map((dp, index) => {
    let key_p = state.product.products_transfer.findIndex(ptr => ptr.product.id == dp.product_id)

    if (key_p >= 0) {
      state.product.products_transfer[key_p].product.quantity = dp.stock
      state.product.products_transfer[key_p].minimum_amount = dp.stock
    }

  })
}

export function setTransfer(state, transfer){
  state.transfer = transfer;
}

export function setParamsTransfer(state, transfer){
  if(transfer.pending_authorization){

    state.product.products_transfer = []
    state.params.sede_salida    =   transfer.document.branchoffice_id
    state.params.bodega_salida  =   transfer.document.branchoffice_warehouse_id
    state.params.sede_entrada   =   transfer.branchoffice_id
    state.params.bodega_entrada =   transfer.branchoffice_warehouse_id
    state.params.observation    =   transfer.observation
    state.params.document_id    =   transfer.id

    transfer.documents_products.forEach(element => {
      var pp = {
        product: {
          id: null,
          description: '',
          code: '',
          quantity: '',
          found: '',
        }
      }
      pp.minimum_amount                   = element.quantity
      pp.product.id                       = element.product_id
      pp.product.code                     = element.product.code
      pp.product.line                     = element.product.line_id
      pp.product.subline                  = element.product.subline_id
      pp.product.price                    = element.product.price.price
      pp.product.brand                    = element.product.brand_id
      pp.product.description              = element.product.description
      pp.product.quantity                 = element.quantity
      pp.product.documents_product_id     = element.id

      state.product.products_transfer.push(pp)
    });
  }else{
    state.product.products_transfer = []
    state.params.bodega_salida  =   null
    state.params.bodega_entrada =   null
    state.params.observation    =   null
    state.params.document_id    =   null
  }
}

export function setParamsTransferupdate(state, transfer){

    state.product.products_transfer = []
    state.params.sede_salida    =   transfer.document.branchoffice_id
    state.params.bodega_salida  =   transfer.document.branchoffice_warehouse_id
    state.params.sede_entrada   =   transfer.branchoffice_id
    state.params.bodega_entrada =   transfer.branchoffice_warehouse_id
    state.params.observation    =   transfer.observation
    state.params.document_id    =   transfer.id

    transfer.documents_products.forEach(element => {
      var pp = {
        product: {
          id: null,
          description: '',
          code: '',
          quantity: '',
          found: '',
        }
      }
      pp.minimum_amount                   = element.quantity
      pp.product.id                       = element.product_id
      pp.product.code                     = element.product.code
      pp.product.line                     = element.product.line_id
      pp.product.subline                  = element.product.subline_id
      pp.product.price                    = element.product.price.price
      pp.product.brand                    = element.product.brand_id
      pp.product.description              = element.product.description
      pp.product.quantity                 = element.quantity
      pp.product.documents_product_id     = element.id

      state.product.products_transfer.push(pp)
    });
}

export function setSelectedModel(state, selected_model) {
  state.selected_model = selected_model;
}

export function setList_code_products(state, list_code_products){
  state.list_code_products = list_code_products
}

export function setProducts_csv_(state, products_csv_){
  state.products_csv_ = products_csv_
}

export function setProductsGridFromCsv(state, products){
    state.product.products_transfer = []
    products.forEach(element => {
      if (element.manage_inventory) {

        var pp = {
          product: {
            id: null,
            description: '',
            code: '',
            quantity: '',
            found: '',
          }
        }

        var quantity = state.products_csv_.find(element2 => element2.code == element.code)

        pp.minimum_amount                   = quantity.quantity
        pp.product.id                       = element.id
        pp.product.code                     = element.code
        pp.product.line                     = element.line_id
        pp.product.subline                  = element.subline_id
        pp.product.price                    = 0
        pp.product.brand                    = element.brand_id
        pp.product.description              = element.description
        pp.product.quantity                 = element.inventory[0] ? element.inventory[0].stock : 0

        state.product.products_transfer.push(pp)
      }else{
        state.error_from_setProductsGridFromCsv = true
        state.product.products_transfer = []
        return
      }

  })
}

export function setLoading_(state, loading_){
  state.loading_ = loading_
}

export function seterror_from_setProductsGridFromCsv(state, error_from_setProductsGridFromCsv){
  state.error_from_setProductsGridFromCsv = false
}
