export const defaultproducts = () => {
  return {
    products_transfer: []
  };
};
export default {
  error_from_setProductsGridFromCsv: false, 
  loading_ : false,
  products_csv_:[],
  selected_model:null,
  transfer:null,
  branchOffices: [],
  branchOffices_entry: [],
  branchoffice_warehouses: [],
  contacts:[],
  contacts_warehouses:[],
  paramsquery:{
    is_customer: false,
    is_provider: false,
    is_employee: false,
    filter_customer: [],
    filter_provider: [],
    filter_employee: [],
    paginate: true,
    page: 1,
    perPage: 50,
    query: '',
    sortBy: 'id',
    sort: 'ASC',
  },

  branchoffice_warehouses_entry: [],
  product: defaultproducts(),

  transferDocument: [],
  list_code_products :[],
  params: {
    additional_contact_id:null,
    additional_contact_warehouse_id:null,
    pdf: true,
    sede_salida: null,
    bodega_salida: null,
    documentEntry: null,
    sede_entrada: null,
    bodega_entrada: null,
    contact_id: null,
    observation: null
  },
  showPdf: false,
  consecutive:null,
  document: {
    file_name:''
  }
};
