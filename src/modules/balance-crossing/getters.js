export function totalBalanceCrossing(state) {
  let total = 0;
  state.gridBalancesContact.items.forEach(
    i => (total += parseInt(i.cross_value))
  );
  return total;
}
