export const defaultFiltersCrossBalances = () => {
  return {
    contact_id: "",
    contact_warehouse_id: "",
    account_id: "",
    account: "",
    model_id: "",
    voucherType_id: "",
    branchoffice_id: "",
    monthDate: ""
  };
};
export const defaultDocumentCrossBalance = () => {
  return {
    document_transactions: []
  };
};

export default {
  action: "create",
  gridBalancesFavor: {
    columns: [
      {
        name: "Fecha Vencimiento",
        field: "document.due_date",
        format: "text"
      },
      {
        name: "Fecha Doc",
        field: "document.document_date",
        format: "text"
      },
      {
        name: "Sede",
        field: "document.branchoffice",
        format: "text"
      },
      {
        name: "Sucursal",
        field: "document.branchoffice_warehouse.warehouse_description",
        format: "text"
      },
      {
        name: "Tip Comp",
        field: "document.vouchertype",
        format: "text"
      },
      {
        name: "Num Com",
        field: "num_com",
        format: "text"
      },
      {
        name: "Num Doc",
        field: "num_doc",
        format: "text"
      },
      {
        name: "Saldo",
        field: "final_balance",
        format: "currency"
      },
      {
        name: "Opciones",
        field: "options",
        format: "text"
      }
    ],
    items: []
  },
  gridBalancesContact: {
    columns: [
      {
        name: "Fecha Vencimiento",
        field: "document.due_date",
        format: "text"
      },
      {
        name: "Fecha Doc",
        field: "document.document_date",
        format: "text"
      },
      {
        name: "Sede",
        field: "document.branchoffice",
        format: "text"
      },
      {
        name: "Sucursal",
        field: "document.branchoffice_warehouse.warehouse_description",
        format: "text"
      },
      {
        name: "Tip Comp",
        field: "document.vouchertype",
        format: "text"
      },
      {
        name: "Num Com",
        field: "num_com",
        format: "text"
      },
      {
        name: "Num Doc",
        field: "num_doc",
        format: "text"
      },
      {
        name: "Saldo",
        field: "final_balance",
        format: "currency"
      },
      {
        name: "Selecc",
        field: "selecc",
        format: "text"
      },
      {
        name: "Valor Cruce",
        field: "cross_value",
        format: "text",
        editable: true,
        format: "currency"
      }
    ],
    items: []
  },
  gridCrossBalances: {
    columns: [
      {
        name: "Cuenta",
        field: "account.code",
        format: "text"
      },
      {
        name: "Detalle",
        field: "detail",
        format: "text"
      },
      {
        name: "Num Com",
        field: "num_com",
        format: "text"
      },
      {
        name: "Num Doc",
        field: "num_doc",
        format: "text"
      },
      {
        name: "Vr Dedito",
        field: "operation_value_d",
        format: "text"
      },
      {
        name: "Vr Credito",
        field: "operation_value_c",
        format: "text"
      },
      {
        name: "Opciones",
        field: "options",
        format: "text"
      }
    ],
    items: []
  },
  filtersCrossBalances: defaultFiltersCrossBalances(),
  accountDefault: "",
  selectedBalancesFavor: null,
  selectedBalancesContact: null,
  showModalBalanceContact: false,
  //
  modalBalanceContact: {
    crossPendingValue: 0,
    crossValue: 0,
    account_id: "",
    account: ""
  },
  warehouses: [],
  documentCrossBalance: defaultDocumentCrossBalance(),
  documentInitiatedByAnotherUser: "",
  finalizeDocument: ""
};
