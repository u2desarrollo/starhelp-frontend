import {
  defaultDocumentCrossBalance,
  defaultFiltersCrossBalances
} from "./state";

export function setInfoDataEdit(state) {}

export function resetFiltersCrossBalances(state) {
  state.filtersCrossBalances = defaultFiltersCrossBalances();
}

export function setAccountDefault(state, data) {
  state.accountDefault = data;

  state.modalBalanceContact.account = data;
  state.modalBalanceContact.account_id = data.id;
}

export function setFavorBalanceContact(state, data) {
  state.gridBalancesFavor.items = data;
}

export function setPendingBalanceContact(state, data) {
  state.gridBalancesContact.items = data.map(d => {
    return {
      ...d,
      checked: false,
      cross_value: 0
    };
  });
}

export function setSelectedBalancesFavor(state, data) {
  state.selectedBalancesFavor = data;
}

export function setSelectedBalancesContact(state, data) {
  state.selectedBalancesContact = data;
}

export function triggerShowModalBalanceContact(state) {
  state.showModalBalanceContact = !state.showModalBalanceContact;
}
export function setShowModalBalanceContact(state, value) {
  state.showModalBalanceContact = value;
}

export function setCrossPendingValue(state, value) {
  state.modalBalanceContact.crossPendingValue = value;
}

export function setCrossValue(state, value) {
  state.modalBalanceContact.crossValue = value;
}

export function calculateCrossingValueItem(state, item) {
  // true
  if (!item.checked) {
    if (state.selectedBalancesFavor && item) {
      const final_balance_favor = state.modalBalanceContact.crossPendingValue;
      const final_balance_contact = item.final_balance;

      const total_balance =
        final_balance_favor > final_balance_contact
          ? final_balance_contact
          : final_balance_favor;

      item.cross_value = total_balance;
      state.modalBalanceContact.crossPendingValue -= total_balance;
    }
  }
  // false
  else {
    state.modalBalanceContact.crossPendingValue += item.cross_value;
    item.cross_value = 0;
  }
}

export function calculateCrossingValueAvaiable(state, item) {
  // true
  if (item.checked) {
    if (state.selectedBalancesFavor && item) {
      const final_balance_favor = state.modalBalanceContact.crossPendingValue;
      const final_balance_contact = item.final_balance;

      const total_balance =
        final_balance_favor > final_balance_contact
          ? final_balance_contact
          : final_balance_favor;

      item.cross_value = total_balance;
      //   state.modalBalanceContact.crossPendingValue -= total_balance;
    }
  }
  // false
  else {
    item.cross_value = 0;
  }
}

export function setWarehouses(state, data) {
  state.warehouses = data;
}

export function setDocumentCrossBalance(state, data) {
  state.documentCrossBalance = data;
}
export function resetDocumentCrossBalance(state) {
  state.documentCrossBalance = defaultDocumentCrossBalance();
}

export function setDocumentInitiatedByAnotherUser(state, data) {
  state.documentInitiatedByAnotherUser = data;
}

export function setFinalizeDocument(state, data) {
  state.finalizeDocument = data;
}

export function resetGridBalancesFavor(state) {
  state.gridBalancesFavor.items = [];
}
export function resetGridBalancesContact(state) {
  state.gridBalancesContact.items = [];
}
export function setAction(state, value) {
  state.action = value;
}
