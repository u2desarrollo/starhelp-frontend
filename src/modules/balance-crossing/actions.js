import $http from "../../axios";
import { Notification } from "element-ui";

export async function accountsDefaultCrossBalances({ commit, state }) {
  await $http
    .get("/api/cuenta-por-defecto-cruce-de-saldos/")
    .then(response => {
      commit("setAccountDefault", response.data.data);
    })
    .catch(error => {
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchFavorBalanceContact({ commit, state }) {
  let dataparams = JSON.parse(JSON.stringify(state.filtersCrossBalances));

  delete dataparams.account;

  const params = {
    params: dataparams
  };

  await $http
    .get("/api/cruce-cuentas-saldo-a-favor-tercero/", params)
    .then(response => {
      commit("setFavorBalanceContact", response.data.data);
    })
    .catch(error => {
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
export async function fetchPendingBalanceContact({ commit, state }) {
  let dataparams = JSON.parse(JSON.stringify(state.filtersCrossBalances));

  delete dataparams.account;

  dataparams.account_id = state.modalBalanceContact.account_id;

  const params = {
    params: dataparams
  };

  await $http
    .get("/api/cruce-cuentas-saldo-pendiente-tercero/", params)
    .then(response => {
      commit("setPendingBalanceContact", response.data.data);
    })
    .catch(error => {
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function fetchWarehouses({ commit, state }, paramsData) {
  const params = {
    params: paramsData
  };

  await $http
    .get("/api/sucursales/", params)
    .then(response => {
      commit("setWarehouses", response.data);
    })
    .catch(error => {
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function createDocumentByCrossBalance({ commit, state }) {
  let dataparams = JSON.parse(JSON.stringify(state.filtersCrossBalances));

  delete dataparams.account;

  const params = dataparams;

  await $http
    .post("/api/create-document-cross-balance/", params)
    .then(response => {
      commit("setDocumentCrossBalance", response.data.data);
    })
    .catch(error => {
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function createTransactionsByDocumentCrossBalance(
  { commit, state },
  data
) {
  const params = {
    ...data,
    document_id: state.documentCrossBalance.id
  };

  await $http
    .post("/api/create-transactions-document-cross-balance/", params)
    .then(response => {
      commit("setDocumentCrossBalance", response.data);
    })
    .catch(error => {
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function deleteDocumentTransactionsBySequence(
  { commit, state },
  data
) {
  const params = {
    params: {
      ...data,
      document_id: state.documentCrossBalance.id
    }
  };

  await $http
    .delete("/api/delete-transactions-document-cross-balance/", params)
    .then(response => {
      commit("setDocumentCrossBalance", response.data);
    })
    .catch(error => {
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function checkDocumentInitiatedByAnotherUser({ commit, state }) {
  const params = {
    params: {
      contact_id: state.filtersCrossBalances.contact_id
    }
  };

  await $http
    .get("/api/varificar-documento-usuario-documento-cruce/", params)
    .then(response => {
      commit("setDocumentInitiatedByAnotherUser", response.data);
    })
    .catch(error => {
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function finalizeDocumentCrossBalance({ commit, state }) {
  const params = {
    document_id: state.documentCrossBalance.id,
    voucherType_id: state.filtersCrossBalances.voucherType_id
  };

  await $http
    .post("/api/finalizar-documento-cruce/", params)
    .then(response => {
      commit("setFinalizeDocument", response.data);
    })
    .catch(error => {
      let data = error.response.data;
      Notification.error({
        title: "Error!",
        message: data.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function getDocumentCross({ commit }, id) {
  await $http
    .get(`/api/documento-cruce/${id}`)
    .then(response => {
      commit("setDocumentCrossBalance", response.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: "Error al traer el documento",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
