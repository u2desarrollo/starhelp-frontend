export const defaultSelectedTypeOperation = () => {
    return {
        code: '',
        description: '',
        parameter_id: null,
        affectation: '',
        inventory_cost: '',
    }
}
export default {
    typesOperation: [],
    error: false,
    errorMessage: '',
    selectedTypeOperation: defaultSelectedTypeOperation(),
    action: 'create',
    getTypeOperationEdit: true,
    parameters: null,
    params: {
        paginate: true,
        page: 1,
        perPage: 15,
        filter: '',
        sortBy: 'id',
        sort: 'ASC',
        paramtable_id: 73,

    }
}
