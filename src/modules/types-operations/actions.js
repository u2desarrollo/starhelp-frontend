import $http from "../../axios"
import { Notification, MessageBox } from 'element-ui'

export async function fetchTypesOperation ({ commit, state }, requestParameters = { params: state.params }) {
  await $http.get('/api/types_operations', requestParameters).then((res) => {
    commit('setTypesOperation', res.data.data);
    commit('setGetTypeOperation', true)
  }).catch((error) => {
    Notification.error({
      title: '¡Error!',
      message: error.message,
      type: 'error',
      duration: 1500,
      customClass: 'notification-box'
    })
    commit('typesOperationError', error.message)
  });
}
export async function addTypeOperation ({ commit, state, dispatch }) {
  await $http.post('/api/types_operations', state.selectedTypeOperation)
    .then((response) => {
      Notification.success({
        title: 'Exito!',
        message: 'El tipo de operacion ha sido creado',
        type: 'success',
        duration: 2000,
        customClass: 'notification-box',
      });
      // dispatch('fetchTypesOperation')
      commit('setTypeOperation', response.data.data)
      state.error = false
    })
  // .catch ((error) => {
  //     let message;
  //         MessageBox.alert(error.message, 'Error', {
  //             confirmButtonText: 'Aceptar',
  //             type: 'error'
  //     })
  //     commit('typesOperationError', error.message)

  // })
}
export async function getTypeOperation ({ commit, state }, id) {
  if (state.getTypeOperationEdit) {
    await $http.get(`/api/types_operations/${id}`)
      .then((response) => {
        commit('setTypeOperation', response.data.data)
      })
      .catch((error) => {
        Notification.error({
          title: 'Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box',
        });
        commit('typesOperationError', error.message)
      })

    commit('setGetTypeOperation', false)
  }
}
export async function updateTypeOperation ({ commit, state, dispatch }) {
  $http.put(`/api/types_operations/${state.selectedTypeOperation.id}`, state.selectedTypeOperation)
    .then((response) => {
      Notification.success({
        title: 'Exito!',
        message: 'La operación ha sido actualizada',
        type: 'success',
        duration: 2000,
        customClass: 'notification-box',
      });
      commit('setTypeOperation', response.data.data)
    })
    .catch((error) => {
      let message;

      if (error.response.status == 422) {
        message = error.response.data.errors.license_code[0]
      } else {
        message = error.message
      }

      MessageBox.alert(message, 'Error', {
        dangerouslyUseHTMLString: true,
        confirmButtonText: 'Aceptar',
        type: 'error',
        customClass: 'notification-error'
      })
      commit('typesOperationError', error.message)
    })
}
export async function removeTypeOperation ({ commit, dispatch }, typeOperation) {
  $http.delete(`/api/types_operations/${typeOperation.id}`).then((response) => {
    Notification.success({
      title: 'Exito!',
      message: 'El tipo de operacion ha sido eliminado',
      type: 'success',
      duration: 2000,
      customClass: 'notification-box',
    });
    dispatch('fetchTypesOperation')
  })
    .catch((error) => {
      Notification.error({
        title: 'Error!',
        message: error.message,
        type: 'error',
        duration: 2000,
        customClass: 'notification-box',
      })
      commit('typesOperationError', error.message)
    })
}
export function setFilterPagination ({ commit, dispatch }, filter) {
  commit('setFilter', filter.target.value)
  dispatch('fetchTypesOperation')
}

export function setPerPagePagination ({ commit, dispatch }, perPage) {
  commit('setPerPage', parseInt(perPage.target.value))
  dispatch('fetchTypesOperation')
}

export function setPagePagination ({ commit, dispatch }, page) {
  commit('setPage', parseInt(page))
  dispatch('fetchTypesOperation')
}

export function setSortByPagination ({ commit, dispatch }, sortBy) {
  commit('setSortBy', sortBy)
}

export function setSortPagination ({ commit, dispatch }, sort) {
  commit('setSort', sort)
}

export async function getParametersByCode ({ commit, state }) {
  await $http.get(`/api/parametros`, state).then(response => {
    commit('setParameters', response.data.data);

  });
}
