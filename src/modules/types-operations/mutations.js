import { defaultSelectedTypeOperation } from "./state";
export function setTypesOperation(state, typesOperation) {
    state.typesOperation = typesOperation;
}
export function setTypeOperation(state, typeOperation) {
    state.selectedTypeOperation = typeOperation;
}
export function setAction(state, action) {
    state.action = action;
}
export function setGetTypeOperation(state, getTypeOperationEdit) {
    state.getTypeOperationEdit = getTypeOperationEdit;
}
export function resetSelectedTypeOperation(state) {
    state.selectedTypeOperation = defaultSelectedTypeOperation();
}

export function typesOperationError(state, payload) {
    state.error = true
    state.errorMessage = payload
    state.typesOperation = []
}
export function setFilter(state, filter) {
    state.params.page = 1
    state.params.filter = filter
}

export function setPerPage(state, perPage) {
    state.params.page = 1
    state.params.perPage = perPage
}

export function setPage(state, page) {
    state.params.page = page
}

export function setSortBy(state, sortBy) {
    state.params.sortBy = sortBy
}

export function setSort(state, sort) {
    state.params.sort = sort
}

export function setParameters(state, parameters) {
    state.parameters = parameters
}
