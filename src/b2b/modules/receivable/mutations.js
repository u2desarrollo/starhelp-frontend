import moment from 'moment'

export function setReceivables (state, receivables) {
    // Asignamos los registros
    state.receivables = receivables
    // Calculamos     
    state.totalReceivables = 0;
    state.receivables.forEach(element => {
      
            state.totalReceivables += element.receivable[0].invoice_balance
       
    });
  
    
}

export function receivableError (state, payload) {
    state.error = true
    state.errorMessage = payload
    state.receivables = []
}
//------------------------ PAGINACIÓN ------------------------//

export function setPerPage (state, perPage) {
    state.params.page = 1
    state.params.perPage = perPage
}

export function setPage (state, page) {
    state.params.page = page
}

export function setSortBy (state, sortBy) {
    state.params.sortBy = sortBy
}

export function setSort (state, sort) {
    state.params.sort = sort
}
