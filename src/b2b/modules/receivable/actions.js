import $http from "../../../axios";
import { Notification } from "element-ui";

// Obtener banners
export async function fetchReceivables(
  { commit, state },
  requestParameters = {
    params: state.params
  }
) {
  requestParameters.params.sortBy = "document_id";

  await $http
    .get("/api/cartera", requestParameters)
    .then(response => {
      commit("setReceivables", data);
    })
    .catch(error => {
      /*Notification.error({
          title: '¡Error!',
          message: error.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box'
      })*/
      commit("receivableError", error.message);
    });
}

// Obtener cartera del cliente
export async function fetchReceivablesByContactId(
  { commit, state },
  contact_id
) {
  await $http
    .get(`/api/cartera/${contact_id}`)
    .then(response => {
      commit("setReceivables", response.data);
    })
    .catch(error => {
      console.error(error);
      /*Notification.error({
          title: '¡Error!',
          message: error.response.data.message,
          type: 'error',
          duration: 1500,
          customClass: 'notification-box'
      })*/
    });
}

//------------------------ PAGINACIÓN ------------------------//

export function setFilterPagination({ commit, dispatch }, filter) {
  commit("setFilter", filter.target.value);
  dispatch("fetchReceivables");
}

export function setFilterReference({ commit, dispatch }, reference) {
  commit("setReference", reference.target.value);
  dispatch("fetchReceivables");
}

export function setPerPagePagination({ commit, dispatch }, perPage) {
  commit("setPerPage", parseInt(perPage.target.value));
  dispatch("fetchReceivables");
}

export function setPagePagination({ commit, dispatch }, page) {
  commit("setPage", parseInt(page));
  dispatch("fetchReceivables");
}

export function setSortByPagination({ commit, dispatch }, sortBy) {
  commit("setSortBy", sortBy);
}

export function setSortPagination({ commit, dispatch }, sort) {
  commit("setSort", sort);
}


export async function getReceivableCSV({ }, id) {
  $http({
    url: `/api/Informe-cartera/${id}`,
    method: 'GET',
    responseType:'blob'
  }).then(response => {
  
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      let date = Date();
      link.setAttribute("download", `CARTERA-${date.toString()}.csv`);
      document.body.appendChild(link);
      link.click();

    


  }).catch(error => {

  })
}
