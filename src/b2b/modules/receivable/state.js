export default {
    receivables: [],
    error: false,
    errorMessage: '',
    totalReceivables: 0,
    params: {
        paginate: true,
        page: 1,
        perPage: 15,
        sortBy: 'invoice_id',
        sort: 'ASC',
        warehouse: 0
    },
}
