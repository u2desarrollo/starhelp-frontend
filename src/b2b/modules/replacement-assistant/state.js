export default {
	products: [],
	brands: [],
	error: false,
	errorMessage: '',
	params: {
		paginate: true,
		page: 1,
		warehouse: 0,
		only_promotions: false,
		filter_date: '2019-01-01',
		filter_brands: [],
        line_id: null,
        lines: []
	}
}
