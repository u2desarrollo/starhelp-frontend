import $http from '../../../axios'
import { Notification } from 'element-ui'

// Obtener banners
export async function fetchProducts({ commit, state, dispatch }, requestParameters = { params: state.params }) {

  let user_b2b = JSON.parse(localStorage.user_b2b);
  requestParameters.params.contact_id = user_b2b.contact_id;
  requestParameters.params.branchoffice_storage = localStorage.selected_branchoffice;
  requestParameters.params.branchoffice_id = user_b2b.branchoffice.id;

	await $http.get('/api/productos-reposicion', requestParameters)
		.then((response) => {
			commit('setProducts', response.data.data)
		}).catch((error) => {
			Notification.error({
				title: '¡Error!',
				message: error.message,
				type: 'error',
				duration: 1500,
				customClass: 'notification-box'
			})
			commit('productError', error.message)
		})

}

// Obtener banners
export async function fetchPricesProducts({ commit, state, rootState }) {
	let products = state.products.data
	await $http.post('/api/calcular-precios/' + rootState.login.user_b2b.id, products)
	.then((response) => {
		commit('setProductsData', response.data.data)
	}).catch((error) => {
	})

}

//------------------------ PAGINACIÓN ------------------------//

export function setFilterPagination({ commit, dispatch }, filter) {
	commit('setFilter', filter.target.value)
	dispatch('fetchProducts')
}

export function setFilterReference({ commit, dispatch }, reference) {
	commit('setReference', reference.target.value)
	dispatch('fetchProducts')
}

export function setPerPagePagination({ commit, dispatch }, perPage) {
	commit('setPerPage', parseInt(perPage.target.value))
	dispatch('fetchProducts')
}

export function setPagePagination({ commit, dispatch }, page) {
	commit('setPage', parseInt(page))
	dispatch('fetchProducts')
}

export function setSortByPagination({ commit, dispatch }, sortBy) {
	commit('setSortBy', sortBy)
		// dispatch('fetchProducts')
}

export function setSortPagination({ commit, dispatch }, sort) {
	commit('setSort', sort)
		// dispatch('fetchProducts')
}

export async function fetchBrands({ dispatch, state, rootState, commit }) {
	if (!state.brands.length) {
		await dispatch('brands/fetchBrands', { params: { paginate: false } }, { root: true })
		commit('setBrands', rootState.brands.brands)
	}
}
