export function setProducts(state, products) {
	products.data.forEach(p => {
		p.quantity = p.documents_products[0].quantity
    p.total = p.price.price * p.documents_products[0].quantity
	});
	state.products = products
}
export function productError(state, payload) {
	state.error = true
	state.errorMessage = payload
	state.products = []
}
export function setProductsData(state, products) {
	state.products.data = products;
}
//------------------------ PAGINACIÓN ------------------------//

export function setFilter(state, filter) {
	state.params.page = 1
	state.params.filter = filter
}
export function setReference(state, reference) {
	state.params.page = 1
	state.params.reference = reference
}

export function setPerPage(state, perPage) {
	state.params.page = 1
	state.params.perPage = perPage
}

export function setPage(state, page) {
	state.params.page = page
}

export function setSortBy(state, sortBy) {
	state.params.sortBy = sortBy
}

export function setSort(state, sort) {
	state.params.sort = sort
}

export function setBrands(state, brands) {
	state.brands = brands.map(p => {
		return { value: p.id, label: p.description, imagen: p.thumbnails }
	})
}

export function setFilterBrand(state, brand) {
	state.params.filter_brands = brand
	//state.category_params.filter_brands = brand;
}
