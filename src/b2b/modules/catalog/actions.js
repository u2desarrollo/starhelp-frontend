import $http from "../../../axios";
import { Notification } from "element-ui";

// Obtener banners
export async function fetchProducts(
  { commit, state, rootState },
  requestParameters = { params: state.params }
) {
  let user_b2b = JSON.parse(localStorage.user_b2b);
  requestParameters.params.contact_id = user_b2b.contact_id;
  requestParameters.params.branchoffice_storage =
    localStorage.selected_branchoffice;
  requestParameters.params.branchoffice_id = user_b2b.branchoffice.id;
  //Loader on
  state.loader = true;

  await $http
    .get("/api/productos", requestParameters)
    .then(response => {
      commit("setProducts", {
        products: response.data.data,
        cart: rootState.cart.cart
      });
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("productError", error.message);
    });

  //Loader off
  state.loader = false;
}

export async function fetchLines({ dispatch, state, rootState, commit }) {
  if (!state.lines.length) {
    await dispatch(
      "lines/fetchLines",
      { params: { paginate: false } },
      { root: true }
    );
    commit("setLines", rootState.lines.lines);
  }
}

export async function fetchSublines(
  { dispatch, state, rootState, commit },
  requestParameters = { params: state.category_params }
) {
  let params = {
    params: {
      lines: state.params.filter_lines,
      filter_brands: state.params.filter_brands,
      filter_groups: state.params.filter_groups
    }
  };

  await dispatch("sublines/listSublines", params, { root: true });
  commit("setGroups", rootState.sublines.sublines);
}

export async function fetchBrand({ dispatch, state, rootState, commit }) {
  console.log(state.params.filter_lines);

  await dispatch(
    "brands/fetchBrands",
    { params: { paginate: false, lines: state.params.filter_lines } },
    { root: true }
  );
  commit("setBrands", rootState.brands.brands);
}

export async function fetchSubbrands(
  { dispatch, state, rootState, commit },
  requestParameters = { params: state.params }
) {
  let params = {
    params: {
      brands: state.params.filter_brands
    }
  };
  await dispatch("subbrands/listSubbrands", params, { root: true });
  commit("setSubbrands", rootState.subbrands.subbrands);
}

export async function fetchCategories(
  { dispatch, state, rootState, commit },
  requestParameters = { params: state.category_params }
) {
  await dispatch("categories/fetchCategories", requestParameters, {
    root: true
  });
  commit("setCategories", rootState.categories.categories);
}

// Obtener banners
export async function fetchPricesProducts({ commit, state, rootState }) {
  let products = state.products.data;

  await $http
    .post("/api/calcular-precios/" + rootState.login.user_b2b.id, products)
    .then(response => {
      commit("setProductsData", response.data.data);
    })
    .catch(error => {});
}

//------------------------ PAGINACIÓN ------------------------//

export function setFilterPagination({ commit, dispatch }, filter) {
  commit("setFilter", filter.target.value);
  dispatch("fetchProducts");
}

export function setPerPagePagination({ commit, dispatch }, perPage) {
  commit("setPerPage", parseInt(perPage.target.value));
  dispatch("fetchProducts");
}

export function setPagePagination({ commit, dispatch }, page) {
  commit("setPage", parseInt(page));
  dispatch("fetchProducts");
}

export function setSortByPagination({ commit, dispatch }, sortBy) {
  commit("setSortBy", sortBy);
  // dispatch('fetchProducts')
}

export function setSortPagination({ commit, dispatch }, sort) {
  commit("setSort", sort);
  // dispatch('fetchProducts')
}

/** Obtener promociones */
export async function getPromotions({ commit, state }, product) {
  var params = {
    params: {
      product_id: product.id,
      line_id: product.line != null ? product.line.id : null,
      subline_id: product.subline != null ? product.subline.id : null,
      brand_id: product.brand != null ? product.brand.id : null,
      subbrand_id: product.subbrand != null ? product.subbrand.id : null,
      branchoffice_warehouse_id:
        product.branchoffice_warehouse_id != null
          ? product.branchoffice_warehouse_id
          : null
    }
  };

  await $http.get("/api/promociones-activas", params).then(response => {
    commit("setSelectedProduct", product);
    commit("setPromotions", response.data.data);
  });
}

/** Buscar productos según los criterios de clasificación */
export function fetchProductsByClasification(
  { commit, state },
  requestParams = { params }
) {
  let user_b2b = JSON.parse(localStorage.user_b2b);
  requestParams.params.contact_id = user_b2b.contact_id;

  $http.get("/api/productos-clasificacion", requestParams).then(response => {
    commit("setProductsFiltered", response.data.data);
  });
}

/** Abrir modal de promociones */
export async function getModalPromotions(
  { state, dispatch, commit, rootState },
  product
) {
  let user = JSON.parse(localStorage.user);
  product.branchoffice_warehouse_id = user.branchoffice_warehouse_id;
  await dispatch("getPromotions", product);

  // Comprobar que productos de la promoción estan en carrito
  let cart = JSON.parse(JSON.stringify(rootState.cart.cart));
  await commit("setProductsPromotionsTable", cart);
  await commit("setProductsBenefitTable", cart);

  commit("setShowPromotionsModal", true);
}

export async function validateAllPromotionRules({
  state,
  commit,
  dispatch,
  rootState
}) {
  state.promotions.map(promotion => {
    dispatch("validatePromotionRules", promotion.id);
  });
}

export async function changePromotionProduct(
  { state, commit, dispatch, rootState },
  promotion_product
) {
  dispatch("validatePromotionRules", promotion_product.promotion_id);
}

export async function validatePromotionRules(
  { state, commit, dispatch, rootState },
  promotion_id = null
) {
  commit("validatePromotion");
}

export async function setBenefitProduct({ state }) {
  // Producto de menor precio
  if (state.selectedPromotion.gift_type == 2) {
    var product_benefit = null;
    var min_val = null;

    state.selectedPromotion.products_promotions.map((pp, index_pp) => {
      if (min_val == null) {
        min_val = pp.product.price.price;
        product_benefit = JSON.parse(JSON.stringify(pp.product));
      } else {
        if (pp.product.price.price < min_val) {
          min_val = pp.product.price.price;
          product_benefit = JSON.parse(JSON.stringify(pp.product));
        }
      }
    });

    var iva = product_benefit.line.taxes.find(t => t.code == 10);

    product_benefit.quantity = 1;
    product_benefit.val_brut =
      product_benefit.price.price * product_benefit.quantity;
    product_benefit.val_iva = (product_benefit.val_brut * iva.percentage) / 100;
    product_benefit.is_benefit = true;
    state.selectedPromotion.specific_product = product_benefit;
  }

  // Producto especifico
  if (state.selectedPromotion.gift_type != 2) {
    if (state.selectedPromotion.specific_product_id != null) {
      await $http
        .get(`/api/productos/${state.selectedPromotion.specific_product_id}`)
        .then(response => {
          product_benefit = response.data.data;

          var iva = product_benefit.line.taxes.find(t => t.code == 10);

          product_benefit.quantity = 1;
          product_benefit.val_brut =
            product_benefit.price.price * product_benefit.quantity;
          product_benefit.val_iva =
            (product_benefit.val_brut * iva.percentage) / 100;
          product_benefit.is_benefit = true;
          state.selectedPromotion.specific_product = product_benefit;
        })
        .catch(error => {
          Notification.error({
            title: "¡Error!",
            message: "Error al traer el beneficio",
            type: "error",
            duration: 1500,
            customClass: "notification-box"
          });
        });
    }
  }
}

export function getBenefitProductById({ state, commit, dispatch, rootState }) {}

export async function getBranchOfficesWarehousesAvailableByProduct(
  { state, commit, dispatch, rootState },
  product_id
) {
  await $http
    .get(`/api/inventario-sedes-por-producto/${product_id}`)
    .then(response => {
      commit("setBranchOfficesWarehousesAvailableProduct", response.data);
    })
    .catch(error => {
      console.error(error);

      Notification.error({
        title: "¡Error!",
        message: "Error al traer el contenido",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}

export async function getProduct({ commit, state, rootState }, product_id) {
  let user_b2b = JSON.parse(localStorage.user_b2b);

  let params = {
    params: {
      branchoffice_id: user_b2b.branchoffice_id,
      product_id: product_id,
      warehouse_id: user_b2b.id
    }
  };

  commit("cart/setLockPromotions", true, { root: true });

  await $http
    .get("/api/promocion-por-producto", params)
    .then(response => {
      commit("setSelectedProduct", response.data.data);

      let products = JSON.parse(JSON.stringify(state.products));

      let indexProduct = products.data.findIndex(p => p.id == product_id);

      if (indexProduct >= 0) {
        products.data.splice(indexProduct, 1, response.data.data);
      }
    })
    .catch(error => {});

  commit("cart/setLockPromotions", false, { root: true });
}
