export function setProducts(state, data) {
  data.products.data.map((p, index_p) => {
    p.quantity = 0;
    p.click = 0;
    p.total = p.price.price;
    p.price.calculated = false;

    if (p.hasPromotion) {
      p.product_promotions.map(pp => {
        pp.comply = false;
        pp.specific_product = pp.specific_product ? pp.specific_product : null;
      });
    }

    // Carrito
    if (data.cart.documents_products) {
      data.cart.documents_products.map((dp, indexdp) => {
        if (dp.product_id) {
          if (dp.product_id == p.id) {
            p.quantity = dp.quantity;
          }
        }
      });
    }

    //test
    var priceInventory = p.inventory.find(
      pi => pi.branchoffice_warehouse_id == 279 && pi.product_id == p.id
    );
    if (priceInventory) {
      p.quantity_available = priceInventory.available;
    }
  });

  state.products = data.products;
}

export function productError(state, payload) {
  state.error = true;
  state.errorMessage = payload;
  state.products = [];
}

export function setLines(state, lines) {
  state.lines = lines.map(p => {
    return { value: p.id, label: p.line_description };
  });
}

export function setSublines(state, sublines) {
  state.sublines = sublines.map(p => {
    return { value: p.id, label: p.subline_description };
  });
}

export function setBrands(state, brands) {
  state.brands = brands.map(p => {
    return { value: p.id, label: p.description, imagen: p.thumbnails };
  });
}

export function setCategories(state, categories) {
  state.categories = categories.map(p => {
    return { value: p.id, label: p.description };
  });
}

export function setGroups(state, groups) {
  state.groups = groups.map(p => {
    return { value: p.id, label: p.subline_description };
  });

  state.sublines = groups.map(p => {
    return { value: p.id, label: p.subline_description };
  });
}

export function setSubbrands(state, subbrands) {
  state.subbrands = subbrands.map(p => {
    return { value: p.id, label: p.description };
  });
}

export function setProductsData(state, products) {
  state.products.data = products;
}

export function setBranchOfficesWarehousesAvailableProduct(state, data) {
  state.branchOfficesWarehousesAvailableProduct = data;
  state.branchOfficesWarehousesAvailableProduct.previous_product_id = null;
}
export function resetBranchOfficesWarehousesAvailableProduct(state, data) {
  state.branchOfficesWarehousesAvailableProduct = {
    product: null,
    branchOfficesAvailable: [],
    previous_product_id: null
  };
}

//------------------------ PAGINACIÓN ------------------------//

export function setFilter(state, filter) {
  state.params.page = 1;
  state.params.filter = filter;
}
export function setReference(state, reference) {
  state.params.page = 1;
  state.params.reference = reference;
}

export function setPerPage(state, perPage) {
  state.params.page = 1;
  state.params.perPage = perPage;
}

export function setPage(state, page) {
  state.params.page = page;
}

export function setSortBy(state, sortBy) {
  state.params.sortBy = sortBy;
}

export function setSort(state, sort) {
  state.params.sort = sort;
}
export function setFilterLine(state, line) {
  state.params.filter_lines = line;
}
export function setFilterSublines(state, sublines) {
  state.params.filter_subline = sublines;
  state.category_params.filter_subline = sublines;
}
export function setFilterBrand(state, brand) {
  state.params.filter_brands = brand;
  state.category_params.filter_brands = brand;
}
export function setFilterGroup(state, sublines = []) {
  state.params.filter_groups = sublines;
  state.category_params.filter_groups = sublines;
}
export function setFilterCategories(state, categories = []) {
  state.params.filter_categories = categories;
}
export function setFilterSubbrands(state, subbrands) {
  state.params.filter_subbrands = subbrands;
}

/** Asignar promociones */
export function setPromotions(state, promotions) {
  state.selectedPromotion = promotions[0];
  state.promotions = promotions;

  var exist_product = state.selectedPromotion.products_promotions.find(
    pp => pp.product.id == state.selectedProduct.id
  );

  if (exist_product === undefined) {
    state.selectedProduct.quantity = 0;
    state.selectedPromotion.products_promotions.push({
      id: null,
      promotion_id: state.selectedPromotion.id,
      product_id: state.selectedProduct.id,
      minimum_amount: null,
      discount: 0,
      pay_soon: null,
      cash_payment: null,
      product: JSON.parse(JSON.stringify(state.selectedProduct))
    });
  }
}

/** SELECCIONAR UNA PROMOCION **/
export function setSelectedPromotion(state, promotion) {
  state.selectedPromotion = promotion;
}

/** Asignar valor al producto seleccionado */
export function setSelectedProduct(state, product) {
  state.selectedProduct = product;
}

/** Asignar productos filtrados por criterios de clasificación */
export function setProductsFiltered(state, products) {
  state.productsFiltered = products;
}

/** Abrir o cerrar modal de promociones */
export function setShowPromotionsModal(state, bool) {
  state.showPromotionsModal = bool;
}

/** Mostrar los productos que estan en carrito en el modal de promociones */
export function setProductsPromotionsTable(state, cart) {
  let promotions = JSON.parse(JSON.stringify(state.promotions));

  promotions.map(promo => {
    promo.products_promotions.map(ppp => {
      ppp.discount_value = 0;
      ppp.product.quantity = 0;
    });
  });

  promotions.map(promo => {
    cart.documents_products.find(dp => {
      if (dp.promotion_id == promo.id) {
        if (promo.products_promotions.length) {
          dp.samePromotion.map(sp => {
            if (sp.unit_value_before_taxes > 0) {
              if (!sp.is_benefit) {
                promo.products_promotions.map(ppp => {
                  if (ppp.product_id == sp.product_id) {
                    ppp.product.quantity = sp.quantity;
                    ppp.product.price.price = parseInt(
                      sp.unit_value_before_taxes
                    );
                    ppp.discount_value = sp.discount_value;
                  }
                });
              }
            }
          });
        } else {
          // Obetener productos que tengan precio
          let hasPrice = dp.samePromotion.filter(
            p => p.unit_value_before_taxes > 0
          );

          // Reemplazar los productos de samePromotion solo por los que tienen precio
          dp.samePromotion = hasPrice;

          dp.samePromotion.map(sp => {
            sp.product.quantity = sp.quantity;
            sp.product.price.price = parseInt(sp.unit_value_before_taxes);
            sp.discount_value = 0;
          });
          promo.products_promotions = dp.samePromotion;
        }
      }
    });
  });

  state.promotions = promotions;
}

/** Mostrar el producto beneficio en el modal de promociones */
export function setProductsBenefitTable(state, cart) {
  let promotions = JSON.parse(JSON.stringify(state.promotions));

  promotions.map(promo => {
    cart.documents_products.find(dp => {
      if (dp.unit_value_before_taxes == 0) {
        promo.specific_product = {
          code: dp.product.code,
          description: dp.product.description,
          price: {
            price: 0
          }
        };
      }
    });
  });

  state.promotions = promotions;
}

export function setLoader(state, data) {
  state.loader = data;
}

export function validatePromotion(state, data) {
  //if (state.selectedPromotion.promotion_type_id == 3) {

  // Validamos por la cantidad minima
  var total_quantity = 0;
  var total_val_brut = 0;

  state.selectedPromotion.comply = false;

  state.selectedPromotion.products_promotions.map((pp, index_pp) => {
    total_quantity += parseInt(pp.product.quantity ? pp.product.quantity : 0);
    total_val_brut += pp.product.quantity
      ? pp.product.price.price * pp.product.quantity
      : 0;
  });

  if (state.selectedPromotion.minimum_amount) {
    if (total_quantity >= state.selectedPromotion.minimum_amount) {
      state.selectedPromotion.comply = true;
    }
  }

  if (state.selectedPromotion.minimum_gross_value) {
    if (total_val_brut >= state.selectedPromotion.minimum_gross_value) {
      state.selectedPromotion.comply = true;
    }
  }

  //}
}

export function setPromotionUpdate(state, prod) {
  var promotion = prod.promotion;
  var samePromotion = prod.samePromotion;

  samePromotion.map((sp, index_sp) => {
    // Validamos que el producto no sea un beneficio
    if (sp.is_benefit == true) {
      promotion.specific_product_id = sp.product.id;
      return;
    }

    // Validamos si el producto existe
    var exist_product_key = promotion.products_promotions.findIndex(
      pp => pp.product.id == sp.product.id
    );

    // No existe
    if (exist_product_key == -1) {
      promotion.products_promotions.push({
        id: null,
        promotion_id: promotion.id,
        product_id: sp.product.id,
        minimum_amount: null,
        discount: 0,
        pay_soon: null,
        cash_payment: null,
        quantity: sp.quantity,
        product: JSON.parse(JSON.stringify(sp.product)),
        is_benefit: sp.is_benefit
      });
    } else {
      promotion.products_promotions[exist_product_key].quantity = sp.quantity;
    }
  });

  let product = {
    product_promotions: [promotion]
  };

  state.selectedProduct = product;
}

export function setlabelButtonPromotion(state, text = "Aplicar Promoción") {
  state.labelButtonPromotion = text;
}

export function setAddingProduct(state, product) {
  let products = JSON.parse(JSON.stringify(state.products))

  products.data.map(p => {
    if (p.id === product.product_id){
      p.adding = true
    }
  })

  state.products = products
}

export function quitAddingProduct(state, product) {
  let products = JSON.parse(JSON.stringify(state.products))

  products.data.map(p => {
      p.adding = false
  })

  state.products = products
}

export function setGettingPromotions(state, product) {
  let products = JSON.parse(JSON.stringify(state.products))

  products.data.map(p => {
    if (p.id === product.id){
      p.gettingPromotions = true
    }
  })

  state.products = products
}

export function quitGettingPromotions(state, product) {
  let products = JSON.parse(JSON.stringify(state.products))

  products.data.map(p => {
    p.gettingPromotions = false
  })

  state.products = products
}
