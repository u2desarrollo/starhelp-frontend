export default {
  brands: [],
  loader: true,
  lines: [],
  sublines: [],
  products: {data:[]},
  groups: [],
  categories: [],
  subbrands: [],
  error: false,
  errorMessage: "",
  params: {
    filter_lines: [],
    filter_sublines: [],
    filter_brands: [],
    filter_subbrands: [],
    filter_groups: [],
    filter_categories: [],
    paginate: true,
    page: 1,
    filter: "",
    reference: "",
    warehouse: 0,
    only_promotions: false,
    clasification: ""
  },
  category_params: {
    filter_brands: [],
    filter_groups: [],
    lines: ""
  },
  promotions: [],
  selectedProduct: [],
  selectedPromotion: null,
  productsFiltered: [],
  showPromotionsModal: false,
  minimumPriceProducts: [],
  labelButtonPromotion: "Aplicar Promoción",
  branchOfficesWarehousesAvailableProduct: {
    product: null,
    branchOfficesAvailable: [],
    previous_product_id: null
  }
};
