import { defaultSelectedCart } from "./state";

export function setCart(state, cart) {

  if (cart.documents_products) {
    cart.documents_products.map((dp, index) => {
      dp.edit_discount_value = false;
    });
  }

  state.cart = cart;
  state.cartInitial = cart;
}

export function setCartItems(state, items) {
  state.cart.items = items;
}

export function setInformation(state, information) {
  state.cart.information = information;
}

export function cartError(state, payload) {
  state.error = true;
  state.errorMessage = payload;
}

export function setTotal(state, total) {
  state.total = total;
}

export function setPdf(state, pdf) {
  state.pdf = pdf;
}

export function addItemCart(state, item) {
  let documents_products = [];

  if (state.cart.documents_products) {
    documents_products = state.cart.documents_products
  }

  documents_products.push(item)

  state.cart.documents_products = documents_products;
}

export function deleteItemCart(state, item) {
  let documents_products = [];

  documents_products = JSON.parse(
    JSON.stringify(state.cart.documents_products)
  );

  let index = documents_products.findIndex(dp => dp.id == item.id);

  if (index >= 0) {
    documents_products.splice(index, 1);
  }

  state.cart.documents_products = documents_products;
}

export function setLastCart(state, last_cart) {
  state.last_cart = last_cart;
}

/**
 * Asigna un valor a la variable de state selected_model
 * @param state
 * @param selected_model
 */
export function setSelectedModel(state, selected_model) {
  state.selected_model = selected_model;
}

export function setLockAdd(state, lockAdd) {
  state.lockAdd = lockAdd
}

export function setLockPromotions(state, lockPromotions) {
  state.lockPromotions = lockPromotions
}
