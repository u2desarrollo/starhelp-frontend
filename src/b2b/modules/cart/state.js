export const defaultSelectedModel = () => {
  return {
    pay_taxes: null
  };
};

export default {
  cartInitial: [],
  cart: {
    documents_information: {
      price_list: ""
    }
  },
  total: 0,
  pdf: "",
  show_pdf: false,
  error: false,
  errorMessage: "",
  last_cart: {
    parameter: {
      name_parameter: ""
    }
  },
  selected_model: defaultSelectedModel(),
  lockAdd: false,
  lockPromotions: false
};
