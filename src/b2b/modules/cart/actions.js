import $http from "../../../axios";
import { Notification } from "element-ui";

// Agregar items al carrito
export async function addCartItems(
  { commit, state, dispatch, rootState },
  products
) {
  commit("setLockAdd", true);
  commit("setLockPromotions", true);
  commit("catalog/setAddingProduct", products, { root: true });
  //products.original_quantity = products.original_quantity != '' || products.original_quantity != 0 ? products.original_quantity : 0;
  let moedel_id = await dispatch("getModelBycode", '010');
  products.model_id = moedel_id;
  products.warehouse_id = rootState.login.user_b2b.id;
  products.branchoffice_id = rootState.login.user_b2b.branchoffice_id;

  await $http.post("/api/items-carrito", products).then(async response => {
    // commit("addItemCart", response.data.data);
    // commit("catalog/quitAddingProduct", null, { root: true });
    await dispatch("fetchCart", {
      contact_id: rootState.login.user_b2b.contact_id,
      branchoffice_id: rootState.login.user_b2b.id,
      user_id: rootState.login.user_logued.id
    });
  });
  commit("setLockAdd", false);
  commit("setLockPromotions", false);
}

// Eliminar items del carrito
export async function removeCartItems({ commit, dispatch, rootState }, item) {
  await $http.delete(`/api/items-carrito/${item.id}`).then(async response => {
    await commit("deleteItemCart", item);
    await dispatch("fetchCart", {
      contact_id: rootState.login.user_b2b.contact_id,
      branchoffice_id: rootState.login.user_b2b.id,
      user_id: rootState.login.user_logued.id
    });
  });
}

// Crear nuevo carrito
export async function newCart({ commit, state }, cart) {
  await $http
    .post("/api/carrito", cart)
    .then(response => {
      commit("setCart", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("cartError", error.message);
    });
}

// Obtener los datos del carrito
export async function fetchCart({ commit, state }, parameters) {
  await $http
    .get(
      `/api/carrito/${parameters.contact_id}/${parameters.branchoffice_id}/${parameters.user_id}`
    )
    .then(async response => {
      await commit("setCart", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("cartError", error.message);
    });
}

/* Agregar items relacionados al carrito */
export async function addToCartFinalize(
  { rootState, dispatch },
  { id, cant, price, line_id, subline_id, brand_id, percentage_iva_sale }
) {
  let product = {
    document_id: rootState.cart.cart.id,
    product_id: id,
    seller: rootState.login.user_b2b.seller_id,
    line_id: line_id,
    subline_id: subline_id,
    brand_id: brand_id,
    applies_inventory: true,
    quantity: cant,
    unit_value_before_taxes: price.price,
    iva_percentage: percentage_iva_sale,
    discount_value: Math.round((price.price * price.percentage) / 100)
  };

  await dispatch("addCartItems", product);
}

/* Agregar información del carrito */
export async function addInformation({ commit, state }, information) {
  await $http
    .post("/api/informacion-carrito", information)
    .then(response => {
      commit("setInformation", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("cartError", error.message);
    });
}

/* Finalizar proceso de pedido */
export async function finalizeOrderAction(
  { dispatch, rootState, commit, state },
  cart
) {
  // Validamos el monto mini por pedido
  let minimum_order_amount =
    rootState.login.user_b2b.branchoffice.minimum_order_amount;
  if (minimum_order_amount) {
    if (cart.total_value < minimum_order_amount) {
      commit(
        "cartError",
        "El valor total del pedido es inferior al monto minimo."
      );

      return;
    }
  }

  let data = {
    id: cart.id,
    observation: cart.observation,
    affects_prefix_document: cart.affects_prefix_document,
    affects_number_document: cart.affects_number_document
  };

  let vouchertype_id = await dispatch("getVouchertype", {code:'001', branchoffice: rootState.login.user_b2b.branchoffice_id});

  await $http
    .post(`/api/finalizar-pedido`, data)
    .then(async response => {
      // Preparar datos obligatorios del carrito
      let cart = {
        warehouse_id: rootState.login.user_b2b.id,
        vouchertype_id: vouchertype_id,
        prefix: "Pd",
        contact_id: rootState.login.user_b2b.contact_id,
        inventory_operation: "pedido",
        in_progress: true,
        pay_condition: rootState.login.user_b2b.pay_condition,
        send_point: rootState.login.user_b2b.send_point,
        seller_id: rootState.login.user_b2b.seller_id,
        user_id: rootState.login.user_logued.id,
        documents_information: {
          price_list: rootState.login.user_b2b.price_list_id
        }
      };
      // Crear carrito
      await dispatch("newCart", cart);

      commit("setLastCart", response.data.data);
      commit("cartError", "");
    })
    .catch(error => {
      console.error(error);
      Notification.error({
        title: "Error!",
        message: "Error al finalizar el pedido",
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit(
        "cartError",
        "Estimado Cliente. <br> Estamos presentando algunos problemas de comunicación, por favor intentelo nuevamente."
      );
    });
}

export async function getVouchertype({commit, state},voucherType){
  var params = {
    code_voucher_type:voucherType.code,
    branchoffice: voucherType.branchoffice
  }
  var voucherType_id
  await $http
    .get(`/api/tipo-comprobante`, {params})
    .then(response => {
      if (response.data) {
        voucherType_id = response.data.id
      }
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("cartError", error.message);
    });
  return voucherType_id
}

export async function getModelBycode({commit, state},code){
  var model_id
  await $http
    .get(`/api/modelo-por-codigo/${code}`)
    .then(response => {
      if (response.data.data) {
        model_id = response.data.data.id
      }
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("cartError", error.message);
    });
  return model_id
}


export async function getPdf({ commit, state }, id) {
  await $http
    .get(`/api/imprimir-pedido/${id}`)
    .then(response => {
      commit("setPdf", response.data.data.pdf);
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("cartError", error.message);
    });
}

export async function getCustomerCategoty({ commit, state }, contact_customer_id) {
  let contactCustomer = null
  await $http
    .get(`/api/categoria-cliente-prospecto/${contact_customer_id}`)
    .then(response => {
      contactCustomer = response.data.data
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
      commit("cartError", error.message);
    });
  return contactCustomer
}
export async function downloadDocument({ state }) {
  let name = state.pdf.split("/").pop();
  // let extension = name.split('.').pop()
  await $http({
    url: state.pdf,
    method: "GET",
    responseType: "blob" // important
  })
    .then(response => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", name);
      document.body.appendChild(link);
      link.click();
    })
    .catch(error => {
      Notification.error({
        title: "Error!",
        message: error.message,
        type: "error",
        duration: 3000,
        customClass: "notification-box"
      });
    });
}

/* Agregar items al carrito */
export async function addToCart({ rootState, dispatch, state }, product) {
  let addProduct = {
    product_id: product.product_id ? product.product_id : product.id,
    seller_id: rootState.login.user_b2b.seller_id,
    contact_id: rootState.login.user_b2b.contact.id,
    quantity: product.quantity,
    original_quantity: product.quantity,
    promotion_id: product.promotion_id,
    is_benefit: product.is_benefit ? product.is_benefit : false,
    discount_value: product.discount_value,
    unit_value_before_taxes: product.unit_value_before_taxes
      ? product.unit_value_before_taxes
      : undefined
  };
  let vouchertype_id = await dispatch("getVouchertype", {code:'001', branchoffice: rootState.login.user_b2b.branchoffice_id});

  // Validar si el carrito existe
  if (!state.cart.id) {
    // Preparar datos obligatorios del carrito
    let cart = {
      warehouse_id: rootState.login.user_b2b.id,
      vouchertype_id: vouchertype_id,
      prefix: "Pd",
      contact_id: rootState.login.user_b2b.contact_id,
      inventory_operation: "pedido",
      in_progress: true,
      pay_condition: rootState.login.user_b2b.pay_condition,
      send_point: rootState.login.user_b2b.send_point,
      seller_id: rootState.login.user_b2b.seller_id,
      user_id: rootState.login.user_logued.id,
      documents_information: {
        price_list: rootState.login.user_b2b.price_list_id
      }
    };

    // Crear carrito
    await dispatch("newCart", cart);
  }

  // Asignar el id del carrito
  addProduct.document_id = rootState.cart.cart.id;
  // Agregar producto al carrito
  if (addProduct.quantity > 0) {
    await dispatch("addCartItems", addProduct);
  }
}

export async function checkCart({ rootState, dispatch, state }) {
  // Validar si el carrito existe
  let vouchertype_id = await dispatch("getVouchertype", {code:'001', branchoffice: rootState.login.user_b2b.branchoffice_id});

  if (!state.cart.id) {
    // Preparar datos obligatorios del carrito
    let cart = {
      warehouse_id: rootState.login.user_b2b.id,
      vouchertype_id: vouchertype_id,
      prefix: "Pd",
      contact_id: rootState.login.user_b2b.contact_id,
      inventory_operation: "pedido",
      in_progress: true,
      pay_condition: rootState.login.user_b2b.pay_condition,
      send_point: rootState.login.user_b2b.send_point,
      seller_id: rootState.login.user_b2b.seller_id,
      user_id: rootState.login.user_logued.id,
      documents_information: {
        price_list: rootState.login.user_b2b.price_list_id
      }
    };

    // Crear carrito
    await dispatch("newCart", cart);
  }
}

/* Agregar productos de promoción al carrito */
//Obsoleto
export async function addPromotionToCart(
  { state, rootState, dispatch },
  product
) {
  let productAdd = {
    product_id: product.id,
    promotion_id: product.promotion,
    seller: rootState.login.user_b2b.seller_id,
    applies_inventory: false,
    quantity: product.quantity,
    unit_value_before_taxes: product.price.price,
    discount_value: Math.round((product.price.price * product.discount) / 100),
    original_quantity: product.quantity,
    line_id: product.line_id,
    brand_id: product.brand_id,
    subbrand_id: product.subbrand_id,
    subline_id: product.subline_id,
    category_id: product.category_id
  };
  let vouchertype_id = await dispatch("getVouchertype", {code:'001', branchoffice: rootState.login.user_b2b.branchoffice_id});

  // Validar si el carrito existe
  if (rootState.cart.cart.id == null || rootState.cart.cart.id == "") {
    // Preparar datos obligatorios del carrito
    let cart = {
      warehouse_id: rootState.login.user_b2b.id,
      vouchertype_id: vouchertype_id,
      prefix: "Pd",
      contact_id: rootState.login.user_b2b.contact_id,
      inventory_operation: "pedido",
      in_progress: true,
      pay_condition: rootState.login.user_b2b.pay_condition,
      send_point: rootState.login.user_b2b.send_point,
      seller_id: rootState.login.user_b2b.seller_id,
      user_id: rootState.login.user_logued.id,
      documents_information: {
        price_list: rootState.login.user_b2b.price_list_id
      }
    };

    // Crear carrito
    await dispatch("newCart", cart);

    // Obtener el id del carrito recién creado
    productAdd.document_id = rootState.cart.cart.id;

    // Agregar producto al carrito
    await dispatch("addCartItems", productAdd);
  } else {
    // Asignar el id del carrito
    productAdd.document_id = rootState.cart.cart.id;

    // Agregar producto al carrito
    await dispatch("addCartItems", productAdd);
  }
}

/** Aplicar descuento a la promoción cuando se cumple */
export async function applyBenefit(
  { state, commit, dispatch, rootState },
  promotion
) {
  // Agregar el beneficio al carrito o actualizar las unidades del beneficio
  if (promotion.comply && promotion.specific_product != null) {
    let product = {
      id: promotion.specific_product.id,
      quantity: promotion.specific_product.quantity,
      promotion_id: promotion.id,
      is_benefit: true
    };
    await dispatch("addToCart", product);
  }
}

/**
 * Obtiene el modelo de acuerdo al id del mismo
 * @param commit
 * @param state
 * @param requestParameters
 * @returns {Promise<void>}
 */
export async function fetchModel({ commit, state }, model_id) {
  await $http
    .get("/api/modelos/" + model_id)
    .then(response => {
      commit("setSelectedModel", response.data.data);
    })
    .catch(error => {
      Notification.error({
        title: "¡Error!",
        message: error.message,
        type: "error",
        duration: 1500,
        customClass: "notification-box"
      });
    });
}
