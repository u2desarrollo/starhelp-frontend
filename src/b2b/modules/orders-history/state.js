export default {
  orders: [],
  transporter: [],
  products: [],
  status: [],
  error: false,
  errorMessage: '',
  states: [],
  params: {
    filter_date: [],
    filter_status: [],
    paginate: true,
    sortBy: 'id',
    sort: 'DESC',
    warehouse: 0
  },
}
