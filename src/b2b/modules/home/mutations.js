import { defaultSelectedBanner } from './state'

export function setBanners(state, bannerB2b) {
    state.banners = bannerB2b.map(p => {
        return {key:p.id, big: p.big_banner, small:p.small_banner}
    })
}
export function setBrands(state, brands) {
    state.brands = brands.map(p => {
        return {value:p.id, brand: p.description, img:p.image}
    })
}

export function setProds(state, prods) {
    state.prods = prods
}

/* export function setBanner (state, banner) {
	state.selectedBanner = banner
}

export function bannerError (state, payload) {
	state.error = true
	state.errorMessage = payload
	state.banners = []
}

export function setLines(state, lines) {
	state.lines = lines.map(p => {
		return {value: p.id, label: p.line_description}
	})
}


 */
