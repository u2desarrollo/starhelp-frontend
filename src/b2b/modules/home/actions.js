import $http from "../../../axios";
import { Notification } from "element-ui";

export async function fetchBanner(
  { dispatch, state, rootState, commit },
  requestParameters = { params: state.params }
) {
  if (!state.banners.length) {
    await dispatch(
      "bannerB2b/listBanner",
      requestParameters /* {params:{idParameter:19, idLine:21}} */,
      { root: true }
    );
    commit("setBanners", rootState.bannerB2b.banners);
  }
}
export async function fetchBrand({ dispatch, state, rootState, commit }) {
  if (!state.brands.length) {
    await dispatch(
      "brands/fetchBrands",
      { params: { paginate: false, lines: state.params.lines } },
      { root: true }
    );
    commit("setBrands", rootState.brands.brands);
  }
}
export async function fetchProducts({ dispatch, state, rootState, commit }) {
  if (!state.products.length) {
    await dispatch(
      "catalog/fetchProducts",
      { params: { paginate: true, lines: state.params.lines } },
      { root: true }
    );
    commit("setProds", rootState.catalog.products);
  }
}

/* export async function fetchPromotions({dispatch, state, rootState, commit}) {

    let requestParameters = {
        params:{
            contact_warehouse_id:rootState.login.user_b2b.id
        }
    }

    await $http.get(`/api/productos-promociones`, requestParameters)
    .then((response) => {
        commit('setProds', response.data.data)
    })
    .catch((error) => {

    })
} */
