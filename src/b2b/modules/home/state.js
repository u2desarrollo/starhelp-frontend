export const defaultSelectedHome = () =>{
    return{
        search:'',
        reference:''
    }
}
export default {
    selectedHome: defaultSelectedHome(),
    banners: [],
    brands:[],
    products:[],
	error: false,
    errorMessage: '',
    prods: [],

    params: {
        idParameter:20,
       // lines:''
    }
}
