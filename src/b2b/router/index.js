export default [
  {
    path: '/b2b/inicio',
    component: resolve => require(['./../default-b2b.vue'], resolve),
    children: [
      {
        path: '',
        component: resolve => require(['./../views/home.vue'], resolve),
        meta: {
          title: 'Inicio',
          breadcrumb: [{
            text: ' Inicio',
            href: '/',
          }]
        }
      },
      {
        path: '/b2b/tracking-pedidos',
        component: resolve => require(['./../views/orders-history.vue'], resolve),
        meta: {
          title: 'Historial de Pedidos',
          breadcrumb: [{
            text: ' Inicio',
            href: '/',
          }]
        }
      },
      {
        path: '/b2b/cartera',
        component: resolve => require(['./../views/receivable.vue'], resolve),
        meta: {
          title: 'Cartera',
          breadcrumb: [{
            text: ' Inicio',
            href: '/',
          }]
        }
      },
      {
        path: '/b2b/catalogo',
        component: resolve => require(['./../views/catalog.vue'], resolve),
        meta: {
          title: 'Catálogo',
          breadcrumb: [{
            text: ' Inicio',
            href: '/',
          }]
        }
      },
      {
        path: '/b2b/productos-relativos',
        component: resolve => require(['./../views/relative-products-form.vue'], resolve),
        meta: {
          title: 'Catálogo',
          breadcrumb: [{
            text: ' Inicio',
            href: '/',
          }]
        }
      },
      {
        path: '/b2b/:id/detalle',
        component: resolve => require(['./../views/product-detail.vue'], resolve),
        meta: {
          title: 'Detalle de Producto',
          breadcrumb: [{
            text: ' Inicio',
            href: '/',
          }]
        }
      },
      {
        path: "/b2b/vendedor/pedidos/nuevo",
        component: resolve => require(["../views/redirection"], resolve),
        meta: {
          title: "",
          breadcrumb: []
        }
      },
      {
        path: "/b2b/portal-clientes",
        component: resolve => require(["../views/redirection"], resolve),
        meta: {
          title: "",
          breadcrumb: []
        }
      },
      {
        path: '/b2b/asistente-reposicion',
        component: resolve => require(['./../views/replacement-assistant.vue'], resolve),
        meta: {
          title: 'Asistente de Reposición',
          breadcrumb: [{
            text: ' Inicio',
            href: '/',
          }]
        }

      }]
  }
]
