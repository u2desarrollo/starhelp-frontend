import Vue from 'vue'

import VueColumnsResizable from 'vue-columns-resizable'

Vue.use(VueColumnsResizable)

export default VueColumnsResizable
