import Vue from 'vue'

import Moment from 'moment'

const moment = Vue.prototype.moment = Moment

export default moment
