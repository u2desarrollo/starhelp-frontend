import Vue from 'vue'

import ToggleSwitch from 'vuejs-toggle-switch'

const toggleSwitch = Vue.use(ToggleSwitch)

export default toggleSwitch
