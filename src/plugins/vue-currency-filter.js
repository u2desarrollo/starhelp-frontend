import Vue from "vue";

import VueCurrencyFilter from "vue-currency-filter";

const currencyFilter = Vue.use(VueCurrencyFilter, {
  symbol: "$", // El símbolo, por ejemplo €
  thousandsSeparator: ",", // Separador de miles
  fractionCount: 0, // ¿Cuántos decimales mostrar?
  fractionSeparator: ".", // Separador de decimales
  symbolPosition: "front", // Posición del símbolo. Puede ser al inicio ('front') o al final ('') es decir, si queremos que sea al final, en lugar de front ponemos una cadena vacía ''
  symbolSpacing: true // Indica si debe poner un espacio entre el símbolo y la cantidad
});

export default currencyFilter;
