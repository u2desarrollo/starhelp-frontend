import bootstrapVue from './bootstrap-vue'
import elementUi from './element-ui'
import moment from './moment'
import toggleSwitch from './toggle-switch'
import veeValidate from './vee-vaidate'
import vueCurrencyFilter from './vue-currency-filter'
import vueColumnsResizable from './vue-columns-resizable'

export default{
  namespaced: true,
  bootstrapVue,
  elementUi,
  moment,
  toggleSwitch,
  veeValidate,
  vueCurrencyFilter,
  vueColumnsResizable
}
