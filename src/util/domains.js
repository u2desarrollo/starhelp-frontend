/**
 * Suscriptores
 */
const subscribers = [
  {
    subscriber: 'Desarrollo',
    domain: 'http://localhost:8080',
    api_domain: 'http://localhost:8085',
    logo: '/static/subscribers_logos/u2.jpg'
  },
  {
    subscriber: 'Desarrollo',
    domain: 'http://127.0.0.1:8080',
    api_domain: 'http://127.0.0.1:8085',
    logo: '/static/subscribers_logos/u2.jpg'
  },
  {
    subscriber: 'Colsaisa',
    domain: 'https://colsaisa.starcommerce.co',
    api_domain: 'https://apicolsaisa.starcommerce.co',
    logo: '/static/subscribers_logos/colsaisa.png'
  },
  {
    subscriber: 'Pruebas Colsaisa',
    domain: 'https://pruebascolsaisa.starcommerce.co',
    api_domain: 'https://pruebasapicolsaisa.starcommerce.co',
    logo: '/static/subscribers_logos/colsaisa.png'
  },
  {
    subscriber: 'Colrecambios',
    domain: 'https://colrecambios.starcommerce.co',
    api_domain: 'https://apicolrecambios.starcommerce.co',
    logo: '/static/subscribers_logos/colrecambios.png'
  },
  {
    subscriber: 'Pruebas Colrecambios',
    domain: 'https://pruebascolrecambios.starcommerce.co',
    api_domain: 'https://pruebasapicolrecambios.starcommerce.co',
    logo: '/static/subscribers_logos/colrecambios.png'
  },
  {
    subscriber: 'Xinetix',
    domain: 'https://xinetix.starcommerce.co',
    api_domain: 'https://apixinetix.starcommerce.co',
    logo: '/static/subscribers_logos/xinetix.png'
  },
  {
    subscriber: 'Pruebas Xinetix',
    domain: 'https://pruebasxinetix.starcommerce.co',
    api_domain: 'https://pruebasapixinetix.starcommerce.co',
    logo: '/static/subscribers_logos/xinetix.png'
  }
]

/**
 * Obtener el suscriptor
 * @returns Object Contiene la información del suscriptor
 * @author Jhon García
 */
export function getSubscriber() {
  const domain = document.location.origin

  return subscribers.find(subscriber => subscriber.domain === domain);
}

/**
 * Obtener el dominio actual
 * @returns {*} Contiene el dominio actual
 * @author Jhon García
 */
export function getDomain() {
  return document.location.origin
}

/**
 * Determinar el dominio del api, de acuerdo al dominio del front
 * @returns {string|null} Contiene el dominio del api o null si no existe
 * @author Jhon García
 */
export function getApiDomain() {
  let subscriber = getSubscriber();

  if (subscriber) {
    return subscriber.api_domain
  }

  return null
}

/**
 * Determinar el dominio del api, de acuerdo al dominio del front
 * @returns {string|null} Contiene el dominio del api o null si no existe
 * @author Jhon García
 */
export function getLogoSubscriber() {
  let subscriber = getSubscriber();

  if (subscriber) {
    return subscriber.logo
  }

  return null
}
