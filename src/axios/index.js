import axios from 'axios'
import { getApiDomain } from "../util/domains";

const $http = axios.create({});

const api_domain = getApiDomain()

$http.interceptors.request.use(req => {
    req.url = api_domain + req.url

    let user = JSON.parse(localStorage.getItem('user')) || [];

    if (user && user.token) {
        req.headers.Authorization = `Bearer ${user.token}`
    } else {
        req.headers.Authorization = ''
    }

    return req

}, error => {
    console.log(`interceptor request ${error}`)
})

$http.interceptors.response.use(resp => resp, error => {

    let originalReq = error.config

    if (error.response.status === 401 && !originalReq._retry) {

        if (error.response.data.message === "TOKEN_INVALID") {
            window.location.href = "/#/login"
            localStorage.removeItem('user')
            localStorage.removeItem('user_b2b')
        }

        if (error.response.data.message === "TOKEN_EXPIRED") {
            window.location.href = "/#/salvapantallas"
            localStorage.removeItem('user')
            localStorage.removeItem('user_b2b')
        }

    }

    return Promise.reject(error)
})


export default $http;
