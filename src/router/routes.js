import b2bRoutes from "../b2b/router";
import reports_routes from "../reports-aggrid/router";
import $http from "../axios";
import routesStarHelp from "./routes-starhelp";

const baseRoutes = [
  {
    path: "/",
    redirect: "/login"
  },
  {
    path: "/inicio",
    component: resolve => require(["../views/default.vue"], resolve),
    children: [
      ...routesStarHelp,
      {
        path: "",
        component: resolve =>
          require(["../components/pages/index.vue"], resolve),
        meta: {
          title: "Dashboard",
          breadcrumb: [
            {
              text: " Dashboard 1",
              href: "/"
            }
          ]
        }
      },
      //Kevin Galindo Test
      {
        path: "/PageTest",
        component: resolve =>
          require(["../views/page_test/Index.vue"], resolve),
        meta: {
          title: "PageTest",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "PageTest",
              href: "#/PageTest"
            }
          ]
        }
      },
      // roles y permisos
      {
        path: "/roles-permisos",
        component: resolve => require(["../views/roles/index.vue"], resolve),
        meta: {
          title: "Permisos de Usuario",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Roles",
              href: "#/roles-permisos"
            }
          ]
        }
      },
      {
        path: "/rol/crear",
        component: resolve => require(["../views/roles/form.vue"], resolve),
        meta: {
          title: "Permisos de Usuario",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Roles",
              href: "#/rol/crear"
            }
          ]
        }
      },
      {
        path: "/rol/:id/editar",
        component: resolve => require(["../views/roles/form.vue"], resolve),
        meta: {
          title: "Permisos de Usuario",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Roles",
              href: "#/rol/:id/editar"
            }
          ]
        }
      },
      {
        path: "/rol/:idv/ver",
        component: resolve => require(["../views/roles/form.vue"], resolve),
        meta: {
          title: "Permisos de Usuario",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Roles",
              href: "#/rol/:idv/ver"
            }
          ]
        }
      },

      //   envio automatico de la cartera
      {
        path: "/envio-cartera",
        component: resolve =>
          require(["../views/automatic-portfolio-delivery/index.vue"], resolve),
        meta: {
          title: "Envio Automatico de Cartera",
          permission: "Informe Automatico Cartera",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Envio Automatico de Cartera",
              href: "#/envio-cartera"
            }
          ]
        }
      },

      //   crear  automatico de la cartera
      {
        path: "/nuevo-envio-cartera",
        component: resolve =>
          require([
            "../views/automatic-portfolio-delivery/maintenance.vue"
          ], resolve),
        meta: {
          title: "Crear Configuracón Envio Automatico de Cartera",
          permission: "Informe Automatico Cartera",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Licencias",
              href: "#/licencias"
            }
          ]
        }
      },
      {
        path: "/licencias",
        component: resolve => require(["../views/licenses/index.vue"], resolve),
        meta: {
          title: "Licencias",
          permission: "Licencias",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Licencias",
              href: "#/licencias"
            }
          ]
        }
      },
      {
        path: "/impuestos",
        component: resolve => require(["../views/taxes/index.vue"], resolve),
        meta: {
          title: "Impuestos",
          permission: "Impuestos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Impuestos",
              href: "#/impuestos"
            }
          ]
        }
      },
      {
        path: "/impuestos/crear",
        component: resolve => require(["../views/taxes/tax-form.vue"], resolve),
        meta: {
          title: "Impuestos",
          permission: "Impuestos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Impuestos",
              href: "#/impuestos"
            }
          ]
        }
      },
      {
        path: "/impuestos/:id/editar",
        component: resolve => require(["../views/taxes/tax-form.vue"], resolve),
        meta: {
          title: "Impuestos",
          permission: "Impuestos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Impuestos",
              href: "#/impuestos"
            }
          ]
        }
      },
      {
        path: "/impuestos/:idv/ver",
        component: resolve => require(["../views/taxes/tax-form.vue"], resolve),
        meta: {
          title: "Impuestos",
          permission: "Impuestos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Impuestos",
              href: "#/impuestos"
            }
          ]
        }
      },
      //   promociones
      {
        path: "/promociones",
        component: resolve =>
          require(["../views/promotions/index.vue"], resolve),
        meta: {
          title: "Promociones",
          permission: "Promociones",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Promociones",
              href: "#/promociones"
            }
          ]
        }
      },
      {
        path: "/promociones/crear",
        component: resolve =>
          require(["../views/promotions/promotion-form.vue"], resolve),
        meta: {
          title: "Promociones",
          permission: "Promociones",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Promociones",
              href: "#/promociones"
            }
          ]
        }
      },
      {
        path: "/promociones/:id/editar",
        component: resolve =>
          require(["../views/promotions/promotion-form.vue"], resolve),
        meta: {
          title: "Promociones",
          permission: "Promociones",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Promociones",
              href: "#/promociones"
            }
          ]
        }
      },
      {
        path: "/promociones/:idv/ver",
        component: resolve =>
          require(["../views/promotions/promotion-form.vue"], resolve),
        meta: {
          title: "Promociones",
          permission: "Promociones",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Promociones",
              href: "#/promociones"
            }
          ]
        }
      },
      {
        path: "/licencias/crear",
        component: resolve =>
          require(["../views/licenses/maintenance.vue"], resolve),
        meta: {
          title: "Licencias",
          permission: "Licencias",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Licencias",
              href: "#/licencias"
            },
            {
              text: "Crear Licencia",
              href: "#/licencias/crear"
            }
          ]
        }
      },
      {
        path: "/licencias/:id/editar",
        component: resolve =>
          require(["../views/licenses/maintenance.vue"], resolve),
        meta: {
          title: "Licencias",
          permission: "Licencias",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Licencias",
              href: "#/licencias"
            },
            {
              text: "Editar Licencia",
              href: "#/licencias/:id/editar"
            }
          ]
        }
      },
      {
        path: "/licencias/:idv/ver",
        component: resolve =>
          require(["../views/licenses/maintenance.vue"], resolve),
        meta: {
          title: "Licencias",
          permission: "Licencias",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Licencias",
              href: "#/licencias"
            },
            {
              text: "Detalle Licencia",
              href: "#/licencias/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/suscriptores",
        component: resolve =>
          require(["../views/subscribers/index.vue"], resolve),
        meta: {
          title: "Suscriptores",
          permission: "Suscriptores",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Suscriptores",
              href: "#/suscriptores"
            }
          ]
        }
      },
      {
        path: "/suscriptores/crear",
        component: resolve =>
          require(["../views/subscribers/wizarFormSubscriber.vue"], resolve),
        meta: {
          title: "Suscriptores",
          permission: "Suscriptores",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Suscriptores",
              href: "#/suscriptores"
            },
            {
              text: "Mantenimiento Suscriptores",
              href: "#/suscriptores/crear"
            }
          ]
        }
      },
      {
        path: "/suscriptores/:id/editar",
        component: resolve =>
          require(["../views/subscribers/wizarFormSubscriber.vue"], resolve),
        meta: {
          title: "Suscriptores",
          permission: "Suscriptores",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Suscriptores",
              href: "#/suscriptores"
            },
            {
              text: "Mantenimiento Suscriptores",
              href: "#/suscriptores/:id/editar"
            }
          ]
        }
      },
      {
        path: "/suscriptores/:idv/ver",
        component: resolve =>
          require(["../views/subscribers/wizarFormSubscriber.vue"], resolve),
        meta: {
          title: "Suscriptores",
          permission: "Suscriptores",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Suscriptores",
              href: "#/suscriptores"
            },
            {
              text: "Suscriptores",
              href: "#/suscriptores/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/tablas-parametros",
        component: resolve =>
          require(["../views/param-tables/index.vue"], resolve),
        meta: {
          title: "Tablas de parámetros",
          permission: "Tablas de parámetro",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Tablas de parametros",
              href: "#/tablas-parametros"
            }
          ]
        }
      },
      {
        path: "/tablas-parametros/crear",
        component: resolve =>
          require(["../views/param-tables/maintenance.vue"], resolve),
        meta: {
          title: "Tablas de parámetros",
          permission: "Tablas de parámetro",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Tablas de parametros",
              href: "#/tablas-parametros"
            },
            {
              text: "Crear Tabla de Parametros",
              href: "#/tablas-parametros/crear"
            }
          ]
        }
      },
      {
        path: "/contabilizacion-operaciones-inventario",
        component: resolve =>
          require([
            "../views/accounting-inventory-movements/Index.vue"
          ], resolve),
        meta: {
          title: "Contabilizacion Operaciones Inventario",
          permission: "",
          breadcrumb: []
        }
      },
      {
        path: "/tablas-parametros/:id/editar",
        component: resolve =>
          require(["../views/param-tables/maintenance.vue"], resolve),
        meta: {
          title: "Tablas de parámetros",
          permission: "Tablas de parámetro",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Tablas de parametros",
              href: "#/tablas-parametros"
            },
            {
              text: "Editar Tabla de Parametros",
              href: "#/tablas-parametros/:id/editar"
            }
          ]
        }
      },
      {
        path: "/tablas-parametros/:idv/ver",
        component: resolve =>
          require(["../views/param-tables/maintenance.vue"], resolve),
        meta: {
          title: "Tablas de parámetros",
          permission: "Tablas de parámetro",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Tablas de parametros",
              href: "#/tablas-parametros"
            },
            {
              text: "Detalle Tabla de Parametros",
              href: "#/tablas-parametros/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/tipos-de-comprobantes",
        component: resolve =>
          require(["../views/vouchers-types/index.vue"], resolve),
        meta: {
          title: "Tipos de comprobantes",
          permission: "Tipos de comprobantes",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Tipos de comprobantes",
              href: "#/tipos-de-comprobantes"
            }
          ]
        }
      },
      {
        path: "/tipos-de-comprobantes/crear",
        component: resolve =>
          require(["../views/vouchers-types/maintenance.vue"], resolve),
        meta: {
          title: "Tipos de comprobantes",
          permission: "Tipos de comprobantes",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Tipos de comprobantes",
              href: "#/tipos-de-comprobantes"
            },
            {
              text: "Crear Tipo de comprobante",
              href: "#/tipos-de-comprobantes/crear"
            }
          ]
        }
      },
      {
        path: "/tipos-de-comprobantes/:id/editar",
        component: resolve =>
          require(["../views/vouchers-types/maintenance.vue"], resolve),
        meta: {
          title: "Tipos de comprobantes",
          permission: "Tipos de comprobantes",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Tipos de comprobantes",
              href: "#/tipos-de-comprobantes"
            },
            {
              text: "Editar Tipo de comprobante",
              href: "#/tipos-de-comprobantes/:id/editar"
            }
          ]
        }
      },
      {
        path: "/tipos-de-comprobantes/:idv/ver",
        component: resolve =>
          require(["../views/vouchers-types/maintenance.vue"], resolve),
        meta: {
          title: "Tipos de comprobantes",
          permission: "Tipos de comprobantes",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Tipos de comprobantes",
              href: "#/tipos-de-comprobantes"
            },
            {
              text: "Detalle Tipo de Comprobante",
              href: "#/tipos-de-comprobantes/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/terceros",
        // component: resolve => require(['../components/pages/gmaps.vue'], resolve),
        component: resolve => require(["../views/contacts/index.vue"], resolve),
        meta: {
          title: "Terceros",
          permission: "Terceros",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Terceros",
              href: "#/terceros"
            }
          ]
        }
      },
      {
        path: "/terceros/crear",
        component: resolve =>
          require(["../views/contacts/maintenance.vue"], resolve),
        meta: {
          title: "Terceros",
          permission: "Terceros",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Terceros",
              href: "#/terceros"
            }
          ]
        }
      },
      {
        path: "/terceros/:id/editar",
        component: resolve =>
          require(["../views/contacts/maintenance.vue"], resolve),
        meta: {
          title: "Terceros",
          permission: "Terceros",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Terceros",
              href: "#/terceros"
            },
            {
              text: "Terceros",
              href: "#/terceros/:id/editar"
            }
          ]
        }
      },
      {
        path: "/terceros/:idv/ver",
        component: resolve =>
          require(["../views/contacts/maintenance.vue"], resolve),
        meta: {
          title: "Terceros",
          permission: "Terceros",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Terceros",
              href: "#/terceros"
            },
            {
              text: "Terceros",
              href: "#/terceros/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/proyecto",
        component: resolve => require(["../views/projects/index.vue"], resolve),
        meta: {
          title: "Proyecto",
          permission: "Proyecto",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Proyecto",
              href: "#/proyecto"
            }
          ]
        }
      },
      {
        path: "/proyecto/crear",
        component: resolve =>
          require(["../views/projects/maintenance.vue"], resolve),
        meta: {
          title: "Proyecto",
          permission: "Proyecto",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Proyecto",
              href: "#/proyecto"
            }
          ]
        }
      },
      {
        path: "/proyecto/:id/editar",
        component: resolve =>
          require(["../views/projects/maintenance.vue"], resolve),
        meta: {
          title: "Proyecto",
          permission: "Proyecto",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Proyecto",
              href: "#/proyecto"
            }
          ]
        }
      },
      {
        path: "/proyecto/:idv/ver",
        component: resolve =>
          require(["../views/projects/maintenance.vue"], resolve),
        meta: {
          title: "Proyecto",
          permission: "Proyecto",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Proyecto",
              href: "#/proyecto"
            }
          ]
        }
      },
      {
        path: "/sedes",
        component: resolve =>
          require(["../views/branch-offices/index.vue"], resolve),
        meta: {
          title: "Sedes",
          permission: "Sedes",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sedes",
              href: "#/sedes"
            }
          ]
        }
      },
      {
        path: "/sedes/crear",
        component: resolve =>
          require(["../views/branch-offices/maintenance.vue"], resolve),
        meta: {
          title: "Sedes",
          permission: "Sedes",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sedes",
              href: "#/sedes"
            }
          ]
        }
      },
      {
        path: "/sedes/:id/editar",
        component: resolve =>
          require(["../views/branch-offices/maintenance.vue"], resolve),
        meta: {
          title: "Sedes",
          permission: "Sedes",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sedes",
              href: "#/sedes"
            },
            {
              text: "Sedes",
              href: "#/sedes/:id/editar"
            }
          ]
        }
      },
      {
        path: "/sedes/:idv/ver",
        component: resolve =>
          require(["../views/branch-offices/maintenance.vue"], resolve),
        meta: {
          title: "Sedes",
          permission: "Sedes",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sedes",
              href: "#/sedes"
            },
            {
              text: "Sedes",
              href: "#/sedes/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/lineas",
        component: resolve => require(["../views/lines/index.vue"], resolve),
        meta: {
          title: "Lineas",
          permission: "Lineas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Lineas",
              href: "#/lineas"
            }
          ]
        }
      },
      {
        path: "/lineas/crear",
        component: resolve =>
          require(["../views/lines/maintenance.vue"], resolve),
        meta: {
          title: "Lineas",
          permission: "Lineas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Lineas",
              href: "#/lineas"
            }
          ]
        }
      },
      {
        path: "/lineas/:id/editar",
        component: resolve =>
          require(["../views/lines/maintenance.vue"], resolve),
        meta: {
          title: "Lineas",
          permission: "Lineas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Lineas",
              href: "#/lineas"
            },
            {
              text: "Lineas",
              href: "#/lineas/:id/editar"
            }
          ]
        }
      },
      {
        path: "/lineas/:idv/ver",
        component: resolve =>
          require(["../views/lines/maintenance.vue"], resolve),
        meta: {
          title: "Lineas",
          permission: "Lineas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Lineas",
              href: "#/lineas"
            },
            {
              text: "Lineas",
              href: "#/lineas/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/usuarios",
        component: resolve => require(["../views/users/index.vue"], resolve),
        meta: {
          title: "Usuarios",
          permission: "Usuarios",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Usuarios",
              href: "#/usuarios"
            }
          ]
        }
      },
      {
        path: "/usuarios/crear",
        component: resolve =>
          require(["../views/users/maintenance.vue"], resolve),
        meta: {
          title: "Usuarios",
          permission: "Usuarios",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Usuarios",
              href: "#/licencias"
            },
            {
              text: "Crear Usuario",
              href: "usuarios/crear"
            }
          ]
        }
      },
      {
        path: "/usuarios/:id/editar",
        component: resolve =>
          require(["../views/users/maintenance.vue"], resolve),
        meta: {
          title: "Usuarios",
          permission: "Usuarios",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Usuarios",
              href: "#/usuarios"
            },
            {
              text: "Detalle Licencia",
              href: "#/usuarios/:id/editar"
            }
          ]
        }
      },
      {
        path: "/usuarios/:idv/ver",
        component: resolve =>
          require(["../views/users/maintenance.vue"], resolve),
        meta: {
          title: "Usuarios",
          permission: "Usuarios",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Usuarios",
              href: "#/usuarios"
            },
            {
              text: "Detalle Licencia",
              href: "#/usuarios/:idv/ver"
            }
          ]
        }
      },
      {
        path: "blank",
        component: resolve =>
          require(["../components/pages/blank.vue"], resolve),
        meta: {
          title: "Blank",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Extra Pages",
              href: "#"
            },
            {
              text: "Blank",
              href: "#/blank"
            }
          ]
        }
      },
      {
        path: "/marcas",
        component: resolve => require(["../views/brands/index.vue"], resolve),
        meta: {
          title: "Marcas",
          permission: "Marcas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Marcas",
              href: "#/marcas"
            }
          ]
        }
      },
      {
        path: "/marcas/crear",
        component: resolve =>
          require(["../views/brands/maintenance.vue"], resolve),
        meta: {
          title: "Marcas",
          permission: "Marcas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Marcas",
              href: "#/marcas"
            }
          ]
        }
      },
      {
        path: "/marcas/:id/editar",
        component: resolve =>
          require(["../views/brands/maintenance.vue"], resolve),
        meta: {
          title: "Marcas",
          permission: "Marcas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Marcas",
              href: "#/marcas"
            },
            {
              text: "Marcas",
              href: "#/marcas/:id/editar"
            }
          ]
        }
      },
      {
        path: "/marcas/:idv/ver",
        component: resolve =>
          require(["../views/brands/maintenance.vue"], resolve),
        meta: {
          title: "Marcas",
          permission: "Marcas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Marcas",
              href: "#/marcas"
            },
            {
              text: "Marcas",
              href: "#/marcas/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/categorias",
        component: resolve =>
          require(["../views/categories/index.vue"], resolve),
        meta: {
          title: "Categorias",
          permission: "Categorias",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Categorias",
              href: "#/categorias"
            }
          ]
        }
      },
      {
        path: "/categorias/crear",
        component: resolve =>
          require(["../views/categories/maintenance.vue"], resolve),
        meta: {
          title: "Categorias",
          permission: "Categorias",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Categorias",
              href: "#/categorias"
            }
          ]
        }
      },
      {
        path: "/categorias/:id/editar",
        component: resolve =>
          require(["../views/categories/maintenance.vue"], resolve),
        meta: {
          title: "Categorias",
          permission: "Categorias",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Categorias",
              href: "#/categorias"
            },
            {
              text: "Categorias",
              href: "#/categorias/:id/editar"
            }
          ]
        }
      },
      {
        path: "/categorias/:idv/ver",
        component: resolve =>
          require(["../views/categories/maintenance.vue"], resolve),
        meta: {
          title: "Categorias",
          permission: "Categorias",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Categorias",
              href: "#/categorias"
            },
            {
              text: "Categorias",
              href: "#/categorias/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/configuracion-menu",
        component: resolve =>
          require(["../views/menu/maintenance.vue"], resolve),
        meta: {
          title: "Configuración de menú",
          permission: "Configuración menú",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Configuración de menú",
              href: "#/configuracion-menu"
            }
          ]
        }
      },
      {
        path: "/banners",
        component: resolve =>
          require(["../views/banners_b2b/index.vue"], resolve),
        meta: {
          title: "Banners",
          permission: "Banners",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Banners",
              href: "#/banners"
            }
          ]
        }
      },
      {
        path: "/banners/crear",
        component: resolve =>
          require(["../views/banners_b2b/maintenance.vue"], resolve),
        meta: {
          title: "Banners",
          permission: "Banners",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Banners",
              href: "#/banners"
            }
          ]
        }
      },
      {
        path: "/documentos-inventario/:model_id",
        component: resolve =>
          require(["../views/base-documents/index.vue"], resolve),
        props: true,
        meta: {
          title: "",
          permission: "",
          function_execute: async model_id => {
            let model = JSON.parse(localStorage.getItem("selectedModel"));

            if (!model || +model.id !== +model_id) {
              await $http.get("/api/modelos/" + model_id).then(response => {
                model = response.data.data;
                localStorage.setItem("selectedModel", JSON.stringify(model));
              });
            }

            if (model.doc_base_management !== 2) {
              return model.code + " " + model.description;
            }
          },
          parameter: "model_id",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/documentos-inventario"
            }
          ]
        }
      },
      {
        path: "/documentos-por-autorizar/",
        component: resolve =>
          require(["../views/authorize_documents/index.vue"], resolve),
        props: true,
        meta: {
          title: "Documentos Por Autorizar",
          permission: "Banners",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Documentos Por Autorizar",
              href: "#/documentos-por-autorizar"
            }
          ]
        }
      },
      {
        path: "/autorizacion-de-documentos/",
        component: resolve =>
          require(["../views/authorize_documents/index.vue"], resolve),
        props: true,
        meta: {
          title: "Informe de documentos autorizados",
          permission: "Banners",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Informe de documentos autorizados",
              href: "#/autorizacion-de-documentos"
            }
          ]
        }
      },
      {
        path: "/banners/:id/editar",
        component: resolve =>
          require(["../views/banners_b2b/maintenance.vue"], resolve),
        meta: {
          title: "Banners",
          permission: "Banners",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Banners",
              href: "#/banners"
            },
            {
              text: "Banners",
              href: "#/banners/:id/editar"
            }
          ]
        }
      },
      {
        path: "/ecommerce",
        component: resolve =>
          require(["../views/ecommerce/maintenance.vue"], resolve),
        meta: {
          title: "Portal Clientes",
          permission: "Portal clientes",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Portal Clientes",
              href: "#/ecommerce"
            }
          ]
        }
      },
      {
        path: "/cliente-prospecto",
        component: resolve =>
          require(["../views/ecommerce/prospective-client.vue"], resolve),
        meta: {
          title: "Cliente Prospecto",
          permission: "Formulario Cliente Prospecto",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Cliente Prospecto",
              href: "#/cliente Prospecto"
            }
          ]
        }
      },
      {
        path: "/gestion-de-cobranzas",
        component: resolve =>
          require(["../views/payment-managment/paymentManagment.vue"], resolve),
        meta: {
          title: "Gestion De Cobranzas",
          permission: "Gestion De Cobranza",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Terceros",
              href: "#/terceros"
            }
          ]
        }
      },
      {
        path: "/ruta-vendedor",
        component: resolve =>
          require(["../views/ecommerce/coordinate-seller.vue"], resolve),
        meta: {
          title: "Ruta vendedor",
          permission: "Ruta vendedor",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Ruta vendedor",
              href: "#/ruta-vendedor"
            }
          ]
        }
      },
      {
        path: "/seguimiento-vendedores",
        component: resolve => require(["../views/visits/index.vue"], resolve),
        meta: {
          title: "Visitas de vendedores",
          permission: "Visitas vendedor",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Visitas de vendedores",
              href: "#/seguimiento-vendedores"
            }
          ]
        }
      },
      {
        path: "/ficha-de-datos",
        component: resolve =>
          require(["../views/data_sheet/index.vue"], resolve),
        meta: {
          title: "Atributos Producto",
          permission: "Atributos Producto",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "ficha-de-datos",
              href: "#/ficha-de-datos"
            }
          ]
        }
      },
      {
        path: "/ficha-de-datos/crear",
        component: resolve =>
          require(["../views/data_sheet/maintenance.vue"], resolve),
        meta: {
          action: "create",
          permission: "Atributos Producto",
          title: "Atributos Producto",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Atributos Producto",
              href: "#/ficha-de-datos/crear"
            }
          ]
        }
      },
      {
        path: "/ficha-de-datos/:id/editar",
        component: resolve =>
          require(["../views/data_sheet/maintenance.vue"], resolve),
        meta: {
          action: "edit",
          title: "Atributos Producto",
          permission: "Atributos Producto",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Atributos Producto",
              href: "#/ficha-de-datos/:id/editar"
            }
          ]
        }
      },
      {
        path: "/ficha-de-datos/:id/ver",
        component: resolve =>
          require(["../views/data_sheet/maintenance.vue"], resolve),
        meta: {
          action: "view",
          title: "ficha-de-datos",
          permission: "Atributos Producto",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "ficha-de-datos",
              href: "#/ficha-de-datos/:id/editar"
            }
          ]
        }
      },
      {
        path: "/productos",
        component: resolve => require(["../views/products/index.vue"], resolve),
        meta: {
          title: "Productos",
          permission: "Productos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Productos",
              href: "#/productos"
            }
          ]
        }
      },
      {
        path: "/productos/crear",
        component: resolve =>
          require(["../views/products/productForm.vue"], resolve),
        meta: {
          title: "Productos",
          permission: "Productos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Productos",
              href: "#/productos"
            }
          ]
        }
      },
      {
        path: "/productos/:idv/ver",
        component: resolve =>
          require(["../views/products/productForm.vue"], resolve),
        meta: {
          title: "Productos",
          permission: "Productos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Productos",
              href: "#/productos/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/productos/:id/editar",
        component: resolve =>
          require(["../views/products/productForm.vue"], resolve),
        meta: {
          title: "Productos",
          permission: "Productos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Productos",
              href: "#/productos/:id/editar"
            }
          ]
        }
      },
      {
        path: "/fecha-firme",
        component: resolve => require(["../views/dateFirm/index.vue"], resolve),
        meta: {
          title: "Fecha Firme",
          permission: "FECHA FIRME",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Fecha Firme",
              href: "#/fecha-firme"
            }
          ]
        }
      },
      {
        path: "/activos-fijos",
        component: resolve =>
          require(["../views/fixAssets/index.vue"], resolve),
        meta: {
          title: "Activos Fijos",
          permission: "ACTIVOS FIJOS",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Activos Fijos",
              href: "#/activos-fijos"
            }
          ]
        }
      },
      {
        path: "/activos-fijos/crear",
        component: resolve =>
          require(["../views/fixAssets/maintenance.vue"], resolve),
        meta: {
          title: "Activos Fijos",
          permission: "ACTIVOS FIJOS",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Activos Fijos",
              href: "#/activos-fijos"
            }
          ]
        }
      },
      {
        path: "/activos-fijos/:idv/ver",
        component: resolve =>
          require(["../views/fixAssets/maintenance.vue"], resolve),
        meta: {
          title: "Activos Fijos",
          permission: "ACTIVOS FIJOS",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Activos Fijos",
              href: "#/activos-fijos/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/activos-fijos/:id/editar",
        component: resolve =>
          require(["../views/fixAssets/maintenance.vue"], resolve),
        meta: {
          title: "Activos Fijos",
          permission: "ACTIVOS FIJOS",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Activos Fijos",
              href: "#/activos-fijos/:id/editar"
            }
          ]
        }
      },
      {
        path: "/read-products",
        component: resolve =>
          require(["../views/products/list-products.vue"], resolve),
        meta: {
          title: "Lista de productos",
          permission: "Productos",
          breadcrumb: [
            {
              text: "DashBoard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Lista de productos",
              href: "#/read-products"
            }
          ]
        }
      },
      {
        path: "/precios",
        component: resolve => require(["../views/prices/index.vue"], resolve),
        meta: {
          title: "Mant Precios",
          permission: "Mantenimiento Lista Precios",
          breadcrumb: [
            {
              text: "DashBoard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Precios",
              href: "#/precios"
            }
          ]
        }
      },
      {
        path: "/documentos",
        component: resolve =>
          require(["../views/documents/index.vue"], resolve),
        meta: {
          title: "Documentos",
          permission: "Documentos",
          breadcrumb: [
            {
              text: "DashBoard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Documentos",
              href: "#/documentos"
            }
          ]
        }
      },
      {
        path: "/inventarios",
        component: resolve =>
          require(["../views/inventories/index.vue"], resolve),
        meta: {
          title: "Inventarios",
          breadcrumb: [
            {
              text: "DashBoard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Inventarios",
              href: "#/inventarios"
            }
          ]
        }
      },
      {
        path: "/envio-de-documentos-finalizados",
        component: resolve =>
          require(["../views/send_documents/index.vue"], resolve),
        meta: {
          title: "Envío de documentos finalizados",
          breadcrumb: [
            {
              text: "DashBoard",
              href: "#/"
            },
            {
              text: "Envío de documentos finalizados",
              href: "#/envio-de-documentos-finalizados"
            }
          ]
        }
      },
      {
        path: "/operaciones",
        component: resolve =>
          require(["../views/types-operation/index.vue"], resolve),
        meta: {
          title: "Operaciones",
          permission: "Tipos de operaciones",
          breadcrumb: [
            {
              text: "DashBoard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Operaciones",
              href: "#/operaciones"
            }
          ]
        }
      },
      {
        path: "/operaciones/crear",
        component: resolve =>
          require(["../views/types-operation/maintenance.vue"], resolve),
        meta: {
          title: "Operaciones",
          permission: "Tipos de operaciones",
          breadcrumb: [
            {
              text: "DashBoard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Operaciones",
              href: "#/operaciones"
            }
          ]
        }
      },
      {
        path: "/operaciones/:idv/ver",
        component: resolve =>
          require(["../views/types-operation/maintenance.vue"], resolve),
        meta: {
          title: "Operaciones",
          permission: "Tipos de operaciones",
          breadcrumb: [
            {
              text: "DashBoard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Operaciones",
              href: "#/operaciones"
            }
          ]
        }
      },
      {
        path: "/operaciones/:id/editar",
        component: resolve =>
          require(["../views/types-operation/maintenance.vue"], resolve),
        meta: {
          title: "Operaciones",
          permission: "Tipos de operaciones",
          breadcrumb: [
            {
              text: "DashBoard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Operaciones",
              href: "#/operaciones"
            }
          ]
        }
      },
      {
        path: "/modelos",
        component: resolve => require(["../views/models/index.vue"], resolve),
        meta: {
          title: "Modelos",
          permission: "Modelos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Modelos",
              href: "#/modelos"
            }
          ]
        }
      },
      {
        path: "/modelos/crear",
        component: resolve =>
          require(["../views/models/modelForm.vue"], resolve),
        meta: {
          title: "Modelos",
          permission: "Modelos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Modelos",
              href: "#/modelos"
            }
          ]
        }
      },
      {
        path: "/modelos/:idv/ver",
        component: resolve =>
          require(["../views/models/modelForm.vue"], resolve),
        meta: {
          title: "Modelos",
          permission: "Modelos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Modelos",
              href: "#/modelos/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/modelos/:id/editar",
        component: resolve =>
          require(["../views/models/modelForm.vue"], resolve),
        meta: {
          title: "Modelos",
          permission: "Modelos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Modelos",
              href: "#/modelos/:id/editar"
            }
          ]
        }
      },
      {
        path: "/solicitud-encabezado",
        component: resolve =>
          require(["../views/request-services/index.vue"], resolve),
        meta: {
          title: "Solicitudes de encabezado",
          permission: "Solicitudes de servicio",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Solicitudes de encabezado",
              href: "#/solicitud-encabezado"
            }
          ]
        }
      },
      {
        path: "/solicitud-encabezado/cliente/:cliente",
        component: resolve =>
          require(["../views/request-headers/index.vue"], resolve),
        meta: {
          title: "Solicitudes de encabezado",
          permission: "Solicitudes de servicio",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Solicitudes de encabezado",
              href: "#/solicitud-encabezado"
            }
          ]
        }
      },
      {
        path: "/solicitud-encabezado/crear",
        component: resolve =>
          require(["../views/request-services/form.vue"], resolve),
        meta: {
          title: "Solicitudes de encabezado",
          permission: "Solicitudes de servicio",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Solicitudes de encabezado",
              href: "#/solicitud-encabezado/crear"
            }
          ]
        }
      },
      {
        path: "/solicitud-encabezado/:idv/ver",
        component: resolve =>
          require(["../views/request-products/index.vue"], resolve),
        meta: {
          title: "Solicitudes de encabezado",
          permission: "Solicitudes de servicio",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Solicitudes de encabezado",
              href: "#/solicitud-encabezado/:idv/ver"
            }
          ]
        }
      },
      {
        path: "/movimientos/:model_id_create/crear",
        component: resolve =>
          require(["../views/inventory-movements/maintenance.vue"], resolve),
        meta: {
          title: "",
          function_execute: async model_id_create => {
            let model = JSON.parse(localStorage.getItem("selectedModel"));

            if (!model || +model.id !== +model_id_create) {
              await $http
                .get("/api/modelos/" + model_id_create)
                .then(response => {
                  model = response.data.data;
                  localStorage.setItem("selectedModel", JSON.stringify(model));
                });
            }

            return model.code + " " + model.description;
          },
          parameter: "model_id_create",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Facturacion",
              href: "#/movimiento-inventario"
            }
          ]
        }
      },
      {
        path: "/movimientos/:model_id_view/:document_id_view/ver",
        component: resolve =>
          require(["../views/inventory-movements/maintenance.vue"], resolve),
        meta: {
          title: "",
          function_execute: async model_id_view => {
            let model = JSON.parse(localStorage.getItem("selectedModel"));

            if (!model || +model.id !== +model_id_view) {
              await $http
                .get("/api/modelos/" + model_id_view)
                .then(response => {
                  model = response.data.data;
                  localStorage.setItem("selectedModel", JSON.stringify(model));
                });
            }

            return model.code + " " + model.description;
          },
          parameter: "model_id_view",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Movimientos de inventario",
              href: "#/movimiento-inventario"
            }
          ]
        }
      },
      {
        path: "/movimientos/:model_id_update/:document_id_update/actualizar",
        component: resolve =>
          require(["../views/inventory-movements/maintenance.vue"], resolve),
        meta: {
          title: "",
          function_execute: async model_id_update => {
            let model = JSON.parse(localStorage.getItem("selectedModel"));

            if (!model || +model.id !== +model_id_update) {
              await $http
                .get("/api/modelos/" + model_id_update)
                .then(response => {
                  model = response.data.data;
                  localStorage.setItem("selectedModel", JSON.stringify(model));
                });
            }
            return model.code + " " + model.description;
          },
          parameter: "model_id_update",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Movimientos de inventario",
              href: "#/movimiento-inventario"
            }
          ]
        }
      },
      {
        path:
          "/movimientos/:model_id_update_doc/:document_id/actualizar/documento",
        component: resolve =>
          require(["../views/inventory-movements/maintenance.vue"], resolve),
        meta: {
          title: "",
          function_execute: async model_id_update_doc => {
            let model = JSON.parse(localStorage.getItem("selectedModel"));

            if (!model || +model.id !== +model_id_update_doc) {
              await $http
                .get("/api/modelos/" + model_id_update_doc)
                .then(response => {
                  model = response.data.data;
                  localStorage.setItem("selectedModel", JSON.stringify(model));
                });
            }
            return model.code + " " + model.description;
          },
          parameter: "model_id_update_doc",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Movimientos de inventario",
              href: "#/movimiento-inventario"
            }
          ]
        }
      },
      {
        path: "/movimientos/:model_id/:document_id",
        component: resolve =>
          require(["../views/inventory-movements/maintenance.vue"], resolve),
        meta: {
          title: "",
          function_execute: async model_id => {
            let model = JSON.parse(localStorage.getItem("selectedModel"));

            if (!model || +model.id !== +model_id) {
              await $http.get("/api/modelos/" + model_id).then(response => {
                model = response.data.data;
                localStorage.setItem("selectedModel", JSON.stringify(model));
              });
            }
            return model.code + " " + model.description;
          },
          parameter: "model_id",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Movimientos de inventario",
              href: "#/movimiento-inventario"
            }
          ]
        }
      },
      {
        path: "/movimientos/:model_id",
        component: resolve =>
          require(["../views/inventory-movements/maintenance.vue"], resolve),
        meta: {
          title: "",
          function_execute: async model_id => {
            let model = JSON.parse(localStorage.getItem("selectedModel"));

            if (!model || +model.id !== +model_id) {
              await $http.get("/api/modelos/" + model_id).then(response => {
                model = response.data.data;
                localStorage.setItem("selectedModel", JSON.stringify(model));
              });
            }
            return model.code + " " + model.description;
          },
          parameter: "model_id",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Facturacion",
              href: "#/movimiento-inventario"
            }
          ]
        }
      },
      /**
       * Estas rutas van relacionadas al
       * modulo de pedidos retenidos
       *
       * @author Kevin Galindo
       */
      {
        path: "/pedidos-retenidos/",
        component: resolve =>
          require(["../views/orders_retained/Index.vue"], resolve),
        meta: {
          title: "Pedidos retenidos",
          permission: "Pedidos Retenidos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "pedidos-retenidos",
              href: "#/pedidos-retenidos"
            }
          ]
        }
      },
      /**
       * modulo de despachos
       * @author Kevin Galindo
       */
      {
        path: "/despachos",
        component: resolve => require(["../views/despatch/Index.vue"], resolve),
        meta: {
          title: "Despachos",
          permission: "Despachos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Despachos",
              href: "#/despachos"
            }
          ]
        }
      },
      {
        path: "/buscar-documentos",
        component: resolve =>
          require([
            "../views/inventory-movements/ListDocumentGlobal.vue"
          ], resolve),
        meta: {
          title: "Buscar Documentos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Buscar Documentos",
              href: "#/buscar-documentos"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/comprobantes",
        component: resolve =>
          require(["../views/various-vouchers/index.vue"], resolve),
        meta: {
          title: "Comprobantes Contables",
          permission: "PlanCuentas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/libro-auxiliar",
        component: resolve =>
          require(["../views/auxiliary-book/index.vue"], resolve),
        meta: {
          title: "Libro Auxiliar",
          permission: "libroAuxiliar",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Hom669
       */
      {
        path: "/trial-balance",
        component: resolve =>
          require(["../views/trial-balance/index.vue"], resolve),
        meta: {
          title: "Balance de Prueba",
          permission: "balancePrueba",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Hom669
       */
      {
        path: "/estados-financieros",
        component: resolve =>
          require(["../views/estados-financieros/index.vue"], resolve),
        meta: {
          title: "Estados Financieros Por Mes",
          permission: "estadoFinanciero",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },

      /**
       *
       * @author Santiago
       */
      {
        path: "/movimientos-cuenta-tercero",
        component: resolve =>
          //window.open('/#/reportes/libro-auxiliar-tercero','_blank'),
          require(["../views/movements-account-contact/index.vue"], resolve),
        meta: {
          title: "Movimientos cuenta tercero",
          permission: "Movimientos cuenta tercero",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/libro-auxiliar-por-tercero",
        component: resolve =>
          //window.open('/#/reportes/libro-auxiliar-tercero','_blank'),
          require(["../views/auxiliary-book-contact/index.vue"], resolve),
        meta: {
          title: "Libro Auxiliar por Tercero",
          permission: "libroAuxiliarTerceero",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Hom669
       */
      {
        path: "/reporte-comisiones",
        component: resolve =>
          //window.open('/#/reportes/libro-auxiliar-tercero','_blank'),
          require(["../views/report-commisions/index.vue"], resolve),
        meta: {
          title: "Reporte Comisiones",
          permission: "Reporte Comisiones",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/cuentas",
        component: resolve => require(["../views/accounts/index.vue"], resolve),
        meta: {
          title: "Plan de Cuentas",
          permission: "PlanCuentas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/job",
        component: resolve =>
          require(["../views/star-jobs/index.vue"], resolve),
        meta: {
          title: "Job",
          permission: "Jobs",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/job/:id",
        component: resolve =>
          require(["../views/star-jobs/index.vue"], resolve),
        meta: {
          title: "Job",
          permission: "Jobs",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/Aurora",
        component: resolve => require(["../views/Aurora/index.vue"], resolve),
        meta: {
          title: "Aurora",
          permission: "aurora",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/cuentas/crear",
        component: resolve =>
          require(["../views/accounts/maintenance.vue"], resolve),
        meta: {
          title: "Plan de Cuentas",
          permission: "PlanCuentas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/cuentas/:id_clone/clonar",
        component: resolve =>
          require(["../views/accounts/maintenance.vue"], resolve),
        meta: {
          title: "Plan de Cuentas",
          permission: "PlanCuentas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/cuentas/:id_edit/editar",
        component: resolve =>
          require(["../views/accounts/maintenance.vue"], resolve),
        meta: {
          title: "Plan de Cuentas",
          permission: "PlanCuentas",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/traslado/:id/ver",
        component: resolve => require(["../views/transfer/index.vue"], resolve),
        meta: {
          title: "Traslado",
          permission: "Traslados",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      /**
       *
       * @author Santiago
       */
      {
        path: "/borrar-erp/",
        component: resolve =>
          require(["../views/clear-erp/index.vue"], resolve),
        meta: {
          title: "Borrar erp",
          permission: "BorrarErp",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      // /**
      //  *
      //  * @author Santiago
      //  */
      // {
      //   path: "/traslado/:ide/editar",
      //   component: resolve => require(["../views/transfer/index.vue"], resolve),
      //   meta: {
      //     title: "Traslado",
      //     permission: "Traslados",
      //     breadcrumb: [
      //       {
      //         text: " Dashboard",
      //         href: "#/"
      //       },
      //       {
      //         text: "Administración Starcommerce",
      //         href: "#"
      //       },
      //       {
      //         text: "Sistema de inventarios",
      //         href: "#/traslado"
      //       }
      //     ]
      //   }
      // },
      // /**
      //  *
      //  * @author Santiago
      //  */
      {
        path: "/traslado",
        component: resolve => require(["../views/transfer/index.vue"], resolve),
        meta: {
          title: "Traslado",
          permission: "Traslados",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      {
        path: "/reporte",
        component: resolve => require(["../views/report/index.vue"], resolve),
        meta: {
          title: "Matriz dinámica",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Matriz dinámica",
              href: "#/reporte"
            }
          ]
        }
      },
      {
        path: "/reporte-de-cartera",
        component: resolve =>
          require(["../views/receivable/index.vue"], resolve),
        meta: {
          title: "Reporte de cartera",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Reporte de cartera",
              href: "#/reporte-de-cartera"
            }
          ]
        }
      },
      {
        path: "/saldos-contables",
        component: resolve =>
          require(["../views/account-balance/index.vue"], resolve),
        meta: {
          title: "Saldos Contables",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Saldos Contables",
              href: "#/saldos-contables"
            }
          ]
        }
      },
      {
        path: "/actualizacion-masiva-saldos",
        component: resolve =>
          require(["../views/update_balances/index.vue"], resolve),
        meta: {
          title: "Actualización masiva de saldos",
          permission: "Actualización masiva de saldos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Actualización masiva de saldos",
              href: "#/actualizacion-masiva-saldos"
            }
          ]
        }
      },
      {
        path: "/traslado-rot/:id/ver",
        component: resolve =>
          require(["../views/transfer-rot/index.vue"], resolve),
        meta: {
          title: "Traslado Rot Gar",
          permission: "Traslados",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      {
        path: "/traslado-rot",
        component: resolve =>
          require(["../views/transfer-rot/index.vue"], resolve),
        meta: {
          title: "Traslado Rot Gar",
          permission: "Traslados",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/traslado"
            }
          ]
        }
      },
      {
        path: "/kardex",
        component: resolve => require(["../views/kardex/index.vue"], resolve),
        meta: {
          title: "kardex",
          permission: "Kardex",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/kardex"
            }
          ]
        }
      },
      {
        path: "/Descuentos",
        component: resolve =>
          require(["../views/discounts/index.vue"], resolve),
        meta: {
          title: "Descuentos",
          permission: "Descuentos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/Descuentos"
            }
          ]
        }
      },
      {
        path: "/Descuentos/crear",
        component: resolve => require(["../views/discounts/form.vue"], resolve),
        meta: {
          title: "Crear Descuentos",
          permission: "Descuentos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/Descuentos/crear"
            }
          ]
        }
      },
      {
        path: "/Descuentos/:id/editar",
        component: resolve => require(["../views/discounts/form.vue"], resolve),
        meta: {
          title: "Editar Descuentos",
          permission: "Descuentos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/Descuentos/:id/editar"
            }
          ]
        }
      },
      {
        path: "/Descuentos-Pronto-Pago",
        component: resolve =>
          require(["../views/discounts-soon-payment/index.vue"], resolve),
        meta: {
          title: "Descuentos-Pronto-Pago",
          permission: "Descuentos pronto pago",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/Descuentos-Pronto-Pago"
            }
          ]
        }
      },
      {
        path: "/Descuentos-Pronto-Pago/crear",
        component: resolve =>
          require(["../views/discounts-soon-payment/form.vue"], resolve),
        meta: {
          title: "Crear Descuentos Pronto Pago",
          permission: "Descuentos pronto pago",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/Descuentos-Pronto-Pago/crear"
            }
          ]
        }
      },
      {
        path: "/Descuentos-Pronto-Pago/:id/editar",
        component: resolve =>
          require(["../views/discounts-soon-payment/form.vue"], resolve),
        meta: {
          title: "Editar Descuentos",
          permission: "Descuentos pronto pago",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/Descuentos-Pronto-Pago/:id/editar"
            }
          ]
        }
      },

      {
        path: "/Informe-Operaciones-Rango-Fecha",
        component: resolve =>
          require(["../views/operations-report/index.vue"], resolve),
        meta: {
          title: "Informe Operaciones Rango Fecha",
          permission: "Informe Operaciones Rango Fecha",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/Descuentos-Pronto-Pago/:id/editar"
            }
          ]
        }
      },

      {
        path: "/consulta-disponibilidad",
        component: resolve =>
          require(["../views/availability-inquiry/index.vue"], resolve),
        meta: {
          title: "Consulta Disponibilidad",
          permission: "Consulta Disponibilidad",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/Descuentos-Pronto-Pago/:id/editar"
            }
          ]
        }
      },

      {
        path: "/reconstructor-global",
        component: resolve =>
          require(["../views/rebuilder/index.vue"], resolve),
        meta: {
          title: "Reconstructor",
          permission: "Reconstructor",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "Administración Starcommerce",
              href: "#"
            },
            {
              text: "Sistema de inventarios",
              href: "#/Descuentos-Pronto-Pago/:id/editar"
            }
          ]
        }
      },
      {
        path: "/informe-inventario",
        component: resolve =>
          require(["../views/inventory-report/index.vue"], resolve),
        meta: {
          title: "Inventario valorizado",
          permission: "Inventario valorizado",
          breadcrumb: [
            {
              text: "Informes",
              href: "#/"
            },
            {
              text: "Inventario valorizado",
              href: "#"
            }
          ]
        }
      },
      {
        path: "/sugerido-de-reposicion",
        component: resolve =>
          require(["../views/suggested_replacement/index.vue"], resolve),
        meta: {
          title: "Sugerido de Reposición",
          permission: "Sugerido de Reposición",
          breadcrumb: [
            {
              text: "Informes",
              href: "#/"
            },
            {
              text: "Sugerido de Reposición",
              href: "#"
            }
          ]
        }
      },
      {
        path: "/business_intelligence",
        component: resolve =>
          require(["../views/business_intelligence/index.vue"], resolve),
        meta: {
          title: "Business Intelligence",
          permission: "Business Intelligence",
          breadcrumb: [
            {
              text: "Informes",
              href: "#/"
            },
            {
              text: "Business Intelligence",
              href: "#"
            }
          ]
        }
      },
      //  informe usuarios
      {
        path: "/informe-usuarios",
        component: resolve =>
          require(["../views/inventory-users/index.vue"], resolve),
        meta: {
          title: "Informe Terceros",
          permission: "Informe Terceros",
          breadcrumb: [
            {
              text: "Informes",
              href: "#/"
            },
            {
              text: "Inventario Terceros",
              href: "#"
            }
          ]
        }
      },
      {
        path: "/b2b/pedidos",
        component: resolve =>
          require(["../views/ordersManagement/index.vue"], resolve),
        meta: {
          title: "Gestión pedidos",
          permission: "Gestión de pedidos",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Gestión pedidos",
              href: "/b2b/pedidos"
            }
          ]
        }
      },
      {
        path: "/b2b/promociones",
        component: resolve =>
          require(["../views/cashReceipts/index.vue"], resolve),
        meta: {
          title: "Recibos de caja",
          permission: "Recibos de caja",
          breadcrumb: [
            {
              text: " Dashboard",
              href: "#/"
            },
            {
              text: "B2B",
              href: "#"
            },
            {
              text: "Recibos de caja",
              href: "/b2b/promociones"
            }
          ]
        }
      },
      {
        path: "/formas-pago",
        component: resolve =>
          require(["../views/payment-methods/index.vue"], resolve),
        meta: {
          title: "Formas de Pago",
          permission: "Formas de pago",
          breadcrumb: []
        }
      },
      {
        path: "/formas-pago/:idv/ver",
        component: resolve =>
          require(["../views/payment-methods/form.vue"], resolve),
        meta: {
          title: "Formas de Pago",
          permission: "Formas de pago",
          breadcrumb: []
        }
      },
      {
        path: "/formas-pago/:id/editar",
        component: resolve =>
          require(["../views/payment-methods/form.vue"], resolve),
        meta: {
          title: "Formas de Pago",
          permission: "Formas de pago",
          breadcrumb: []
        }
      },
      {
        path: "/formas-pago/crear",
        component: resolve =>
          require(["../views/payment-methods/form.vue"], resolve),
        meta: {
          title: "Formas de Pago",
          permission: "Formas de pago",
          breadcrumb: []
        }
      },
      {
        path: "/estado-cuenta",
        component: resolve =>
          require(["../views/customer_account_statement/index.vue"], resolve),
        meta: {
          title: "Estado Cuenta",
          permission: "Estado Cuenta",
          breadcrumb: []
        }
      },
      {
        path: "/factura-detalle/:id",
        component: resolve =>
          require(["../views/invoice_detail/index.vue"], resolve),
        meta: {
          title: "Detalle factura",
          permission: "Detalle factura",
          breadcrumb: []
        }
      },
      {
        path: "/cuadre-de-caja",
        component: resolve =>
          require(["../views/cash-register-square/Index.vue"], resolve),
        meta: {
          title: "Cuadre de Caja",
          permission: "",
          breadcrumb: []
        }
      },
      {
        path: "/cruce-de-saldos",
        component: resolve =>
          require(["../views/balance-crossing/index.vue"], resolve),
        meta: {
          title: "Cruce de Saldos",
          permission: "",
          breadcrumb: []
        }
      },
      {
        path: "/cruce-de-saldos/:id",
        component: resolve =>
          require(["../views/balance-crossing/index.vue"], resolve),
        meta: {
          title: "Cruce de Saldos",
          permission: "",
          breadcrumb: []
        }
      },
      {
        path: "/reasignacion-vendedor",
        component: resolve =>
          require(["../views/reassign_seller/index.vue"], resolve),
        meta: {
          title: "Reasignacion Vendedor",
          permission: "Reasignacion Vendedor",
          breadcrumb: []
        }
      },
      {
        path: "/auditoria/:id",
        component: resolve => require(["../views/audit/index.vue"], resolve),
        meta: {
          title: "Auditoria Factura",
          breadcrumb: []
        }
      }
    ]
  },
  {
    path: "/500",
    component: resolve => require(["../components/pages/500.vue"], resolve),
    meta: {
      title: "500",
      breadcrumb: ``
    }
  },
  {
    path: "/login",
    component: resolve => require(["../components/pages/login.vue"], resolve),
    meta: {
      title: "Login",
      breadcrumb: ``
    }
  },
  {
    path: "/register",
    component: resolve =>
      require(["../components/pages/register.vue"], resolve),
    meta: {
      title: "Register",
      breadcrumb: ``
    }
  },
  {
    path: "/salvapantallas",
    component: resolve =>
      require(["../components/pages/lockscreen.vue"], resolve),
    meta: {
      title: "Lockscreen",
      breadcrumb: ``
    }
  },
  {
    path: "/olvide/contrasena",
    component: resolve =>
      require(["../components/pages/forgot_password.vue"], resolve),
    meta: {
      title: "Olvide mi contraseña",
      breadcrumb: ``
    }
  },
  {
    path: "/restablecer/contrasena/:token",
    component: resolve =>
      require(["../components/pages/reset_password.vue"], resolve),
    meta: {
      title: "Restablecer contraseña",
      breadcrumb: ``
    }
  },
  {
    path: "/cambiar/contrasena",
    component: resolve =>
      require(["../components/pages/change_password.vue"], resolve),
    meta: {
      title: "Cambiar contraseña",
      breadcrumb: []
    }
  },
  {
    path: "*",
    component: resolve => require(["../components/pages/404.vue"], resolve),
    meta: {
      title: "404",
      breadcrumb: ``
    }
  }
];

let routes = baseRoutes.concat(b2bRoutes);

routes = routes.concat(reports_routes);

export default routes;
