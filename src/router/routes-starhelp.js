const routesStarHelp = [
  {
    path: "/casos",
    component: resolve => require(["../views/cases/Index.vue"], resolve),
    meta: {
      title: "Casos",
      breadcrumb: [
        {
          text: "Casos",
          href: "/casos"
        }
      ]
    }
  },
  {
    path: "/casos/crear",
    component: resolve => require(["../views/cases/Create.vue"], resolve),
    meta: {
      title: "Casos",
      breadcrumb: [
        {
          text: "Casos",
          href: "/casos"
        }
      ]
    }
  },
  {
    path: "/casos/:id",
    component: resolve => require(["../views/cases/Interact.vue"], resolve),
    meta: {
      title: "Interactuar caso",
      breadcrumb: [
        {
          text: "Interactuar caso",
          href: "/casos"
        }
      ]
    }
  }
];

export default routesStarHelp;
