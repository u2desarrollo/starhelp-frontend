import Vue from "vue";
import Router from "vue-router";
import routes from "./routes";
import store from "../store/store";

Vue.use(Router);

const router = new Router({
  routes,
  linkActiveClass: "active"
});

router.beforeEach((to, from, next) => {
  store.commit("routeChange", "start");

  let publicPage = ["/login", "/restablecer/contrasena", "/olvide/contrasena"];
  let authRequired = !publicPage.includes(to.path);

  let user_logued = localStorage.getItem("user");

  const loggedIn = user_logued !== null ? JSON.parse(user_logued) : false;

  if (to.path.indexOf("/restablecer/contrasena/") >= 0) {
    authRequired = false;
  }

  if (authRequired) {
    if (loggedIn) {
      if (to.path != "/cambiar/contrasena") {
        if (!loggedIn.password_changed) {
          return next("/cambiar/contrasena");
        }
      }
    } else {
      return next("/login");
    }
  }

  if (to.fullPath === "/login" && loggedIn) {
    return next("/inicio");
  }

  if (to.path === "/b2b/inicio") {
    store.commit("hideCart", "hide");
  }

  if (to.meta.permission && to.meta.permission !== "") {
    if (!validatePermission(to.meta.permission)) {
      return next(from.path);
    }
  }

  next();
});

router.afterEach((to, from) => {
  document.title = to.meta.title;
  store.commit("routeChange", "end");
  store.commit("left_menu", "close");
});

function validatePermission(permission) {
  let permissions = JSON.parse(localStorage.getItem("permissions"));

  let foundPermission = permissions.find(p => p.permission === permission);

  return !!foundPermission;
}

export default router;
