import "es6-promise/auto";
import Vue from "vue";
import Vuex from "vuex";

import mutations from "./mutations";
import licenses from "../modules/licenses";
import paramTables from "../modules/param-tables";
import parameters from "../modules/parameters";
import tabsViews from "../modules/tabs";
import subscribers from "../modules/subscribers";
import vouchersTypes from "../modules/vouchers-types";
import branchOffices from "../modules/branch-offices";
import projects from "../modules/projects";
import branchOfficeWarehouses from "../modules/branchoffices-warehouses";
import contacts from "../modules/contacts";
import contactClerks from "../modules/contacts-clerks";
import contactWarehouses from "../modules/contacts-warehouses";
import contactDocuments from "../modules/contacts-documents";
import departments from "../modules/departments";
import cities from "../modules/cities";
import lines from "../modules/lines";
import cashRegisterSquare from "../modules/cash-register-square";
import balanceCrossing from "../modules/balance-crossing";

import dataSheet from "../modules/data_sheet";

import modaldocs from "../modules/modaldocs";
import transfer from "../modules/transfer";
import account from "../modules/account";
import aurora from "../modules/Aurora";
import starjobs from "../modules/star-jobs";
import various_vouchers from "../modules/various-vouchers";
import transfer_rot from "../modules/transfer-rot";
import kardex from "../modules/kardex";
import clear_erp from "../modules/clear-erp";
import seller_route from "../modules/seller-route";
import rebuild from "../modules/rebuild";
import availabilityInquiry from "../modules/availability-inquiry";

import sublines from "../modules/sublines";
import users from "../modules/users";
import login from "../modules/login";
import brands from "../modules/brands";
import subbrands from "../modules/subbrands";
import categories from "../modules/categories";
import subcategories from "../modules/subcategories";
import menu from "../modules/menu";
import bannerB2b from "../modules/banners-b2b";
import home from "../b2b/modules/home";
import catalog from "../b2b/modules/catalog";
import ecommerce from "../modules/ecommerce";
import productDetail from "../b2b/modules/product-detail";
import cart from "../b2b/modules/cart";
import relativeProductsForm from "../b2b/modules/relative-products-form";
import ordersHistory from "../b2b/modules/orders-history";
import receivable from "../b2b/modules/receivable";
import replacementAssistant from "../b2b/modules/replacement-assistant";
import prospective_client from "../modules/prospective-client";
import payment_managment from "../modules/payment-managment";
import products from "../modules/products";
import visits from "../modules/visits";
import listproducts from "../modules/listproducts";
import prices from "../modules/prices";
import documents from "../modules/documents";
import inventories from "../modules/inventories";
import typeoperations from "../modules/types-operations";
import models from "../modules/models";
import taxes from "../modules/taxes";
import promotions from "../modules/promotions";
import baseDocuments from "../modules/base-documents";
import requestheaders from "../modules/request-headers";
import requestproducts from "../modules/request-products";
import inventoryMovements from "../modules/inventory-movements";

import ordersRetained from "../modules/orders_retained";
import despatch from "../modules/despatch";
import discount from "../modules/discount";
import auxiliarybook from "../modules/auxiliary-book";
import auxiliarybookcontact from "../modules/auxiliary-book-contact";
import reportcommisions from "../modules/report-commisions";
import accountbalance from "../modules/account-balance";
import statefinaze from "../modules/estados-financieros";
import trialbalance from "../modules/trial-balance";
import requestService from "../modules/request-service";
import discount_soon_payment from "../modules/discount-soon-payment";
import automatic_portfolio_delivery from "../modules/automatic_portfolio_delivery";
import operations_report from "../modules/operations-report";
import roles from "../modules/roles";
import inventoryReport from "../modules/inventory-report";
import inventoryUsers from "../modules/inventory-users";
import ordersManagement from "../modules/ordersManagement";
import cashReceipts from "../modules/cashReceipts";
import Notifications from "../modules/notifications";
import paymentMethods from "../modules/payment-methods";
import customerAccountStatement from "../modules/customer_account_statement";
import invoiceDetail from "./../modules/invoice_detail";
import reassignSeller from "./../modules/reassign_seller";
import replacementAssistantAdmin from "../modules/suggested_replacement";
import audit from "./../modules/audit";
import sendDocuments from "./../modules/send_documents";
import report from "./../modules/report";
import reportDynamicMatrix from "./../reports-aggrid/modules/dynamic-matrix";
import reportSalesProducts from "./../reports-aggrid/modules/sales-products";
import updateBalances from "./../modules/update_balances";
import fixAssets from "./../modules/fixAssets";
import dateFirm from "./../modules/dateFirm";
import Receivable from "./../modules/receivable";
import ReceivableReport from "./../modules/receivable-report";
import accountingInventoryMovements from "../modules/accounting-inventory-movements";
import movementsAccountContact from "../modules/movements-account-contact";
import cases from "../modules/cases";

Vue.use(Vuex);

//=======vuex store start===========
const store = new Vuex.Store({
  state: {
    left_open: true,
    right_open: false,
    cart_open: false,
    preloader: true,
    user: {
      name: "Addison",
      picture: "images/avatar1.jpg",
      job: "Project Manager"
    }
  },
  mutations,
  modules: {
    cases,
    dateFirm,
    accountingInventoryMovements,
    fixAssets,
    modaldocs,
    projects,
    roles,
    despatch,
    ordersRetained,
    licenses,
    paramTables,
    parameters,
    tabsViews,
    subscribers,
    vouchersTypes,
    branchOffices,
    branchOfficeWarehouses,
    contacts,
    contactClerks,
    contactWarehouses,
    contactDocuments,
    departments,
    cities,
    lines,
    sublines,
    users,
    login,
    brands,
    subbrands,
    categories,
    subcategories,
    menu,
    bannerB2b,
    home,
    catalog,
    ecommerce,
    productDetail,
    cart,
    relativeProductsForm,
    ordersHistory,
    receivable,
    replacementAssistant,
    prospective_client,
    payment_managment,
    products,
    visits,
    listproducts,
    prices,
    documents,
    inventories,
    typeoperations,
    models,
    taxes,
    baseDocuments,
    requestheaders,
    requestproducts,
    inventoryMovements,
    promotions,
    automatic_portfolio_delivery,
    dataSheet,
    statefinaze,
    discount_soon_payment,
    cashRegisterSquare,
    discount,
    auxiliarybook,
    accountbalance,
    auxiliarybookcontact,
    reportcommisions,
    trialbalance,
    various_vouchers,
    transfer,
    account,
    aurora,
    starjobs,
    transfer_rot,
    kardex,
    clear_erp,
    seller_route,
    availabilityInquiry,
    operations_report,
    requestService,
    inventoryReport,
    rebuild,
    inventoryUsers,
    ordersManagement,
    cashReceipts,
    Notifications,
    paymentMethods,
    customerAccountStatement,
    invoiceDetail,
    reassignSeller,
    replacementAssistantAdmin,
    audit,
    sendDocuments,
    report,
    reportDynamicMatrix,
    reportSalesProducts,
    updateBalances,
    ReceivableReport,
    Receivable,
    movementsAccountContact,
    balanceCrossing
  }
}); //=======vuex store end===========

export default store;
