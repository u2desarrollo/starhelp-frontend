const app = {
    state: {
        left_open: true,
        right_open: false,
        cart_open: false,
        preloader: true,
        user: {
            name: "Addison",
            picture: "images/avatar1.jpg",
            job: "Project Manager"
        },
        visitedViews: []
    },
    actions: {
        addVisitedViews: ({ commit }, view) => {
          commit('ADD_VISITED_VIEWS', view)
        },
        delVisitedViews: ({ commit }, view) => {
          commit('DEL_VISITED_VIEWS', view)
        }
    }
};

export default app;
