import $http from '../../../axios';

export async function getSales({commit}, contact_id) {
  commit('setLoading', true)
  await $http.get('/api/reporte-ventas-detalle?contact_id=' + contact_id).then(async (response) => {
    await commit('setSales', response.data.sales);
    await commit('setContact', response.data.contact);
    commit('setLoading', false)
  })
}
