import $http from '../../../axios';

export async function getSales({commit}) {
  commit('setLoading', true)
  await $http.get('/api/reporte-ventas').then(async (response) => {
    await commit('setSales', response.data);
    commit('setLoading', false)
  })
}
