export default [
  {
    path: '/reportes',
    component: resolve => require(['./../index.vue'], resolve),
    children: [
      {
        path: '/reportes/libro-auxiliar-tercero',
        component: resolve => require(['../views/Auxiliary-book-contact/index.vue'], resolve),
        meta: {
          title: 'Libro Auxiliar por Tercero',
          permission: "libroAuxiliarTerceero"
        }
      },
      {
        path: '/reportes/report-commisions',
        component: resolve => require(['../views/Report-commisions/index.vue'], resolve),
        meta: {
          title: 'Reporte Comisiones',
          permission: "Reporte Comisiones"
        }
      },
      {
        path: '/reportes/movimientos-cuenta-tercero',
        component: resolve => require(['../views/movements-account-contact/index.vue'], resolve),
        meta: {
          title: 'Movimientos cuenta tercero',
          permission: "Movimientos cuenta tercero"
        }
      },
      {
        path: '/reportes/libro-auxiliar',
        component: resolve => require(['../views/Auxiliary-book/index.vue'], resolve),
        meta: {
          title: 'Libro Auxiliar',
          permission: "libroAuxiliar"
        }
      },
      {
        path: '/reportes/trial-balance',
        component: resolve => require(['../views/Balance-test/index.vue'], resolve),
        meta: {
          title: 'Balance de Prueba',
          permission: "Analytrix Ventas Ultimo Semestre"
        }
      },
      {
        path: '/reportes/matriz-dinamica',
        component: resolve => require(['../views/dynamic-matrix/index.vue'], resolve),
        meta: {
          title: 'Analytrix Ventas Ultimo Semestre',
          permission: "Analytrix Ventas Ultimo Semestre"
        }
      },
      {
        path: '/reportes/detalle-matriz-dinamica',
        component: resolve => require(['../views/sales-products/index.vue'], resolve),
        meta: {
          title: 'Detalle matriz dinámica',
          permission: "Analytrix Ventas Ultimo Semestre"
        }
      },
      {
        path: '/reportes/reporte-de-cartera',
        component: resolve => require(['../views/report-receivable/index.vue'], resolve),
        meta: {
          title: 'Reporte de cartera',
          permission: "Reporte de cartera"
        }
      },
      {
        path: '/reportes/saldos-contables',
        component: resolve => require(['../views/Account-balance/index.vue'], resolve),
        meta: {
          title: 'Saldos Contables',
          permission: "Saldos Contables"
        }
      },
      {
        path: '/reportes/estados-financieros',
        component: resolve => require(['../views/estados-financieros/index.vue'], resolve),
        meta: {
          title: 'Reporte de Estados Financieros Por Mes',
          permission: "Analytrix Ventas Ultimo Semestre"
        }
      }
    ]
  },

]
