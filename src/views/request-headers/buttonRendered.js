import Vue from "vue";
import { mapActions, mapState, mapMutations } from 'vuex';
export default Vue.extend({
    template: `
        <span>
            <button style="height: 20px; line-height: 0.5" v-on:click="invokeParentMethod" class="btn btn-info">Actualizar</button>
        </span>
    `,
    data: function () {
        return {
        };
    },
    beforeMount () {
    },
    mounted () {
    },
    methods: {
        invokeParentMethod () {
            this.updateRequestHeaders(this.params.data);
        },
        ...mapActions("requestheaders", [
            "updateRequestHeaders"
        ]),
    }
});
